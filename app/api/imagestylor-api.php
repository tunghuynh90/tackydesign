<?php

class ImageStylor_Api{
    /*const IMAGE_STYLOR_API_URI = '';
    const IMAGE_STYLOR_DESIGN_URL = '';
    const CURRENT_SITE = '';*/
    /**
     * ImageStylor REST API version
     */
    const IMAGE_STYLOR_VERSION = '1';

    const IMAGE_STYLOR_CODE = 'VI';

    /**
     * Refresh authentication token every 20 minutes
     */
    const TOKEN_TIMEOUT = 1200;

    /**
     * Time outs settings
     */
    const API_CONNECT_TIMEOUT = 30;
    const API_PROCESS_TIMEOUT = 120;

    /*Config Design Tool*/
    private $v_allow_back_product_list = false;
    private $v_allow_add_text = true;
    private $v_allow_add_image = true;
    private $v_allow_add_shape = true;
    private $v_allow_edit_text = true;
    private $v_allow_edit_image = true;
    private $v_allow_edit_shape = true;
    private $v_allow_preview = true;
    private $v_allow_share = true;
    private $v_allow_proceed = true;

    private $v_image_stylor_api_uri = '';
    private $v_image_stylor_design_url = '';
    private $v_current_url = '';

    /*
     * $arr_customer = array(customer_id, visitor_id, session_id, first_name, last_name, phone, email, admin_mode);
     */
    private $arr_customer = array();
    /**
     * @var string indicate HOME URL
     */
    private $v_home_url = '';
    /**
     * @var string NEXT URL after saving design
     */
    private $v_next_url = '';
    /**
     * @var string indicate LOGIN URL
     */
    private $v_login_url = '';
    /**
     * @var array(id, code, title)
     */
    private $arr_fonts = array();
    private $arr_product = array();
    private $arr_current_product = array();
    private $v_include_dir = '';

    private $v_success_code = 1;
    private $v_success_message = 'OK';
    private $v_api_key;
    private $v_session_id;
    private $v_use_https;
    private $v_session_id_timestamp;
    public function __construct($p_api_key, $p_use_https = false){
        $this->v_api_key = $p_api_key;
        $this->v_session_id = NULL;
        $this->v_session_id_timestamp = NULL;
        $this->v_use_https = $p_use_https;
        if(isset($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], 'tackydesign.com')!== false){
            $this->v_image_stylor_api_uri = 'stylor.tackydesign.com/api';
            $this->v_image_stylor_design_url = 'http://stylor.tackydesign.com/';
            $this->v_current_url= 'http://tackydesign.com/';
        } else {
            $this->v_image_stylor_api_uri = 'imagestylor.local/api';
            $this->v_image_stylor_design_url = 'http://imagestylor.local/';
            $this->v_current_url = 'http://tackydesign.local/';
        }
        /*$this->v_image_stylor_api_uri = ImageStylor_Api::IMAGE_STYLOR_API_URI;
        $this->v_image_stylor_design_url = ImageStylor_Api::IMAGE_STYLOR_DESIGN_URL;
        $this->v_current_url = ImageStylor_Api::CURRENT_SITE;*/
        $v_pos = strrpos($this->v_image_stylor_design_url,'/');
        if($v_pos!==strlen($this->v_image_stylor_design_url)-1){
            $this->v_image_stylor_design_url .= '/';
        }
        if($this->v_current_url!=''){
            $v_pos = strrpos($this->v_current_url,'/');
            if($v_pos!==strlen($this->v_current_url)-1){
                $this->v_current_url .= '/';
            }
            if(strpos($this->v_current_url, 'http')!==0) $this->v_current_url = 'http://'.$this->v_current_url;
        }
    }

    public function set_design_url($p_design_url){
        $p_design_url = trim($p_design_url);
        $v_protocol = '';
        $v_pos = strpos($p_design_url, 'http://');
        if($v_pos===0){
            $v_protocol = 'http://';
        }
        $v_pos = strpos($p_design_url, 'https://');
        if($v_pos===0) $v_protocol = 'https://';
        $p_design_url = str_replace($v_protocol, '', $p_design_url);
        if($v_protocol=='') $v_protocol = 'http://';
        $v_pos = strpos($p_design_url,'/');
        if($v_pos!==false) $p_design_url = substr($p_design_url, 0, $v_pos);
        $this->v_image_stylor_design_url = $v_protocol.$p_design_url;
        $this->v_image_stylor_api = $p_design_url.'/api';
    }

    public function get_design_url(){
        return $this->v_image_stylor_design_url;
    }

    public function set_allow_product_list($p_allow_product_list){
        $this->v_allow_back_product_list = $p_allow_product_list;
    }
    public function set_image_stylor_api_uri($p_image_stylor_api_uri){
        $this->v_image_stylor_api_uri = $p_image_stylor_api_uri;
    }

    public function set_image_stylor_design_url($p_image_stylor_design_url){
        $this->v_image_stylor_design_url = $p_image_stylor_design_url;
        $v_pos = strrpos($this->v_image_stylor_design_url,'/');
        if($v_pos!==strlen($this->v_image_stylor_design_url)-1){
            $this->v_image_stylor_design_url .= '/';
        }
    }

    public function set_include_dir($p_include_dir){
        $this->v_include_dir = trim($p_include_dir);
    }

    public function get_include_dir(){
        return $this->v_include_dir;
    }

    public function set_home_url($p_home_url){
        if(strpos($p_home_url, '/')!==strlen($p_home_url)-1) $p_home_url.='/';
        $this->v_home_url = $p_home_url;
    }

    public function get_home_url(){
        return $this->v_home_url;
    }

    public function set_next_url($p_next_url){
        $this->v_next_url = $p_next_url;
    }

    public function get_next_url(){
        return $this->v_next_url;
    }

    public function set_login_url($p_login_url){
        $this->v_login_url = $p_login_url;
    }

    public function get_login_url(){
        return $this->v_login_url;
    }
    public function set_current_site($p_current_url=''){
        if($p_current_url==''){
            $v_page_url = 'http';
            if (isset($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] == "on")) {$v_page_url .= "s";}
            $v_page_url .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $v_page_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
            } else {
                $v_page_url .= $_SERVER["SERVER_NAME"];
            }
            $p_current_url = $v_page_url;
        }
        if(strrpos($p_current_url,'/')!==strlen($p_current_url)-1) $p_current_url .= '/';
        $this->v_current_url = $p_current_url;
    }

    public function set_customer(array $arr_customer = array()){
        $this->arr_customer = $arr_customer;
    }

    public function set_product(array $arr_product = array()){
        $this->arr_product = $arr_product;
    }

    public function set_font(array $arr_font = array()){
        $this->arr_fonts = $arr_font;
    }

    public function set_current_product(array $arr_current_product = array()){
        $this->arr_current_product = $arr_current_product;
    }

    public function set_allow_add_text($p_allow_add_text = true){
        $this->v_allow_add_text = $p_allow_add_text;
    }
    public function set_allow_add_image($p_allow_add_image = true){
        $this->v_allow_add_image = $p_allow_add_image;
    }
    public function set_allow_add_shape($p_allow_add_shape = true){
        $this->v_allow_add_shape = $p_allow_add_shape;
    }

    public function set_allow_edit_text($p_allow_edit_text = true){
        $this->v_allow_edit_text = $p_allow_edit_text;
    }
    public function set_allow_edit_image($p_allow_edit_image = true){
        $this->v_allow_edit_image = $p_allow_edit_image;
    }
    public function set_allow_edit_shape($p_allow_edit_shape = true){
        $this->v_allow_edit_shape = $p_allow_edit_shape;
    }
    public function set_allow_preview($p_allow_preview = true){
        $this->v_allow_preview = $p_allow_preview;
    }
    public function set_allow_share($p_allow_share = true){
        $this->v_allow_share = $p_allow_share;
    }
    public function set_allow_proceed($p_allow_proceed = true){
        $this->v_allow_proceed = $p_allow_proceed;
    }

    public function show_design_tool($p_root_dir=''){
        $v_ds = DIRECTORY_SEPARATOR;
        $v_dir = $this->v_include_dir;
        if($v_dir!=''){
            $v_last = substr($v_dir, strlen($v_dir)-1);
            if($v_last!='/' && $v_last!='\\') $v_dir .= $v_ds;
        }
        if(!file_exists($v_dir.'dsp_product_design.php')){
            die('Not found "dsp_product_design.php" in directory: '.$v_dir);
        }
        if(sizeof($this->arr_fonts)==0)
            $arr_fonts = $this->get_font_list();
        else
            $arr_fonts = $this->arr_fonts;
        $v_design_url = $this->v_image_stylor_design_url;
        if(substr($v_design_url, strlen($v_design_url)-1)!='/') $v_design_url .= '/';


        $arr_products = array();
        for($i=0;$i<count($this->arr_product);$i++){
            $v_code = isset($this->arr_product[$i]['code'])?$this->arr_product[$i]['code']:'';
            $v_id = isset($this->arr_product[$i]['id'])?$this->arr_product[$i]['id']:0;
            $v_title = isset($this->arr_product[$i]['title'])?$this->arr_product[$i]['title']:'';
            if($v_code!='' && $v_id>0)  $arr_products[$v_code] = array('id'=>$v_id, 'title'=>$v_title, 'active'=>1);
        }
        $arr_products['blankDesign'] = array('id'=>0, 'title'=>'Blank Design', 'active'=>1);
        $arr_user = array(
            'user_id'=>isset($this->arr_customer['user_id'])?$this->arr_customer['user_id']:0
        ,'user_name'=>isset($this->arr_customer['user_name'])?$this->arr_customer['user_name']:''
        ,'user_ip'=>isset($this->arr_customer['user_ip'])?$this->arr_customer['user_ip']:''
        ,'user_agent'=>isset($this->arr_customer['user_agent'])?$this->arr_customer['user_agent']:''
        ,'visitor_id'=>isset($this->arr_customer['visitor_id'])?$this->arr_customer['visitor_id']:0
        ,'session_id'=>isset($this->arr_customer['session_id'])?$this->arr_customer['session_id']:session_id()
        ,'first_name'=>isset($this->arr_customer['first_name'])?$this->arr_customer['first_name']:''
        ,'last_name'=>isset($this->arr_customer['last_name'])?$this->arr_customer['last_name']:''
        ,'email'=>isset($this->arr_customer['email'])?$this->arr_customer['email']:''
        ,'phone'=>isset($this->arr_customer['phone'])?$this->arr_customer['phone']:''
        ,'site_code'=>ImageStylor_Api::IMAGE_STYLOR_CODE
        ,'admin_mode'=>isset($this->arr_customer['admin_mode'])?$this->arr_customer['admin_mode']:0
        ,'company_id'=>isset($this->arr_customer['company_id'])?$this->arr_customer['company_id']:0
        ,'location_id'=>isset($this->arr_customer['location_id'])?$this->arr_customer['location_id']:0
        );
        $v_home_url = $this->get_home_url();
        $v_next_url = $this->get_next_url();
        $v_login_url = $this->get_login_url();

        $v_allow_add_text = $this->v_allow_add_text;
        $v_allow_add_image = $this->v_allow_add_image;
        $v_allow_add_shape = $this->v_allow_add_shape;
        $v_allow_edit_text = $this->v_allow_edit_text;
        $v_allow_edit_image = $this->v_allow_edit_image;
        $v_allow_edit_shape = $this->v_allow_edit_shape;
        $v_allow_preview = $this->v_allow_preview;
        $v_allow_share = $this->v_allow_share;
        $v_allow_proceed = $this->v_allow_proceed;
        $v_allow_product_list = $this->v_allow_back_product_list;
        $v_site_key = $this->v_api_key;

        $v_add_image_dir = $v_dir;
        if($p_root_dir!=''){
            if(strrpos($p_root_dir, $v_ds)!==strlen($p_root_dir)-1) $p_root_dir.=$v_ds;
            $v_add_image_dir = str_replace($p_root_dir,'', $v_add_image_dir);
        }
        $v_add_image_url = $this->v_current_url.(str_replace("\\", '/',$v_add_image_dir));
        $arr_current_product = $this->arr_current_product;
        $v_product_id = isset($arr_current_product['id'])?$arr_current_product['id']:'0';
        if(!isset($arr_current_product['id'])) $arr_current_product['id']=0;
        if(!isset($arr_current_product['code'])) $arr_current_product['code']='blankDesign';
        if(!isset($arr_current_product['title'])) $arr_current_product['title']='Blank Design';


        include $v_dir."dsp_product_design.php";
    }

    public function get_success_code(){
        return $this->v_success_code;
    }

    public function get_success_message(){
        return $this->v_success_message;
    }
    /**
     * Returns namespace associated to given method name
     *
     * @param  string $p_method
     * @return string
     * @throws ImageStylor_Api_Exception
     */
    protected function get_namespace($p_method){
        switch ($p_method) {
            case 'permission':
            case 'design':
            case 'design/list':
            case 'design/data':
            case 'design/image':
            case 'design/info':
            case 'design/count':
            case 'design/label':
            case 'design/preview':
            case 'design/preview3d':
            case 'design/delete':
            case 'design/clone':
            case 'design/zip':
            case 'template/zip':
            case 'template/list':
            case 'template/data':
            case 'template/style':
            case 'template/count':
            case 'template/info':
            case 'template/preview':
            case 'template/preview3d':
            case 'template/category':
            case 'template/industry':
            case 'template/theme':
            case 'template/font':
            case 'template/label':
            case 'template/options':
            case 'theme/preview':
            case 'theme/preview3d':
            case 'theme/count':
            case 'theme/info':
            case 'permission/login':
            case 'permission/feature':
                return '';
            default:
                throw new ImageStylor_Api_Exception('Unknown or unsupported method: ' . $p_method);
        }
    }

    /**
     * Returns current session id
     *
     * @param  boolean $p_auto_refresh_token if set to TRUE, session token will be refreshed if needed
     * @return string
     * @throws ImageStylor_Api_Exception
     */
    protected function get_session_id($p_auto_refresh_token = TRUE){
        if ($this->v_session_id && time() > $this->v_session_id_timestamp + ImageStylor_Api::TOKEN_TIMEOUT && $p_auto_refresh_token ) {
            $res = $this->api('token', array(), FALSE);
            $this->v_session_id = $res['session_token'];
            $this->v_session_id_timestamp = time();
        }

        return $this->v_session_id;
    }

    /**
     * Generate the full URI to use for API calls
     *
     * @param  string $p_method
     * @param  array  $arr_query
     * @return string
     */
    private function get_full_uri($p_method, array $arr_query = NULL){
        $v_scheme = $this-> v_use_https ? 'https' : 'http';

        $v_namespace = $this->get_namespace($p_method);
        if (!empty($v_namespace)) {
            $v_namespace .= '/';
        }

        $uri = $v_scheme . '://' . $this->v_image_stylor_api_uri . '/' . $v_namespace . $p_method;

        if ($arr_query !== NULL) {
            $uri .= '?' . http_build_query($arr_query);
        }

        return $uri;
    }

    /**
     * @param bool $p_auto_refresh_token
     * @param bool $p_force_non_empty_session_id
     * @return string
     * @throws ImageStylor_Api_Exception
     */
    private function get_http_auth($p_auto_refresh_token = TRUE, $p_force_non_empty_session_id = FALSE){
        $v_auth = $this->v_api_key . ':';
        $v_session_id = $this->get_session_id($p_auto_refresh_token);
        if (!empty($v_session_id)) {
            $v_auth .= $v_session_id;
        } elseif ($p_force_non_empty_session_id) {
            throw new ImageStylor_Api_Exception('Needs a valid session ID');
        }

        return $v_auth;
    }


    /**
     * Returns a valid cUrl handler
     *
     * @param  string $p_uri
     * @param  array $arr_post_data
     * @param  boolean $p_auto_refresh_token if set to TRUE, session token will be refreshed if needed
     * @return a valid cRul handler
     */
    protected function get_curl_handler($p_uri, array $arr_post_data = NULL, $p_auto_refresh_token = TRUE){
        $c_url = curl_init($p_uri);

        //curl_setopt($c_url, CURLOPT_USERPWD, $this->get_http_auth($p_auto_refresh_token));
        curl_setopt($c_url, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c_url, CURLOPT_CONNECTTIMEOUT, ImageStylor_Api::API_CONNECT_TIMEOUT);
        curl_setopt($c_url, CURLOPT_TIMEOUT, ImageStylor_Api::API_PROCESS_TIMEOUT);

        if ($arr_post_data !== NULL) {
            curl_setopt($c_url, CURLOPT_POST, TRUE);
            curl_setopt($c_url, CURLOPT_POSTFIELDS, http_build_query($arr_post_data));
        }
        return $c_url;
    }

    /**
     * Magic method used to call fotolia rest functions
     *
     * @param  string $p_method
     * @param  array  $arr_args
     * @param  string  $p_key_index
     * @param  boolean $p_auto_refresh_token if set to TRUE, session token will be refreshed if needed
     * @return array
     * @throws ImageStylor_Api_Exception
     */
    protected function api($p_method, array $arr_args = array(), $p_key_index = NULL, $p_auto_refresh_token = TRUE){
        static $cnt = 1;

        if (!$this->is_post_method($p_method)) {
            $arr_query = $arr_args;
            $arr_post_data = NULL;
        } else {
            $arr_query = NULL;
            $arr_post_data = $arr_args;
        }

        $v_uri = $this->get_full_uri($p_method, $arr_query);

        $c_url = $this->get_curl_handler($v_uri, $arr_post_data, $p_auto_refresh_token);
        $v_time_start = microtime(TRUE);
        $v_response = curl_exec($c_url);
        $v_http_code = curl_getinfo($c_url, CURLINFO_HTTP_CODE);

        if ($v_response === FALSE) {
            throw new ImageStylor_Api_Exception('Failed to reach URL "' . $v_uri . '": ' . curl_error($c_url));
        }

        curl_close($c_url);
        $arr_res = json_decode($v_response, true);

        if (isset($arr_res['error']) || $v_http_code != 200) {
            $v_error_code = 0;

            if (isset($arr_res['error'])) {
                $v_error_msg = $arr_res['error'];

                if (!empty($arr_res['code'])) {
                    $v_error_code = (int) $arr_res['code'];
                }
            } else {
                $v_error_msg = 'Invalid response HTTP code: ' . $v_http_code;
            }

            throw new ImageStylor_Api_Exception($v_error_msg, $v_error_code);
        }

        $v_time_end = microtime(TRUE);

        if (!headers_sent()) {
            header('X-ImageStylor-API-Call-Method-' . $cnt . ': ' . $p_method);
            header('X-ImageStylor-API-Call-Time-' . $cnt . ': ' . ($v_time_end - $v_time_start));
        }
        $cnt++;
        $this->v_success_code = isset($arr_res['success'])?$arr_res['success']:0;
        $this->v_success_message = isset($arr_res['message'])?$arr_res['message']:'';
        if($p_key_index){
            if(isset($arr_res[$p_key_index]))
                return $arr_res[$p_key_index];
            else
                return $arr_res;
        }else{
            return $arr_res;
        }
    }

    /**
     * Returns TRUE if the method requires
     *
     * @param  string $p_method
     * @return boolean
     */
    private function is_post_method($p_method){
        switch ($p_method) {
            case 'permission':
            case 'design':
            case 'design/list':
            case 'design/info':
            case 'design/data':
            case 'design/image':
            case 'design/count':
            case 'design/label':
            case 'design/preview':
            case 'design/preview3d':
            case 'design/delete':
            case 'design/clone':
            case 'design/zip':
            case 'template/zip':
            case 'template/list':
            case 'template/data':
            case 'template/style':
            case 'template/count':
            case 'template/info':
            case 'template/preview':
            case 'template/preview3d':
            case 'template/category':
            case 'template/industry':
            case 'template/theme':
            case 'template/font':
            case 'template/options':
            case 'template/label':
            case 'theme/preview':
            case 'theme/preview3d':
            case 'theme/count':
            case 'theme/info':
            case 'permission/login':
            case 'permission/feature':
                $p_is_post_method = TRUE;
                break;

            default:
                $p_is_post_method = FALSE;
                break;
        }

        return $p_is_post_method;
    }

    /**
     * @param $p_user_name
     * @param $p_user_password
     * @return array
     */
    public function login($p_user_name, $p_user_password){
        return $this->api('permission/login',
            array(
                'license_key' => $this->v_api_key,
                'user_name'=>$p_user_name,
                'password'=>$p_user_password
            ),'data'
        );
    }

    private function feature(){
        return $this->api('permission/feature',
            array(
                'license_key' => $this->v_api_key,
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $arr_template array
     * @return array
     */
    public function get_template_list(array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array(), $p_company_id = 0, array $arr_template = array()){
        return $this->api('template/list',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'template'=>$arr_template,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $arr_template array
     * @return array
     */
    public function get_template_theme(array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array(), $p_company_id = 0, array $arr_template = array()){
        return $this->api('template/theme',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'template'=>$arr_template,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $arr_template array
     * @return array
     */
    public function get_template_count(array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array(), $p_company_id = 0, array $arr_template = array()){
        return $this->api('template/count',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'template'=>$arr_template,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param int $p_template_id
     * @return array
     */
    public function get_template_info($p_template_id = 0){
        return $this->api('template/info',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id
            ),'data'
        );
    }

    /**
     * @param int $p_template_id
     * @return array
     */
    public function get_template_text_label($p_template_id = 0){
        return $this->api('template/label',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id
            ),'data'
        );
    }

    /**
     * @param $p_template_id
     * @param int $p_theme_id
     * @param int $p_max_size
     * @param array $arr_record
     * @param $p_output_extension
     * @return array
     */
    public function get_template_preview($p_template_id, $p_theme_id=0, $p_max_size=600, array $arr_record = array(), $p_output_extension = 'png'){
        return $this->api('template/preview',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id,
                'theme_id'=>$p_theme_id,
                'max_size'=>$p_max_size,
                'record'=> $arr_record,
                'extension'=>$p_output_extension
            ),'data'
        );
    }

    /**
     * @param $p_template_id
     * @param int $p_theme_id
     * @param array $arr_record
     * @param $p_output_extension
     * @return array
     */
    public function get_template_zip($p_template_id, $p_theme_id=0, array $arr_record = array(), $p_output_extension = 'png'){
        return $this->api('template/zip',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id,
                'theme_id'=>$p_theme_id,
                'record'=> $arr_record,
                'extension'=>$p_output_extension
            ),'data'
        );
    }

    /**
     * @param $p_template_id
     * @param int $p_theme_id
     * @param int $p_max_size
     * @return array
     */
    public function get_template_preview3d($p_template_id, $p_theme_id=0, $p_max_size = 600){
        return $this->api('template/preview3d',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id,
                'theme_id'=>$p_theme_id,
                'max_size'=>$p_max_size
            ),'data'
        );
    }

    /**
     * @param $p_template_id
     * @param $p_theme_id
     * @param int $p_max_size
     * @return array
     */
    public function get_theme_preview($p_template_id, $p_theme_id, $p_max_size=600){
        return $this->api('theme/preview',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id,
                'theme_id'=>$p_theme_id,
                'max_size'=>$p_max_size
            ),'data'
        );
    }

    /**
     * @param $p_template_id
     * @param $p_theme_id
     * @param int $p_max_size
     * @return array
     */
    public function get_theme_preview3d($p_template_id, $p_theme_id, $p_max_size = 600){
        return $this->api('theme/preview3d',
            array(
                'license_key' => $this->v_api_key,
                'template_id'=>$p_template_id,
                'theme_id'=>$p_theme_id,
                'max_size'=>$p_max_size
            ),'data'
        );
    }

    /**
     * @param $p_design_id
     * @param int $p_max_size
     * @param array $arr_record
     * @param $p_output_extension
     * @return array
     */
    public function get_design_preview($p_design_id, $p_max_size=600, array $arr_record = array(), $p_output_extension = 'png'){
        return $this->api('design/preview',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id,
                'max_size'=>$p_max_size,
                'record'=>$arr_record,
                'extension'=>$p_output_extension
            ),'data'
        );
    }

    /**
     * @param $p_design_id
     * @param array $arr_record
     * @param $p_output_extension
     * @return array
     */
    public function get_design_zip($p_design_id, array $arr_record = array(), $p_output_extension = 'png'){
        return $this->api('design/zip',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id,
                'record'=>$arr_record,
                'extension'=>$p_output_extension
            ),'data'
        );
    }

    /**
     * @param $p_design_id
     * @param int $p_max_size
     * @return array
     */
    public function get_design_preview3d($p_design_id, $p_max_size=600){
        return $this->api('design/preview3d',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id,
                'max_size'=>$p_max_size
            ),'data'
        );
    }

    /**
     * @param int $p_design_id
     * @return array
     */
    public function get_design_text_label($p_design_id = 0){
        return $this->api('design/label',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id
            ),'data'
        );
    }

    /**
     * @param int $p_design_id
     * @param int $p_user_id
     * @return array
     */
    public function delete_design($p_design_id, $p_user_id){
        return $this->api('design/delete',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id,
                'user_id'=>$p_user_id
            ),'data'
        );
    }

    /**
     * @param int $p_design_id
     * @param array $arr_user
     * @explanation: $arr_user = array('user_id'=>$v_user_id, 'user_name'=>$v_user_name, 'company_id'=>$v_company_id,'location_id'=>$v_location_id);
     * @return array
     */
    public function clone_design($p_design_id, array $arr_user = array()){
        return $this->api('design/clone',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id,
                'user'=>$arr_user
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $p_user_id int
     * @param $p_product_id int
     * @return array
     */
    public function get_design_list($p_company_id = 0, $p_product_id=0, $p_user_id=0, array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array()){
        return $this->api('design/list',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'product_id'=>$p_product_id,
                'user_id'=>$p_user_id,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $p_user_id int
     * @param $p_product_id int
     * @param $p_offset int
     * @param $p_row int
     * @return array
     */
    public function get_limit_design_list($p_company_id = 0, $p_product_id=0, $p_user_id=0, $p_offset =0, $p_row = 20, array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array()){
        return $this->api('design/list',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'product_id'=>$p_product_id,
                'user_id'=>$p_user_id,
                'offset'=>$p_offset,
                'row'=>$p_row,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @param $p_company_id int
     * @param $p_user_id int
     * @param $p_product_id int
     * @return array
     */
    public function get_count_design_list($p_company_id = 0, $p_product_id=0, $p_user_id=0, array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array()){
        return $this->api('design/count',
            array(
                'license_key' => $this->v_api_key,
                'company_id'=>$p_company_id,
                'product_id'=>$p_product_id,
                'user_id'=>$p_user_id,
                'filter'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param int $p_design_id
     * @return array
     */
    public function get_design_info($p_design_id = 0){
        return $this->api('design/info',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id
            ),'data'
        );
    }

    /**
     * @param int $p_design_id
     * @return array
     */
    public function get_design_image($p_design_id = 0){
        return $this->api('design/image',
            array(
                'license_key' => $this->v_api_key,
                'design_id'=>$p_design_id
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @param array $arr_styles
     * @param array $arr_industries
     * @return array
     */
    public function get_options_list(array $arr_categories = array(), array $arr_styles = array(), array $arr_industries = array()){
        return $this->api('template/options',
            array(
                'license_key' => $this->v_api_key,
                'selected'=>array('category'=>$arr_categories, 'style'=>$arr_styles, 'industry'=>$arr_industries)
            ),'data'
        );
    }

    /**
     * @param array $arr_categories
     * @return array
     */
    public function get_category_list(array $arr_categories = array()){
        return $this->api('template/category',
            array(
                'license_key' => $this->v_api_key,
                'selected'=>array('category'=>$arr_categories)
            ),'data'
        );
    }
    /**
     * @param array $arr_styles
     * @return array
     */
    public function get_style_list(array $arr_styles = array()){
        return $this->api('template/style',
            array(
                'license_key' => $this->v_api_key,
                'selected'=>array('style'=>$arr_styles)
            ),'data'
        );
    }
    /**
     * @param array $arr_industries
     * @return array
     */
    public function get_industry_list(array $arr_industries = array()){
        return $this->api('template/industry',
            array(
                'license_key' => $this->v_api_key,
                'selected'=>array('industry'=>$arr_industries)
            ),'data'
        );
    }
    /**
     * @return array
     */
    public function get_font_list(){
        return $this->api('template/font',
            array(
                'license_key' => $this->v_api_key
            ),'data'
        );
    }
}

/**
 * API Exception
 */
class ImageStylor_Api_Exception extends Exception {
    public function __construct($p_message, $p_code = 0, Exception $p_previous = null){
        parent::__construct($p_message, $p_code, $p_previous);
    }
    public function to_string(){
        return __CLASS__.": [{$this->code}]: {$this->message}";
    }
}

?>