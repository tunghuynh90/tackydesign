<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendNewletterCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'newsletter:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Newsletter';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$current_date = time();
		$config = Newsletter::get()->first();
		if(is_null($config))
			return false;
		if($config->interval_check == -1)
			return false;
		if(is_null($config->last_sent_date))
			$last_sent_date = 0;
		else
			$last_sent_date = strtotime($config->last_sent_date);
		$hour = 3600;
		if($current_date - $last_sent_date < $config->interval_check * $hour)
			return false;

		$users = UserMember::select('email')
							->where('subscribe','=',1)
							->get();
		if(is_null($users))
			return false;
		$product_content = '';
		$product_content = '';
		if(strpos($config->content, '[PRODUCT_CONTENT]')!==false){
			if(is_null($config['last_id_product']))
				$config['last_id_product'] = 0;
			if(Product::where('id','>',$config['last_id_product'])->count() < $config['min_product'])
				return false;
			$arr_products = array();
			$products = Product::select('id','name','short_name','main_image','category_id','description')
						->where('id','>',$config['last_id_product'])
						->orderBy('id','desc')
						->take($config->min_product)
						->get();
			$max_id = 0;
			if($products->count()){
				$i = 0;
				$url = Request::root();
				if(strpos($url, 'localhost') !== false)
					$url = 'http://tackydesign.com';
				foreach($products as $product){
					if($max_id < $product->id)
						$max_id = $product->id;
					$price = ProductController::getMainPriceByID($product->id);
	        		$product->sell_price = isset($price['sell_price']) ? $price['sell_price'] : 0;
	        		$category = ProductCategory::select('short_name','name')
	        										->where('id','=',$product->category_id)
	        										->get()
	        										->first();
					$arr_products[] = array(
					                        'category'=>$category['name'],
					                        'category_short_name'=>$category['short_name'],
					                        'id'=>$product->id,
					                        'name'=>$product->name,
					                        'short_name'=>$product->short_name,
					                        'sell_price'=>$product->sell_price,
					                        'main_image'=>$url.'/assets/upload/'.$product->main_image,
					                        'description'=>$product->description,
					                        );
				}
			}
			$product_content = NewsletterController::renderProductTemplate($arr_products);
		}
		foreach($users as $user){
			if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)) continue;
			$domain = substr($user->email, strpos($user->email, '@') + 1);
			if(!checkdnsrr($domain)) continue;
			$data = array(
			              'content'=>$config->content,
			              'product_content'=>$product_content,
			              'user'=>array(
			                            'firstname'=>$user->firstname,
			                            'lastname'=>$user->lastname,
			                            )
			              );
			Mail::later(5,'emails.newletter',  $data, function($message) use($user,$config)
	        {
	            $message->to($user->email, $user->firstname.' '.$user->lastname)->subject($config->subject);
	        });

		}
		$arr_update = array('last_sent_date'=> new \DateTime);
		if(isset($max_id))
			$arr_update['last_id_product'] = $max_id;
		Newsletter::where('id',1)->update($arr_update);
	}


}
