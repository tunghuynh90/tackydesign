<?php
if(isset($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], 'tackydesign.com')!== false)
	define('SERVER','tackydesign.com');
else
	define('SERVER','tackydesign.local');
define('DS', DIRECTORY_SEPARATOR );
define('MAX_UPLOAD_FILE', 25 );
define('SITE_THEME', 'default');
define('ATT_SIZE', 1);
define('ATT_QUANTITY', 2);
define('ATT_FOLDING', 3);
define('ATT_TURNAROUND', 4);
define('ATT_PAPER_TYPE', 5);
define('ATT_COLOR', 6);
define('ATT_SIDE', 7);
//define permission constant
define('_R_VIEW', 2);
define('_R_INSERT', 6);
define('_R_UPDATE', 8);
define('_R_DELETE', 10);
define('_R_FULL', 240);

define('_BASE_UPLOAD_PATH', 'upload/');

// IMAGE DIMENSION CONSTANTS
define('_THUMB_WIDTH_IMG', 150);
define('_THUMB_HEIGHT_IMG', 150);

// ADS DIMENSION CONSTANTS
define('_ADS_UPLOAD_PATH', 'ads/');

// ATTRIBUTES DIMENSION CONSTANTS
define('_ATTRIBUTE_UPLOAD_PATH', 'assets/attributes/');
define('_THUMB_ATTRIBUTE_UPLOAD_PATH', 'assets/attributes/thumbs/');
define('_WIDTH_ATTRIBUTE', 300);
define('_HEIGHT_ATTRIBUTE', 300);

// TEMPLATES CONSTANTS
define('_TEMPLATE_UPLOAD_PATH', 'assets/templates/');
define('_TEMPLATE_TYPE_UPLOAD_PATH', 'assets/templates/type/');
define('_WIDTH_TEMPLATE_TYPE', 37);
define('_HEIGHT_TEMPLATE_TYPE', 37);
define('_TEMPLATE_IMG_UPLOAD_PATH', 'assets/products/template-image/');
define('_WIDTH_TEMPLATE_IMG', 150);
define('_HEIGHT_TEMPLATE_IMG', 105);
define('_TEMPLATE_LIST_LIMIT', 12);

// CATEGORY CONSTANTS
define('_CATEGORY_UPLOAD_PATH', 'assets/category/');
define('_THUMB_CATEGORY_UPLOAD_PATH', 'assets/category/thumbs/');
define('_WIDTH_CATEGORY', 144);
define('_HEIGHT_CATEGORY', 131);

// HELP CONSTANTS
define('_HELP_UPLOAD_PATH', 'assets/help/');
define('_THUMB_HELP_UPLOAD_PATH', 'assets/help/thumbs/');
define('_WIDTH_HELP', 144);
define('_HEIGHT_HELP', 131);

// IDEAS CONSTANTS
define('_IDEA_UPLOAD_PATH', 'assets/ideas/');
define('_THUMB_IDEA_UPLOAD_PATH', 'assets/ideas/thumbs/');
define('_WIDTH_IDEA', 750);
define('_HEIGHT_IDEA', 430);

// PRODUCT CONSTANTS
define('_PRODUCT_UPLOAD_PATH', 'assets/products/');
define('_L_PRODUCT_UPLOAD_PATH', 'assets/products/large/');
define('_M_PRODUCT_UPLOAD_PATH', 'assets/products/medium/');
define('_THUMB_PRODUCT_UPLOAD_PATH', 'assets/products/thumbs/');
define('_L_WIDTH_PRODUCT', 750);
define('_L_HEIGHT_PRODUCT', 300);
define('_M_WIDTH_PRODUCT', 350);
define('_M_HEIGHT_PRODUCT', 150);
define('_S_WIDTH_PRODUCT', 750);
define('_S_HEIGHT_PRODUCT', 300);

// BANNER CONSTANTS
define('_BANNER_UPLOAD_PATH', 'ads/banners/');
define('_L_BANNER_UPLOAD_PATH', 'ads/banners/large/');
define('_THUMB_BANNER_UPLOAD_PATH', 'ads/banners/thumbs/');
define('_L_WIDTH_BANNER', 750);
define('_L_HEIGHT_BANNER', 300);
define('_S_WIDTH_BANNER', 350);
define('_S_HEIGHT_BANNER', 150);


define('IMAGE_STYLOR_KEY', 'ZEjPDxIabRdAcyazLbcwRaJXWDTSUI');
?>