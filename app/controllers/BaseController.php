<?php
class BaseController extends Controller {

    /**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct()
    {
    	#use to test csrf
    	//$this->beforeFilter('csrf', array('on' => array('post', 'put', 'patch', 'delete')));
        View::share('url',Request::root());
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}