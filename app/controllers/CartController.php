<?php

class CartController extends HomeController {
    static $_discount_price = 0;
    public function __construct()
    {
        parent::__construct();
    }

    public static function getShippingMethod()
    {
        $arr_shipping_price = array();
        $shippings = ShipPrice::select(
                                       'ship_price.*',
                                       'country.name AS country','country.id AS country_id',
                                       'ship_price_detail.id AS detail_id','ship_price_detail.shipping_price','ship_price_detail.shipping_method')
                                    ->where('ship_price.publish','=',1)
                                    ->leftJoin('country', 'ship_price.country_id', '=', 'country.id')
                                    ->leftJoin('ship_price_detail', 'ship_price.id', '=', 'ship_price_detail.ship_price_id')
                                    ->get();
        foreach($shippings as $ship){
            $arr_shipping_price[$ship['id']]['country'] = $ship->country;
            $arr_shipping_price[$ship['id']]['country_id'] = $ship->country_id;
            $arr_shipping_price[$ship['id']]['shipping_price'][$ship->detail_id] = array(
                                                                                    'shipping_method' => $ship->shipping_method,
                                                                                    'shipping_price' => number_format($ship->shipping_price,2),
                                                       );
        }
        return $arr_shipping_price;
    }

    public function cart()
    {
        if(Input::has('update'))
            self::updateCart();
        else if(Input::has('checkout'))
            return self::updateCart();
        else if(Input::has('paypal_express')){
            $cart = self::getCart(true);
            if(!empty($cart['items']))
                return PaypalController::doPaypalExpressPayment( $cart);
        } else if(Input::has('token') && Input::has('PayerID') )
            return PaypalController::paymentSuccess();
        $carts = array();
        $all_product = Cart::content();
        /*
        $countries = array();
        $$all_countries = Country::select('id','name')
                                ->where('publish','=',1)
                                ->get();
        foreach($all_countries as $country){
            $countries[$country->id] = array('name'=>$country->name,'cities'=>array());
            $cities = City::select('id','name')
                    ->where('publish','=',1)
                    ->where('country_id','=',$country->id)
                    ->get();
            foreach($cities as $city)
                $countries[$country->id]['cities'][$city->id] = $city->name;
        }*/
        $quantity = 0;
        foreach($all_product as $key=>$product){
            if($product->id == 'promo_code') continue;
            if($product->id == 'shipping_method') continue;
            $carts[$key] = array(
                                    'title'=>$product->name,
                                    'image'=>$product->options->image,
                                    'price'=>$product->price,
                                    'url'   =>$product->options->url,
                                    'quantity'=>(int)$product->qty,
                                    'subtotal'=>$product->subtotal,
                                );
            $quantity += (int)$product->qty;
        }
        $discount = 0;
        $promo_code = '';
        $is_shipping_discount = 0;
        $ship_price = -1;
        // $address = array();
        $row_id = Cart::search(array('id'=>'promo_code'));
        if($row_id){
            $discount = Cart::get($row_id[0])->options->discount;
            $promo_code = Cart::get($row_id[0])->options->promo_code;
            $is_shipping_discount = Cart::get($row_id[0])->options->is_shipping_discount;
        }
        /*$row_id = Cart::search(array('id'=>'address'));
        if($row_id){
            $ship_price = Cart::get($row_id[0])->options->price;
            $address = Cart::get($row_id[0])->options->address;
        }*/

        return View::make(self::$theme.'.cart')
                        ->with(array(
                               'carts'   => $carts,
                               // 'countries'   => $countries,
                               'discount'   => $discount,
                               'promo_code'   => $promo_code,
                               'is_shipping_discount'   => $is_shipping_discount,
                               'ship_price'   => $ship_price,
                               'arr_shipping_price'=>self::getShippingMethod(),
                               // 'address'   => $address,
                               'quantity'   => $quantity,
                               'total_price'    => Cart::total()
                               ));
    }

    public function add(){
        // Cart::destroy();
        $product = Product::select('name','main_image','id')
                    ->where('id','=',Input::get('id'))
                    ->where('publish','=',1)
                    ->first();
        $options = array();
        $rowId = '';
        foreach(array('color','material') as $property){
            if(Input::has($property)){
               $property_data = ProductPropertyController::getPropertyById((int)Input::get($property));
               $options[$property] = $property_data['name'];
               $rowId .= Input::get($property);
           }
        }
        $product->sell_price = 0;
        if(Input::has('size')){
            $price = ProductOptionPrice::select('sell_price','sizew','sizeh','weight')
                                ->where('id','=',(int)Input::get('size'))
                                ->get()
                                ->first();
           $product->sell_price = isset($price->sell_price) ? $price->sell_price : 0;
           $options['size'] = $price->sizew. ' x '.$price->sizeh;
           $options['weight'] = $price->weight;
           $options['size_id'] = Input::get('size');
        }
        $options['image'] = Request::root().'/assets/upload/'.$product->main_image;
        $options['url'] = Input::get('url');
        $product_name = $product->name.' '.(isset($options['size']) ? ' - '.$options['size']: '').(isset($options['color']) ? ' / ' .$options['color'] : '');
        $options['product_id'] = $product->id;
        Cart::add(md5($product->id.$rowId), $product_name, Input::get('quantity'), $product->sell_price,$options);
        if (!Request::ajax())
            return Redirect::to('/cart');
        $arr_return = array(
                            'title'=>$product_name,
                            'image'=>Request::root().'/assets/upload/'.$product->main_image,
                            'price'=>$product->sell_price,
                            'url'   =>Input::get('url'),
                            'quantity'=>Input::get('quantity')
                            );
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public static function getCart($isReturnArray = false)
    {
        $arr_return = array();
        $all_product = Cart::content();
        $quantity = 0;
        $arr_return['items'] = array();
        foreach($all_product as $row_id=>$product){
            if($product->id == 'promo_code') continue;
            if($product->id == 'shipping_method') continue;
            $arr_return['items'][] = array(
                                    'title'=>$product->name,
                                    'image'=>$product->options->image,
                                    'price'=>$product->price,
                                    'url'   =>$product->options->url,
                                    'quantity'=>(int)$product->qty
                                );
            $quantity+= (int)$product->qty;
        }
        $arr_return['items'] = array_reverse($arr_return['items']);
        $arr_return['item_count'] = $quantity;
        $arr_return['note'] = '';
        $arr_return['requires_shipping'] = true;
        $arr_return['total_price'] = Cart::total();
        if($isReturnArray)
            return $arr_return;
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function deleteCart($row_id)
    {
        if(Cart::get($row_id))
            Cart::remove($row_id);
        $all_product = Cart::content();
        $i = 0;
        foreach($all_product as $product){
            if($product->id == 'promo_code') continue;
            if($product->id == 'shipping_method') continue;
            $i++;
        }
        if($i == 0 )
            Cart::destroy();
        return Redirect::to(self::$root.'/cart');
    }

    public static function updateCart()
    {
        $arr_post = Input::all();
        if(isset($arr_post['quantity']) && !empty($arr_post['quantity'])){
            foreach($arr_post['quantity'] as $row_id => $quantity){
                if(Cart::get($row_id)){
                    Cart::update($row_id,(int)$quantity);
                }
            }
        }
        // if(isset($arr_post['promo_code']) && $arr_post['promo_code'] != ''){
        //     $voucher = Voucher::select('value')
        //             ->where('key','=',$arr_post['promo_code'])
        //             ->where('active','=',1)
        //             ->where('used','<>',1)
        //             ->where('valid_from','<=',date('Y-m-d'))
        //             ->where('valid_to','>=',date('Y-m-d'))
        //             ->get()
        //             ->first();
        //     $row_id = Cart::search(array('id'=>'promo_code'));
        //     if(is_null($voucher)){
        //         if($row_id)
        //             Cart::remove($row_id[0]);
        //     }
        //     else{
        //         if($row_id)
        //             Cart::update($row_id[0],array('discount'=>$voucher['value'].'%','promo_code'=>$arr_post['promo_code']));
        //         else
        //             Cart::add('promo_code','promo_code',1,0,array('discount'=>$voucher['value'].'%','promo_code'=>$arr_post['promo_code']));
        //     }
        // }
        if(isset($arr_post['checkout']))
            return Redirect::to('/checkout');
        View::share('cart_quantity',HomeController::__getCartQuantity());
    }

    public function changeShippingMethod()
    {
        $arr_return = array('status' => 'error', 'message' => 'Invalid Shipping Method');
        $id = Input::has("method") ? Input::get("method") : 0;
        if( $id ){
            $ship_price = ShipPriceDetail::select('ship_price_detail.*','ship_price.country_id')
                                            ->where('ship_price_detail.id','=',$id)
                                            ->leftJoin('ship_price','ship_price.id','=','ship_price_detail.ship_price_id')
                                            ->get()
                                            ->first();
            if( !is_null($ship_price) ){
                $ship_price->shipping_price = round( $ship_price->shipping_price ,2);
                $row_id = Cart::search(array('id'=>'shipping_method'));
                if($row_id)
                    Cart::update($row_id[0],array('options'=>array(
                                                         'shipping_price'=>$ship_price->shipping_price,
                                                         'country_id'=>$ship_price->country_id,
                                                         'ship_price_detail_id'=>$ship_price->id,
                                                         'ship_price_id'=>$ship_price->ship_price_id)
                                                ));
                else
                    Cart::add('shipping_method','shipping_method',1,0,array(
                                                                        'shipping_price'=>$ship_price->shipping_price,
                                                                        'country_id'=>$ship_price->country_id,
                                                                        'ship_price_detail_id'=>$ship_price->id,
                                                                        'ship_price_id'=>$ship_price->ship_price_id
                                                                        ));
                $arr_return = array('status'=> 'ok', 'shipping_price'=> number_format($ship_price->shipping_price,2), 'cost'=> number_format( self::getSubTotal($ship_price->shipping_price) + $ship_price->shipping_price,2), 'discount_price' => number_format(self::$_discount_price,2));
            }
        } else {
            $row_id = Cart::search(array('id'=>'shipping_method'));
            if($row_id)
               Cart::remove($row_id[0]);
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    /*public function getShipPrice()
    {
        $arr_post = Input::get('address');
        $message = 'Error : country can\'t be blank; country is not supported.';
        $arr_return['status'] = 'error';
        $row_id = Cart::search(array('id'=>'address'));
        if($row_id)
            Cart::remove($row_id[0]);
        if(!isset($arr_post['country']) || $arr_post['country']=='')
            $message = 'Error : country can\'t be blank; country is not supported.';
        else if(isset($arr_post['zip']) && $arr_post['zip']=='')
            $message = 'Error : zip can\'t be blank';
        else{
            $zip = $arr_post['zip'];
            $country = (int)$arr_post['country'];
            $zipcode = ZipCode::where('zip','=',$zip)
                                ->pluck('zip');
            if(is_null($zipcode))
                $message = 'Error : zip is not valid';
            else{
                $all_product = Cart::content();
                $weight = 0;
                foreach($all_product as $key=>$product){
                    if($product->id == 'promo_code') continue;
                    if($product->id == 'address') continue;
                    $weight += $product->options->weight * (int)$product->qty;
                }
                $shipping_price = ShippingController::getRate($weight,$zipcode);
                usort( $shipping_price, function($a,$b){
                    return $a['price'] < $b['price'] ? 0 : 1;
                });
                $shipping_rate = '<ul id="shipping-rate">';
                foreach($shipping_price as $price){
                    if(!isset($minimun_price))
                        $minimun_price = $price['price'];
                    $shipping_rate .= "<li>{$price['service_name']} at $ {$price['price']}</li>";
                }
                $shipping_rate .= '</ul>';
                $row_id = Cart::search(array('id'=>'address'));
                if($row_id)
                    Cart::update($row_id[0],array('address'=>$arr_post,'price'=>$minimun_price));
                else
                    Cart::add('address','address',1,0,array('address'=>$arr_post,'price'=>$minimun_price));
                $message = "There are ".count($shipping_price)." shipping rates available for {$zipcode}, staring at $ {$minimun_price}";
                $message .= $shipping_rate;
                $arr_return['status'] = 'success';
                $arr_return['ship_price'] = $minimun_price;
            }
        }
        $arr_return['message'] = $message;
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }*/

    public function getPromoCode()
    {
        $arr_return = array();
        $arr_post = Input::all();
        $row_id = Cart::search(array('id'=>'promo_code'));
        if(!isset($arr_post['promo_code']) || strlen($arr_post['promo_code']) == 0){
            if($row_id)
                Cart::remove($row_id[0]);
            $message = 'This promo code can not be blank.';
            $arr_return['total_price'] = number_format(self::getShippingPrice() + Cart::total(),2);
        }
        else{
            $voucher = Voucher::select('value','is_shipping_discount')
                    ->where('key','=',$arr_post['promo_code'])
                    ->where('active','=',1)
                    ->where('used','<>',1)
                    ->where('valid_from','<=',date('Y-m-d'))
                    ->where('valid_to','>=',date('Y-m-d'))
                    ->get()
                    ->first();
            if(is_null($voucher)){
                if($row_id)
                    Cart::remove($row_id[0]);
                $message = 'This promo code is not existed or was used or was expired.';
                $arr_return['total_price'] = number_format(self::getShippingPrice() + Cart::total(),2);
            }
            else{
                $message = 'Valid code!';
                $arr_return['discount'] = $voucher['value'].' %';
                if($voucher->is_shipping_discount){
                    $arr_return['discount_price'] = round(self::getShippingPrice() * ($voucher['value']/100),2);
                    $options = array('discount'=>$voucher['value'].'%','promo_code'=>$arr_post['promo_code'],'is_shipping_discount' => 1);
                    $arr_return['is_shipping_discount'] = 1;
                } else {
                    $arr_return['discount_price'] = round(Cart::total() * ($voucher['value']/100),2);
                    $options = array('discount'=>$voucher['value'].'%','promo_code'=>$arr_post['promo_code'],'is_shipping_discount' => 0);
                }
                $arr_return['total_price'] = self::getShippingPrice() + Cart::total() - $arr_return['discount_price'];
                $arr_return['total_price'] = number_format($arr_return['total_price'],2);
                $arr_return['discount_price'] = '- $ '.number_format($arr_return['discount_price'],2);
                if($row_id)
                    Cart::update($row_id[0],array('options'=>$options));
                else
                    Cart::add('promo_code','promo_code',1,0,$options);
            }
        }
        $arr_return['message'] = $message;
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }



    public function addDesign()
    {
        $design_id = Input::has('design_id') ? Input::get('design_id') : 0;
        $product_id = Input::has('product_id') ? Input::get('product_id') : 0;
        $quantity = Input::has('quantity') ? Input::get('quantity') : 1;
        $color_id = Input::has('color') &&  Input::get('color') != 'undefined' ? Input::get('color') : 0;
        $size_id = Input::has('size')  &&  Input::get('size') != 'undefined' ? Input::get('size') : 0;
        $material_id = Input::has('material') ? Input::get('material') : 0;
        $url = Input::has('url') ? Input::get('url') : '';
        if($product_id == 0 || $design_id == 0)
            return Redirect::to('/');
        $product = Product::select('name','short_name','main_image','id','material','color')
                    ->where('publish','=',1)
                    ->where('id','=',$product_id)
                    ->first();

        if(is_null( $product))
            return Redirect::to('/');
        $api = new ImageStylor_Api(IMAGE_STYLOR_KEY);
        $design_info = $api->get_design_info($design_id);
        $design_image = Request::root().'/assets/upload/'.$product->main_image;
        if(isset($design_info['image']))
            $design_image = $design_info['image'];
        $options = array();
        $rowId = '';
        foreach(array('color','material') as $property){
            $var = $property.'_id';
            if($$var){
                if(strpos($product->$property, $$var)!==false){
                   $property_data = ProductPropertyController::getPropertyById($$var);
                   $options[$property] = $property_data['name'];
                   $rowId .= $$var;
                }
           }
        }
        $product->sell_price = 0;
        if($size_id){
            $price = ProductOptionPrice::select('sell_price','sizew','sizeh')
                                ->where('id','=',(int)$size_id)
                                ->get()
                                ->first();
           $product->sell_price = isset($price->sell_price) ? $price->sell_price : 0;
           $options['size'] = $price->sizew. ' x '.$price->sizeh;
        }
        $options['image'] = $design_image;
        $rowId .= $options['image'];
        $options['url'] = base64_decode($url);
        $product_name = $product->name.' '.(isset($options['size']) ? ' - '.$options['size'] : '').(isset($options['color']) ? ' / '.$options['color'] : '');
        Cart::add(md5($product->id.$rowId), $product_name, $quantity, $product->sell_price,$options);
        return Redirect::to('/cart');
    }

    private static function getProductForCheckout()
    {
        $all_product = Cart::content();
        $arr_items = array();
        $i = $j = 0;
        foreach($all_product as $row_id=>$product){
            if($product->id == 'promo_code') continue;
            if($product->id == 'shipping_method') continue;
            $arr_items[$i][$j] = array(
                                    'title'=>$product->name,
                                    'image'=>$product->options->image,
                                    'options'=>$product->options,
                                    'price'=>$product->price,
                                    'quantity'=>(int)$product->qty
                                );
            $j++;
            if($j != 0 && $j %2 == 0)
                $i++;
        }
        return $arr_items;
    }

    public static function getSubTotal($shipping_price = 0)
    {
        $total = Cart::total();
        $row_id = Cart::search(array('id'=>'promo_code'));
        if($row_id){
            $discount = Cart::get($row_id[0])->options->discount;
            $is_shipping_discount = Cart::get($row_id[0])->options->is_shipping_discount;
            $discount = (float)str_replace('%','',$discount);
            if($is_shipping_discount)
                $discount = round($shipping_price * $discount / 100,2);
            else
                $discount = round($total * $discount / 100,2);
            self::$_discount_price = $discount;
            $total -= $discount;
        }
        return $total;
    }

    public static function getShippingPrice()
    {
        $shipping_price = 0;
        $row_id = Cart::search(array('id'=>'shipping_method'));
        if($row_id){
            $shipping_price = Cart::get($row_id[0])->options->shipping_price;
        }
        return $shipping_price;
    }

    public function checkout()
    {
        if(!Cart::count())
            return Redirect::to('/cart');
        $shipping_price = self::getShippingPrice();
        $customData = array(
                            'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping_price,
                            );
        return PaypalController::doPaypalExpressPayment( self::getCart(true), $customData);
    }

    /*public function checkout()
    {
        $arr_post = (Session::has('_old_input') ? Session::get('_old_input') : Input::all());
        View::share('config',HomeController::__getSiteInfo());
        if(!Cart::count())
            return Redirect::to('/cart');
        $message = '';
        if(Session::has('order_info')){
            $order_info = Session::get('order_info');
            $order_info = (array)json_decode($order_info);
            $order_info['shipping_address'] = (array) $order_info['shipping_address'] ;
            $order_info['billing_address'] = (array) $order_info['billing_address'] ;
            $province = $order_info['shipping_address']['province'];
            if(is_numeric($order_info['shipping_address']['province'])){
                $province = City::where('id','=',$order_info['shipping_address']['province'])->pluck('name');
            }
            $country = $order_info['shipping_address']['country'];
            if(is_numeric($order_info['shipping_address']['country'])){
                $country = Country::where('id','=',$order_info['shipping_address']['country'])->pluck('name');
            }
            $shipping_price = self::getShippingPrice($order_info['shipping_address']['country']);
            // $shipping_price = Session::get('shipping_price');
            // $shipping_price = (array)json_decode($shipping_price);
            // $key = 0;
            // if(!Session::has('shipping_key')){
            //     $minimun_price = 0;
            //     foreach( $shipping_price as $price){
            //         $minimun_price = $price->price;
            //         break;
            //     }
            // } else{
            //     $key = Session::get('shipping_key');
            //     $minimun_price = $shipping_price[$key]->price;
            // }

            $tax_percent = Tax::where('city_id','=',$order_info['shipping_address']['province'])->pluck('tax');
            $total = self::getSubTotal();
            $tax = $total * $tax_percent / 100;
            $total += $tax;
            $total +=  $shipping_price;
            $arr_return = array(
                                'arr_items'=> self::getProductForCheckout(),
                                'total'=> $total,
                                'arr_data'=>array(
                                                'step_2' => true,
                                                'province' => $province,
                                                'country' =>  $country,
                                                'country_id' => $order_info['shipping_address']['country'],
                                                'shipping_price' => $shipping_price,
                                                'tax' => $tax,
                                                'tax_percent' => $tax_percent,
                                                )
                              );
            if(Input::has('commit')){
                if(Session::token() == Input::get('_token')){
                    if(Input::has('buyer_accepts_marketing')){
                        if(!Confide::user())
                            HomeController::newsletter(array('contact'=>array('email'=>$order_info['email'])));
                    }
                    $gateway = Input::get('gateway');
                    if($gateway == 'paypal_express'){
                        $order_info['tax'] = $tax;
                        $order_info['shipping_price'] = $shipping_price;
                        $order_info = json_encode($order_info);
                        Session::put('order_info',$order_info);
                        $customData = array(
                                            'PAYMENTREQUEST_0_TAXAMT' => $tax,
                                            'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping_price,
                                            );
                        return PaypalController::doPaypalExpressPayment( self::getCart(true), $customData);
                    } else {
                        $arr_post = Input::all();
                        foreach($arr_post['credit_card'] as $value){
                            if(strlen($value) ==0){
                                $arr_return['arr_post'] = $arr_post;
                                return View::make(self::$theme.'.checkout')->with($arr_return);
                            }
                        }
                        if(!PaypalController::isValidCardNumber($arr_post['credit_card'])){
                            $arr_post['credit_card']['not_valid_number'] = true;
                            $arr_return['arr_post'] = $arr_post;
                            return View::make(self::$theme.'.checkout')->with($arr_return);
                        }
                        if(!PaypalController::isValidCardNumber($arr_post['credit_card'])
                            ||!PaypalController::checkSum($arr_post['credit_card'])){
                            $arr_post['credit_card']['not_valid_number'] = true;
                            $arr_return['arr_post'] = $arr_post;
                            return View::make(self::$theme.'.checkout')->with($arr_return);
                        }
                        if(!PaypalController::checkExpDate($arr_post['credit_card'])){
                            $arr_post['credit_card']['expired'] = true;
                            $arr_return['arr_post'] = $arr_post;
                            return View::make(self::$theme.'.checkout')->with($arr_return);
                        }
                        if(!PaypalController::validateCVV($arr_post['credit_card'])){
                            $arr_post['credit_card']['not_valid_cvv'] = true;
                            $arr_return['arr_post'] = $arr_post;
                            return View::make(self::$theme.'.checkout')->with($arr_return);
                        }
                        $arr_post['credit_card'] = array_merge($arr_post['credit_card'],array(
                                                                                        'PAYMENTREQUEST_0_TAXAMT' => $tax,
                                                                                        'PAYMENTREQUEST_0_SHIPPINGAMT' => $minimun_price,
                                                                                        ));
                        return PaypalController::doCreditCardPayment( self::getCart(true), $arr_post['credit_card']);
                    }
                }
                else
                    $message = 'Token mismatch. Please submit again.';
            } else {
                return View::make(self::$theme.'.checkout')->with($arr_return);
            }

        }
        if(Input::has('commit') ){
            if(Session::token() == Input::get('_token'))
                return self::doCheckout();
            else
                $message = 'Token mismatch. Please submit again.';
        }
        $countries = array();
        $all_countries = Country::select('id','name')
                                ->where('publish','=',1)
                                ->get();
        foreach($all_countries as $country){
            $countries[$country->id] = array('name'=>$country->name,'cities'=>array());
            $cities = City::select('id','name')
                    ->where('publish','=',1)
                    ->where('country_id','=',$country->id)
                    ->get();
            foreach($cities as $city)
                $countries[$country->id]['cities'][$city->id] = $city->name;
        }
        $user = Confide::user();
        $email = '';
        if($user)
            $email = $user->email;
        return View::make(self::$theme.'.checkout')
                        ->with(array(
                               'arr_items'=>self::getProductForCheckout(),
                               'total'=>Cart::total(),
                               'countries'=>$countries,
                               'arr_post' => $arr_post,
                               'email'  => $email,
                               'message'  => $message
                               ));
    }*/

    public static function checkCheckout($arr_post)
    {
        $arr_check = array('last_name','address_1','city','zip','country');
        if(isset($arr_post['billing_is_shipping']))
            $arr_post['shipping_address'] = $arr_post['billing_address'];
        foreach(array('billing_address','shipping_address') as $address_key){
            foreach($arr_check as $check){
                if(!isset( $arr_post[$address_key][$check] ) || strlen($arr_post[$address_key][$check]) == 0)
                    return false;
            }
            if( isset($arr_post[$address_key]['country']) && strlen($arr_post[$address_key]['country'])
                    && (!isset($arr_post[$address_key]['province']) || strlen($arr_post[$address_key]['province']) == 0) )
                return false;
            $state = ZipCode::where('zip','=',$arr_post[$address_key]['zip'])->pluck('state');
            $not_match_zip = false;
            if(is_numeric($arr_post[$address_key]['province'])){
                 if(is_null($state))
                    $not_match_zip = true;
                else {
                    $province = City::where('id','=',$arr_post[$address_key]['province'])->pluck('province_code');
                    if($province != $state)
                        $not_match_zip = true;
                }
            }
            if($not_match_zip){
                $arr_post[$address_key]['not_match_zip'] = true;
                Input::merge(array($address_key=>$arr_post[$address_key]));
                return false;
            }
        }

        return true;
    }

    /*public static function changeShippingPrice()
    {
        $key = Input::has('key') ? Input::get('key') : 0;
        $shipping_price = Session::get('shipping_price');
        $shipping_price = (array)json_decode($shipping_price);
        $old_key = Session::get('shipping_key');
        Session::put('shipping_key',$key);
        $price = $shipping_price[$key]->price;
        $old_price = $shipping_price[$old_key]->price;
        $order_info = Session::get('order_info');
        $order_info = (array)json_decode($order_info);
        $order_info['shipping_address'] = (array) $order_info['shipping_address'] ;
        $tax_percent = Tax::where('city_id','=',$order_info['shipping_address']['province'])->pluck('tax');
        $total = self::getSubTotal();
        $tax = $total * $tax_percent / 100;
        $total += $tax;
        $arr_return = array(
                            'cost'=>number_format(($total  + $price), 2),
                            'shipping_price'=>$price,
                            'status'=>'ok'
                            );
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;

    }*/

    public static function changeShippingPrice()
    {
        $method_id = Input::has('method_id') ? Input::get('method_id') : 0;
        $method = ShipPriceDetail::select('ship_price_detail.*','ship_price.country_id')
                                        ->where('ship_price_detail.id','=',$method_id)
                                        ->leftJoin('ship_price','ship_price.id','=','ship_price_detail.ship_price_id')
                                        ->get()
                                        ->first();
        $shipping_price = 0;
        if( !is_null($method) ){
            $method->shipping_price = round( $method->shipping_price ,2);
            $row_id = Cart::search(array('id'=>'shipping_method'));
            if($row_id)
                Cart::update($row_id[0],array('options'=>array(
                                                     'shipping_price'=>$method->shipping_price,
                                                     'country_id'=>$method->country_id,
                                                     'ship_price_detail_id'=>$method->id,
                                                     'ship_price_id'=>$method->ship_price_id)
                                            ));
            else
                Cart::add('shipping_method','shipping_method',1,0,array(
                                                                    'shipping_price'=>$method->shipping_price,
                                                                    'country_id'=>$method->country_id,
                                                                    'ship_price_detail_id'=>$method->id,
                                                                    'ship_price_id'=>$method->ship_price_id
                                                                    ));
            $shipping_price = $method->shipping_price;
        }
        $total = self::getSubTotal($shipping_price);
        $arr_return = array(
                            'cost'=>number_format(($total  + $shipping_price), 2),
                            'shipping_price'=>number_format($shipping_price,2),
                            'status'=>'ok'
                            );
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;

    }

    public static function doCheckout()
    {
        $arr_post = Input::all();
        if(self::checkCheckout($arr_post)){
            if(isset($arr_post['checkout']['email']) && ( !filter_var($arr_post['checkout']['email'], FILTER_VALIDATE_EMAIL) || !checkdnsrr(substr($arr_post['checkout']['email'], strpos($arr_post['checkout']['email'], '@') + 1))))
                return Redirect::to('/checkout')->withInput(Input::except('commit'))->with('email_error','Email not valid.');
            //Create Salesorder then send confirm email to customer
            if(isset($arr_post['billing_is_shipping']))
                $arr_post['shipping_address'] = $arr_post['billing_address'];
            /*$all_product = Cart::content();
            $weight = 0;
            foreach($all_product as $key=>$product){
                if($product->id == 'promo_code') continue;
                if($product->id == 'shipping_method') continue;
                $weight += $product->options->weight * (int)$product->qty;
            }
            $shipping_price = ShippingController::getRate($weight,$arr_post['shipping_address']['zip']);
            usort( $shipping_price, function($a,$b){
                return $a['price'] < $b['price'] ? 0 : 1;
            });
            Session::put('shipping_price',json_encode($shipping_price));
            Session::put('shipping_key',0);*/
            $user = Confide::user();
            if( $user )
                $arr_post['email'] = $user->email;
            else
                $arr_post['email'] = $arr_post['checkout']['email'];
            unset($arr_post['_token'],$arr_post['checkout'],$arr_post['commit']);
            Session::put('order_info',json_encode($arr_post));
                return Redirect::to('/checkout');
            OrderController::createOrder($arr_post);
            Cart::destroy();
            return Redirect::to('/cart')->with('order_success','Thank you for shopping with Tacky Design. Your order has been processed and you will receive a confirmation email shortly.');
        }
        return Redirect::to('/checkout')->withInput(Input::except('commit'));
    }

    public static function confirmCode($token)
    {
        $order = Order::select('id','status','email')
                ->where('token','=',$token)
                ->get()
                ->first();
        if(is_null($order))
            return Redirect::to('/cart')->with('order_confirm','This order was not existed');
        if($order['status'] > 0)
            return Redirect::to('/cart')->with('order_confirm','Your order had been confirmed.');
        Order::where('id',$order['id'])
                ->update(array('status'=>2));
        $config = EmailTemplate::where('type','=','order_success')
                                    ->get()
                                    ->first();
        if($config){
            $data = array(
                          'content' => $config['content'],
                          );
            $email = $order['email'];
            Mail::queue('emails.order_success', $data, function($message) use($email,$config)
            {
                $message->to($email, 'Guest')->subject($config['subject']);
            });
        }
        return Redirect::to('/cart')->with('order_confirm','Your order has been submitted');
    }
}