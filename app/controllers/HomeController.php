<?php

class HomeController extends BaseController {
    public static $theme = 'themes.default';
	public static $root;
    public function __construct()
    {
        parent::__construct();
        $user = Auth::user();
        self::$root = Request::root();
		self::$theme = 'themes.default';
        View::share('theme',self::$theme );
        View::share('menu',self::__getMenu());
        View::share('cart_quantity',self::__getCartQuantity());
        View::share('customer_services',self::getCustomerService());
        View::share('flash_sales',self::getFlashSale());
        View::share('config',self::__getSiteInfo());
        View::share('static_homes',self::getStaticHome());
        View::share('home_social_network',self::__getHomeSocialNetWork());
        View::share('social_network',self::__getSocialNetWork());
        View::share('widget_homes',self::getWidgetHome());
    }

    public static function index(){
        Session::forget('order_info');
        View::share('slides',self::__getSlide());
        View::share('new_products',self::__getNewProducts());
        View::share('two_articles',self::getTwoArticles());
        return View::make(self::$theme .'.contents');
    }

    public static function __getSiteInfo()
    {
        $config = AdminConfig::get()
                            ->first();
        if($config['logo_image']=='')
            $config['logo_image'] = 'default.png';
        if($config['logo_retina_image']=='')
            $config['logo_retina_image'] = 'default-retina.png';
        return $config;
    }

    public static function __getHomeSocialNetWork()
    {
        return SocialNetwork::where('publish','=',1)
                        ->where('on_home','=',1)
                        ->orderBy('id','asc')
                        ->take(3)
                        ->remember(30)
                        ->get();
    }

    public static function __getSocialNetWork()
    {
        return SocialNetwork::where('publish','=',1)
                        ->orderBy('id','asc')
                        ->remember(30)
                        ->get();
    }

    public static function __getCartQuantity()
    {
        $all_product = Cart::content();
        $quantity = 0;
        foreach($all_product as $key=>$product){
            if($product->id == 'promo_code') continue;
            if($product->id == 'shipping_method') continue;
            $quantity += (int)$product->qty;
        }
        return $quantity;
    }

    public static function __getSlide()
    {
        $arr_slides = array();
        $slide =  Slide::where('publish','=',1)
                ->orderBy('order_no','asc')
                ->remember(30)
                ->get();
        $slide_path = public_path().DS.'assets'.DS.'upload'.DS.'slide'.DS;
        foreach($slide as $value){
            if(!file_exists($slide_path.$value->image)) continue;
            $arr_slides[] = self::$root.'/assets/upload/slide/'.$value->image;
        }
        return $arr_slides;
    }

    private static function __getMenu()
    {
        $arr_menu = array();
        $module = AdminModuleGroup::orderBy('orderno','asc')
                            ->where('module_type_id','=',3)
                            ->where('publish','=',1)
                            ->remember(30)
                            ->get();
        foreach($module as $menu){
            $link = $menu->short_name;
            if($menu->short_name == 'shop' || $menu->short_name == 'home')
                $link = '';
            else if($menu->short_name == 'blog')
                $link = $menu->short_name;
            else if($menu->short_name == 'moodboard' || $menu->short_name == 'lookbooks')
                $link = $menu->short_name;
            else if($menu->short_name != 'collections')
                $link = 'pages/'.$menu->short_name;
            $arr_menu[$menu->short_name] = array(
                                                 'title'=>$menu->name
                                                 ,'link'=>self::$root.'/'.$link
                                                 );
        }
        foreach(array('shop'=>'shop','collections'=>'collection') as $key => $value){
            if(isset($arr_menu[$key])){
                if($key=='shop'){
                    $query = ProductCategory::select('short_name','id','name')
                                    ->orderBy('order_no','asc')
                                    ->where('on_'.$value,'=',1)
                                    ->where('parent_id','=',0)
                                    ->where('publish','=',1)
                                    ->remember(30)
                                    ->get();
                    foreach($query as $menu){
                        $sub_menu = ProductCategory::select('short_name','id','name')
                                        ->orderBy('order_no','asc')
                                        ->where('parent_id','=',$menu->id)
                                        ->where('publish','=',1)
                                        ->remember(30)
                                        ->get();
                        if(!$sub_menu->count())
                            $arr_menu[$key]['submenu'][$menu->name] = array();
                        foreach($sub_menu as $sub){
                            $arr_menu[$key]['submenu'][$menu->name][] = array(
                                                                            'title'=>$sub->name,
                                                                            'link'=>self::$root.'/collections/'.$sub->short_name
                                                                          );
                        }
                    }
                } else {
                    $sub_menu = ProductCategory::select('short_name','id','name')
                                    ->orderBy('order_no','asc')
                                    ->where('on_'.$value,'=',1)
                                    ->where('publish','=',1)
                                    ->remember(30)
                                    ->get();
                    foreach($sub_menu as $sub){
                        $arr_menu[$key]['submenu'][0][] = array(
                                                                        'title'=>$sub->name,
                                                                        'link'=>self::$root.'/collections/'.$sub->short_name
                                                                      );
                    }
                }
            }
        }
        return $arr_menu;
    }

    private static function __getNewProducts()
    {
        $arr_products = array();
        $products = ProductController::getNewProduct();
        if(is_null($products) || $products->count() == 0)
            return array();
        $image_path = public_path().DS.'assets'.DS.'upload'.DS;
        foreach($products as $product){
            if(!file_exists($image_path.$product->main_image))
                $product->main_image = self::$root.'/assets/images/no_image.jpg';
            else
                $product->main_image = self::$root.'/assets/upload/'.$product->main_image;
            $arr_products[] = array(
                                    'main_image'=>$product->main_image,
                                    'name'=>$product->name,
                                    'sell_price'=>$product->sell_price,
                                    'short_name'=>$product->short_name,
                                    'id'=>$product->id,
                                    );
        }
        return $arr_products;
    }

    public static function collections($collection_name,$param = '',$param2 = '')
    {
        $more_collection_name = '';
        $page = 1;
        $current_route = Route::currentRouteName();
        if($current_route == 'page'){
            if($param == '')
                $page = 1;
            else
                $page = (int)$param;

        } else if($current_route == 'pageWithCollection'){
            $more_collection_name = $param;
            if($param2 == '')
                $page = 1;
            else
                $page = (int)$param2;
        }else if($param!='')
            $more_collection_name = $param;
        if($collection_name != 'all'){
            $collection = ProductCategory::select('id','short_name','name','image')
                            ->where('short_name','=',"$collection_name")
                            ->get()
                            ->first();
            if(is_null($collection))
                return self::pageNotFound();
        }
        if($more_collection_name)
            $more_collection = ProductCategory::select('id','short_name','name','image')
                        ->where('short_name','=',"$more_collection_name")
                        ->get()
                        ->first();
        $arr_tag = $arr_products = array();
        if($collection_name == 'all' || $collection->count()){
            if($collection_name != 'all')
                $arr_where[] = array('field'=>'category_id', 'operator'=>'LIKE', 'value'=>'%'.$collection->id.'%');
            if(isset($more_collection->id))
                $arr_where[] = array('field'=>'category_id', 'operator'=>'LIKE', 'value'=>'%'.$more_collection->id.'%');
            $arr_where[] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
            $arr_order[] = array('field'=>'id', 'asc'=>false);
            $v_page = $page;
            $v_page_size = 15;
            $arr_products = ProductController::getPageProduct(true,array('arr_where'=>$arr_where,'arr_order'=>$arr_order,'v_page'=>$v_page,'v_page_size'=>$v_page_size,'getPrice'=>true));

            $product_categories = Product::select('category_id');
            if($collection_name != 'all')
                $product_categories->where('category_id','LIKE',"%{$collection->id}%");
            if(isset($more_collection->id))
                $product_categories->where('category_id','LIKE',"%$more_collection->id%");
            $product_categories = $product_categories->get();
            foreach($product_categories as $category_id){
                $category_id = explode(',', $category_id);
                foreach($category_id as $id){
                    if($collection_name != 'all')
                        if($id == $collection->id) continue;
                    $arr_tag[$id] = $id;
                }
            }
            if(!empty( $arr_tag)){
                $tags = ProductCategory::select('id','short_name','name')
                        ->whereIn('id',array_values($arr_tag))
                        ->orderBy('name','asc')
                        ->remember(5)
                        ->get();
                $arr_tag = array();
                foreach($tags as $tag){
                    $arr_tag[] = $tag;
                }
            }
        }
        if($collection_name != 'all' )
            $arr_return = array(
                                 'collection'       =>$collection->name,
                                 'collection_name'  =>$collection->short_name,
                                 'collection_image' =>$collection->image,
                                 'arr_products'     =>$arr_products,
                                 'page'             =>$page,
                                 'arr_tag'          =>$arr_tag,
                                 'title'            => $collection->name,
                                 'more_collection_name' => (isset($more_collection->name) ? $more_collection->name : '')
                                 );
        else
            $arr_return = array(
                                 'collection'       =>'All',
                                 'collection_name'  =>'all',
                                 'collection_image' =>'',
                                 'arr_products'     =>$arr_products,
                                 'page'             =>$page,
                                 'arr_tag'          =>$arr_tag,
                                 'title'            => 'Products',
                                 'more_collection_name' => (isset($more_collection->name) ? $more_collection->name : '')
                                 );
        return View::make(self::$theme.'.collections')->with($arr_return);
    }

	public function collectionDetail($collection_name = '', $product_id = 0){
        preg_match("/(\d+)(?!.*\d)/", $product_id,$match);
        if(empty($match) || !is_numeric($match[0]))
            return self::pageNotFound();
        $product_id = $match[0];
        $product = Product::where('id','=',$product_id)
                ->where('publish','=',1)
                ->get()
                ->first();
        if(is_null($product))
            return self::pageNotFound();
        $price = ProductController::getMainPriceByID($product_id);
        $product->sell_price = isset($price['sell_price']) ? $price['sell_price'] : 0;
        $product->bigger_price = isset($price['bigger_price']) ? $price['bigger_price'] : 0;
        $collection = ProductCategory::select('id','short_name','name','image')
                        ->where('short_name','=',"$collection_name")
                        ->get()
                        ->first();
        if(is_null($collection))
            return self::pageNotFound();
        $next_product = $prev_product = $similar_products = $product_template = array();
        if(isset($product->id)){
            $next_product = ProductController::getNextProduct($collection->id,$product->id);
            $prev_product = ProductController::getPrevProduct($collection->id,$product->id);
            $similar_products = ProductController::getSimilarProducts($collection->id,$product->id);
            foreach(array('color','material') as $property){
                if($product->$property != ''){
                    $property_tmp = explode(',', $product->$property);
                    $array = array();
                    foreach($property_tmp as $property_id){
                        $array[] = ProductPropertyController::getPropertyById($property_id);
                    }
                    $product->$property = $array;
                } else
                    $product->$property = array();
            }
            foreach(array('color','material') as $property){
                $array = $product->$property;
                usort($array, function($a, $b){
                    return strcmp($a['name'], $b['name']);
                });
                $product->$property = $array;
            }
            if($product->other_image != ''){
                $product->other_image  = unserialize($product->other_image);
            }
        } else
            return self::pageNotFound();
        $product_template = self::getProductTemplate($product->id);
        View::share('social_network',self::__getSocialNetWork());
        return View::make(self::$theme .'.collection_detail')
                        ->with(array(
                                'collection'        =>$collection->name,
                                'collection_name'   =>$collection->short_name,
                                'product'           =>$product,
                                'next_product'      =>$next_product,
                                'prev_product'      =>$prev_product,
                                'similar_products'  =>$similar_products,
                                'title'            => $product->name,
                                'product_template' => $product_template,
                                'option_price'      => ProductController::getOptionPrice($product->id)
                               ));
    }

    public static function getProductTemplate($product_id){
        return ProductTemplate::select('id','template_id','product_id','publish')
                                ->where('publish','=',1)
                                ->where('product_id','=',$product_id)
                                ->get()
                                ->first();
    }

    public function products($short_name = '')
    {
        if(!$short_name)
            return self::pageNotFound();
        $product = Product::where('short_name','=',$short_name)
                ->where('publish','=',1)
                ->get()
                ->first();
        if(is_null($product))
            return self::pageNotFound();
        foreach(array('color','material') as $property){
            if($product->$property != ''){
                $property_tmp = explode(',', $product->$property);
                $array = array();
                foreach($property_tmp as $property_id){
                    $array[] = ProductPropertyController::getPropertyById($property_id);
                }
                $product->$property = $array;
            } else
                $product->$property = array();
        }
        if($product->other_image != ''){
            $product->other_image  = unserialize($product->other_image);
        }
        return View::make(self::$theme .'.collection_detail')
                        ->with(array(
                                'collection'        =>'Products',
                                'collection_name'   =>'collections',
                                'product'           =>$product,
                                'title'            => $product->name,
                                'from_product'      => true
                               ));
    }

    public function collectionAll()
    {
        $arr_categories = array();
        $all_categories = ProductCategory::select('id','image','short_name','name')
                        ->where('publish','=',1)
                        ->get();
        $image_path = public_path().DS.'assets'.DS.'upload'.DS;
        foreach($all_categories as $category)
        {
            $products_belong_to_category = Product::select('id','main_image')
                                                    ->where('publish','=',1)
                                                    ->where('category_id','LIKE',"%{$category->id}%")
                                                    ->get();
            $num_of_products = $products_belong_to_category->count();
            if($num_of_products > 0){
                if($category->image != '' && file_exists($image_path.$category->image))
                    $category->image = self::$root.'/assets/upload/'.$category->image ;
                else{
                    foreach($products_belong_to_category as $product){
                        if($product->main_image != '' && file_exists($image_path.$product->main_image)){
                            $category->image = self::$root.'/assets/upload/'.$product->main_image ;
                            break;
                        }
                    }
                    if($category->image=='')
                        $category->image = self::$root.'/assets/images/no-image-large.gif';
                }
                $arr_categories[] = array(
                                          'id'=>$category->id,
                                          'image'=>$category->image,
                                          'short_name'=>$category->short_name,
                                          'name'=>$category->name,
                                          'num_of_product'=>$num_of_products
                                          );
            }
        }
        return View::make(self::$theme.'.collection_all')
                                ->with(array(
                                       'arr_categories'=>$arr_categories,
                                       'title'          =>'Collections'
                                        ));
    }

    public function getStaticHome(){
        $query = AdminHelp::select('id','short_name','description','content','name','group_id')
                            ->where('publish','=',1)
                            ->where('group_id','=',1)
                            ->orderBy('orderno','asc')
                            ->get();
        return $query;
    }

    public function getCustomerService(){
        $arr_customer = AdminHelp::select('id','short_name','description','content','name')
                                        ->where('publish','=',1)
                                        ->where('group_id','=',2)
                                        ->orderBy('orderno','asc')
                                        ->get();
        return $arr_customer;
    }

    public function getFlashSale(){
        $arr_flash = FlashSale::select('id','name','short_name','image')
                                    ->where('publish','=',1)
                                    ->get();
        return $arr_flash;
    }

    public function getWidgetHome(){
        $arr_widget = Widget::select('id','short_name','image','name','sub_title','link')
                            ->where('publish','=',1)
                            ->get();
        return $arr_widget;
    }

    public function getStaticPage($param){
        $message = '';
        if(Input::has('contact_email'))
            $message = self::getContactForm();
        if($param == 'contact'){
            $query_contact = AdminHelp::select('id','short_name','description','content','orderno')
                    ->where('short_name','=',$param)
                    ->where('publish','=',1)
                    ->remember(5)
                    ->get()
                    ->first();
            if(is_null($query_contact))
                return self::pageNotFound();
            return View::make(self::$theme.'/static/contact')->with(array('query' => $query_contact,'message'=>$message));
        }else{
            $query = AdminHelp::select('id','short_name','description','content','orderno')
                                ->where('short_name','=',$param)
                                ->where('publish','=',1)
                                ->remember(5)
                                ->get()
                                ->first();
            if(is_null($query))
                return self::pageNotFound();
            return View::make(self::$theme.'/static/static')->with(array('query'=>$query));
        }
    }

    public static function pageNotFound()
    {
        return View::make(self::$theme.'.static.error404');
    }

    public static function getContactForm(){
        $contact = new AdminContact;
        $contact->contact_name = Input::get('contact_name');
        $contact->contact_phone = Input::get('contact_phone');
        $contact->contact_email = Input::get('contact_email');
        $contact->contact_message = Input::get('contact_message');
        $contact->save();
        return  'Thanks for contacting us! We will get back to you as soon as possible.';
    }

    public function getAllBlog($param=''){
        $page = 1;
        if($param == '')
            $page = 1;
        else
            $page = (int)$param;
        $arr_where = array();
        $arr_where[] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
        $arr_order[] = array('field'=>'id', 'asc'=>false);
        $v_page = $page;
        $v_page_size = Input::has('pageSize')?Input::get('pageSize'):5;
        settype($v_page, 'int');
        settype($v_page_size, 'int');
        if($v_page < 1) $v_page = 1;
        if($v_page_size < 5) $v_page_size = 5;
        $v_total_rows = AdminBlog::condition($arr_where)->sort($arr_order)->count();
        $v_total_pages = ceil($v_total_rows / $v_page_size);
        if($v_total_pages < 1) $v_total_pages = 1;
        if($v_total_pages < $v_page) $v_page = $v_total_pages;
        $v_skip = ($v_page - 1) * $v_page_size;
        $arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
        $arr_blog = self::getRecentArticles();
        $arr_return = array('total_pages'=>$v_total_pages, 'blog'=>$arr_columns,'page'=>$v_page,'recent_article'=>$arr_blog);
        return View::make(self::$theme.'.blog_all')->with(array('blog'=>$arr_return));
    }

    public function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
        $arr_columns = array();
        if($limit <= 0) $limit = 999999;
        $v_size_field = sizeof($arr_fields);
        if($v_size_field == 0)
            $blog = AdminBlog::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $blog = AdminBlog::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
        if($blog){
            $i=0;
            $v_row = $offset;
            $arr_content_group_name = array();
            foreach($blog as $one){
                $arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['title'] = isset($one->title)?$one->title:'';
                    $arr_columns[$i]['content'] = isset($one->content)?$one->content:'';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
                    $arr_columns[$i]['created_by'] = isset($one->created_by)?$one->created_by:'';
                    $arr_columns[$i]['posted_date'] = isset($one->posted_date)?$one->posted_date:'';
                }else{
                    for($j=0; $j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
                $i++;
            }
        }
        return $arr_columns;
    }

    public function  getBlogDetail($param){
        $arr_blog = array();
        $arr_blog = AdminBlog::select('id','name','short_name','title','content','created_by','posted_date')
                                ->where('publish','=',1)
                                ->where('short_name','=',$param)
                                ->get()
                                ->first();
        $next_blog = $prev_blog = array();
        if(isset($arr_blog->id)){
            $next_blog = self::getNextBlog($arr_blog->id);
            $prev_blog = self::getPrevBlog($arr_blog->id);
        }
        $arr_recent_articles = self::getRecentArticles();
        return View::make(self::$theme.'.blogs_detail')
                        ->with(array(
                               'blog'=>$arr_blog,
                               'next_blog'=>$next_blog,
                               'prev_blog'=>$prev_blog,
                               'recent_articles'=>$arr_recent_articles,
                               ));
    }

    public static function getPrevBlog($blog_id){
        return AdminBlog::select('id','short_name','name')
                            ->where('id','<',$blog_id)
                            ->where('publish','=',1)
                            ->get()
                            ->first();
    }

    public static function getNextBlog($blog_id){
        return AdminBlog::select('id','short_name','name')
                            ->where('id','>',$blog_id)
                            ->where('publish','=',1)
                            ->get()
                            ->first();
    }

    public static function getRecentArticles(){
        $arr_blog = array();
        $arr_blog = AdminBlog::where('publish','=',1)
                                ->orderBy('id','desc')
                                ->take(5)
                                ->get();
        return $arr_blog;
    }

    public static function getTwoArticles(){
        $arr_blog = array();
        $arr_blog = AdminBlog::where('publish','=',1)
                                ->orderBy('id','desc')
                                ->take(2)
                                ->get();
        return $arr_blog;
    }

    public function getFirstLookbook()
    {
        $lookbook =  Lookbook::select('short_name')
                    ->where('publish','=',1)
                    ->orderBy('id','desc')
                    ->get()
                    ->first();
        if(is_null($lookbook))
            return self::pageNotFound();
        return Redirect::to('/lookbooks/'.$lookbook->short_name);
    }

    public function lookbooks($lookbook_name)
    {
        $this_lookbook = Lookbook::where('short_name','=',$lookbook_name)
                    ->where('publish','=',1)
                    ->get()
                    ->first();

        $all_lookbook = Lookbook::where('publish','=','1')
                                ->orderBy('id','desc')
                                ->get();
        $arr_lookbook = array();
        $arr_lookbook_from_this = array();
        foreach($all_lookbook as $lookbook){
            if($lookbook->id != $this_lookbook->id && empty($arr_lookbook_from_this))
                $arr_lookbook[$lookbook->name] = $lookbook;
            else
                $arr_lookbook_from_this[$lookbook->name] = $lookbook;
        }
        $arr_lookbook_from_this = array_merge($arr_lookbook_from_this, $arr_lookbook);
        return View::make(self::$theme.'.lookbooks')->with(array('this_lookbook'=>$this_lookbook,'all_lookbook'=>$arr_lookbook_from_this));
    }

    public function moodboard($moodboard_name = '')
    {
        if($moodboard_name == ''){
            $all_moodboard = Moodboard::where('publish','=',1)
                    ->orderBy('id','desc')
                    ->take(4)
                    ->get();
            if($all_moodboard->count() == 0)
                return self::pageNotFound();
            return View::make(self::$theme.'.moodboard')->with(array('all_moodboard'=>$all_moodboard));
        } else {
            $moodboard = Moodboard::where('short_name','=',$moodboard_name)
                    ->where('publish','=',1)
                    ->get()
                    ->first();
            if(is_null($moodboard))
                return self::pageNotFound();
            $arr_image = array();
            if($moodboard->other_image!=''){
                $other_image = unserialize($moodboard->other_image);
                $i = $j = 0;
                foreach($other_image as $image){
                    if($j == 5)
                        break;
                    $arr_image[$j][] = $image;
                    if($i != 0 && $i % 4 == 0)
                        $j++;
                    $i++;
                }
            }
            return View::make(self::$theme.'.moodboard_detail')->with(array('moodboard'=>$moodboard,'arr_image'=>$arr_image,'path'=>self::$root.'/assets/upload/moodboard'));
        }
    }

    public function loadTemplate(){
        $size = Input::has('size')?Input::get('size'):0;
        $quantity = Input::has('quantity')?Input::get('quantity'):1;
        $color = Input::has('color')?Input::get('color'):0;
        $material = Input::has('material')?Input::get('material'):0;
        $url = base64_encode((Input::has('url') ? Input::get('url') : '/'));
        $product_id = Input::has('product_id')?Input::get('product_id'):0;
        settype($product_id, 'int');
        $template_id = Input::has('template_id')?Input::get('template_id'):0;
        settype($template_id, 'int');
        $user = Auth::user();
        $v_user_id = $user?$user->id:0;
        $v_user_name = $user?$user->username:'NoName';
        $v_user_email = $user?$user->email:'noname@email.com';
        $product = Product::where('id', $product_id)->first();
        if($product){
            $current_product = array(
                'id'=>$product->id
                ,'short_name'=>$product->short_name
                ,'name'=>$product->name
                ,'active'=>1
            );
        }else{
            return Redirect::to('/');
            /*
            $current_product = array(
                'id'=>0
                ,'code'=>'blankDesign'
                ,'title'=>'Blank Design'
                ,'active'=>1
            );
            */
        }
        $arr_design_user = array(
            'user_id'=>$v_user_id
            ,'user_ip'=>Request::getClientIp()
            ,'user_name'=>$v_user_name
            ,'user_agent'=>$_SERVER['HTTP_USER_AGENT']
            ,'visitor_id'=>(int) rand(0,1000000)
            ,'session_id'=>session_id()
            ,'site_code'=>IMAGE_STYLOR_KEY
            ,'first_name'=>$user?$user->firstname:''
            ,'last_name'=>$user?$user->lastname:''
            ,'email'=>$v_user_email
            ,'phone'=>$user?$user->phone_number:''
            ,"cart_items_count"=>0
            ,"ffr_cart_items_count"=>0
            ,'admin_mode'=>0
            ,"company_id"=>0
            ,"location_id"=>0,
        );
        //$user = json_encode($arr_design_user);

        $arr_product = array();
        $arr_product[] = 0;
        if($template_id>0){
            $template = ProductTemplate::where('template_id', $template_id)->where('publish',1)->get();
            foreach($template as $temp){
                $arr_product[] = $temp->product_id;
            }
        }else $arr_product[] = $product_id;
        $arr_products = array();
        $arr_products['blank_design'] = array('id'=>0, 'title'=>'Blank Design', 'active'=>1);
        $products = Product::whereIn('id', $arr_product)->get();
        foreach($products as $pro){
            $arr_products[$pro->short_name] = array(
                'id'=>$pro->id
                ,'name'=>$pro->name
                ,'active'=>1
            );
        }
        $api = new ImageStylor_Api(IMAGE_STYLOR_KEY);
        $arr_font = $api->get_font_list();


        $arr_permission = array(
            'allow_preview'=>true
            ,'allow_share'=>false
            ,'allow_proceed'=>true
            ,'allow_product_list'=>false
            ,'allow_add_text'=>true
            ,'allow_add_image'=>true
            ,'allow_add_shape'=>true
        );
        return View::make(self::$theme.'.design.designtool')->with(array(
            'current_user'=>$arr_design_user
            ,'current_product'=>$current_product
            ,'template_product'=>$arr_products
            ,'font'=>$arr_font
            ,'add_image'=>'add-image'
            ,'design_url'=>$api->get_design_url()
            ,'login_url'=>''
            ,'next_url'=>'cart/add-design?design_id=[@DESIGN_ID]&product_id='.$current_product['id']."&quantity=$quantity&color=$color&size=$size&material=$material&url=$url"
            ,'type_design'=>'template'
            ,'design'=>''
            ,'site_key'=>IMAGE_STYLOR_KEY
            ,'permission'=>$arr_permission
            ,'product_id'=>$product_id
            ,'product_sizes' => ProductController::getOptionPrice($product->id)
            ,'current_size' => $size
            ,'quantity' => $quantity
        ));
    }

    public function loadAddImage()
    {
        $api = new ImageStylor_Api(IMAGE_STYLOR_KEY);
        return View::make(self::$theme.'.design.addimage')->with(array(
            'stylor_url'=>$api->get_design_url()
            ,'license_key'=>IMAGE_STYLOR_KEY
        ));
    }
    public function loadDesign(){
        $product_id = Input::has('product_id')?Input::get('product_id'):0;
        settype($product_id, 'int');
        $design_id = Input::has('design_id')?Input::get('design_id'):0;
        settype($design_id, 'int');

        $user = Auth::user();
        $v_user_id = $user?$user->id:0;
        $v_user_name = $user?$user->username:'NoName';
        $v_user_email = $user?$user->email:'noname@email.com';

        $product = Product::where('id', $product_id)->first();
        if($product){
            $current_product = array(
                'id'=>$product->id
                ,'short_name'=>$product->short_name
                ,'name'=>$product->name
                ,'active'=>1
            );
        }else{
            /*
            $current_product = array(
                'id'=>0
                ,'code'=>'blankDesign'
                ,'title'=>'Blank Design'
                ,'active'=>1
            );
            */
            return Redirect::to('/');
        }
        $arr_design_user = array(
            'user_id'=>$v_user_id
            ,'user_ip'=>Request::getClientIp()
            ,'user_name'=>$v_user_name
            ,'user_agent'=>$_SERVER['HTTP_USER_AGENT']
            ,'visitor_id'=>(int) rand(0,1000000)
            ,'session_id'=>session_id()
            ,'site_code'=>IMAGE_STYLOR_KEY
            ,'first_name'=>$user?$user->firstname:''
            ,'last_name'=>$user?$user->lastname:''
            ,'email'=>$v_user_email
            ,'phone'=>$user?$user->phone_number:''
            ,"cart_items_count"=>0
            ,"ffr_cart_items_count"=>0
            ,'admin_mode'=>0
            ,"company_id"=>0
            ,"location_id"=>0
        );
        //$user = json_encode($arr_design_user);

        $arr_products = array();
        $arr_products['blank_design'] = array('id'=>0, 'title'=>'Blank Design', 'active'=>1);
        $arr_products[$product->short_name] = array(
            'id'=>$product->id
            ,'name'=>$product->name
            ,'active'=>1
        );
        $api = new ImageStylor_Api(IMAGE_STYLOR_KEY);
        $arr_font = $api->get_font_list();


        $arr_permission = array(
            'allow_preview'=>true
            ,'allow_share'=>true
            ,'allow_proceed'=>true
            ,'allow_product_list'=>true
            ,'allow_add_text'=>true
            ,'allow_add_image'=>true
            ,'allow_add_shape'=>true
        );
        return View::make(self::$theme.'.design.designtool')->with(array(
            'current_user'=>$arr_design_user
            ,'current_product'=>$current_product
            ,'template_product'=>$arr_products
            ,'font'=>$arr_font
            ,'add_image'=>'add-image'
            ,'next_url'=>'design/product/'.$current_product['code'].'/[@DESIGN_ID]'
            ,'design_url'=>$api->get_design_url()
            ,'login_url'=>''
            ,'type_design'=>'design'
            ,'design'=>'/'.$design_id
            ,'site_key'=>IMAGE_STYLOR_KEY
            ,'permission'=>$arr_permission
            ,'content_data'=>array()
            ,'isCustomSize'=>0
        ));
    }

    public static function newsletter($arr_post = array())
    {
        if(empty($arr_post))
            $arr_post = Input::all();
        if(isset($arr_post['contact'])){
            $arr['email'] = isset($arr_post['contact']['email']) ? $arr_post['contact']['email'] : '';
            if(filter_var($arr['email'], FILTER_VALIDATE_EMAIL)){
                $arr['firstname'] = isset($arr_post['contact']['firstname']) ? $arr_post['contact']['firstname'] : 'Subscriber';
                $arr['lastname'] = isset($arr_post['contact']['lastname']) ? $arr_post['contact']['lastname'] : 'Newsletter';
                $arr['password'] = Hash::make((string)time());
                $arr['confirmed'] = 0;
                $arr['created_at']  = $arr['updated_at'] = new \DateTime;
                $subscriber = UserMember::select('id')
                                        ->where('email','=',$arr['email'])
                                        ->get()
                                        ->first();
                if(is_null($subscriber)){
                    UserMember::insertGetId($arr);
                }
            }
        }
        return 'ok';
    }

    public static function unsubcriber($user_member_id){
        $unsubcriber = UserMember::select('subscribe')
                                ->where('id','=',$user_member_id)
                                ->get()
                                ->first();
        $arr_columns = array();
        $arr_columns['subscribe'] = 0;
        UserMember::condition(array(array('field'=>'id','operator'=>'=','value'=>$user_member_id)))->update($arr_columns);
        return Redirect::to('/');
    }

    public static function search($name){
       $searchResult = Product::where('name','LIKE','%'.$name.'%')
                                ->select('id','name','main_image','description','short_name','category_id')
                                ->get();
       return View::make(self::$theme.'.search')->with(array('arr_searchs'=>$searchResult,'name_search'=>$name));
    }

    public function getAllSearch($param='',$name=''){
        $page = 1;
        //$param = 1;
        if($param == '')
            $page = 1;
        else
            $page = (int)$param;

        $arr_where = array();
        $arr_where[] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		if($name!='')
        	$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$name.'%');
		else
			$arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'');
        $arr_order[] = array('field'=>'id', 'asc'=>false);

        $v_page = $page;
        $v_page_size = Input::has('pageSize')?Input::get('pageSize'):20;
        settype($v_page, 'int');
        settype($v_page_size, 'int');
        if($v_page < 1) $v_page = 1;
        if($v_page_size < 20) $v_page_size = 20;
        $v_total_rows = Product::condition($arr_where)->sort($arr_order)->count();
        $v_total_pages = ceil($v_total_rows / $v_page_size);
        if($v_total_pages < 1) $v_total_pages = 1;
        if($v_total_pages < $v_page) $v_page = $v_total_pages;
        $v_skip = ($v_page - 1) * $v_page_size;
        $arr_columns = self::getLimitSearch($v_skip, $v_page_size, $arr_where, $arr_order);
        $arr_return = array('total_pages'=>$v_total_pages, 'arr_searchs'=>$arr_columns,'page'=>$v_page);
        return View::make(self::$theme.'.search')->with(array('arr_searchs'=>$arr_return, 'name_search'=>$name));
    }

    public function getLimitSearch($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
        $arr_columns = array();
        if($limit <= 0) $limit = 999999;
        $v_size_field = sizeof($arr_fields);
        if($v_size_field == 0)
            $arr_search = Product::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $arr_search = Product::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
        if($arr_search){
            $i=0;
            $v_row = $offset;
            $arr_content_group_name = array();
            foreach($arr_search as $one){
                $arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['description'] = isset($one->description)?$one->description:'';
                    $arr_columns[$i]['category_id'] = isset($one->category_id)?$one->category_id:'';
                    $arr_columns[$i]['main_image'] = isset($one->main_image)?$one->main_image:'';
                }else{
                    for($j=0; $j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
                $i++;
            }
        }
        return $arr_columns;
    }

}