<?php
use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;
class PaypalController extends BaseController{
    private static $_username = 'kn_api1.tackydesign.com';
    private static $_password = '8E32MYUCBU2BBZMM';
    private static $_signature = 'A-nJ7sed7rXuNMdC4h3u2fAl6ImLAyo0M7HCx1JW8.bHvhxYGvAWdoGY';

   /* private static $_username = 'kn-facilitator_api1.tackydesign.com';
    private static $_password = '1402554772';
    private static $_signature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31ARVjNJwxD4Mwf10QxGhmZ3zDvLQe';*/
    private static $_currency = 'CAD';
    private static $_cart = array();

    private static $testMode = false;
    private static $liveEndpoint = 'https://api-3t.paypal.com/nvp';
    private static $testEndpoint = 'https://api-3t.sandbox.paypal.com/nvp';

    private static function getProduct(&$cart)
    {
        $arr_items = array();
        foreach($cart['items'] as $item)
            $arr_items[] = array('name' => $item['title'], 'description'=>$item['title'], 'quantity' => $item['quantity'], 'price' => number_format($item['price'],2));
        $row_id = Cart::search(array('id'=>'promo_code'));
        if($row_id){
            $discount = Cart::get($row_id[0])->options->discount;
            $discount = (float)str_replace('%','',$discount);
            $is_shipping_discount = Cart::get($row_id[0])->options->is_shipping_discount;
            $promo_code = Cart::get($row_id[0])->options->promo_code;
            $total = $cart['total_price'];
            if($is_shipping_discount){
                $discount_price = round(CartController::getShippingPrice() * $discount / 100,2);
                $name = 'Shipping Discount';
            } else {
                $discount_price = $total * $discount / 100;
                $name = 'Discount';
            }
            $cart['total_price'] -= $discount_price;
            $cart['total_price'] = round($cart['total_price'],2);
            $arr_items[] = array('name' => "{$name} {$discount}%", 'description'=>"Voucher: {$promo_code}", 'quantity' => 1, 'price' =>-number_format($discount_price,2));
        }
        return $arr_items;
    }

    public static function doPaypalExpressPayment($cart, $customData = array())
    {
        $arr_items = self::getProduct($cart);
        if(!empty($customData)){
            $customData['PAYMENTREQUEST_0_ITEMAMT'] = number_format($cart['total_price'],2);
            /*if(isset($customData['PAYMENTREQUEST_0_TAXAMT'] )){
                $cart['total_price'] += $customData['PAYMENTREQUEST_0_TAXAMT'];
                $customData['PAYMENTREQUEST_0_TAXAMT'] = number_format($customData['PAYMENTREQUEST_0_TAXAMT'],2);
            }*/

            if(isset($customData['PAYMENTREQUEST_0_SHIPPINGAMT'] )){
                $cart['total_price'] += $customData['PAYMENTREQUEST_0_SHIPPINGAMT'];
                $customData['PAYMENTREQUEST_0_SHIPPINGAMT'] = number_format($customData['PAYMENTREQUEST_0_SHIPPINGAMT'],2);
            }
        }
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername(self::$_username);
        $gateway->setPassword(self::$_password);
        $gateway->setSignature(self::$_signature);
        $gateway->setTestMode(self::$testMode);
        $params =  array(
                'cancelUrl' =>Request::root().'/collections',
                'returnUrl' =>Request::root().'/cart',
                'amount'    => number_format($cart['total_price'],2),
                'currency'  => self::$_currency,
                );
        session_start();
        $_SESSION['payment_params'] = $params;
        try {
            if(empty($customData))
                $response =  $gateway->purchase($params)->setItems($arr_items)->send();
            else{
                $request = $gateway->purchase($params)->setItems($arr_items);
                $data = $request->getData();
                // $customData['LANDINGPAGE'] = 'Login';
                $data = array_merge($data,$customData);
                ksort($data);
                $response = $request->sendData($data);
            }
            if ($response->isSuccessful()) {
               pr($response);
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                return Redirect::to('/cart')->with('order_message',$response->getMessage());
            }
        } catch (\Exception $e) {
            return Redirect::to('/cart')->with('order_message','Sorry, there was an error processing your payment. Please try again later.');
        }
    }

    public static function doCreditCardPayment($cart, $arr_data)
    {
        $arr_items = self::getProduct($cart);
        $gateway = Omnipay::create('PayPal_Pro');
        $gateway->setUsername(self::$_username);
        $gateway->setPassword(self::$_password);
        $gateway->setSignature(self::$_signature);
        $gateway->setTestMode(self::$testMode);
        $order_info = Session::get('order_info');
        $order_info = (array)json_decode($order_info);
        $order_info['billing_address'] = (array)$order_info['billing_address'];
        $order_info['shipping_address'] = (array)$order_info['shipping_address'];
        $customData['PAYMENTREQUEST_0_ITEMAMT'] = number_format($cart['total_price'],2);
        if(isset($arr_data['PAYMENTREQUEST_0_TAXAMT'] )){
            $cart['total_price'] += $arr_data['PAYMENTREQUEST_0_TAXAMT'];
            $order_info['tax'] = $customData['PAYMENTREQUEST_0_TAXAMT'] = number_format($arr_data['PAYMENTREQUEST_0_TAXAMT'],2);

        }

        if(isset($arr_data['PAYMENTREQUEST_0_SHIPPINGAMT'] )){
            $cart['total_price'] += $arr_data['PAYMENTREQUEST_0_SHIPPINGAMT'];
            $order_info['shipping_price'] = $customData['PAYMENTREQUEST_0_SHIPPINGAMT'] = number_format($arr_data['PAYMENTREQUEST_0_SHIPPINGAMT'],2);
        }
        $billing_province = $order_info['billing_address']['province'];
        if(is_numeric($order_info['billing_address']['province'])){
            $billing_province = City::where('id','=',$order_info['billing_address']['province'])->pluck('province_code');
        }
        $billing_country = $order_info['billing_address']['country'];
        if(is_numeric($order_info['billing_address']['country'])){
            $billing_country = Country::where('id','=',$order_info['billing_address']['country'])->pluck('name');
        }
        $shipping_province = $order_info['shipping_address']['province'];
        if(is_numeric($order_info['shipping_address']['province'])){
            $shipping_province = City::where('id','=',$order_info['shipping_address']['province'])->pluck('province_code');
        }
        $shipping_country = $order_info['shipping_address']['country'];
        if(is_numeric($order_info['shipping_address']['country'])){
            $shipping_country = Country::where('id','=',$order_info['shipping_address']['country'])->pluck('name');
        }
        $card = new CreditCard(array(
                               'firstName' => $arr_data['first_name'],
                               'lastName' => $arr_data['last_name'],
                               'number' => $arr_data['number'],
                               'expiryMonth' => $arr_data['month'],
                               'expiryYear' => $arr_data['year'],
                               'cvv' => $arr_data['verification_value'],
                               'billingAddress1'=>$order_info['billing_address']['address_1'],
                               'billingAddress2'=>$order_info['billing_address']['address_2'],
                               'billingCity'=>$order_info['billing_address']['city'],
                               'billingState'=>$billing_province,
                               'billingCountry'=>$billing_country,
                                'shippingAddress1'=>$order_info['shipping_address']['address_1'],
                               'shippingAddress2'=>$order_info['shipping_address']['address_2'],
                               'shippingCity'=>$order_info['shipping_address']['city'],
                               'shippingState'=>$shipping_province,
                               'shippingCountry'=>$shipping_country,
                               'company'=>$order_info['billing_address']['company'],
                               'email'=>$order_info['email'],
                               ));
        $params = [
            'amount' => number_format($cart['total_price'],2),
            'card' => $card,
            'cancelUrl' =>Request::root().'/collections',
            'returnUrl' =>Request::root().'/cart',
            'currency'  => self::$_currency,
        ];
        $request = $gateway->authorize($params);
        $card_info = $request->getData();
        try {
            $arr_order = $order_info;
            $arr_order['CREDITCARDTYPE'] = ucfirst($card_info['CREDITCARDTYPE']);
            $arr_order['ACCT'] = $card_info['ACCT'];
            $request = $gateway->purchase($params)->setItems($arr_items);
            $data = $request->getData();
            $data = array_merge($data,$customData);
            ksort($data);
            $response = $request->sendData($data);
            if ($response->isSuccessful()) {
                $data = $response->getData();
                $data = $gateway->fetchTransaction(array('transactionReference'=>$data['TRANSACTIONID']))->getData();
                $arr_order = array_merge($arr_order,$data);
                OrderController::createCreditCardPayPalOrder($arr_order);
                Cart::destroy();
                Session::forget('order_info');
                return Redirect::to('/cart')->with('order_success','Thank you for shopping with Tacky Design. Your order has been processed and you will receive a confirmation email shortly.');
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                return Redirect::to('/cart')->with('order_message',$response->getMessage());
            }
        } catch (\Exception $e) {
            return Redirect::to('/cart')->with('order_message','Sorry, there was an error processing your payment. Please try again later.');
        }
    }


    public static function paymentSuccess()
    {
        session_start();
        if(!isset($_SESSION['payment_params']))
            return Redirect::to('cart')->with('order_message','Missing payment params.');
        $params = $_SESSION['payment_params'];
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername(self::$_username);
        $gateway->setPassword(self::$_password);
        $gateway->setSignature(self::$_signature);
        $gateway->setTestMode(self::$testMode);
        $response = $gateway->completePurchase($params)->send();
        unset($_SESSION['payment_params']);
        if ($response->isSuccessful()) {
            $data = $response->getData();
            $data = $gateway->fetchTransaction(array('transactionReference'=>$data['PAYMENTINFO_0_TRANSACTIONID']))->getData();

            $url = (self::$testMode ? self::$testEndpoint : self::$liveEndpoint)."?USER={$data['USER']}&PWD={$data['PWD']}&SIGNATURE={$data['SIGNATURE']}&METHOD=GetTransactionDetails&VERSION={$data['VERSION']}&TRANSACTIONID={$data['TRANSACTIONID']}";
            parse_str (file_get_contents( $url ),$output);
            $data = array_merge($data,$output);
            ksort($data);
            OrderController::createPayPalOrder($data);
            Cart::destroy();
            return Redirect::to('/cart')->with('order_success','Thank you for shopping with Tacky Design. Your order has been processed and you will receive a confirmation email shortly.');
        } else {
            return Redirect::to('/cart')->with('order_message',$response->getData());
        }
        die;
    }

    public static function isValidCardNumber(&$arr_data)
    {
        $creditcard = array(  "visa"=>"/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/",
                        "mastercard"=>"/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/",
                        "amex"=>"/^3[4,7]\d{13}$/",);
        $match=false;
        foreach($creditcard as $type=>$pattern) {
            if(preg_match($pattern,$arr_data['number']) === 1) {
                $match=true;
                $arr_data['type'] = $type;
                break;
            }
        }
        if(!$match)
            return false;
        return true;
    }

    public static function checkSum($arr_data)
    {
        $checksum = 0;
        for ($i=(2-(strlen($arr_data['number']) % 2)); $i<=strlen($arr_data['number']); $i+=2)
            $checksum += (int)($arr_data['number']{$i-1});
        for ($i=(strlen($arr_data['number'])% 2) + 1; $i<strlen($arr_data['number']); $i+=2)
        {
            $digit = (int)($arr_data['number']{$i-1}) * 2;
                if ($digit < 10)
                    $checksum += $digit;
                else
                    $checksum += ($digit-9);
        }
        if (($checksum % 10) == 0)
            return true;
        else
            return false;
    }

    public static function checkExpDate($arr_data)
    {
        $expTs = mktime(0, 0, 0, $arr_data['month'] + 1, 1, $arr_data['year']);
        $curTs = time();
        $maxTs = $curTs + (10 * 365 * 24 * 60 * 60);
        if ($expTs > $curTs && $expTs < $maxTs)
            return true;
        return false;
    }

    public static function validateCVV($arr_data)
    {
        $count = ($arr_data['type'] === 'amex') ? 4 : 3;
        if(preg_match('/^[0-9]{'.$count.'}$/', $arr_data['verification_value']))
            return true;
        return false;
    }
}