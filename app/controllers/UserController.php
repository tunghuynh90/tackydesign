<?php

class UserController extends HomeController {
    public function __construct()
    {
        parent::__construct();
    }


    public function create(){
        if(Input::has('email'))
            return self::store();
        return View::make(self::$theme.'.signup');
    }

    public static function store(){
        $user = new User;
        $user->email = Input::get('email');
        $user->password = Input::get('password');
        $user->firstname = Input::get('first_name');
        $user->lastname = Input::get('last_name');
        $user->confirmation_code = md5(md5(time().rand(0,12569).date('D-M-Y')));
        $user->save();
        if($user->id){
            User::where('id',$user->id)->update(array(
                                               'firstname'=>Input::get('first_name'),
                                               'lastname'=>Input::get('last_name'),
                                               ));
            return Redirect::to('user/login')->with('notice',Lang::get('confide::confide.alerts.account_created'));
        }else{
            $error = $user->errors()->all(':message');
            return  Redirect::action('UserController@create')->withInput(Input::except('password'))->with('error',$error);
        }
    }

    public function confirm($code){
        if(Confide::confirm($code)){
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::to('user/login')->with('notice',$notice_msg);
        }else{
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::to('user/login')->with('error',$error_msg);
        }
    }

    public function login(){
        if(Confide::user()){
            return Redirect::to('/user/details');
        }else{
            return View::make(self::$theme.'.login');
        }
    }

    public function do_login(){
        $input = array(
            'email'    => Input::get( 'email' ),
            'password' => Input::get( 'password' ),
        );
        if ( Confide::logAttempt( $input) ){
            if(Input::has('redirect_url'))
                 return Redirect::intended('/'.Input::get('redirect_url'));
            else
                return Redirect::intended('/user/details');
        }else{
            $user = new User;
            if( Confide::isThrottled( $input ) ){
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            }
            elseif( $user->checkUserExists( $input ) and ! $user->isConfirmed( $input ) ){
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            }
            else{
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::action('UserController@login')
                            ->withInput(Input::except('password'))
                            ->with( 'error', $err_msg );
        }
    }

     public function forgot_password(){
        return View::make(self::$theme.'.forgot_password');
        //return Redirect::to('user/forgot_password');
    }

    public function do_forgot_password(){
        if( Confide::forgotPassword(Input::get('email'))){
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
                        return Redirect::action('UserController@login')
                            ->with( 'notice', $notice_msg );
        }else{
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
                        return Redirect::to('user/forgot_password')
                            ->withInput()
                ->with( 'error', $error_msg );
        }
    }

    public function reset_password($token){
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }


    public function do_reset_password(){
        $input = array(
            'token'=>Input::get('token'),
            'password'=>Input::get('password'),
             'password_confirmation'=>Input::get('password_confirmation'),
        );
        if( Confide::resetPassword( $input )){
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::to('user/login')
                            ->with( 'notice', $notice_msg );
        }else{
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::to('user/reset/'.$input['token'])
                            ->with( 'error', $error_msg );
        }
    }

    public  function logout(){
        Confide::logout();
        // Session::flush();
        return Redirect::to('/');
    }

    public function details()
    {
        $user = Confide::user();
        if(! $user)
            return Redirect::to('/user/login');
        $all_countries = Country::select('id','name')
                                ->where('publish','=',1)
                                ->get();
        foreach($all_countries as $country){
            $countries[$country->id] = $country->name;
        }
        return View::make(self::$theme.'.static.user_details')->with(array(
                                                                     'user'=>$user,
                                                                     'countries'=>$countries,
                                                                     ));
    }

    public function addresses()
    {
        $user = Confide::user();
        if(Input::has('update_address'))
            $user = self::updateAddress($user);
        $all_countries = Country::select('id','name')
                                ->where('publish','=',1)
                                ->get();
        foreach($all_countries as $country){
            $countries[$country->id] = array('name'=>$country->name,'cities'=>array());
            $cities = City::select('id','name')
                    ->where('publish','=',1)
                    ->where('country_id','=',$country->id)
                    ->get();
            foreach($cities as $city)
                $countries[$country->id]['cities'][$city->id] = $city->name;
        }
        return View::make(self::$theme.'.static.user_addresses')->with(array(
                                                                       'countries'=>$countries,
                                                                       'user'=>$user,
                                                                       ));
    }

    public static function updateAddress($user)
    {
        $arr_addresses = array();
        if(isset($user['address']) && $user['address']!='')
            $arr_addresses = unserialize($user['address']);
        $address = Input::get('address');
        if(!isset($address['province']))
            $address['province'] = 0;
        if(isset($address['key'])){
            $arr_addresses[$address['key']] = $address;
            $current_key = $address['key'];
            unset($address['key']);
        } else
            $arr_addresses[] = $address;
        if(!isset($current_key))
            $current_key = key(array_slice( $arr_addresses, -1, 1, TRUE ));
        if(count($arr_addresses) == 1)
            $address_default_key = 0;
        else if(isset($address['default']))
            $address_default_key = $current_key;
        $arr_addresses = serialize($arr_addresses);
        $arr_update['address'] = $arr_addresses;
        if(isset($address_default_key))
            $arr_update['address_default_key'] = $address_default_key;
        User::where('id',$user->id)
                ->update($arr_update);
        return User::where('id','=',$user->id)
                    ->get()
                    ->first();
    }

    public function deleteAddress($key)
    {
        $user = User::where('id','=',Confide::user()->id)
                    ->get()
                    ->first();
        $arr_addresses = array();
        if(isset($user['address']) && $user['address']!='')
            $arr_addresses = unserialize($user['address']);
        if(isset($arr_addresses[$key]))
            unset($arr_addresses[$key]);
        if(count($arr_addresses))
            $address_default_key = 0;
        else
            $address_default_key = null;
        $arr_addresses = serialize($arr_addresses);
        User::where('id',$user->id)
                ->update(array(
                         'address'=>$arr_addresses,
                         'address_default_key'=>$address_default_key
                         ));
        return Redirect::to('/user/addresses');
    }

}