<?php
class AdminBlogController extends AdminController{

	public function __contruct(){
		parrent::__contruct();
	}

	public static function adminAllBlog(){
		$v_title = 'Blog';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/blog';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.adminAllBlog')->with(array('session'=>$v_session_id,'quick'=>$v_quick,'icon'=>$arr_icons,'title'=>$v_title));
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$blog = AdminBlog::condition($arr_where)->sort($arr_order)->first();
		return $blog;
	}

	public function adminOneBlog($id=0){
		$v_title = 'Blog';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/blog';
		$v_session = 'ss_save_blog_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .='Saved with error';
		}else{
			$arr_columns = array();
			$blog = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)),array(array('field'=>'id','asc'=>true)));
			if($blog){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($blog->id)?$blog->id:0;
				$arr_columns['name'] = isset($blog->name)?$blog->name:'';
				$arr_columns['short_name'] = isset($blog->short_name)?$blog->short_name:'';
				$arr_columns['title'] = isset($blog->title)?$blog->title:'';
				$arr_columns['content'] = isset($blog->content)?$blog->content:'';
				$arr_columns['posted_date'] = isset($blog->posted_date)?date('Y-m-d H:i:s',strtotime($blog->posted_date)):'';

				$v_posted_date = isset($blog->posted_date)?date('Y-m-d H:i:s',strtotime($blog->posted_date)):'';
				$v_posted_date = date('Y-m-d H:i:s',strtotime($v_posted_date));
				$arr_columns['posted_date'] = $v_posted_date;
				$arr_columns['publish'] = isset($blog->publish)?$blog->publish:1;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['title']= '';
				$arr_columns['content'] = '';
				$arr_columns['posted_date'] = date("Y-m-d H:i:s");
				$arr_columns['publish'] = 1;
			}
		}
		return View::make('admin.adminOneBlog')->with(array('columns'=>$arr_columns,'message'=>$v_message,'icon'=>$arr_icons,'title'=>$v_title));
	}

	public function saveBlog(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_help`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_help')?Input::get('action_help'):'';
		if($v_action=='new'){
			$help = new AdminBlog;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$help = AdminBlog::condition($arr_where)->first();
			$v_allow_edit = !is_null($help);
			if(!$v_allow_edit){
				$help = new AdminBlog;
				$v_action = 'new';
			}
		}
		$v = AdminBlog::validate(Input::all());
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		$v_short_name = Input::has('short_name')?Input::get('short_name'):'';
		$arr_columns['short_name'] = $v_short_name;
		$v_title = Input::has('title')?Input::get('title'):'';
		$arr_columns['title'] = $v_title;
		$v_content = Input::has('content')?Input::get('content'):'';
		$arr_columns['content'] = htmlentities($v_content);
		$v_posted_date = date('Y-m-d H-i-s',strtotime(Input::has('posted_date')))?date('Y-m-d H:i:s',strtotime(Input::get('posted_date'))):'';
		$arr_columns['posted_date'] = $v_posted_date;
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_passes = $v->passes();
		if($v_passes){
			$help->name = $arr_columns['name'];
			$help->short_name = $arr_columns['short_name'];
			$help->title = $arr_columns['title'];
			$help->content = $arr_columns['content'];
			$help->posted_date = date('Y-m-d H:i:s',strtotime($arr_columns['posted_date']));
			$help->publish = $arr_columns['publish'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('title');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('content');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('posted_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_help_'.$v_id.'_';
		if($v_passes && $v_message==''){
			parent::clearCached();
			if($v_action=='new'){
				$admin = json_decode(Session::get('ss_admin'));
				$arr_columns['created_by'] = $admin->fullname;
				$help->created_by = $arr_columns['created_by'];
				$help->save();
				$insertId = $help->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeBlog($help)>0){
					if($v_allow_edit){
						$affectRow = $help->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/blog');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/blog/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/blog/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public static function getPageBlog(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			//$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			//$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminBlog::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'help'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$help = AdminBlog::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$help = AdminBlog::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($help){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($help as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
                    if(!isset($arr_content_group_name[$one->ghelp_id])){
                        $arr_where = array();
                        $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$one->ghelp_id);
                    }
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
					$arr_columns[$i]['title'] = isset($one->title)?$one->title:'';
					$arr_columns[$i]['posted_date'] = isset($one->posted_date)?date('Y-m-d',strtotime($one->posted_date)):'';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	private static function detectChangeBlog(AdminBlog $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function getDeleteBlog($blog){
		$arr_where = array();
		$arr_where[] = array('field'=>'id','operator'=>'=','value'=>$blog);
		$blog = AdminBlog::condition($arr_where)->get();
		if($blog)
			AdminBlog::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/blog');
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateBlog(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateBlog(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = AdminBlog::condition($arr_where)->update($arr_columns);
		else
			$v_rows = AdminBlog::update($arr_columns);
		return $v_rows;
	}

}