<?php
class AdminConfigController extends AdminController{

	static $v_module_group_short_name = 'system';
	static $v_module_short_name = 'configuration';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
		parent::__construct();
		$this->beforeFilter(function(){
			return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
		});
	}

	/**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model AdminConfig
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$config = AdminConfig::condition($arr_where)->sort($arr_order)->first();
		return $config;
	}

	/**
	 * View Config
	 * @param int $id
	 * @return array
	 */
	public function getViewConfig($id = 0){
		if(isset(self::$arr_permit[_R_VIEW])){
			return self::getOneConfig($id);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Update Config
	 * @param int $id
	 * @return array
	 */
	public function getEditConfig($id = 0){
		if(Input::has('submit'))
			return self::saveConfig();
		if(isset(self::$arr_permit[_R_UPDATE])){
			return self::getOneConfig($id, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Insert Config
	 * @param int $id
	 * @return array
	 */
	public function getAddConfig($id = 0){
		if(isset(self::$arr_permit[_R_INSERT])){
			return self::getOneConfig(0, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	private static function getOneConfig($id = 0, $p_edit = false){
        if($id==0) $id = 1;
		$v_title = 'Config - ';
		$arr_icons = array();
		$v_session = 'ss_save_config_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Save changes';
		}else{
			$arr_columns = array();
			$config = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($config){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($config->id)?$config->id:0;
				$arr_columns['title_site'] = isset($config->title_site)?$config->title_site:'';
				$arr_columns['meta_keywords'] = isset($config->meta_keywords)?$config->meta_keywords:'';
				$arr_columns['meta_description'] = isset($config->meta_description)?$config->meta_description:'';
				$arr_columns['logo_image'] = isset($config->logo_image)?$config->logo_image:'';
				$logo_url = Request::root().'/assets/images/logos/';
				$logo_path = public_path().DS.'assets'.DS.'images'.DS.'logos'.DS;
				if($arr_columns['logo_image'] != '' && file_exists($logo_path.$arr_columns['logo_image']) )
					$arr_columns['logo_image_link'] = $logo_url.$arr_columns['logo_image'];
				else
					$arr_columns['logo_image_link'] = '';
				$arr_columns['logo_retina_image'] = isset($config->logo_retina_image)?$config->logo_retina_image:'';
				if($arr_columns['logo_retina_image'] != '' && file_exists($logo_path.$arr_columns['logo_retina_image']) )
					$arr_columns['logo_retina_image_link'] = $logo_url.$arr_columns['logo_retina_image'];
				else
					$arr_columns['logo_retina_image_link'] ='';
				$arr_columns['favicon'] = isset($config->favicon)?$config->favicon:'';
				if($arr_columns['favicon'] != '' && file_exists($logo_path.$arr_columns['favicon']) )
					$arr_columns['favicon_link'] = $logo_url.$arr_columns['favicon'];
				else
					$arr_columns['favicon_link'] ='';
				$arr_columns['created_date'] = isset($config->created_date)?$config->created_date:'0000-00-00 00:00:00';
				$arr_columns['modified_date'] = isset($config->modified_date)?$config->modified_date:'0000-00-00 00:00:00';
				$arr_columns['created_by'] = isset($config->created_by)?$config->created_by:0;
				$arr_columns['modified_by'] = isset($config->modified_by)?$config->modified_by:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['title_site'] = '';
				$arr_columns['meta_keywords'] = '';
				$arr_columns['meta_description'] = '';
				$arr_columns['logo_image'] = $arr_columns['logo_retina_image']  = '';
				$arr_columns['logo_image_link'] = $arr_columns['logo_retina_image_link'] ='';
				$arr_columns['favicon'] = $arr_columns['favicon_link'] ='';
				$arr_columns['created_date'] = '0000-00-00 00:00:00';
				$arr_columns['modified_date'] = '0000-00-00 00:00:00';
				$arr_columns['created_by'] = 0;
				$arr_columns['modified_by'] = 0;
			}
		}
		return View::make('admin.one_config')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit,
                // 'theme'=>json_encode($arr_theme), 'selected_theme'=>$v_selected_theme
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllConfig(){
		if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Config - View All';
		$arr_icons = array();
		if(!isset(self::$arr_permit[_R_INSERT]))
			$arr_icons['new'] = self::$v_module_group_short_name.'/'.self::$v_module_short_name;
		$v_session_id = session_id();
		$v_quick = Input::has('quick')?Input::get('quick'):'';
		return View::make('admin.all_config')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateConfig(array $arr_columns, $arr_where){
			if(isset(self::$arr_permit[_R_UPDATE])){
			if(sizeof($arr_where)>0)
				$v_rows = AdminConfig::condition($arr_where)->update($arr_columns);
			else
				$v_rows = AdminConfig::update($arr_columns);
			return $v_rows;
		}else return 0;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminConfig $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeConfig(AdminConfig $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$config = AdminConfig::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$config = AdminConfig::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($config){
			$i=0;
			$v_row = $offset;
			foreach($config as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['title_site'] = isset($one->title_site)?$one->title_site:'';
					$arr_columns[$i]['meta_keywords'] = isset($one->meta_keywords)?$one->meta_keywords:'';
					$arr_columns[$i]['meta_description'] = isset($one->meta_description)?$one->meta_description:'';
					$arr_columns[$i]['created_date'] = isset($one->created_date)?$one->created_date:'0000-00-00 00:00:00';
					$arr_columns[$i]['modified_date'] = isset($one->modified_date)?$one->modified_date:'0000-00-00 00:00:00';
					$arr_columns[$i]['created_by'] = isset($one->created_by)?$one->created_by:0;
					$arr_columns[$i]['modified_by'] = isset($one->modified_by)?$one->modified_by:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageConfig(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'field_search', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['asc']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminConfig::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'config'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = AdminConfig::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $config
	 * @return Redirect
	 */
	public function getDeleteConfig($config){
		if(isset(self::$arr_permit[_R_DELETE])){			$arr_where = array();
			$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$config);
			$config = AdminConfig::condition($arr_where)->get();
			if($config) AdminConfig::condition($arr_where)->delete();
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = AdminConfig::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>html_entity_decode($r->$field_text), 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveConfig(){
		if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_config`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_config')?Input::get('action_config'):'';
		if($v_action=='new'){
			$config = new AdminConfig;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$config = AdminConfig::condition($arr_where)->first();
			$v_allow_edit = !is_null($config);
			if(!$v_allow_edit){
				$config = new AdminConfig;
				$v_action = 'new';
			}
		}
        $admin = AdminController::getAdmin();
		$v = AdminConfig::validate(Input::all());
		$v_title_site = Input::has('title_site')?Input::get('title_site'):'';
		$arr_columns['title_site'] = $v_title_site;
		$v_meta_keywords = Input::has('meta_keywords')?Input::get('meta_keywords'):'';
		$arr_columns['meta_keywords'] = $v_meta_keywords;
		$v_meta_description = Input::has('meta_description')?Input::get('meta_description'):'';
		$arr_columns['meta_description'] = $v_meta_description;
		$v_created_date = Input::has('created_date')?Input::get('created_date'):'0000-00-00 00:00:00';
		$arr_columns['created_date'] = $v_created_date;
		$v_modified_date = date('Y-m-d H:i:s');
		$arr_columns['modified_date'] = $v_modified_date;
		$v_created_by = Input::has('created_by')?Input::get('created_by'):0;
		settype($v_created_by,'int');
		$arr_columns['created_by'] = $v_created_by;
		$v_modified_by = $admin->id;
		$arr_columns['modified_by'] = $v_modified_by;
		$arr_columns['logo_image_link'] = $arr_columns['logo_retina_image_link'] ='';
		if(Input::has('logo_image')){
			$arr_columns['logo_image'] = Input::get('logo_image');
			if($arr_columns['logo_image'] != '')
				$arr_columns['logo_image_link'] = Request::root().'/assets/images/logos/'.$arr_columns['logo_image'];
		}
		if(Input::has('logo_retina_image')){
			$arr_columns['logo_retina_image'] = Input::get('logo_retina_image');
			if($arr_columns['logo_retina_image'] != '')
				$arr_columns['logo_retina_image_link'] = Request::root().'/assets/images/logos/'.$arr_columns['logo_retina_image'];
		}
		if(Input::has('favicon')){
			$arr_columns['favicon'] = Input::get('favicon');
			if($arr_columns['favicon'] != '')
				$arr_columns['favicon_link'] = Request::root().'/assets/images/logos/'.$arr_columns['favicon'];
		}
		$v_passes = $v->passes();
		if($v_passes){
			$config->title_site = $arr_columns['title_site'];
			$config->meta_keywords = $arr_columns['meta_keywords'];
			$config->meta_description = $arr_columns['meta_description'];
			$config->created_date = $arr_columns['created_date'];
			$config->modified_date = $arr_columns['modified_date'];
			$config->logo_image = $arr_columns['logo_image'];
			$config->favicon = $arr_columns['favicon'];
			$config->logo_retina_image = $arr_columns['logo_retina_image'];
			$config->created_by = $arr_columns['created_by'];
			$config->modified_by = $arr_columns['modified_by'];
		}else{
			$v_field_message = $v->messages()->first('title_site');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('meta_keywords');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('meta_description');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('created_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('modified_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('created_by');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('modified_by');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_config_'.$v_id.'_';
		$v_result = false;
		if($v_passes  && $v_message==''){
			if($v_action=='new'){
				if(isset(self::$arr_permit[_R_INSERT])){
					$config->save();
					$insertId = $config->id;
					$v_result = $insertId > 0;
					$v_id = $insertId;
				}
			}else{
				if(isset(self::$arr_permit[_R_UPDATE])){
					if(self::detectChangeConfig($config)>0){
						if($v_allow_edit){
							$arr_update = $arr_columns;
							unset($arr_update['logo_retina_image_link'],$arr_update['logo_image_link'],$arr_update['favicon_link']);
							$affectRow = $config->condition($arr_where)->update($arr_update);
							$v_result = $affectRow>0;
						}else{
							$v_message .= '<li>Current record not found!</li>';
						}
					}else{
						$v_result = true;
					}
				}
			}
			if($v_result){
				//if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				//if(Session::has($v_session.'message')) Session::forget($v_session.'message');
                $v_message = '<li>Update successful!</li>';
                $arr_columns['id'] = $v_id;

				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
                    array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
                );
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;

				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

}
?>