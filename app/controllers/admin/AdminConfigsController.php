<?php
class AdminConfigsController extends AdminController{

    static $v_module_group_short_name = 'system';
    static $v_module_short_name = 'configs';
    static $arr_permit = array();
	static $arr_setting = array();

    /**
     * constructor function
     */
    public function __construct(){
        parent::__construct();
        $this->beforeFilter(function(){
			self::__setting();
            return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
        });
    }

	public function __setting(){
		AdminConfigs::LoadSetting();
		self::$arr_setting = AdminConfigs::$field_setting;
	}



	/**
	 * ListView
	 * @return View
	 */
	public function ListView(){
		$arr_icons = array();
		$arr_icons['new'] = 'system/configs';
		$arr_icons['module_group'] = 'system';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.list_view')->with(
			array(
				'session'=>$v_session_id,
				'quick'=>$v_quick,
				'icon'=>$arr_icons,
				'title'=>self::$arr_setting['title'].' - List view',
				'permit'=>self::$arr_permit,
				'module_name'=>self::$v_module_short_name,
				'arr_setting'=>self::$arr_setting,
			)
		);
	}


	/**
	 * Get array by field key
	 * @param int $id
	 * @return array
	 */
	public function AddItem($id = 0){
		$v_title = '';
		$arr_icons = array();
		$dropdown = array();
		$lock = array();
		$arr_icons['view'] = 'system/configs';
		$v_session = 'ss_save_configs_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){

			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$module = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($module){

				$v_title .= 'Edit: ';
				$arr_columns['id'] = isset($module->id)?$module->id:0;
				$arr_columns['name'] = isset($module->name)?$module->name:'';
				$v_title .= $arr_columns['name'];
				$arr_columns['key_id'] = isset($module->key_id)?$module->key_id:'';
				$arr_columns['key_value'] = isset($module->key_value)?$module->key_value:'';
				$arr_columns['type'] = isset($module->type)?$module->type:1;
				$arr_columns['created_date'] = isset($module->created_date)?$module->created_date:time();
				$arr_columns['modified_date'] = isset($module->modified_date)?$module->modified_date:time();
				$arr_columns['created_by'] = isset($module->created_by)?$module->created_by:0;
				$arr_columns['modified_by'] = isset($module->modified_by)?$module->modified_by:0;
				$arr_columns['active'] = isset($module->active)?$module->active:1;
				$lock['key_id'] = '1';
				
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = 'Name or Description';
				$arr_columns['key_id'] = 'config_key';
				$arr_columns['key_value'] = 'Config value';
				$arr_columns['type'] = 1;
				$arr_columns['created_date'] = time();
				$arr_columns['modified_date'] = time();
				$arr_columns['created_by'] = 0;
				$arr_columns['modified_by'] = 0;
				$arr_columns['active'] = 1;
				$arr_columns['deleted'] = 0;
				
			}
		}

	   	$dropdown['type_json'] = json_encode(array(
									array('id'=>'1','name'=>'System'),
									array('id'=>'2','name'=>'Back end'),
								));
		
		

		return View::make('admin.detail_edit')->with(
			array(
				'columns' 	=> $arr_columns,
				//'message'	=> $v_message,
				'icon'		=> $arr_icons,
				'title'		=> self::$arr_setting['title'].' - '.$v_title,
				'session'	=> session_id(),
				'permit'	=> self::$arr_permit,
				'arr_setting'=>self::$arr_setting,
				'dropdown'	=> $dropdown,
				'lock'	=> $lock,
			)
		);
	}


	/**
	 * Insert or Update
	 */
	public function saveConfigs(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		
		//You must remove row below, if there is not field `publish` in table `upt_module`
		$v_action = Input::has('action_module')?Input::get('action_module'):'';
		if($v_action=='new'){
			$module = new AdminConfigs;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		
		//
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$module = AdminConfigs::condition($arr_where)->first();
			$v_allow_edit = !is_null($module);
			if(!$v_allow_edit){
				$module = new AdminConfigs;
				$v_action = 'new';
			}
		}
		$v = AdminConfigs::validate(Input::all());
		$v_name = Input::has('key_id')?Input::get('key_id'):'';
		$arr_columns['key_id'] = $v_name;
		$v_passes = $v->passes();
		if($v_passes){
			$module->key_id = $arr_columns['key_id'];
			$module->key_value = Input::has('key_value')?Input::get('key_value'):'';
			$module->name = Input::has('name')?Input::get('name'):'';
			$module->type = Input::has('type')?Input::get('type'):1;
			$module->active = Input::has('active')?Input::get('active'):1;
			$module->created_date = time();
			$module->modified_date = time();
            if(self::checkDuplicate($v_name, $v_id)==0){
                $v_message .= '<li>Duplicate Name!</li>';
            }
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('key_id');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('key_value');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('type');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('active');
		}
		$v_session = 'ss_save_configs_'.$v_id.'_';
		if($v_passes && $v_message==''){
			// tao moi
			if($v_action=='new'){
				$module->save();
				$insertId = $module->id;
				$v_result = $insertId > 0;
			//update
			}else{
				if(self::detectChangeConfigs($module)>0){
					if($v_allow_edit){
						$affectRow = $module->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				parent::clearCached();
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/system/configs');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/system/configs/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/system/configs/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}



    /**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model AdminModule
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$module = AdminConfigs::condition($arr_where)->sort($arr_order)->first();
		return $module;
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateConfigs(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = AdminConfigs::condition($arr_where)->update($arr_columns);
		else
			$v_rows = AdminConfigs::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminModule $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeConfigs($model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$module = AdminConfigs::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$module = AdminConfigs::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($module){
			$i=0;
			$v_row = $offset;
            $arr_group = array();
			foreach($module as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
				$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
				$arr_columns[$i]['key_id'] = isset($one->key_id)?$one->key_id:'';
				$arr_columns[$i]['key_value'] = isset($one->key_value)?$one->key_value:'';
				$arr_columns[$i]['type'] = isset($one->type)?$one->type:1;
				$arr_columns[$i]['active'] = isset($one->active)?$one->active:0;
				$arr_columns[$i]['created_date'] = isset($one->created_date)?$one->created_date:'';
				$arr_columns[$i]['modified_date'] = isset($one->modified_date)?$one->modified_date:'';
				$arr_columns[$i]['created_by'] = isset($one->created_by)?$one->created_by:'';
				$arr_columns[$i]['modified_by'] = isset($one->modified_by)?$one->modified_by:'';
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageConfigs(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		$arr_where[] = array('field'=>'deleted', 'operator'=>'=', 'value'=>0);
		if($v_quick_search != ''){
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
            $arr_order[] = array('field'=>'id', 'asc'=>false);
        }
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminConfigs::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;

		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'configs'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = AdminModule::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $module
	 * @return Redirect
	 */
	public function getDeleteConfigs($module){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$module);
		$module = AdminModule::condition($arr_where)->get();
		if($module) AdminModule::condition($arr_where)->delete();
		return Redirect::to('/admin/system/configs');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = AdminConfigs::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>$r->$field_text, 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}



    /**
     * Check unique name
     * @return mixed
     */
    public function getAjaxConfigs(){
        $v_type = Input::has('ajax_type')?Input::get('ajax_type'):'';
        $v_id = Input::has('id')?Input::get('id'):'0';
        $v_success = 0;
        $v_error = 0;
        $arr_return = array();
        settype($v_id, 'int');
        if($v_type=='check_unique_name'){
            $v_name = Input::has('name')?Input::get('name'):'';
            if($v_name!=''){
                $v_success = self::checkDuplicate($v_name, $v_id);
            }
            $arr_return['success'] = $v_success;
        }else if($v_type=='change_publish'){
            $v_status = Input::has('status')?Input::get('status'):'0';
            settype($v_status, 'int');
            if($v_status!=0) $v_status = 1;
            $module = AdminModule::where('id', $v_id)->where('publish', $v_status)->first();
            if($module){
                $v_status = 1-$v_status;
                $module->publish = $v_status;
                $module->save();
                $v_success = 1;
            }
            $arr_return = array(
                'error'=>$v_error, 'success'=>$v_success, 'status'=>$v_status
            );
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private function checkDuplicate($name, $id=0){
        $v_short = setUnSignedName($name);
        $arr_where = array();
        $arr_where[] = array('field'=>'id', 'operator'=>'!=', 'value'=>$id);
        $arr_where[] = array('field'=>'short_name', 'value'=>$v_short);
        $module = AdminModule::condition($arr_where)->first();
        $v_success = is_null($module)?1:0;
        return $v_success;
    }
}
?>