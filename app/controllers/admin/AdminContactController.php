<?php
class AdminContactController extends  AdminController{
	static $v_module_group_short_name = 'customers';
	static $v_module_short_name = 'contact-us';
	static $arr_permit = array();

	public function __construct(){
		parent::__construct();
		$this->beforeFilter(function(){
			return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
		});
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$contact = AdminContact::condition($arr_where)->sort($arr_order)->first();
		return $contact;
	}

	/**
	 * View Contact
	 * @param int $id
	 * @return array
	 */
	public function getViewContact($id = 0){
		if(isset(self::$arr_permit[_R_VIEW])){
			return self::getOneContact($id);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Update Contact
	 * @param int $id
	 * @return array
	 */
	public function getEditContact($id = 0){
		if(isset(self::$arr_permit[_R_UPDATE])){
			return self::getOneContact($id, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Insert Contact
	 * @param int $id
	 * @return array
	 */
	public function getAddContact($id = 0){
		if(isset(self::$arr_permit[_R_INSERT])){
			return self::getOneContact(0, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	private static function getOneContact($id = 0, $p_edit = false){
		$v_title = 'Contact - ';
		$arr_icons = array();
		$arr_icons['view'] = self::$v_module_group_short_name.'/'.self::$v_module_short_name;
		$v_session = 'ss_save_contact_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$contact = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($contact){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($contact->id)?$contact->id:0;
				$arr_columns['contact_name'] = isset($contact->contact_name)?$contact->contact_name:'';
				$arr_columns['contact_phone'] = isset($contact->contact_phone)?$contact->contact_phone:'';
				$arr_columns['contact_email'] = isset($contact->contact_email)?$contact->contact_email:'';



				$arr_columns['contact_message'] = isset($contact->contact_message)?$contact->contact_message:'';
				$arr_columns['posted_date'] = isset($contact->posted_date)?$contact->posted_date:'0000-00-00 00:00:00';

				$v_posted_date = isset($contact->posted_date)?$contact->posted_date:'0000-00-00 00:00:00';
				$v_posted_date = date('d-M-Y H:i:s',strtotime($v_posted_date));
				$arr_columns['posted_date'] = $v_posted_date;
				$arr_columns['publish'] = isset($contact->publish)?$contact->publish:1;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['contact_name'] = '';
				$arr_columns['contact_phone'] = '';
				$arr_columns['contact_email'] = '';
				$arr_columns['contact_message'] = '';
				$arr_columns['posted_date'] = date("d-F-Y H:i:s");
				$arr_columns['publish'] = 1;
			}
		}
		return View::make('/admin/adminOneContact')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllContact(){
		if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Contact - View All';
		$arr_icons = array();
		if(isset(self::$arr_permit[_R_INSERT]))
			$arr_icons['new'] = self::$v_module_group_short_name.'/'.self::$v_module_short_name;
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('/admin/adminAllContact')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateContact(array $arr_columns, $arr_where){
			if(isset(self::$arr_permit[_R_UPDATE])){
			if(sizeof($arr_where)>0)
				$v_rows = AdminContact::condition($arr_where)->update($arr_columns);
			else
				$v_rows = AdminContact::update($arr_columns);
			return $v_rows;
		}else return 0;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminContact $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeContact(AdminContact $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$contact = AdminContact::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$contact = AdminContact::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($contact){
			$i=0;
			$v_row = $offset;
			foreach($contact as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['contact_name'] = isset($one->contact_name)?$one->contact_name:'';
					$arr_columns[$i]['contact_phone'] = isset($one->contact_phone)?$one->contact_phone:'';
					$arr_columns[$i]['contact_email'] = isset($one->contact_email)?$one->contact_email:'';
					$arr_columns[$i]['contact_message'] = isset($one->contact_message)?$one->contact_message:'';
					$arr_columns[$i]['posted_date'] = isset($one->posted_date)?$one->posted_date:'';
					$arr_columns[$i]['publish'] = isset($one->publish)?$one->publish:1;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageContact(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'contact_name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			//$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			//$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminContact::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		for($i=0; $i<sizeof($arr_columns);$i++){
			$v_posted_date = $arr_columns[$i]['posted_date'];
			if($v_posted_date!=''){
				$v_posted_date = date('d-M-Y H:i:s', strtotime($v_posted_date));
			}else{
				$v_posted_date = date('d-M-Y H:i:s', time());
			}
			$arr_columns[$i]['posted_date'] = $v_posted_date;
		}
		$arr_return = array('total_rows'=>$v_total_rows, 'contact'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
/*	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = AdminContact::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}*/

	/**
	 * Delete record
	 * @param int $contact
	 * @return Redirect
	 */
	public function getDeleteContact($contact){
		if(isset(self::$arr_permit[_R_DELETE])){			$arr_where = array();
			$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$contact);
			$contact = AdminContact::condition($arr_where)->get();
			if($contact) AdminContact::condition($arr_where)->delete();
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Insert or Update
	 */
	public function saveContact(){
		if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_contact`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_contact')?Input::get('action_contact'):'';
		if($v_action=='new'){
			$contact = new AdminContact;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{	// luon vao day
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$contact = AdminContact::condition($arr_where)->first();
			$v_allow_edit = !is_null($contact);


			if(!$v_allow_edit){	// luon vao day
				$contact = new AdminContact;
				$v_action = 'new';
			}
		}
		$v = AdminContact::validate(Input::all());
		$v_contact_name = Input::has('contact_name')?Input::get('contact_name'):'';
		$arr_columns['contact_name'] = $v_contact_name;
		$v_contact_phone = Input::has('contact_phone')?Input::get('contact_phone'):'';
		$arr_columns['contact_phone'] = $v_contact_phone;
		$v_contact_email = Input::has('contact_email')?Input::get('contact_email'):'';
		$arr_columns['contact_email'] = $v_contact_email;
		$v_contact_message = Input::has('contact_message')?Input::get('contact_message'):'';
		$arr_columns['contact_message'] = $v_contact_message;
		$v_posted_date = Input::has('hide_posted_date')?Input::get('hide_posted_date'):'0000-00-00 00:00:00';
		$arr_columns['posted_date'] = $v_posted_date;
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_passes = $v->passes();
		if($v_passes){
			$contact->contact_name = $arr_columns['contact_name'];
			$contact->contact_phone = $arr_columns['contact_phone'];
			$contact->contact_email = $arr_columns['contact_email'];
			$contact->contact_message = $arr_columns['contact_message'];
			$contact->posted_date = $arr_columns['posted_date'];
			$contact->publish = $arr_columns['publish'];
		}else{
			$v_field_message = $v->messages()->first('contact_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('mask_phone');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('contact_phone');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('contact_phone_ext');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('contact_email');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('subject_id');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('contact_message');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('posted_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('orderno');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_contact_'.$v_id.'_';
		$v_result = false;
		if($v_passes  && $v_message==''){
			if($v_action=='new'){
				if(isset(self::$arr_permit[_R_INSERT])){
					$contact->save();
					$insertId = $contact->id;
					$v_result = $insertId > 0;
					$v_id = $insertId;
				}
			}else{
				if(isset(self::$arr_permit[_R_UPDATE])){
					if(self::detectChangeContact($contact)>0){
						if($v_allow_edit){
							$affectRow = $contact->condition($arr_where)->update($arr_columns);
							$v_result = $affectRow>0;
						}else{
							$v_message .= '<li>Current record not found!</li>';
						}
					}else{
						$v_result = true;
					}
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name);
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				$arr_columns['posted_date'] = date('d-M-Y H:i:s', strtotime($arr_columns['posted_date']));
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name.'/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			$arr_columns['posted_date'] = date('d-M-Y H:i:s', strtotime($arr_columns['posted_date']));
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name.'/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}
	public function getAjaxContact(){
        $v_type = Input::has('ajax_type')?Input::get('ajax_type'):'';
        $v_id = Input::has('id')?Input::get('id'):'0';
        $v_success = 0;
        $v_error = 0;
        $arr_return = array();
        settype($v_id, 'int');
        if($v_type=='check_unique_name'){
            $v_name = Input::has('name')?Input::get('name'):'';
            if($v_name!=''){
                $v_success = self::checkDuplicate($v_name, $v_id);
            }
            $arr_return['success'] = $v_success;
        }else if($v_type=='change_publish'){
            $v_status = Input::has('status')?Input::get('status'):'0';
            settype($v_status, 'int');
            if($v_status!=0) $v_status = 1;
            $module = AdminContact::where('id', $v_id)->where('publish', $v_status)->first();
            if($module){
                $v_status = 1-$v_status;
                $module->publish = $v_status;
                $module->save();
                $v_success = 1;
            }
            $arr_return = array(
                'error'=>$v_error, 'success'=>$v_success, 'status'=>$v_status
            );
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }


}