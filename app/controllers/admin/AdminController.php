<?php
class AdminController extends \BaseController {
    public function __construct()
    {
        parent::__construct();
        defined('CACHED_DIR') || define('CACHED_DIR', app_path().DS.'storage'.DS.'cache');
        $arr_menus = array();
        $admin = self::getAdmin();
        if(isset($admin) && $admin->login){
            $v_role_id = $admin?$admin->role_id:0;
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'value'=>$v_role_id);
            $arr_where[] = array('field'=>'publish', 'value'=>1);
            $role = AdminRole::condition($arr_where)->first();
            if($role){
                $v_permit_grant = $role->permit_grant;
                $v_is_restricted = $role->is_restricted==1;
                $arr_permit = json_decode($v_permit_grant, true);
                if(!is_array($arr_permit)) $arr_permit = array();
            }else{
                $arr_permit = array();
                $v_is_restricted = true;
            }


            $arr_where = array();
            $arr_where[] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
            $arr_where[] = array('field'=>'module_type_id', 'operator'=>'=', 'value'=>1);
            $arr_order = array();
            $arr_order[] = array('field'=>'orderno', 'asc'=>true);
            $groups = AdminModuleGroup::condition($arr_where)->sort($arr_order)->get();
            $i=0;
            foreach($groups as $g){
                $v_group_id = $g->id;
                $arr_where = array();
                $arr_where[] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
                $arr_where[] = array('field'=>'module_group_id', 'operator'=>'=', 'value'=>$v_group_id);
                $arr_order = array();
                $arr_order[] = array('field'=>'orderno', 'asc'=>true);
                $modules = AdminModule::condition($arr_where)->sort($arr_order)->get();
                if(isset($arr_permit[$v_group_id]) || !$v_is_restricted){
                    $arr_menus[$i] = array(
                        'sub'=>array(),
                        'name'=>$g->name,
                        'key'=>$g->short_name
                    );
                    $j=0;
                    foreach($modules as $m){
                        $v_module_id = $m->id;
                        if(isset($arr_permit[$v_group_id][$v_module_id]) || !$v_is_restricted){
                            $arr_menus[$i]['sub'][$j++] = array(
                                'name'=>$m->name,
                                'key'=>$m->short_name
                            );
                        }
                    }
                    $i++;
                }
            }
            $v_title = 'Home';
            View::share('admin', $admin);
            $arr_icons = array();
            View::share('icon', $arr_icons);
            View::share('title', $v_title);
            View::share('menus', $arr_menus);
            View::share('url', Request::root());
            View::share('config',HomeController::__getSiteInfo());
        }
    }

    public static function checkModule($p_module_group_short_name, $p_module_short_name, & $arr_permit){
        $admin = self::getAdmin();
        $arr = self::getModuleGroup($p_module_group_short_name, $p_module_short_name);
        $v_module_group_id = isset($arr['group'])?$arr['group']:0;
        $v_module_id = isset($arr['module'])?$arr['module']:0;
        $arr_permit = self::getUserPermission($v_module_group_id, $v_module_id, $admin->role_id);
        if(!isset($arr_permit[_R_VIEW])){
            return Redirect::to('/admin/error');
        }
    }

    public static function getUserPermission($p_group_id, $p_module_id, $p_role_id){
        $arr_return = array();
        $arr_where = array();
        $arr_where[] = array('field'=>'publish', 'value'=>1);
        $arr_where[] = array('field'=>'id', 'value'=>$p_role_id);
        $role = AdminRole::condition($arr_where)->first();
        if($role){
            $v_permit = $role->permit_grant;
            $v_is_restricted = $role->is_restricted==1;
            if($v_is_restricted){
                $arr_where = array();
                $arr_where[] = array('field'=>'publish', 'value'=>1);
                $arr_where[] = array('field'=>'id', 'value'=>$p_module_id);
                $arr_where[] = array('field'=>'module_group_id', 'value'=>$p_group_id);
                $module = AdminModule::condition($arr_where)->first();
                if($module){
                    $arr_permit = json_decode($v_permit, true);
                    if(!is_array($arr_permit)) $arr_permit = array();
                    if(isset($arr_permit[$p_group_id][$p_module_id])){
                        $v_rule = (int) $arr_permit[$p_group_id][$p_module_id];
                        if($v_rule % _R_VIEW ==0) $arr_return[_R_VIEW] = true;
                        if($v_rule % _R_INSERT ==0) $arr_return[_R_INSERT] = true;
                        if($v_rule % _R_UPDATE ==0) $arr_return[_R_UPDATE] = true;
                        if($v_rule % _R_DELETE ==0) $arr_return[_R_DELETE] = true;
                    }
                }
            }else{
                $arr_return[_R_VIEW] = true;
                $arr_return[_R_INSERT] = true;
                $arr_return[_R_UPDATE] = true;
                $arr_return[_R_DELETE] = true;
            }
        }
        return $arr_return;
    }

    private static function getModuleGroup($p_group_short, $p_module_short){
        $group_id = AdminModuleGroup::where('short_name', $p_group_short)->pluck('id');
        $module_id = AdminModule::where('short_name', $p_module_short)->where('module_group_id', $group_id)->pluck('id');
        return array('group'=>$group_id, 'module'=>$module_id);
    }

    public static function check(){
        $admin = self::getAdmin();
        return !((is_null($admin) || !isset($admin) || !$admin->login));
    }

    public static function index()
    {
        return View::make('admin.index');
    }

    public static function login()
    {
        if(Input::has('submit')){
            return self::doLogin();
        } else {
            $current_date = date('Y-m-d H:i:s');
            $current_date = md5($current_date);
            Session::put('ss_admin_login_token', $current_date);
            $try    = (int) Session::has('ss_admin_login_try')   ?   Session::get('ss_admin_login_try')  :   0;
            $time   = (int) Session::has('ss_admin_login_time')  ?   Session::get('ss_admin_login_time') :   0;
            $time   =  $time + 5*60 - time();
            return View::make('admin.login')->with(
                array(
                    'message'   =>  $time < 0 ? '' : '<li>Try again after: <label id="lbl_time">'.self::getTimeBySecond($time).'</label></li>'
                    ,'token'    =>  $current_date
                    ,'enable'   =>  $time >= 0
                    ,'second'   =>  $time
                )
            );
        }
    }

    private static function getTimeBySecond($second){
        $v_second = $second % 60;
        $v_minute = ($second - $v_second)/60;
        return substr('0'.$v_minute,strlen('0'.$v_minute)-2).':'.substr('0'.$v_second,strlen('0'.$v_second)-2);
    }

    private static function doLogin(){
        $v_username   = Input::has('username')?Input::get('username'):'';
        $v_password = Input::has('password')?Input::get('password'):'';
        $v_token = Input::has('token')?Input::get('token'):'';
        $v_session_token = Session::has('ss_admin_login_token')?Session::get('ss_admin_login_token'):'';
        $v_message = '';

        $v_date = date('Y-m-d H:i:s');
        $v_date = md5($v_date);
        Session::put('ss_admin_login_token', $v_date);
        $v_try = Session::has('ss_admin_login_try')?Session::get('ss_admin_login_try'):0;
        $v_time = Session::has('ss_admin_login_time')?Session::get('ss_admin_login_time'):0;
        settype($v_try, 'int');
        settype($v_time, 'int');
        $v_time =  $v_time + 5*60 - time();
        if($v_time<=5){

            if($v_try<=3){
                if($v_session_token!=$v_token){
                    $v_message .= '<li>Invalid token. Try again</li>';
                }else{
                    if($v_username==''){
                        $v_message .= '<li>Empty username.</li>';
                    }else{
                        $admin = Admin::condition(array(array('field'=>'username', 'value'=>$v_username)))->first();
                        if($admin){
                            if(($admin->password=='' || is_null($admin->password)) && $v_password=='') $v_password = session_id();
                            if($admin->password==md5($v_password)){
                                $admin->lastest_login = date('Y-m-d H:i:s', time());
                                $admin->save();
                                if($admin->is_active==1){
                                    $admin->login = true;
                                    $admin->ip = Request::getClientIp();
                                    $v_is_restricted = AdminRoleController::getScalar('is_restricted', array(array('field'=>'id', 'value'=>$admin->role_id)))==1;
                                    $admin->restricted = $v_is_restricted;
                                    self::storeAdmin($admin);
                                    if(Session::has('ss_admin_login_try')) Session::forget('ss_admin_login_try');
                                    if(Session::has('ss_admin_login_time')) Session::forget('ss_admin_login_time');
                                    if(Session::has('ss_admin_login_token')) Session::forget('ss_admin_login_token');
                                }else{
                                    $v_message = '<li>Account "'.$admin->username.'" is locked!</li>';
                                }
                            }else{
                                $v_message = '<li>Invalid username or password.</li>';
                            }
                        }else{
                            $v_message = '<li>Invalid username.</li>';
                        }
                    }
                }
                if($v_message!=''){
                    $v_try++;
                    Session::put('ss_admin_login_try', $v_try);
                }
            }else{
                Session::put('ss_admin_login_try', 0);
                Session::put('ss_admin_login_time', time());
                $v_time = 300;
                $v_message = '<li>Please try again after: <label id="lbl_time">'.self::getTimeBySecond($v_time).'</label></li>';
            }
        }else{
            $v_message = '<li>Please try again after: <label id="lbl_time">'.self::getTimeBySecond($v_time).'</label></li>';
        }
        if($v_message!=''){
            return View::make('admin.login')->with(
                array(
                    'message'   => $v_message
                    ,'enable'   => $v_time>0
                    ,'token'    => $v_date
                    ,'title'    => 'Admin Login'
                    ,'second'   => $v_time
                )
            );
        }else{
            return Redirect::to('/admin');
        }
    }

    public static function storeAdmin($admin){
        $arr_store = array();
        $arr_store['id'] = $admin->id;
        $arr_store['username'] = $admin->username;
        $arr_store['password'] = $admin->password;
        $arr_store['fullname'] = $admin->fullname;
        $arr_store['phone'] = $admin->phone;
        $arr_store['email'] = $admin->email;
        $arr_store['mobile'] = $admin->mobile;
        $arr_store['role_id'] = $admin->role_id;
        $arr_store['created_date'] = $admin->created_date;
        $arr_store['modified_date'] = $admin->modified_date;
        $arr_store['lastest_login'] = $admin->lastest_login;
        $arr_store['is_active'] = $admin->is_active;
        $arr_store['login'] = $admin->login;
        $arr_store['ip'] = $admin->ip;
        $arr_store['restricted'] = $admin->restricted;
        Session::put('ss_admin', json_encode($arr_store));
    }

    public static function getAdmin(){
        $v_lost = true;
        if(Session::has('ss_admin')){
            $arr_store = json_decode(Session::get('ss_admin'), true);
            $v_id = isset($arr_store['id'])?$arr_store['id']:0;
            $admin = Admin::condition(array(array('field'=>'id', 'value'=>$v_id)))->first();
            if($admin){
                $admin->ip = isset($arr_store['ip'])?$arr_store['ip']:'';
                $admin->login = isset($arr_store['login'])?$arr_store['login']:false;
                if($admin->login && $admin->ip!='')
                    $v_lost = false;
                if(!$v_lost){
                    $v_is_restricted = AdminRoleController::getScalar('is_restricted', array(array('field'=>'id', 'value'=>$admin->role_id)))==1;
                    $admin->restricted = $v_is_restricted;
                }
            }
        }
        if($v_lost)
            return null;
        else
            return $admin;
    }

    public function account(){
        if(Input::has('submit'))
            return self::accountSave();
        return View::make('admin.admin_account')->with(
            array(
                'title'=>'Edit Info',
                'message'=>''
            )
        );
    }

    public static function accountSave(){
        $v_current_password = Input::has('cpassword')?Input::get('cpassword'):'';
        $v_new_password = Input::has('npassword')?Input::get('npassword'):'';
        $v_repeat_password = Input::has('rpassword')?Input::get('rpassword'):'';
        $v_fullname = Input::has('fullname')?Input::get('fullname'):'';
        $v_phone = Input::has('phone')?Input::get('phone'):'';
        $v_mobile = Input::has('mobile')?Input::get('mobile'):'';
        $v_email = Input::has('email')?Input::get('email'):'';

        $v_id = Input::has('id')?Input::get('id'):0;
        settype($v_id, 'int');
        $admin = self::getAdmin();
        if($v_id!=$admin['id']){
            Session::flush();
            return Redirect::to('/admin/login');
        }else{
            $admin = Admin::condition(array(array('field'=>'id', 'value'=>$v_id)))->first();
            if($admin){
                $v_message = '';
                if($admin->password==md5($v_current_password)){
                    if($v_new_password!=''){
                        if($v_new_password!=$v_repeat_password)
                            $v_message .= '<li>Repeat password is not matched.</li>';
                    }
                    if($v_message==''){
                        $admin->fullname = $v_fullname;
                        if($v_new_password!='') $admin->password = md5($v_new_password);
                        $admin->phone = $v_phone;
                        $admin->mobile = $v_mobile;
                        $admin->email = $v_email;
                        $admin->modified_date = date('Y-m-d H:i:s', time());
                        $admin->save();
                        self::storeAdmin($admin);
                        $v_message = '<li>Your info has changed successful</li>';
                    }
                }else{
                    $v_message = '<li>Your password is not matched!</li>';
                }
                return View::make('admin.admin_account')->with(array(
                    'title'=>'Edit Info'
                    ,'message'=>$v_message
                ));
            }else{
                Session::flush();
                return Redirect::to('/admin/login');
            }
        }
    }

    public static function logout(){
        if(Session::has('ss_admin'))
            Session::forget('ss_admin');
        Session::flush();
        return Redirect::to('/');
    }

    public static function clearCached($dir = CACHED_DIR)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if(in_array($object, array('.','..','.gitignore'))) continue;
                if (filetype($dir."/".$object) == "dir")
                    self::clearCached($dir."/".$object);
                else
                    unlink($dir."/".$object);
            }
            reset($objects);
            @rmdir($dir);
        }
    }


}