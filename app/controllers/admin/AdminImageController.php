<?php
class AdminImageController extends AdminController{

	/**
	 * constructor function
	 */
	public function __construct()
	{
		parent::__construct();

	}

	public function listImages()
	{
        $v_dir = public_path()."/assets/upload/";
        if(Input::has('image_path'))
            $v_dir = public_path()."/assets/".Input::get('image_path').'/';
        $handle = opendir($v_dir);
        $arr_list_image = array();
        if($handle){
            while(($file = readdir($handle))!==false){
                if($file!='.' && $file!='..'){
                    $imagefile = $file;
                    if(is_file($v_dir.DS.$file)){
                        $arr_list_image[] = array(
                            'name'=>$imagefile
                        ,'type'=>'f'
                        ,'size'=>filesize($v_dir.DS.$file)
                        );
                    }
                }
            }
        }
        $response = Response::json($arr_list_image);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function deleteFile(){
        $include_thumb = Input::has('include_thumb') ? Input::get('include_thumb') : 0;
        $v_name = Input::has('name') ? Input::get('name') : '';
        $v_path = public_path().DS.'assets'.DS.'upload'.DS;
        if(Input::has('image_path')){
            $image_path = Input::get('image_path');
            $v_path = public_path().DS.'assets'.DS.$image_path.DS;
        }
        if(file_exists($v_path.$v_name))
            unlink ($v_path.$v_name);
        if($include_thumb && file_exists($v_path.'thumb'.DS.$v_name))
            unlink ($v_path.'thumb'.DS.$v_name);
    }

    public function uploadImage()
    {
        $v_file_size = 200;
        $v_path = public_path().DS.'assets'.DS.'upload'.DS;
        $arr_list_image = array();
        if(Input::has('image_path')){
            $image_path = Input::get('image_path');
            switch ($image_path) {
                case 'logo':
                    $v_path = public_path().DS.'assets'.DS.'images'.DS.'logos'.DS;
                    break;
                case 'social_icon':
                    $v_path = public_path().DS.'assets'.DS.'images'.DS.'social_icon'.DS;
                    break;
                case 'moodboard':
                    $v_path = public_path().DS.'assets'.DS.'upload'.DS.'moodboard'.DS;
                    break;
                case 'lookbook':
                    $v_path = public_path().DS.'assets'.DS.'upload'.DS.'lookbook'.DS;
                    break;
                case 'slide':
                    $v_path = public_path().DS.'assets'.DS.'upload'.DS.'slide'.DS;
                    break;
                default:
                    $v_path = public_path().DS.'assets'.DS.'upload'.DS;
                    break;
            }
        }
        if(Input::hasFile('file')){
            $file = Input::file('file');
            $filename = $file->getClientOriginalName();
            $uploadSuccess = Input::file('file')->move($v_path, $filename);
            if( $uploadSuccess ) {
                $arr_list_image = array(
                    'name'=>$filename
                    ,'type'=>'f'
                    ,'size'=>$v_file_size
                );
            }
        } else {
            if(Input::hasFile('other_image')){
                $files = Input::file('other_image');
            } else if(Input::hasFile('other_image_lookbook')){
                $v_path = public_path()."/assets/upload/lookbook/";
                $files = Input::file('other_image_lookbook');
            } else if(Input::hasFile('other_image_moodboard')){
                $v_path = public_path()."/assets/upload/moodboard/";
                $files = Input::file('other_image_moodboard');
            }
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $uploadSuccess = $file->move($v_path, $filename);
                if( $uploadSuccess ) {
                    $arr_list_image[] = array(
                        'name'=>$filename
                        ,'type'=>'f'
                        ,'size'=>$v_file_size
                    );
                }
            }
        }
        $response = Response::json($arr_list_image);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function thumbImage(){
        $v_dir = public_path().DS.'assets'.DS.'upload'.DS;
        if(Input::has('image_path')){
            $image_path = Input::get('image_path');
            switch ($image_path) {
                case 'logo':
                    $v_dir = public_path().DS.'assets'.DS.'images'.DS.'logos'.DS;
                    break;
                case 'social_icon':
                    $v_dir = public_path().DS.'assets'.DS.'images'.DS.'social_icon'.DS;
                    break;
                case 'moodboard':
                    $v_dir = public_path().DS.'assets'.DS.'upload'.DS.'moodboard'.DS;
                    break;
                case 'lookbook':
                    $v_dir = public_path().DS.'assets'.DS.'upload'.DS.'lookbook'.DS;
                    break;
                case 'slide':
                    $v_dir = public_path().DS.'assets'.DS.'upload'.DS.'slide'.DS;
                    break;
                default:
                    $v_dir = public_path().DS.'assets'.DS.'upload'.DS;
                    break;
            }
        }
        $v_image_file = Input::get('path');
        if($v_image_file){
            if(file_exists($v_dir.$v_image_file) && is_file($v_dir.$v_image_file)){
                $v_file = $v_dir.$v_image_file;
                list($width, $height, $type) = @getimagesize($v_file);
                $v_thumb_width = 150;
                $v_percent = $v_thumb_width / $width;
                $v_thumb_height = floor($height * $v_percent);
                switch($type){
                    case 1;//Gif
                        $im_source = @imagecreatefromgif($v_file);
                        break;
                    case 2;//Jpg
                        $im_source = @imagecreatefromjpeg($v_file);
                        break;
                    case 3;//Png
                        $im_source = @imagecreatefrompng($v_file);
                        break;
                    default ;
                        $im_source = @imagecreatefromjpeg($v_file);
                        break;
                }
                $tmp_image = imagecreatetruecolor( $v_thumb_width, $v_thumb_height );
                imagecopyresized($tmp_image, $im_source, 0, 0, 0, 0, $v_thumb_width, $v_thumb_height, $width, $height );

                /* Output the image with headers */
                header("Content-Type: image/jpeg");
                imagejpeg($tmp_image);
                imagedestroy($tmp_image);
                imagedestroy($im_source);
            }echo null;
        }echo null;
    }


}
?>