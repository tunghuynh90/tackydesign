<?php
class AdminModuleGroupController extends AdminController{

    static $v_module_group_short_name = 'system';
    static $v_module_short_name = 'module-group';
    static $arr_permit = array();

    /**
     * constructor function
     */
    public function __construct(){
        parent::__construct();
        $this->beforeFilter(function(){
            return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
        });
    }


    /**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model AdminModuleGroup
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$module_group = AdminModuleGroup::condition($arr_where)->sort($arr_order)->first();
		return $module_group;
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @return array
	 */
	public function getOneModuleGroup($id = 0){
		$v_title = 'Module Group - ';
		$arr_icons = array();
		$arr_icons['view'] = 'system/module-group';
		$v_session = 'ss_save_module_group_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$module_group = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($module_group){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($module_group->id)?$module_group->id:0;
				$arr_columns['module_type_id'] = isset($module_group->module_type_id)?$module_group->module_type_id:0;
				$arr_columns['name'] = isset($module_group->name)?$module_group->name:'';
				$arr_columns['short_name'] = isset($module_group->short_name)?$module_group->short_name:'';
				$arr_columns['publish'] = isset($module_group->publish)?$module_group->publish:1;
				$arr_columns['orderno'] = isset($module_group->orderno)?$module_group->orderno:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['module_type_id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['publish'] = 1;
				$arr_columns['orderno'] = 0;
			}
		}
		return View::make('admin.module_group_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'session'=>session_id(), 'permit'=>self::$arr_permit
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllModuleGroup(){
		$v_title = 'Module_group - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'system/module-group';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.module_group_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateModuleGroup(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = AdminModuleGroup::condition($arr_where)->update($arr_columns);
		else
			$v_rows = AdminModuleGroup::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminModuleGroup $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeModuleGroup(AdminModuleGroup $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$module_group = AdminModuleGroup::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$module_group = AdminModuleGroup::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($module_group){
			$i=0;
			$v_row = $offset;
			foreach($module_group as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['module_type_id'] = isset($one->module_type_id)?$one->module_type_id:0;
					$arr_columns[$i]['module_type'] = $arr_columns[$i]['module_type_id']==1?'Back End':($arr_columns[$i]['module_type_id']==2?'Member':($arr_columns[$i]['module_type_id']==3?'Front End':'--------'));
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
					$arr_columns[$i]['publish'] = isset($one->publish)?$one->publish:1;
					$arr_columns[$i]['orderno'] = isset($one->orderno)?$one->orderno:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageModuleGroup(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminModuleGroup::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'module_group'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	public static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = AdminModuleGroup::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $module_group
	 * @return Redirect
	 */
	public function getDeleteModuleGroup($module_group){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$module_group);
		$module_group = AdminModuleGroup::condition($arr_where)->get();
		if($module_group) AdminModuleGroup::condition($arr_where)->delete();
		return Redirect::to('/admin/system/module-group');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	public static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = AdminModuleGroup::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>html_entity_decode($r->$field_text), 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveModuleGroup(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_module_group`
		$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_module_group')?Input::get('action_module_group'):'';
		if($v_action=='new'){
			$module_group = new AdminModuleGroup;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$module_group = AdminModuleGroup::condition($arr_where)->first();
			$v_allow_edit = !is_null($module_group);
			if(!$v_allow_edit){
				$module_group = new AdminModuleGroup;
				$v_action = 'new';
			}
		}
		$v = AdminModuleGroup::validate(Input::all());
		$v_module_type_id = Input::has('module_type_id')?Input::get('module_type_id'):0;
		settype($v_module_type_id,'int');
		$arr_columns['module_type_id'] = $v_module_type_id;
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		if(Input::has('short_name'))
			$arr_columns['short_name'] = Input::get('short_name');
		else{
			$v_short_name = setUnSignedName($v_name);
			$arr_columns['short_name'] = $v_short_name;
		}
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_orderno = Input::has('orderno')?Input::get('orderno'):0;
		settype($v_orderno,'int');
		$arr_columns['orderno'] = $v_orderno;
		$v_passes = $v->passes();
		if($v_passes){
			$module_group->module_type_id = $arr_columns['module_type_id'];
			$module_group->name = $arr_columns['name'];
			$module_group->short_name = $arr_columns['short_name'];
			$module_group->publish = $arr_columns['publish'];
			$module_group->orderno = $arr_columns['orderno'];
            if(!self::checkDuplicate($v_name, $v_id)){
                $v_message .= '<li>Duplicate Name!</li>';
            }
		}else{
			$v_field_message = $v->messages()->first('module_type_id');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('orderno');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_module_group_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$module_group->save();
				$insertId = $module_group->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeModuleGroup($module_group)>0){
					if($v_allow_edit){
						$affectRow = $module_group->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				parent::clearCached();
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/system/module-group');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/system/module-group/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/system/module-group/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

    /**
     * Check unique name
     * @return mixed
     */
    public function getAjaxModuleGroup(){
        $v_type = Input::has('ajax_type')?Input::get('ajax_type'):'';
        $v_id = Input::has('id')?Input::get('id'):'0';
        $v_success = 0;
        $v_error = 0;
        $arr_return = array();
        settype($v_id, 'int');
        if($v_type=='check_unique_name'){
            $v_name = Input::has('name')?Input::get('name'):'';
            if($v_name!=''){
                $v_success = self::checkDuplicate($v_name, $v_id);
            }
            $arr_return['success'] = $v_success;
        }else if($v_type=='change_publish'){
            $v_status = Input::has('status')?Input::get('status'):'0';
            settype($v_status, 'int');
            if($v_status!=0) $v_status = 1;
            $group = AdminModuleGroup::where('id', $v_id)->where('publish', $v_status)->first();
            if($group){
                $v_status = 1-$v_status;
                $group->publish = $v_status;
                $group->save();
                $v_success = 1;
            }
            $arr_return = array(
                'error'=>$v_error, 'success'=>$v_success, 'status'=>$v_status
            );
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private function checkDuplicate($name, $id=0){
        $v_short = setUnSignedName($name);
        $arr_where = array();
        $arr_where[] = array('field'=>'id', 'operator'=>'!=', 'value'=>$id);
        $arr_where[] = array('field'=>'short_name', 'value'=>$v_short);
        $group = AdminModuleGroup::condition($arr_where)->first();
        $v_success = is_null($group)?1:0;
        return $v_success;
    }

}
?>