<?php
class AdminRoleController extends AdminController{
	/**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model AdminRole
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$role = AdminRole::condition($arr_where)->sort($arr_order)->first();
		return $role;
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @return array
	 */
	public function getOneRole($id = 0){
		$v_title = 'Role - ';
		$arr_icons = array();
		$arr_icons['view'] = 'account/user-group';
		$v_session = 'ss_save_role_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_permit = Session::get($v_session.'permit');
            $arr_permit = json_decode($v_permit, true);
            if(!is_array($arr_permit)) $arr_permit = array();
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$role = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($role){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($role->id)?$role->id:0;
				$arr_columns['name'] = isset($role->name)?$role->name:'';
				$arr_columns['short_name'] = isset($role->short_name)?$role->short_name:'';
				$arr_columns['permit_grant'] = isset($role->permit_grant)?$role->permit_grant:'';
				$arr_columns['is_restricted'] = isset($role->is_restricted)?$role->is_restricted:1;
				$arr_columns['publish'] = isset($role->publish)?$role->publish:1;
				$arr_columns['orderno'] = isset($role->orderno)?$role->orderno:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['permit_grant'] = '';
				$arr_columns['is_restricted'] = 1;
				$arr_columns['publish'] = 1;
				$arr_columns['orderno'] = 0;
			}
            $arr_permit = self::getModulePermission($role);
		}
		return View::make('adminOneRole')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>$arr_permit
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllRole(){
		$v_title = 'Role - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'account/user-group';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('adminAllRole')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateRole(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = AdminRole::condition($arr_where)->update($arr_columns);
		else
			$v_rows = AdminRole::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminRole $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeRole(AdminRole $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$role = AdminRole::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$role = AdminRole::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($role){
			$i=0;
			$v_row = $offset;
			foreach($role as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
					$arr_columns[$i]['permit_grant'] = isset($one->permit_grant)?$one->permit_grant:'';
					$arr_columns[$i]['is_restricted'] = isset($one->is_restricted)?$one->is_restricted:1;
					$arr_columns[$i]['publish'] = isset($one->publish)?$one->publish:1;
					$arr_columns[$i]['orderno'] = isset($one->orderno)?$one->orderno:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageRole(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminRole::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'role'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	public static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = AdminRole::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $role
	 * @return Redirect
	 */
	public function getDeleteRole($role){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$role);
		$role = AdminRole::condition($arr_where)->get();
		if($role) AdminRole::condition($arr_where)->delete();
		return Redirect::to('/admin/account/user-group');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	public static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = AdminRole::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
        if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>$r->$field_text, 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveRole(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_role`
		$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_role')?Input::get('action_role'):'';
		if($v_action=='new'){
			$role = new AdminRole;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$role = AdminRole::condition($arr_where)->first();
			$v_allow_edit = !is_null($role);
			if(!$v_allow_edit){
				$role = new AdminRole;
				$v_action = 'new';
			}
		}
		$v = AdminRole::validate(Input::all());
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		$v_short_name = setUnSignedName($v_name);
		$arr_columns['short_name'] = $v_short_name;
		$v_permit_grant = Input::has('permit_grant')?Input::get('permit_grant'):'';
		//$arr_columns['permit_grant'] = $v_permit_grant;
		$v_is_restricted = Input::has('is_restricted')?1:0;
		settype($v_is_restricted,'int');
		$arr_columns['is_restricted'] = $v_is_restricted;
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_orderno = Input::has('orderno')?Input::get('orderno'):0;
		settype($v_orderno,'int');
		$arr_columns['orderno'] = $v_orderno;

        $v_permit_grant = Input::has('permit_grant')?Input::get('permit_grant'):'';
        if($v_permit_grant!=''){
            if(get_magic_quotes_gpc()) $v_permit_grant = stripslashes($v_permit_grant);
            $arr_per = json_decode($v_permit_grant, true);
            if(!is_array($arr_per))
                $arr_permit = array();
            else{
                $arr_permit = array();
                for($i=0; $i<sizeof($arr_per);$i++){
                    $gid = $arr_per[$i]['gid'];
                    $arr = $arr_per[$i]['mod'];
                    $arr_mod = array();
                    $k = 0;
                    for($j=0; $j<sizeof($arr); $j++){
                        $arr_mod[$arr[$j]['mid']] = $arr[$j]['val'];
                        $k++;
                    }
                    if($k>0) $arr_permit[$gid] = $arr_mod;
                }
            }
        }else $arr_permit = array();
        $arr_columns['permit_grant'] = json_encode($arr_permit);
		$v_passes = $v->passes();
		if($v_passes){
			$role->name = $arr_columns['name'];
			$role->short_name = $arr_columns['short_name'];
			$role->permit_grant = $arr_columns['permit_grant'];
			$role->is_restricted = $arr_columns['is_restricted'];
			$role->publish = $arr_columns['publish'];
			$role->orderno = $arr_columns['orderno'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('permit_grant');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('is_restricted');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('orderno');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_role_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$role->save();
				$insertId = $role->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeRole($role)>0){
					if($v_allow_edit){
						$affectRow = $role->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/account/user-group');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
                $arr_permit = self::getModulePermission($role, $arr_permit);
				return Redirect::to('/admin/account/user-group/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message, $v_session.'permit'=>json_encode($arr_permit))
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
            $arr_permit = self::getModulePermission($role, $arr_permit);
			return Redirect::to('/admin/account/user-group/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message, $v_session.'permit'=>json_encode($arr_permit)
				)
			);
		}
	}

    public static function getModulePermission($role, array $arr_permit = null){
        if(!is_null($role)){
            $v_permit_grant = $role->permit_grant;
            $v_rule = $role->is_restricted==1?1:_R_FULL;
        }else{
            $v_permit_grant = json_encode(array());
            $v_rule = 1;
        }

        if(is_null($arr_permit) || !is_array($arr_permit)) $arr_permit = json_decode($v_permit_grant, true);
        if(!is_array($arr_permit)) $arr_permit = array();
        $arr_return = array();
        $group = AdminModuleGroup::condition(array(array('field'=>'publish', 'value'=>1)))->sort(array(array('field'=>'orderno', 'asc'=>true)))->get();
        foreach($group as $g){
            $v_group_id = $g->id;
            $v_group_name = $g->short_name;
            $arr_module = array();
            $module = AdminModule::condition(array(array('field'=>'module_group_id', 'value'=>$v_group_id), array('field'=>'publish', 'value'=>1)))->sort(array(array('field'=>'orderno', 'asc'=>true)))->get();
            foreach($module as $m){
                $v_module_name = $m->short_name;
                $v_module_id = $m->id;
                $arr_module[$v_module_id] = array(
                    'name'=>$m->name
                    ,'short'=>$v_module_name
                    ,'rule'=>isset($arr_permit[$v_group_id][$v_module_id])?$arr_permit[$v_group_id][$v_module_id]:$v_rule
                );
            }
            if(sizeof($arr_module)>0){
                $arr_return[$v_group_id] = array(
                    'name'=>$g->name
                    ,'short'=>$v_group_name
                    ,'module'=>$arr_module
                );

            }
        }
        return $arr_return;
    }
}
?>