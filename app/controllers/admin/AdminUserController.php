<?php
class AdminUserController extends AdminController{

    static $v_group_module_id = 0;
    static $v_module_id = 0;
	/**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model Admin
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$user_admin = Admin::condition($arr_where)->sort($arr_order)->first();
		return $user_admin;
	}


    public function edit_account(){
        $v_current_password = Input::has('cpassword')?Input::get('cpassword'):'';
        $v_new_password = Input::has('npassword')?Input::get('npassword'):'';
        $v_repeat_password = Input::has('rpassword')?Input::get('rpassword'):'';
        $v_fullname = Input::has('fullname')?Input::get('fullname'):'';
        $v_phone = Input::has('phone')?Input::get('phone'):'';
        $v_mobile = Input::has('mobile')?Input::get('mobile'):'';
        $v_email = Input::has('email')?Input::get('email'):'';

        $v_id = Input::has('id')?Input::get('id'):0;
        settype($v_id, 'int');
        $admin = parent::getAdmin();
        if($v_id!=$admin['id']){
            Session::flush();
            return Redirect::to('/admin/login');
        }else{
            $admin = Admin::condition(array(array('field'=>'id', 'value'=>$v_id)))->first();
            if($admin){
                $v_message = '';
                if($admin->password==md5($v_current_password)){
                    if($v_new_password!=''){
                        if($v_new_password!=$v_repeat_password)
                            $v_message .= '<li>Repeat password is not matched.</li>';
                    }
                    if($v_message==''){
                        $admin->fullname = $v_fullname;
                        if($v_new_password!='') $admin->password = md5($v_new_password);
                        $admin->phone = $v_phone;
                        $admin->mobile = $v_mobile;
                        $admin->email = $v_email;
                        $admin->modified_date = date('Y-m-d H:i:s', time());
                        $admin->save();
                        parent::storeAdmin($admin);
                        $v_message = '<li>Your info has changed successful</li>';
                    }
                }else{
                    $v_message = '<li>Your password is not matched!</li>';
                }
                return View::make('adminAdminAccount')->with(array(
                    'title'=>'Edit Info'
                    ,'message'=>$v_message
                ));
            }else{
                Session::flush();
                return Redirect::to('/admin/login');
            }
        }
    }
	/**
	 * Get array by field key
	 * @param int $id
	 * @return array
	 */
	public function getOneUserAdmin($id = 0){
		$v_title = 'Admin user - ';
		$arr_icons = array();
		$arr_icons['view'] = 'account/user';
		$v_session = 'ss_save_user_admin_'.$id.'_';
		$v_message = '';
        $arr_json = array();
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$user_admin = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($user_admin){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($user_admin->id)?$user_admin->id:0;
				$arr_columns['username'] = isset($user_admin->username)?$user_admin->username:'';
				$arr_columns['password'] = isset($user_admin->password)?$user_admin->password:'';
				$arr_columns['fullname'] = isset($user_admin->fullname)?$user_admin->fullname:'';
				$arr_columns['phone'] = isset($user_admin->phone)?$user_admin->phone:'';
				$arr_columns['mobile'] = isset($user_admin->mobile)?$user_admin->mobile:'';
				$arr_columns['email'] = isset($user_admin->email)?$user_admin->email:'';
				$arr_columns['role_id'] = 1;
				$arr_columns['created_date'] = isset($user_admin->created_date)?$user_admin->created_date:'0000-00-00 00:00:00';
				$arr_columns['modified_date'] = isset($user_admin->modified_date)?$user_admin->modified_date:'0000-00-00 00:00:00';
				$arr_columns['is_active'] = isset($user_admin->is_active)?$user_admin->is_active:1;
				$arr_columns['lastest_login'] = isset($user_admin->lastest_login)?$user_admin->lastest_login:'0000-00-00 00:00:00';
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['username'] = '';
				$arr_columns['password'] = '';
				$arr_columns['fullname'] = '';
				$arr_columns['phone'] = '';
				$arr_columns['mobile'] = '';
				$arr_columns['email'] = '';
				$arr_columns['role_id'] = 1;
				$arr_columns['created_date'] = '0000-00-00 00:00:00';
				$arr_columns['modified_date'] = '0000-00-00 00:00:00';
				$arr_columns['is_active'] = 1;
				$arr_columns['lastest_login'] = '0000-00-00 00:00:00';
			}
            $arr_json = AdminRoleController::getForOption('id', 'name',$arr_columns['role_id'], array(array('field'=>'publish', 'value'=>1)), array(array('field'=>'orderno', 'asc'=>true)));
            $arr_columns['role_json'] = json_encode($arr_json);
		}
		return View::make('admin.admin_user_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllUserAdmin(){
		$v_title = 'Admin User - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'account/user';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.admin_user_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateUserAdmin(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = Admin::condition($arr_where)->update($arr_columns);
		else
			$v_rows = Admin::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param Admin $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeUserAdmin(Admin $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$user_admin = Admin::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$user_admin = Admin::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($user_admin){
            $arr_role = array();
			$i=0;
			$v_row = $offset;
			foreach($user_admin as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['username'] = isset($one->username)?$one->username:'';
					$arr_columns[$i]['password'] = isset($one->password)?$one->password:'';
					$arr_columns[$i]['fullname'] = isset($one->fullname)?$one->fullname:'';
					$arr_columns[$i]['phone'] = isset($one->phone)?$one->phone:'';
					$arr_columns[$i]['mobile'] = isset($one->mobile)?$one->mobile:'';
					$arr_columns[$i]['email'] = isset($one->email)?$one->email:'';
                    $v_role_id = isset($one->role_id)?$one->role_id:0;
					$arr_columns[$i]['role_id'] = 1;
                    // if(!isset($arr_role[$v_role_id])){
                    //     $arr_role[$v_role_id] = AdminRoleController::getScalar('name', array(array('field'=>'id', 'value'=>$v_role_id)));
                    // }
					$arr_columns[$i]['created_date'] = isset($one->created_date)?date('d-M-Y H:i:s',strtotime($one->created_date)):'--------';
					$arr_columns[$i]['modified_date'] = isset($one->modified_date)?date('d-M-Y H:i:s',strtotime($one->modified_date)):'--------';
					$arr_columns[$i]['is_active'] = isset($one->is_active)?$one->is_active:1;
					$arr_columns[$i]['lastest_login'] = isset($one->lastest_login)?date('d-M-Y H:i:s',strtotime($one->lastest_login)):'--------';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageUserAdmin(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'username', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
			$arr_where[] = array('field'=>'fullname', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%', 'or');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Admin::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'user_admin'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = Admin::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $user_admin
	 * @return Redirect
	 */
	public function getDeleteUserAdmin($user_admin){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$user_admin);
		$user_admin = Admin::condition($arr_where)->get();
		if($user_admin) Admin::condition($arr_where)->delete();
		return Redirect::to('/admin/account/user');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = Admin::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
        if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>$r->$field_text, 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveUserAdmin(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_user_admin`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_user_admin')?Input::get('action_user_admin'):'';
		if($v_action=='new'){
			$user_admin = new Admin;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$user_admin = Admin::condition($arr_where)->first();
			$v_allow_edit = !is_null($user_admin);
			if(!$v_allow_edit){
				$user_admin = new Admin;
				$v_action = 'new';
			}
		}
		$v = Admin::validate(Input::all());
		$v_username = Input::has('username')?Input::get('username'):'';
		//$arr_columns['username'] = $v_username;
		$v_password = Input::has('password')?Input::get('password'):'';
        if($v_password!='')
		    $arr_columns['password'] = md5($v_password);
		$v_fullname = Input::has('fullname')?Input::get('fullname'):'';
		$arr_columns['fullname'] = $v_fullname;
		$v_phone = Input::has('phone')?Input::get('phone'):'';
		$arr_columns['phone'] = $v_phone;
		$v_mobile = Input::has('mobile')?Input::get('mobile'):'';
		$arr_columns['mobile'] = $v_mobile;
		$v_email = Input::has('email')?Input::get('email'):'';
		$arr_columns['email'] = $v_email;
		// $v_role_id = Input::has('role_id')?Input::get('role_id'):0;
		// settype($v_role_id,'int');
		$arr_columns['role_id'] = 1;
		//$v_created_date = Input::has('created_date')?Input::get('created_date'):'0000-00-00 00:00:00';
		//$arr_columns['created_date'] = $v_created_date;
		$v_modified_date = date('Y-m-d H:i:s', time());
		$arr_columns['modified_date'] = $v_modified_date;
		$v_is_active = Input::has('is_active')?Input::get('is_active'):1;
		settype($v_is_active,'int');
		$arr_columns['is_active'] = $v_is_active;
		$v_lastest_login = date('Y-m-d H:i:s');
		$arr_columns['lastest_login'] = $v_lastest_login;
		$v_passes = $v->passes();
		if($v_passes){
			$user_admin->username = $v_username;
            if($v_password!='')
			    $user_admin->password = md5($v_password);
			$user_admin->fullname = $arr_columns['fullname'];
			$user_admin->phone = $arr_columns['phone'];
			$user_admin->mobile = $arr_columns['mobile'];
			$user_admin->email = $arr_columns['email'];
			$user_admin->role_id = $arr_columns['role_id'];
			$user_admin->created_date = $arr_columns['modified_date'];
			$user_admin->modified_date = $arr_columns['modified_date'];
			$user_admin->is_active = $arr_columns['is_active'];
			$user_admin->lastest_login = $arr_columns['modified_date'];
		}else{
			$v_field_message = $v->messages()->first('username');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('password');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('fullname');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('phone');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('mobile');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('email');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('role_id');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('created_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('modified_date');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('is_active');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('lastest_login');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_user_admin_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$user_admin->save();
				$insertId = $user_admin->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeUserAdmin($user_admin)>0){
					if($v_allow_edit){
						$affectRow = $user_admin->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/account/user');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
                $arr_columns['username'] = $v_username;
                $arr_json = AdminRoleController::getForOption('id', 'name',$arr_columns['role_id'], array(array('field'=>'publish', 'value'=>1)), array(array('field'=>'orderno', 'asc'=>true)));
                $arr_columns['role_json'] = json_encode($arr_json);
				return Redirect::to('/admin/account/user/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
            $arr_columns['username'] = $v_username;
            $arr_json = AdminRoleController::getForOption('id', 'name',$arr_columns['role_id'], array(array('field'=>'publish', 'value'=>1)), array(array('field'=>'orderno', 'asc'=>true)));
            $arr_columns['role_json'] = json_encode($arr_json);
			return Redirect::to('/admin/account/user/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

}
?>