<?php
class CountryController extends AdminController{

    static $v_module_group_short_name = 'system';
    static $v_module_short_name = 'country';
    static $arr_permit = array();

    /**
     * constructor function
     */
    public function __construct(){
        parent::__construct();
        $this->beforeFilter(function(){
            return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
        });
    }

    /**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model Country
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$country = Country::condition($arr_where)->sort($arr_order)->first();
		return $country;
	}

    public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            if(isset(self::$arr_permit[_R_UPDATE])){
                $rows = self::updateCountry(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
                if($rows) $arr_return['error'] = 0;
            }
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * View Country
     * @param int $id
     * @return array
     */
    public function getViewCountry($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneCountry($id);
        }else return Redirect::to('/admin/error');
    }

    /**
     * Edit Country
     * @param int $id
     * @return array
     */
    public function getEditCountry($id=0){
        if(isset(self::$arr_permit[_R_UPDATE])){
            return self::getOneCountry($id, true);
        }else return Redirect::to('/admin/error');
    }

    /**
     * Add Country
     * @return array
     */
    public function getAddCountry(){
        if(isset(self::$arr_permit[_R_INSERT])){
            return self::getOneCountry(0, true);
        }else return Redirect::to('/admin/error');
    }

    /**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	private static function getOneCountry($id = 0, $p_edit = false){
		$v_title = 'Country - ';
		$arr_icons = array();
		$arr_icons['view'] = 'system/country';
		$v_session = 'ss_save_country_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$country = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($country){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($country->id)?$country->id:0;
				$arr_columns['name'] = isset($country->name)?$country->name:'';
				$arr_columns['short_name'] = isset($country->short_name)?$country->short_name:'';
				$arr_columns['publish'] = isset($country->publish)?$country->publish:1;
				$arr_columns['orderno'] = isset($country->orderno)?$country->orderno:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['publish'] = 1;
				$arr_columns['orderno'] = 0;
			}
		}
		return View::make('admin.country_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllCountry(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Country - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'system/country';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.country_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateCountry(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = Country::condition($arr_where)->update($arr_columns);
            else
                $v_rows = Country::update($arr_columns);
            return $v_rows;
        }else return 0;
	}

	/**
	 * Detect Eloquent changes
	 * @param Country $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeCountry(Country $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$country = Country::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$country = Country::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($country){
			$i=0;
			$v_row = $offset;
			foreach($country as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
//					$arr_columns[$i]['publish'] = isset($one->publish)?$one->publish:1;
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
					$arr_columns[$i]['orderno'] = isset($one->orderno)?$one->orderno:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageCountry(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Country::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'country'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	public static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = Country::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $country
	 * @return Redirect
	 */
	public function getDeleteCountry($country){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$country);
            $country = Country::condition($arr_where)->get();
            if($country) Country::condition($arr_where)->delete();
            return Redirect::to('/admin/system/country');
        }else return Redirect::to('/admin/error');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = Country::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>html_entity_decode($r->$field_text), 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveCountry(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_country`
		$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_country')?Input::get('action_country'):'';
		if($v_action=='new'){
			$country = new Country;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$country = Country::condition($arr_where)->first();
			$v_allow_edit = !is_null($country);
			if(!$v_allow_edit){
				$country = new Country;
				$v_action = 'new';
			}
		}
		$v = Country::validate(Input::all());
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		$v_short_name = Input::has('short_name')?Input::get('short_name'):'';
		$arr_columns['short_name'] = $v_short_name;
		$v_publish = Input::has('publish')?Input::get('publish'):1;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_orderno = Input::has('orderno')?Input::get('orderno'):0;
		settype($v_orderno,'int');
		$arr_columns['orderno'] = $v_orderno;
		$v_passes = $v->passes();
		if($v_passes){
			$country->name = $arr_columns['name'];
			$country->short_name = $arr_columns['short_name'];
			$country->publish = $arr_columns['publish'];
			$country->orderno = $arr_columns['orderno'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('orderno');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_country_'.$v_id.'_';
        $v_result = false;
		if($v_passes && $v_message==''){
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
				    $country->save();
				    $insertId = $country->id;
				    $v_result = $insertId > 0;
                }
			}else{
				if(self::detectChangeCountry($country)>0){
					if($v_allow_edit){
                        if(isset(self::$arr_permit[_R_UPDATE])){
						    $affectRow = $country->condition($arr_where)->update($arr_columns);
						    $v_result = $affectRow>0;
                        }
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/system/country');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/system/country/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/system/country/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

}
?>