<?php
class CustomerServiceController extends AdminController{

	public function __contruct(){
		parrent::__contruct();
	}

	public static function getAllCustomer(){
		$v_title = 'Help - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/customer-service';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('/admin/customer_service_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$help = AdminHelp::condition($arr_where)->sort($arr_order)->first();
		return $help;
	}

	public function getOneCustomer($id = 0){
		$v_title = 'Help - ';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/customer-service';
		$v_session = 'ss_save_help_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$help = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($help){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($help->id)?$help->id:0;
				$arr_columns['name'] = isset($help->name)?$help->name:'';
				$arr_columns['short_name'] = isset($help->short_name)?$help->short_name:'';

				$arr_columns['image_url'] = isset($help->image)?('/assets/upload/'.$help->image):''; // image name
				if(file_exists($arr_columns['image_url'])){
                    $arr_columns['image_url'] = Request::root()."/". $arr_columns['image_url'];
                }
                else{
                    $arr_columns['image_url'] =Request::root(). '/assets/images/no_image.jpg';
                }
                $arr_columns['image'] = $help->image;
				$arr_columns['description'] = isset($help->description)?$help->description:'';
				$arr_columns['content'] = isset($help->content)?$help->content:'';
				$arr_columns['publish'] = isset($help->publish)?$help->publish:1;
				$arr_columns['orderno'] = isset($help->orderno)?$help->orderno:0;
				$arr_columns['group_id'] = isset($help->group_id)?$help->group_id:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['image'] = '';
				$arr_columns['image_url'] = Request::root()."/assets/images/no_image.jpg";
				$arr_columns['description'] = '';
				$arr_columns['content'] = '';
				$arr_columns['publish'] = 1;
				$arr_columns['orderno'] = 0;
				$arr_columns['group_id'] = 0;
			}
		}
		return View::make('/admin/customer_service_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	public function saveCustomer(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_help`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_help')?Input::get('action_help'):'';
		if($v_action=='new'){
			$help = new AdminHelp;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$help = AdminHelp::condition($arr_where)->first();
			$v_allow_edit = !is_null($help);
			if(!$v_allow_edit){
				$help = new AdminHelp;
				$v_action = 'new';
			}
		}
		$v = AdminHelp::validate(Input::all());
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		$v_short_name = Input::has('short_name')?Input::get('short_name'):'';
		$arr_columns['short_name'] = $v_short_name;
		$v_description = Input::has('description')?Input::get('description'):'';
		$arr_columns['description'] = $v_description;
		$v_content = Input::has('content')?Input::get('content'):'';
		$arr_columns['content'] = htmlentities($v_content);
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_orderno = Input::has('orderno')?Input::get('orderno'):0;
		settype($v_orderno,'int');
		$arr_columns['orderno'] = $v_orderno;
		$v_group_id = Input::has('group_id')?Input::get('group_id'):0;
		settype($v_group_id,'int');
		$arr_columns['group_id'] = $v_group_id;
		$v_passes = $v->passes();
		if($v_passes){
			$help->name = $arr_columns['name'];
			$help->short_name = $arr_columns['short_name'];
			$help->description = $arr_columns['description'];
			$help->content = $arr_columns['content'];
			$help->publish = $arr_columns['publish'];
			$help->orderno = $arr_columns['orderno'];
			$help->group_id = $arr_columns['group_id'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('description');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('content');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('orderno');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('group_id');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_help_'.$v_id.'_';
		if($v_passes && $v_message==''){
			parent::clearCached();
			if($v_action=='new'){
				$help->save();
				$insertId = $help->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeHelp($help)>0){
					if($v_allow_edit){
						$affectRow = $help->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/customer-service');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/customer-service/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/customer-service/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function getPageCustomer(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$arr_where[] = array('field'=>'group_id','operator'=>'=','value'=>2);
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = AdminHelp::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'help'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$help = AdminHelp::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$help = AdminHelp::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($help){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($help as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
                    if(!isset($arr_content_group_name[$one->ghelp_id])){
                        $arr_where = array();
                        $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$one->ghelp_id);
                        $arr_where[] = array('field' =>'group_id', 'operator'=>'=','value'=>2);
                    }
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['image'] = $one->image != ''?(Request::root().'/assets/upload/'.$one->image):Request::root().'/assets/images/no_image.jpg';
					$arr_columns[$i]['description'] = isset($one->description)?$one->description:'';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
                    $arr_columns[$i]['group_id'] = isset($one->group_id)?$one->group_id:0;
                    $arr_columns[$i]['group_name'] = $arr_columns[$i]['group_id']==1?'Information':($arr_columns[$i]['group_id']==2?'Customer Service':('-----'));
                    $arr_columns[$i]['orderno'] = isset($one->orderno)?$one->orderno:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getDeleteCustomer($help){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$help);
		$help = AdminHelp::condition($arr_where)->get();
		if($help) AdminHelp::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/customer-service');
	}

	private static function detectChangeHelp(AdminHelp $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateHelp(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateHelp(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = AdminHelp::condition($arr_where)->update($arr_columns);
		else
			$v_rows = AdminHelp::update($arr_columns);
		return $v_rows;
	}
}