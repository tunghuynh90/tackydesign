<?php
class EmailTemplateController extends AdminController{
	static $v_module_group_short_name = 'static-contents';
    static $v_module_short_name = 'email-template';
    static $arr_permit = array();

    /**
     * constructor function
     */
    public function __construct(){
        parent::__construct();
        $this->beforeFilter(function(){
            return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
        });
    }

    public static function getAllEmailTemplate(){
		$v_title = 'Email Template - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/email-template';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.email_template_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$email_template = EmailTemplate::condition($arr_where)->sort($arr_order)->first();
		return $email_template;
	}

	public function getOneEmailTemplate($id = 0){
		$v_title = 'Email Template - ';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/email-template';
		$v_session = 'ss_save_template_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$arr_columns['type_list'] = array(
			                                  array('name'=>'---- Select ----','value'=>''),
			                                  array('name'=>'User confirm','value'=>'user_confirm'),
			                                  array('name'=>'Order confirm','value'=>'order_confirm'),
			                                  array('name'=>'Order summited','value'=>'order_summited'),
			                                  );
			$email_template = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($email_template){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($email_template->id)?$email_template->id:0;
				$arr_columns['type'] = isset($email_template->type)?$email_template->type:'';
				$arr_columns['subject'] = isset($email_template->subject)?$email_template->subject:'';
				$arr_columns['content'] = isset($email_template->content)?$email_template->content:'';
				$arr_columns['publish'] = isset($email_template->publish)?$email_template->publish:1;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['type'] = '';
				$arr_columns['subject'] = '';
				$arr_columns['content'] = '';
				$arr_columns['publish'] = 1;
			}
		}
		return View::make('admin.email_template_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	public function saveEmailTemplate(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_help`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_email_template')?Input::get('action_email_template'):'';
		if($v_action=='new'){
			$email_template = new EmailTemplate;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$email_template = EmailTemplate::condition($arr_where)->first();
			$v_allow_edit = !is_null($email_template);
			if(!$v_allow_edit){
				$email_template = new EmailTemplate;
				$v_action = 'new';
			}
		}
		$v = EmailTemplate::validate(Input::all());
		$arr_columns['type'] = Input::get('type');
		$arr_columns['subject'] = Input::get('subject');
		$arr_columns['content'] = htmlentities(Input::get('content'));
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_passes = $v->passes();
		if($v_passes){
			$email_template->type = $arr_columns['type'];
			$email_template->subject = $arr_columns['subject'];
			$email_template->content = $arr_columns['content'];
			$email_template->publish = $arr_columns['publish'];
		}else{
			$v_field_message = $v->messages()->first('type');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('subject');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('content');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_template_'.$v_id.'_';
		if($v_passes && $v_message==''){
			parent::clearCached();
			if($v_action=='new'){
				$email_template->save();
				$insertId = $email_template->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeHelp($email_template)>0){
					if($v_allow_edit){
						$affectRow = $email_template->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/email-template');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/email-template/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/email-template/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function getPageEmailTemplate(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'type', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = EmailTemplate::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'email_template'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$email_template = EmailTemplate::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$email_template = EmailTemplate::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($email_template){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($email_template as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['type'] = isset($one->type)? ucfirst(str_replace('_', ' ', $one->type)):'';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getDeleteEmailTemplate($email_template){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$email_template);
		$email_template = EmailTemplate::condition($arr_where)->get();
		if($email_template) EmailTemplate::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/email-template');
	}

	private static function detectChangeHelp(EmailTemplate $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateEmailTemplate(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateEmailTemplate(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = EmailTemplate::condition($arr_where)->update($arr_columns);
		else
			$v_rows = EmailTemplate::update($arr_columns);
		return $v_rows;
	}
}