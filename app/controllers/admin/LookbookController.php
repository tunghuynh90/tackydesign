<?php
class LookbookController extends AdminController{
	static $v_module_group_short_name = 'static-content';
	static $v_module_short_name = 'lookbook';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public function addLookbook()
	{
		return self::getOneLookbookView(0,true);
	}

	public static function index()
	{
		return self::getAllLookbookView();
	}

	private static function getOneLookbookView($id = 0, $p_edit = false, $returnColums = false){
		$v_title = 'Lookbook - ';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/lookbook';
		$v_session = 'ss_save_lookbook_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$lookbook = '';
			if($id)
				$lookbook = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($lookbook){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($lookbook->id)?$lookbook->id:0;
				$arr_columns['name'] = isset($lookbook->name)?$lookbook->name:'';
				$arr_columns['short_name'] = isset($lookbook->short_name)?$lookbook->short_name:'';
                $arr_columns['main_image'] = $lookbook->main_image != '' ? $lookbook->main_image:''; // image name
                $arr_columns['other_image'] = $arr_columns['hidden_other_image'] = array();
                if($lookbook->other_image != ''){
                	$other_image = unserialize($lookbook->other_image);
                	foreach ($other_image as $image) {
                		if(!file_exists(public_path().DS.'assets'.DS.'upload'.DS.'lookbook'.DS.$image)) continue;
                		$arr_columns['other_image'][] = array(
                		                                      'name' 	=> $image,
                		                                      'size'	=> filesize(public_path().DS.'assets'.DS.'upload'.DS.'lookbook'.DS.$image),
                		                                      'link'	=> 'assets/upload/lookbook/'.$image
                		                                      );
                		$arr_columns['hidden_other_image'][] = $image;
                	}
                }
                $arr_columns['hidden_other_image'] = implode('@_@',$arr_columns['hidden_other_image']);
                $arr_columns['other_image'] = json_encode($arr_columns['other_image']);
                $arr_columns['image_link'] = public_path().'/assets/upload/lookbook/'.$lookbook->main_image;
                if($lookbook->main_image != '' && file_exists($arr_columns['image_link'])){
                    $arr_columns['image_link'] = Request::root()."/assets/upload/lookbook/". $lookbook->main_image;
                }
                else{
                    $arr_columns['image_link'] = '';
                }
                $arr_columns['publish'] = isset($lookbook->publish)?$lookbook->publish:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
                $arr_columns['main_image'] = ''; // image name
                $arr_columns['other_image'] = '';
                $arr_columns['image_link'] = '';
                $arr_columns['hidden_other_image'] = '';
                $arr_columns['publish'] = 1;
			}
		}
		if($returnColums)
			return $arr_columns;
		return View::make('admin.lookbook_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		return Lookbook::condition($arr_where)->sort($arr_order)->first();
	}

	public static function getAllLookbookView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Lookbooks - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/lookbook';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.lookbook_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
        $v_size_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_size_field==0)
		    $lookbook = Lookbook::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $lookbook = Lookbook::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($lookbook){
			$i=0;
			$v_row = $offset;
			foreach($lookbook as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['main_image'] = $one->main_image != ''?(Request::root().'/assets/upload/lookbook/'.$one->main_image):Request::root().'/assets/images/no_image.jpg';
                    $arr_columns[$i]['publish'] = $one->publish==1?'icon-unhide.png':'icon-hide.png';
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public static function getPageLookbook($isReturnArray = false, $arr_option = array()){
		if(empty($arr_option)){
			$v_quick_search = Input::has('quick')?Input::get('quick'):'';
			//Create for where clause
			$arr_where = array();
			if($v_quick_search != ''){
				//Please replace 'field_search' by 'field' you want
				$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
			}
			//Create for order by
			$arr_order = array();
			$arr_tmp = Input::has('sort')?Input::get('sort'):array();
			if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
				for($i=0; $i<sizeof($arr_tmp); $i++){
					$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
				}
			}
			//Create for page limit
			$v_page = Input::has('page')?Input::get('page'):1;
			$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
			settype($v_page, 'int');
			settype($v_page_size, 'int');
		} else {
			$arr_where 	= isset($arr_option['arr_where']) ? $arr_option['arr_where'] : array();
			$arr_order 	= isset($arr_option['arr_order']) ? $arr_option['arr_order'] : array();
			$v_page 	= isset($arr_option['v_page']) 	? $arr_option['v_page'] : 1;
			$v_page_size 	= isset($arr_option['v_page_size']) 	? $arr_option['v_page_size'] : 10;
		}
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Lookbook::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows,'total_pages'=>$v_total_pages, 'lookbook'=>$arr_columns);
		if($isReturnArray)
			return $arr_return;
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function updateStatus(){
        $id = 	Input::has('txt_id')  ?  (int)Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  (int) Input::get('txt_value') : 0;
        $field = Input::has('txt_name')  ?  Input::get('txt_name') : 'publish';
        $arr_return = array('error'=>1);
        if($id!=0){
            if(isset(self::$arr_permit[_R_UPDATE])){
                $rows = self::updateLookbook(array($field=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
                if($rows) $arr_return['error'] = 0;
            }
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateLookbook(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = Lookbook::condition($arr_where)->update($arr_columns);
            else
                $v_rows = Lookbook::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editLookbook($id=0){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOneLookbookView($id, true);
        return Redirect::to('/admin/error');
    }

    public function deleteLookbook($lookbook){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$lookbook);
            $lookbook = lookbook::condition($arr_where)->get()->first();
            if($lookbook){
            	$other_image = $lookbook->other_image;
            	if($other_image){
            		$other_image = unserialize($other_image);
            		foreach($other_image as $image){
            			$image_path = public_path().DS.'assets'.DS.'upload'.DS.'lookbook'.DS.$image;
            			if(file_exists($image_path))
            				unlink($image_path);
            		}
            	}
            	lookbook::condition($arr_where)->delete();
            }
            return Redirect::to('/admin/static-content/lookbook');
        }else return Redirect::to('/admin/error');
	}

    public function viewLookbook($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneLookbookView($id);
        }else return Redirect::to('/admin/error');
    }

	public function saveLookbook(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_category`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_lookbook')?Input::get('action_lookbook'):'';
		if($v_action=='new'){
			$lookbook = new lookbook;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$lookbook = lookbook::condition($arr_where)->first();
			if(!$lookbook){
				$lookbook = new lookbook;
				$v_action = 'new';
			}
		}
		$v = lookbook::validate(Input::all());
		$arr_columns['name'] = Input::has('name')?Input::get('name'):'';
		$arr_columns['short_name'] = Input::has('short_name')?Input::get('short_name'):'';
        $arr_columns['main_image'] = Input::has('main_image')?Input::get('main_image'):''; // image name
        $arr_columns['other_image']= '';
        if(Input::has('other_image')){
        	$other_image = trim(Input::get('other_image'));
        	$other_image = explode('@_@', $other_image);
        	$arr_images = array();
        	foreach($other_image as $image){
        		if($image == '') continue;
        		$arr_images[] =  $image;
        	}
        	$arr_columns['other_image'] = serialize($arr_images);
        }
        $arr_columns['publish'] = Input::has('publish')?Input::get('publish'):0;
        $arr_columns['main_image'] = Input::get('main_image');
        $arr_columns['publish']  = Input::has('publish')?1:0;
		$v_passes = $v->passes();
		if($v_passes){
			$lookbook->name = $arr_columns['name'];
			$lookbook->short_name = $arr_columns['short_name'];
	        $lookbook->main_image = $arr_columns['main_image'];
	        $lookbook->other_image = $arr_columns['other_image'];
	        $lookbook->publish = $arr_columns['publish'];
		}else{
			$arr_post = Input::all();
			foreach($arr_post as $field=>$value){
				if($field == 'id') continue;
				$v_field_message = $v->messages()->first($field);
				if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			}
		}
		$v_session = 'ss_save_lookbook_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			$arr_user = json_decode(Session::get('ss_admin'));
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
				    $lookbook->save();
				    $insertId = $lookbook->id;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $lookbook->condition($arr_where)->update($arr_columns);
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/lookbook');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
                $arr_columns['image_link'] = $arr_columns['main_image'];
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/lookbook/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns = array_merge(self::getOneLookbookView($v_id,true,true),$arr_columns);
			$arr_columns['id'] = $v_id;
            $arr_columns['image_link'] = $arr_columns['main_image'];
			return Redirect::to('/admin/static-content/lookbook/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public static function getPrevLookbook($lookbook_id)
	{
		return lookbook::select('id','short_name','name','main_image')
							->where('id','<',$lookbook_id)
							->where('publish','=',1)
							->get()
							->first();
	}

	public static function getNextLookbook($lookbook_id)
	{
		return lookbook::select('id','short_name','name','main_image')
							->where('id','>',$lookbook_id)
							->where('publish','=',1)
							->get()
							->first();
	}

}