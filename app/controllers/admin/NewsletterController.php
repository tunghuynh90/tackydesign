<?php
class NewsletterController extends AdminController{
	static $v_module_group_short_name = 'customers';
	static $v_module_short_name = 'newsletter';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$config = Newsletter::condition($arr_where)->sort($arr_order)->first();
		return $config;
	}

	/**
	 * View Config
	 * @param int $id
	 * @return array
	 */
	public function getViewNewsletter($id = 0){
		if(isset(self::$arr_permit[_R_VIEW])){
			return self::getOneNewsletter($id);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Update Config
	 * @param int $id
	 * @return array
	 */
	public function getEditNewsletter($id = 0){
		if(Input::has('submit'))
			return self::saveNewsletter();
		if(isset(self::$arr_permit[_R_UPDATE])){
			return self::getOneNewsletter($id, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Insert Config
	 * @param int $id
	 * @return array
	 */
	public function getAddNewsletter($id = 0){
		if(isset(self::$arr_permit[_R_INSERT])){
			return self::getOneNewsletter(0, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	private static function getOneNewsletter($id = 0, $p_edit = false){
        if($id==0) $id = 1;
		$v_title = 'Newletter - ';
		$arr_icons = array();
		$v_session = 'ss_save_newsletter_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Save changes';
		}else{
			$arr_columns = array();
			$config = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($config){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($config->id)?$config->id:0;
				$arr_columns['content'] = isset($config->content)?$config->content:'';
				$arr_columns['subject'] = isset($config->subject)?$config->subject:'';
				$arr_columns['interval_check'] = isset($config->interval_check)?$config->interval_check:1;
				$arr_columns['min_product'] = isset($config->min_product)?$config->min_product:1;

			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['content'] = '';
				$arr_columns['subject'] = '';
				$arr_columns['interval_check'] = 1;
				$arr_columns['min_product'] = 1;
			}
		}
		return View::make('admin.newsletter_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit,
			)
		);
	}

	private static function detectChangeNewsletter(Newsletter $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = Newsletter::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}


	public function getDeleteNewsletter($config){
		if(isset(self::$arr_permit[_R_DELETE])){			$arr_where = array();
			$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$config);
			$config = Newsletter::condition($arr_where)->get();
			if($config) Newsletter::condition($arr_where)->delete();
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name);
		}else return Redirect::to('/admin/error');
	}
	/**
	 * Insert or Update
	 */
	public function saveNewsletter(){
		if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_config`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_newsletter')?Input::get('action_newsletter'):'';
		if($v_action=='new'){
			$config = new Newsletter;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$config = Newsletter::condition($arr_where)->first();
			$v_allow_edit = !is_null($config);
			if(!$v_allow_edit){
				$config = new Newsletter;
				$v_action = 'new';
			}
		}
        $admin = AdminController::getAdmin();
		$v = Newsletter::validate(Input::all());
		$arr_columns['content'] = htmlentities(Input::has('content')?Input::get('content'):'');
		$arr_columns['subject'] = Input::has('subject')?Input::get('subject'):'';
		$arr_columns['interval_check'] = Input::has('interval_check')?(int)Input::get('interval_check'):1;
		$arr_columns['min_product'] = Input::has('min_product')?(int)Input::get('min_product'):1;
		$v_passes = $v->passes();
		if($v_passes){
			$config->subject = $arr_columns['subject'];
			$config->content = $arr_columns['content'];
		}else{
			$v_field_message = $v->messages()->first('subject');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('content');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('interval_check');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('min_product');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_newsletter_'.$v_id.'_';
		$v_result = false;
		if($v_passes  && $v_message==''){
			if($v_action=='new'){
				if(isset(self::$arr_permit[_R_INSERT])){
					$config->save();
					$insertId = $config->id;
					$v_result = $insertId > 0;
					$v_id = $insertId;
				}
			}else{
				if(isset(self::$arr_permit[_R_UPDATE])){
					if(self::detectChangeNewsletter($config)>0){
						if($v_allow_edit){
							$arr_update = $arr_columns;
							$affectRow = $config->condition($arr_where)->update($arr_update);
							$v_result = $affectRow>0;
						}else{
							$v_message .= '<li>Current record not found!</li>';
						}
					}else{
						$v_result = true;
					}
				}
			}
			if($v_result){
				//if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				//if(Session::has($v_session.'message')) Session::forget($v_session.'message');
                $v_message = '<li>Update successful!</li>';
                $arr_columns['id'] = $v_id;

				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
                    array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
                );
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;

				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name)->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function sendNewsletter()
	{
		$arr_post = Input::all();
		$data = array(
		              'content'=>$arr_post['event_content'],
		              );
		if($arr_post['event_send_to'] == 'one_user'){
			if(!isset($arr_post['event_email']) || !filter_var($arr_post['event_email'], FILTER_VALIDATE_EMAIL) || !checkdnsrr(substr($arr_post['event_email'], strpos($arr_post['event_email'], '@') + 1)))
				return "Email not valid!";
			Mail::queue('emails.newletter',  $data, function($message) use($arr_post)
	        {
	            $message->to($arr_post['event_email'], '')->subject($arr_post['event_subject']);
	        });
	        return 'ok';
		}
		$users = UserMember::select('email')
							->where('subscribe','=',1)
							->get();
		foreach($users as $user){
			if(!filter_var($user->email, FILTER_VALIDATE_EMAIL)) continue;
			$domain = substr($user->email, strpos($user->email, '@') + 1);
			if(!checkdnsrr($domain)) continue;
			Mail::queue('emails.newletter',  $data, function($message) use($user,$arr_post)
	        {
	            $message->to($user->email, $user->firstname.' '.$user->lastname)->subject($arr_post['event_subject']);
	        });

		}
	    return 'ok';
	}

	public static function renderProductTemplate($products)
	{
		$url = Request::root();
		if(strpos($url, 'localhost') !== false)
			$url = 'http://tackydesign.com';
		View::share('url',$url);
		return View::make('admin.render_product_template',array('products'=>$products))->render();
	}

}