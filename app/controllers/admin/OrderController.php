<?php
class OrderController extends AdminController{
	static $v_module_group_short_name = 'customers';
	static $v_module_short_name = 'orders';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public function addOrder()
	{
		return self::getOneOrderView(0,true);
	}

	public static function index()
	{
		return self::getAllOrderView();
	}

	private static function getOneOrderView($id = 0, $p_edit = false, $returnColums = false){
		$v_title = 'Orders - ';
		$arr_icons = array();
		$arr_icons['view'] = 'customers/orders';
		$v_session = 'ss_save_order_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
			$arr_template = array();
		}else{
			$arr_columns = array();
			$order = '';
			$arr_columns['status_list'] = array(
			                                     array('value'=>0,'name'=>'Not confirm yet'),
			                                     array('value'=>1,'name'=>'In Progress'),
			                                     array('value'=>2,'name'=>'Submited'),
			                                     array('value'=>3,'name'=>'Completed'),
			                                     array('value'=>4,'name'=>'Cancel'),
			                                     );
			$arr_columns['payment_status_list'] = array(
			                                     array('value'=>0,'name'=>'Pending'),
			                                     array('value'=>1,'name'=>'Paid'),
			                                     );
			$order = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($order){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($order->id)?$order->id:0;
				$arr_columns['email'] = isset($order->email)?$order->email:'';
				$arr_columns['user_id'] = isset($order->user_id)?$order->user_id:'';
				$arr_columns['user'] = 'Guest';
				if($arr_columns['user_id']){
					$user = UserMember::select('firstname','lastname')
								->where('id','=',$arr_columns['user_id'])
								->get()
								->first();
					$arr_columns['user'] = (isset($user['firstname']) ? $user['firstname'].' ' : '').(isset($user['lastname']) ? $user['lastname'] : '');
				}
				$arr_columns['billing_address'] = isset($order->billing_address)?$order->billing_address:'';
				$arr_columns['shipping_address'] = isset($order->shipping_address)?$order->shipping_address:'';
				$arr_columns['order_status'] = isset($order->order_status)?$order->order_status:0;
				$arr_columns['payment_status'] = isset($order->payment_status)?$order->payment_status:0;
                $arr_columns['total'] = $order->total ? $order->total:0;
                $arr_columns['voucher'] = isset($order->voucher)?$order->voucher:'';
                $arr_columns['discount'] = 0;
                if($arr_columns['voucher'])
                	$arr_columns['discount'] = Voucher::where('key','=',$arr_columns['voucher'])->pluck('value');
                $arr_columns['shipping_price'] = isset($order->shipping_price)?$order->shipping_price:'';
                $arr_columns['note'] = isset($order->note)?$order->note:'';
                $arr_columns['created_date'] = isset($order->created_date)?date('m/d/Y',strtotime($order->created_date)):'';
                $product_details = self::getProductInfo($arr_columns['id'] );
			}
		}
		if($returnColums)
			return $arr_columns;
		return View::make('admin.order_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit,'product_details'=>$product_details
			)
		);
	}

	public static function getProductInfo($order_id)
	{
		$order_details = OrderDetail::where('order_id','=',$order_id)
					->orderBy('id','asc')
					->get();
		$arr_details = array();
		foreach($order_details as $detail){
			$arr_details[] = array(
			                       'id'=>$detail->product_id,
			                       'name'=> Product::where('id','=',$detail->product_id)->pluck('name'),
			                       'image'=>$detail->image,
			                       'quantity'=>$detail->quantity,
			                       'sell_price'=>$detail->sell_price,
			                       'total'=>$detail->sell_price*$detail->quantity,
			                       'size' => $detail->size,
			                       );
		}
		return $arr_details;
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$order = Order::condition($arr_where)->sort($arr_order)->first();
		return $order;
	}

	public static function getAllOrderView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Orders - View All';
		$arr_icons = array();
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.order_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array(), $getPrice){
		$arr_columns = array();
        $v_size_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_size_field==0)
		    $order = Order::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $order = Order::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($order){
			$i=0;
			$v_row = $offset;
			$status_list = array('Not confirm yet',
			                     'In Progress',
			                     'Submited',
			                     'Completed',
			                     'Cancel');
			$payment_status_list = array(
			                            'Pending',
			                            'Paid',
			                        );
			foreach($order as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['email'] = isset($one->email)?$one->email:'';
                    $arr_columns[$i]['order_status'] = isset($one->order_status)?$status_list[$one->order_status]:'';
                    $arr_columns[$i]['payment_status'] = isset($one->payment_status)?$payment_status_list[$one->payment_status]:'';
                    $arr_columns[$i]['total'] = isset($one->total)?number_format($one->total,2):'0';
                    $arr_columns[$i]['created_date'] = isset($one->created_date)?$one->created_date:'0';
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public static function getPageOrder($arr_option = array()){
		$getPrice = false;
		if(empty($arr_option)){
			$v_quick_search = Input::has('quick')?Input::get('quick'):'';
			//Create for where clause
			$arr_where = array();
			if($v_quick_search != ''){
				//Please replace 'field_search' by 'field' you want
				$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
			}
			//Create for order by
			$arr_order = array();
			$arr_tmp = Input::has('sort')?Input::get('sort'):array();
			if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
				for($i=0; $i<sizeof($arr_tmp); $i++){
					$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
				}
			}
			if(empty($arr_order))
				$arr_order[] = array('field'=>'id','asc'=>false);
			//Create for page limit
			$v_page = Input::has('page')?Input::get('page'):1;
			$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
			settype($v_page, 'int');
			settype($v_page_size, 'int');
		} else {
			$arr_where 	= isset($arr_option['arr_where']) ? $arr_option['arr_where'] : array();
			$arr_order 	= isset($arr_option['arr_order']) ? $arr_option['arr_order'] : array();
			$v_page 	= isset($arr_option['v_page']) 	? $arr_option['v_page'] : 1;
			$v_page_size 	= isset($arr_option['v_page_size']) 	? $arr_option['v_page_size'] : 10;
			$getPrice 	= isset($arr_option['getPrice']) 	? true : false;
		}
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Order::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order, array(), $getPrice);
		$arr_return = array('total_rows'=>$v_total_rows,'total_pages'=>$v_total_pages, 'order'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public static function updateStatus(){
        if(Input::has('order_id')){
        	$order_id = Order::where('id','=',Input::get('order_id'))->pluck('id');
        	if($order_id){
        		$arr_update = array();
        		if(Input::has('order_status'))
        			$arr_update['order_status'] = Input::get('order_status');
        		if(Input::has('payment_status'))
        			$arr_update['payment_status'] = Input::get('payment_status');
        		if(!empty($arr_update))
        			Order::where('id','=',$order_id)->update($arr_update);
        	}
        }
    }

    private static function updateOrder(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = Order::condition($arr_where)->update($arr_columns);
            else
                $v_rows = Order::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editOrder($id=0){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOneOrderView($id, true);
        return Redirect::to('/admin/error');
    }

    public function deleteOrder($order){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$order);
           	$arr_where_detail[] = array('field' =>'order_id', 'operator'=>'=', 'value'=>$order);
            $order_detail = OrderDetail::condition($arr_where_detail)->get()->first();
            if($order_detail){
            	OrderDetail::condition($arr_where_detail)->delete();
            }
            $order = Order::condition($arr_where)->get()->first();
            if($order){
            	Order::condition($arr_where)->delete();
            }
            return Redirect::to('/admin/customers/orders');
        }else return Redirect::to('/admin/error');
	}

    public function viewOrder($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneOrderView($id);
        }else return Redirect::to('/admin/error');
    }

	public function saveOrder(){

	}

	public static function createOrder($arr_info)
	{
		$arr_save = array();
		$arr_save['email'] = (isset($arr_info['email']) ? $arr_info['email'] : '');
		$user = Confide::user();
		$arr_save['user_id'] = 0;
		if($user)
			$arr_save['user_id'] = $user->id;
		$shipping_address = $billing_address = $arr_info['billing_address'];
		if(isset($arr_info['shipping_address']))
			$shipping_address= $arr_info['shipping_address'];
		$arr_save['billing_address'] = serialize($billing_address);
		$arr_save['shipping_address'] = serialize($shipping_address);
		$arr_save['order_status'] = 2; //Order Status: Summited
		if($user)
			$arr_save['email'] = $user->email;
		$arr_save['token'] = getToken();
		$arr_save['created_date'] = new \DateTime;
		$cart = Cart::content();
		$discount = 0;
		$arr_items = array();
		foreach($cart as $cart_item){
			if($cart_item->id == 'promo_code') {
				$arr_save['voucher'] = $cart_item->options->promo_code;
				$discount = $cart_item->options->discount;
				continue;
			}
            if($cart_item->id == 'address') continue;
            $options = (array)$cart_item->options;
            $arr_items[] = array(
                                    'product_id'=>$cart_item->options->product_id,
                                    'image'=>str_replace(' ', '%20', $cart_item->options->image),
                                    'sell_price'=>$cart_item->price,
                                    'quantity'=>(int)$cart_item->qty,
                                    'size'=>$cart_item->options->size,
                                    'url'	=>$cart_item->options->url,
                                    'name'	=>$cart_item->name,
                                    'options'	=> reset($options),
                                );
		}
		$discount = (float)str_replace('%','',$discount);
		$city_id = $arr_info['shipping_address']['province'];
		$arr_save['shipping_price'] = ShipPrice::where('city_id','=',$city_id)->pluck('unit_price');
		$arr_save['sub_total'] = $arr_save['total'] = Cart::total();
		$arr_save['discount'] = $arr_save['total']*$discount/100;
		$arr_save['total'] -= $arr_save['discount'];
		$arr_save['total'] += $arr_save['shipping_price'];
		//Save Order
		$order_id = Order::insertGetId($arr_save);
		$arr_save['discount_percent'] = $discount;
		$arr_save['order_id'] = $order_id;
		foreach($arr_items as $item){
			$item = array_merge($item,array('order_id'=>$order_id));
			$arr_detail = $item;
			unset($arr_detail['url']);
			unset($arr_detail['name']);
			unset($arr_detail['options']);
			OrderDetail::insert($arr_detail);
		}
		if(isset($arr_save['voucher'])){
			voucher::where('key',$arr_save['voucher'])->update(array('used'=>1));
		}
		self::sendSubmmitedOrderEmail($arr_save,$arr_items);
	}

	public static function sendConfirmOrderEmail($token,$email,$products)
	{
		$config = EmailTemplate::where('type','=','order_summited')
									->get()
									->first();
		if(is_null($config))
			return false;
		$data = array(
		              'content' => $config['content'],
		              'confirm_link'	=> Request::root().'/confirm-order/'.$token,
		              'products'	=> $products
		              );
		Mail::queue('emails.order_confirm', $data, function($message) use($email,$config)
        {
            $message->to($email, 'Guest')->subject($config['subject']);
        });
	}

	public static function sendSubmmitedOrderEmail($arr_data,$products)
	{
		Config::set('mail.username', 'orders@tackydesign.com');
		Config::set('mail.from.address', 'orders@tackydesign.com');
		Config::set('mail.from.name', 'Tacky Design - Orders');
		$config = EmailTemplate::where('type','=','order_summited')
									->get()
									->first();
		if(is_null($config))
			return false;
		$config = $config->toArray();
		if(!isset($arr_data['customer_name']))
			$arr_data['customer_name'] = 'Guest';
		foreach(array('billing_address','shipping_address') as $address_key)
			if(isset($arr_data[$address_key]) && $arr_data[$address_key]){
				$tmp_address = unserialize($arr_data[$address_key]);
				if(count($tmp_address) == 1){
					$arr_data[$address_key] = $arr_data['customer_name'].'<br />'.$tmp_address[0];
					continue;
				}
				$customer_name = (isset($tmp_address['first_name']) ? $tmp_address['first_name'].' ' : '').(isset($tmp_address['last_name']) ? $tmp_address['last_name'].' ' : '');
				if($address_key == 'billing_address')
					$arr_data['customer_name'] = $customer_name;
				$address_1 = (isset($tmp_address['address_1']) ? $tmp_address['address_1'] : '');
				$address_2 = (isset($tmp_address['address_2']) ? $tmp_address['address_2'] : '');
				$city = (isset($tmp_address['city']) ? $tmp_address['city'] : '');
				$province = $tmp_address['province'];
				if(isset($tmp_address['province']) && is_numeric($tmp_address['province']))
					$province = City::where('id','=',$tmp_address['province'])->pluck('name');
				$zip = (isset($tmp_address['zip']) ? $tmp_address['zip'] : '');
				$country = $tmp_address['country'];
				if(isset($tmp_address['country']) && is_numeric($tmp_address['country']))
					$country = Country::where('id','=',$tmp_address['country'])->pluck('name');
				$arr_data[$address_key] = $customer_name.'<br />';
				$arr_data[$address_key] .= $address_1.'<br />'.$address_2.'<br />';
				$arr_data[$address_key] .= $province.', '.$city.'<br />';
				$arr_data[$address_key] .= $zip.'<br />';
				$arr_data[$address_key] .= $country.'<br />';
			}
		$arr_data['order_number'] = str_pad($arr_data['order_id'],6,0,STR_PAD_LEFT);
		$arr_data['order_date'] = date('d M Y');
		if(!$arr_data['discount_percent'])
			$arr_data['discount_percent'] = '';
		foreach(array('sub_total','total','discount','shipping_price') as $price_key)
			$arr_data[$price_key] = number_format($arr_data[$price_key],2);
		$arr_data['product_content'] = $products;
		$data = array(
		              'config' => $config,
		              'arr_data' => $arr_data,
		              );
		Mail::queue('emails.order_submitted', $data, function($message) use($arr_data,$config)
        {
        	if(strpos($config['subject'], '[ORDER_NUMBER]')!==false)
        		$config['subject'] = str_replace('[ORDER_NUMBER]', $arr_data['order_number'], $config['subject']);
            $message->to($arr_data['email'], $arr_data['customer_name'])->subject($config['subject']);
        });
	}

	public static function createPayPalOrder($arr_data)
	{
		$arr_save = array();
		$arr_save['email'] = $arr_data['EMAIL'];
		$arr_save['billing_address'] = array('Pay via Paypal');
		$arr_save['shipping_address'] = array(
		                                      'first_name'=>$arr_data['FIRSTNAME'],
		                                      'last_name'=>$arr_data['LASTNAME'],
		                                      'company'=> '',
		                                      'address_1'=> isset($arr_data['SHIPTOSTREET']) ? $arr_data['SHIPTOSTREET'] : '',
		                                      'address_2'=> '',
		                                      'city'=> isset($arr_data['SHIPTOCITY']) ? $arr_data['SHIPTOCITY'] : '',
		                                      'province'=> isset($arr_data['SHIPTOSTATE']) ? $arr_data['SHIPTOSTATE'] : '',
		                                      'zip'=> isset($arr_data['SHIPTOZIP']) ? $arr_data['SHIPTOZIP'] : '',
		                                      'country'=> isset($arr_data['SHIPTOCOUNTRYNAME']) ? $arr_data['SHIPTOCOUNTRYNAME'] : '',
		                                      );
		$arr_save['billing_address'] = serialize($arr_save['billing_address']);
		$arr_save['shipping_address'] = serialize($arr_save['shipping_address']);
		$arr_save['order_status'] = 2;
		$arr_save['payment_status'] = 1;
		$cart = Cart::content();
		$discount = 0;
		$arr_items = array();
		foreach($cart as $cart_item){
			if($cart_item->id == 'promo_code') {
				$arr_save['voucher'] = $cart_item->options->promo_code;
				$discount = $cart_item->options->discount;
				continue;
			}
            if($cart_item->id == 'shipping_method') continue;
            $options = (array)$cart_item->options;
            $arr_items[] = array(
                                    'product_id'=>$cart_item->options->product_id,
                                    'image'=>str_replace(' ', '%20', $cart_item->options->image),
                                    'sell_price'=>$cart_item->price,
                                    'quantity'=>(int)$cart_item->qty,
                                    'size'=>$cart_item->options->size,
                                    'url'	=>$cart_item->options->url,
                                    'name'	=>$cart_item->name,
                                    'options'	=> reset($options),
                                );
		}
		$discount = (float)str_replace('%','',$discount);
		$arr_save['shipping_price'] = CartController::getShippingPrice();
		$arr_save['sub_total'] = Cart::total();
		$arr_save['discount'] = $discount * $arr_save['sub_total'] / 100;
		$arr_save['total'] = $arr_data['AMT'];
		// $arr_save['token'] = $arr_data['TOKEN'];
		$arr_save['created_date'] = new \DateTime;
		$arr_save['note'] = "Pay via Paypal\nTransactionID: {$arr_data['TRANSACTIONID']}\n";
		$arr_save['note'] .= isset($arr_data['PAYMENTREQUEST_0_NOTETEXT']) ? 'Customer note: '.$arr_data['PAYMENTREQUEST_0_NOTETEXT'] : '';
		//Save Order
		$order_id = Order::insertGetId($arr_save);
		$arr_save['discount_percent'] = $discount;
		$arr_save['order_id'] = $order_id;
		$arr_save['card_number'] = 'Pay via Paypal';
		$arr_save['card_type'] = 'Paypal';
		$arr_save['customer_name'] = $arr_data['FIRSTNAME'].' '.$arr_data['LASTNAME'];
		foreach($arr_items as $item){
			$item = array_merge($item,array('order_id'=>$order_id));
			$arr_detail = $item;
			unset($arr_detail['url']);
			unset($arr_detail['name']);
			unset($arr_detail['options']);
			OrderDetail::insert($arr_detail);
		}
		if(isset($arr_save['voucher']))
			Voucher::where('key',$arr_save['voucher'])->update(array('used'=>1));
		self::sendSubmmitedOrderEmail($arr_save,$arr_items);
	}

	public static function createCreditCardPayPalOrder($arr_data)
	{
		$arr_save = array();
		$arr_save['email'] = $arr_data['EMAIL'];
		$arr_save['billing_address'] = $arr_data['billing_address'];
		$arr_save['shipping_address'] = $arr_data['shipping_address'];
		$arr_save['billing_address'] = serialize($arr_save['billing_address']);
		$arr_save['shipping_address'] = serialize($arr_save['shipping_address']);
		$arr_save['order_status'] = 2;
		$arr_save['payment_status'] = 1;
		$cart = Cart::content();
		$discount = 0;
		$arr_items = array();
		foreach($cart as $cart_item){
			if($cart_item->id == 'promo_code') {
				$arr_save['voucher'] = $cart_item->options->promo_code;
				$discount = $cart_item->options->discount;
				continue;
			}
            if($cart_item->id == 'address') continue;
            $options = (array)$cart_item->options;
            $arr_items[] = array(
                                    'product_id'=>$cart_item->options->product_id,
                                    'image'=>str_replace(' ', '%20', $cart_item->options->image),
                                    'sell_price'=>$cart_item->price,
                                    'quantity'=>(int)$cart_item->qty,
                                    'size'=>$cart_item->options->size,
                                    'url'	=>$cart_item->options->url,
                                    'name'	=>$cart_item->name,
                                    'options'	=> reset($options),
                                );
		}
		$discount = (float)str_replace('%','',$discount);
		$arr_save['shipping_price'] = $arr_data['shipping_price'];
		$arr_save['sub_total'] = Cart::total();
		$arr_save['discount'] = $discount * $arr_save['sub_total'] / 100;
		$arr_save['total'] = $arr_data['AMT'];
		$arr_save['token'] = $arr_data['TRANSACTIONID'];
		$arr_save['created_date'] = new \DateTime;
		$arr_save['note'] = "Pay via Paypal\nTransactionID: {$arr_data['TRANSACTIONID']}\n";
		$arr_save['note'] .= "Card type: ".$arr_data['CREDITCARDTYPE']."\nCard number: {$arr_data['ACCT']}\n";
		$arr_save['note'] .= isset($arr_data['PAYMENTREQUEST_0_NOTETEXT']) ? 'Customer note: '.$arr_data['PAYMENTREQUEST_0_NOTETEXT'] : '';
		//Save Order
		$order_id = Order::insertGetId($arr_save);
		$arr_save['discount_percent'] = $discount;
		$arr_save['order_id'] = $order_id;
		$card_number = $arr_data['ACCT'];
		$card_number = str_pad(substr($arr_data['ACCT'], -3),strlen($arr_data['ACCT']),'*',STR_PAD_LEFT);
		$arr_save['card_number'] = $card_number;
		$arr_save['card_type'] = $arr_data['CREDITCARDTYPE'];
		$arr_save['customer_name'] = $arr_data['FIRSTNAME'].' '.$arr_data['LASTNAME'];
		foreach($arr_items as $item){
			$item = array_merge($item,array('order_id'=>$order_id));
			$arr_detail = $item;
			unset($arr_detail['url']);
			unset($arr_detail['name']);
			unset($arr_detail['options']);
			OrderDetail::insert($arr_detail);
		}
		if(isset($arr_save['voucher']))
			voucher::where('key',$arr_save['voucher'])->update(array('used'=>1));
		self::sendSubmmitedOrderEmail($arr_save,$arr_items);
	}
}