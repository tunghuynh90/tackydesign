<?php
class ProductCategoryController extends AdminController{
	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'categories';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public function addCategory()
	{
		return self::getOneCategoryView(0,true);
	}

	public static function index()
	{
		return self::getAllCategoryView();
	}

	private static function getOneCategoryView($id = 0, $p_edit = false){
		$v_title = 'Product Categories - ';
		$arr_icons = array();
		$arr_icons['view'] = 'products/categories';
		$v_session = 'ss_save_category_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$category = '';
			if($p_edit)
				$arr_columns['category_list'] = json_encode(self::getAllCategory());
			else
				$arr_columns['category_list'] = json_encode(array());
			if($id)
				$category = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($category){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($category->id)?$category->id:0;
				$arr_columns['name'] = isset($category->name)?$category->name:'';
				$arr_columns['short_name'] = isset($category->short_name)?$category->short_name:'';
                $arr_columns['image_link'] = $category->image ? ('/assets/upload/'.$category->image):'';
				$arr_columns['image'] = $category->image;
				$arr_columns['publish'] = $category->publish;
				$arr_columns['on_shop'] = $category->on_shop;
				$arr_columns['on_collection'] = $category->on_collection;
				$arr_columns['description'] = isset($category->description)?$category->description:'';
				$arr_columns['parent_category'] = $category->parent_id;
				$arr_columns['order_no'] = $category->order_no;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['image'] = '';
                $arr_columns['image_link'] = Request::root()."/assets/images/no_image.jpg";
				$arr_columns['description'] = '';
				$arr_columns['publish'] = 1;
				$arr_columns['on_shop'] = 0;
				$arr_columns['on_collection'] = 0;
				$arr_columns['parent_category'] = 0;
				$arr_columns['order_no'] = 0;
			}
		}
		return View::make('admin.product_one_category')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$category = ProductCategory::condition($arr_where)->sort($arr_order)->first();
		return $category;
	}

	public static function getAllCategoryView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Product Categories - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'products/categories';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.product_all_category')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
        $v_size_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_size_field==0)
		    $category = ProductCategory::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $category = ProductCategory::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($category){
			$i=0;
			$v_row = $offset;
			foreach($category as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['image'] = $one->image != ''?(Request::root().'/assets/upload/'.$one->image):Request::root().'/assets/images/no_image.jpg';
                    $arr_columns[$i]['publish'] = $one->publish==1?'icon-unhide.png':'icon-hide.png';
                    $arr_columns[$i]['on_shop'] = $one->on_shop==1?'icon-unhide.png':'icon-hide.png';
                    $arr_columns[$i]['on_collection'] = $one->on_collection==1?'icon-unhide.png':'icon-hide.png';
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getPageCategory(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = ProductCategory::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'category'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function updateStatus(){
        $id = 	Input::has('txt_id')  ?  (int)Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  (int) Input::get('txt_value') : 0;
        $field = Input::has('txt_name')  ?  Input::get('txt_name') : 'publish';
        $arr_return = array('error'=>1);
        if($id!=0){
            if(isset(self::$arr_permit[_R_UPDATE])){
                $rows = self::updateCategory(array($field=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
                if($rows) $arr_return['error'] = 0;
            }
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateCategory(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = ProductCategory::condition($arr_where)->update($arr_columns);
            else
                $v_rows = ProductCategory::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editCategory($id=0){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOneCategoryView($id, true);
        return Redirect::to('/admin/error');
    }

    public function deleteCategory($category){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$category);
            $category = ProductCategory::condition($arr_where)->get();
            if($category) ProductCategory::condition($arr_where)->delete();
            return Redirect::to('/admin/products/categories');
        }else return Redirect::to('/admin/error');
	}

    public function viewCategory($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneCategoryView($id);
        }else return Redirect::to('/admin/error');
    }

	public function saveCategory(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_category`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_category')?Input::get('action_category'):'';
		if($v_action=='new'){
			$category = new ProductCategory;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$category = ProductCategory::condition($arr_where)->first();
			if(!$category){
				$category = new ProductCategory;
				$v_action = 'new';
			}
		}
		$v = ProductCategory::validate(Input::all());
		$arr_columns['name'] = Input::has('name')?Input::get('name'):'';
		$arr_columns['short_name'] = Input::has('short_name')?Input::get('short_name'):'';
       	$arr_columns['image'] = Input::get('image');
		$arr_columns['description'] = Input::has('description')?Input::get('description'):'';
        $arr_columns['publish']  = Input::has('publish')?1:0;
        $arr_columns['on_collection']  = Input::has('on_collection')?1:0;
        $arr_columns['on_shop']  = Input::has('on_shop')?1:0;
        $arr_columns['parent_id']  = Input::has('parent_category')?Input::get('parent_category'):0;
        $arr_columns['order_no']  = Input::has('order_no')?Input::get('order_no'):0;
		$v_passes = $v->passes();
		if($v_passes){
			$category->name = $arr_columns['name'];
			$category->short_name = $arr_columns['short_name'];
			$category->image = $arr_columns['image'];
			$category->description = $arr_columns['description'];
			$category->parent_id = $arr_columns['parent_id'];
			$category->publish = $arr_columns['publish'];
			$category->on_shop = $arr_columns['on_shop'];
			$category->on_collection = $arr_columns['on_collection'];
			$category->order_no = $arr_columns['order_no'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('image');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('description');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('on_shop');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('on_collection');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_category_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			$arr_user = json_decode(Session::get('ss_admin'));
			$category->modified_by = $arr_user->id;
			$arr_columns['modified_by'] = $category->modified_by;
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
                	$category->created_by = $category->modified_by;
				    $category->save();
				    $insertId = $category->id;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $category->condition($arr_where)->update($arr_columns);
				    //$v_result = $affectRow>0;
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				parent::clearCached();
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/products/categories');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
                $arr_columns['image_link'] = $arr_columns['image'];
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/products/categories/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
            $arr_columns['image_link'] = $arr_columns['image'];
			return Redirect::to('/admin/products/categories/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}
	public static function getAllCategory(){
		$arr_categories = array(array('name'=>'Select','value'=>0));
		$categories = ProductCategory::select('id','name')->get();
		if($categories->count())
			foreach($categories as $category){
				$arr_categories[] = array('name'=>$category->name,'value'=>$category->id);
			}
		return $arr_categories;
	}
}