<?php
class ProductController extends AdminController{
	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'products';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    if (Request::segment(1)!='design-online')
		    $this->beforeFilter(function(){
		        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
		    });
	}

	public function addProduct()
	{
		return self::getOneProductView(0,true);
	}

	public static function index()
	{
		return self::getAllProductView();
	}

	private static function getOneProductView($id = 0, $p_edit = false, $returnColums = false){
		$v_title = 'Products - ';
		$arr_icons = array();
		$arr_icons['view'] = 'products/products';
		$v_session = 'ss_save_product_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
			$arr_template = array();
		}else{
			$arr_columns = array();
			$product = '';
			if($p_edit){
				$category_list = ProductCategoryController::getAllCategory();
				unset($category_list[0]);
				$category_list = array_values($category_list);
				$arr_columns['category_list'] = $category_list;
				$arr_columns['property_list'] = ProductPropertyController::getProperties();
			}
			else
				$arr_columns['category_list'] = $arr_columns['property_list'] = array();
			if($id)
				$product = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($product){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($product->id)?$product->id:0;
				$arr_columns['sku'] = isset($product->sku)?$product->sku:'';
				$arr_columns['name'] = isset($product->name)?$product->name:'';
				$arr_columns['short_name'] = isset($product->short_name)?$product->short_name:'';
				$arr_columns['gift_card'] = isset($product->gift_card)?$product->gift_card:0;
				// $arr_columns['bigger_price'] = isset($product->bigger_price)?$product->bigger_price:0;
				// $arr_columns['sell_price'] = isset($product->sell_price)?$product->sell_price:0;
				$arr_columns['is_sale'] = isset($product->is_sale)?$product->is_sale:0;
				$arr_columns['is_sold_out'] = isset($product->is_sold_out)?$product->is_sold_out:0;
				$arr_columns['design_online'] = isset($product->design_online)?$product->design_online:0;
                $arr_columns['main_image'] = $product->main_image ? $product->main_image:''; // image name
                $arr_columns['other_image'] = $arr_columns['hidden_other_image'] = array();
                $arr_columns['template_image'] = isset($products->template_image)?$products->template_image:'';
                if($product->other_image != ''){
                	$other_image = unserialize($product->other_image);
                	foreach ($other_image as $image) {
                		if(!file_exists(public_path().DS.'assets'.DS.'upload'.DS.$image)) continue;
                		$arr_columns['other_image'][] = array(
                		                                      'name' 	=> $image,
                		                                      'size'	=> filesize(public_path().DS.'assets'.DS.'upload'.DS.$image),
                		                                      'link'	=> 'assets/upload/'.$image
                		                                      );
                		$arr_columns['hidden_other_image'][] = $image;
                	}
                }
                $arr_columns['hidden_other_image'] = implode('@_@',$arr_columns['hidden_other_image']);
                $arr_columns['other_image'] = json_encode($arr_columns['other_image']);
                $arr_columns['image_link'] = public_path().'/assets/upload/'.$product->main_image;
                if($product->main_image != '' && file_exists($arr_columns['image_link'])){
                    $arr_columns['image_link'] = Request::root()."/assets/upload/". $product->main_image;
                }
                else{
                    $arr_columns['image_link'] = '';
                }
                $property_list = $arr_columns['property_list'];
                foreach(array('color','material') as $property){
                	$arr_columns[$property] = array();
                	if($product->$property){
                	$arr_data = explode(',', $product->$property);
	                	foreach($arr_data as $data_id){
	                		if(isset($property_list[$property]))
		                		foreach($property_list[$property] as $key=>$value){
		                			if($value['value'] == $data_id){
		                				$arr_columns[$property][] = $value;
		                				unset($property_list[$property][$key]);
		                			}
		                		}
	                	}
	                }
                }
                $arr_columns['category_id'] = array();
                if($product->category_id){
                	$category_list = $arr_columns['category_list'];
                	$arr_category_id = explode(',', $product->category_id);
                	foreach($arr_category_id as $category_id){
                		foreach($category_list as $key=>$value){
                			if($value['value'] == $category_id){
                				$arr_columns['category_id'][] = $value;
                				unset($category_list['category_id'][$key]);
                			}
                		}
                	}
	            }
				$arr_columns['description'] = isset($product->description)?$product->description:'';
                $arr_columns['publish'] = isset($product->publish)?$product->publish:0;
                $templates = ProductTemplate::where('product_id', $id)->where('publish','=', 1)->get();
                $arr_template = array();
	            foreach($templates as $t){
	                $arr_template[] = $t->template_id;
	            }
			}else{
				$arr_template = array();
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['sku'] = '';
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['gift_card'] = 0;
				// $arr_columns['bigger_price'] = 0;
				// $arr_columns['sell_price'] = 0;
				$arr_columns['is_sale'] = 0;
				$arr_columns['is_sold_out'] = 0;
				$arr_columns['design_online'] = 0;
                $arr_columns['main_image'] = ''; // image name
                $arr_columns['other_image'] = json_encode(array());
                $arr_columns['template_image'] = '';
                $arr_columns['image_link'] = '';
                $arr_columns['hidden_other_image'] = '';
				$arr_columns['color'] = '';
				$arr_columns['material'] = '';
				$arr_columns['category_id'] = array();
				$arr_columns['description'] = '';
                $arr_columns['publish'] = 1;
			}
		}
		if($returnColums)
			return $arr_columns;
		return View::make('admin.product_one_product')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit,'template'=>$arr_template,
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$product = Product::condition($arr_where)->sort($arr_order)->first();
		return $product;
	}

	public static function getAllProductView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Products - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'products/products';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.product_all_product')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array(), $getPrice){
		$arr_columns = array();
        $v_size_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_size_field==0)
		    $product = Product::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $product = Product::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($product){
			$i=0;
			$v_row = $offset;
			foreach($product as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
                    $arr_columns[$i]['category_id'] = isset($one->category_id)?$one->category_id:'';
                    $arr_columns[$i]['sku'] = isset($one->sku)?$one->sku:'';
                    $arr_columns[$i]['is_sale'] = isset($one->is_sale)?$one->is_sale:0;
                    $arr_columns[$i]['is_sold_out'] = isset($one->is_sold_out)?$one->is_sold_out:0;
                    if($getPrice){
                    	$price = self::getMainPriceByID($arr_columns[$i]['id'] );
                    	$arr_columns[$i]['sell_price'] = isset($price['sell_price'])?$price['sell_price']:0;
                   		$arr_columns[$i]['bigger_price'] = isset($price['bigger_price'])?$price['bigger_price']:0;
                    }
                    // $arr_columns[$i]['sell_price'] = isset($one->sell_price)?$one->sell_price:0;
                    // $arr_columns[$i]['bigger_price'] = isset($one->bigger_price)?$one->bigger_price:0;
                    $arr_columns[$i]['main_image'] = $one->main_image != ''?(Request::root().'/assets/upload/'.$one->main_image):Request::root().'/assets/images/no_image.jpg';
                    $arr_columns[$i]['publish'] = $one->publish==1?'icon-unhide.png':'icon-hide.png';
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public static function getPageProduct($isReturnArray = false, $arr_option = array()){
		$getPrice = false;
		if(empty($arr_option)){
			$v_quick_search = Input::has('quick')?Input::get('quick'):'';
			//Create for where clause
			$arr_where = array();
			if($v_quick_search != ''){
				//Please replace 'field_search' by 'field' you want
				$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
			}
			//Create for order by
			$arr_order = array();
			$arr_tmp = Input::has('sort')?Input::get('sort'):array();
			if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
				for($i=0; $i<sizeof($arr_tmp); $i++){
					$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
				}
			}
			if(empty($arr_order))
				$arr_order[] = array('field'=>'id','asc'=>false);
			//Create for page limit
			$v_page = Input::has('page')?Input::get('page'):1;
			$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
			settype($v_page, 'int');
			settype($v_page_size, 'int');
		} else {
			$arr_where 	= isset($arr_option['arr_where']) ? $arr_option['arr_where'] : array();
			$arr_order 	= isset($arr_option['arr_order']) ? $arr_option['arr_order'] : array();
			$v_page 	= isset($arr_option['v_page']) 	? $arr_option['v_page'] : 1;
			$v_page_size 	= isset($arr_option['v_page_size']) 	? $arr_option['v_page_size'] : 10;
			$getPrice 	= isset($arr_option['getPrice']) 	? true : false;
		}
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Product::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order, array(), $getPrice);
		$arr_return = array('total_rows'=>$v_total_rows,'total_pages'=>$v_total_pages, 'product'=>$arr_columns);
		if($isReturnArray)
			return $arr_return;
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function updateStatus(){
        $id = 	Input::has('txt_id')  ?  (int)Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  (int) Input::get('txt_value') : 0;
        $field = Input::has('txt_name')  ?  Input::get('txt_name') : 'publish';
        $arr_return = array('error'=>1);
        if($id!=0){
            if(isset(self::$arr_permit[_R_UPDATE])){
                $rows = self::updateProduct(array($field=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
                if($rows) $arr_return['error'] = 0;
            }
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateProduct(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = Product::condition($arr_where)->update($arr_columns);
            else
                $v_rows = Product::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editProduct($id=0){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOneProductView($id, true);
        return Redirect::to('/admin/error');
    }

    public function deleteProduct($product){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$product);
            $product = Product::condition($arr_where)->get()->first();
            if($product){
            	$other_image = $product->other_image;
            	if($other_image){
            		$other_image = unserialize($other_image);
            		foreach($other_image as $image){
            			$image_path = public_path().DS.'assets'.DS.'upload'.DS.$image;
            			$thumb_image_path = public_path().DS.'assets'.DS.'upload'.DS.'thumb'.DS.$image;
            			if(file_exists($image_path))
            				unlink($image_path);
            			if(file_exists($thumb_image_path))
            				unlink($thumb_image_path);
            		}
            	}
            	ProductOptionPrice::where('product_id','=',$product->id)->delete();
            	Product::condition($arr_where)->delete();
            }
            return Redirect::to('/admin/products/products');
        }else return Redirect::to('/admin/error');
	}

    public function viewProduct($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneProductView($id);
        }else return Redirect::to('/admin/error');
    }

	public function saveProduct(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_category`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_product')?Input::get('action_product'):'';
		if($v_action=='new'){
			$product = new Product;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$product = Product::condition($arr_where)->first();
			if(!$product){
				$product = new Product;
				$v_action = 'new';
			}
		}
		$v = Product::validate(Input::all());
		$arr_columns['sku'] = Input::has('sku')?Input::get('sku'):'';
		$arr_columns['name'] = Input::has('name')?Input::get('name'):'';
		$arr_columns['short_name'] = Input::has('short_name')?Input::get('short_name'):'';
		$arr_columns['gift_card'] = Input::has('gift_card')?1:0;
		// $arr_columns['bigger_price'] = Input::has('bigger_price')?Input::get('bigger_price'):0;
		// $arr_columns['sell_price'] = Input::has('sell_price')?Input::get('sell_price'):0;
		$arr_columns['is_sale'] = Input::has('is_sale')?1:0;
		$arr_columns['is_sold_out'] = Input::has('is_sold_out')?1:0;
        $arr_columns['main_image'] = Input::has('main_image')?Input::get('main_image'):''; // image name
        $arr_columns['template_image'] = Input::has('template_image')?Input::get('template_image'):'';
        foreach(array('color','material','category_id') as $value){
        	$arr_columns[$value] = '';
	        if(Input::has($value) && is_array(Input::get($value)))
	        	$arr_columns[$value] = implode(',', Input::get($value));
        }
        $arr_columns['other_image']= '';
        if(Input::has('other_image')){
        	$other_image = trim(Input::get('other_image'));
        	$other_image = explode('@_@', $other_image);
        	$arr_images = array();
        	foreach($other_image as $image){
        		if($image == '') continue;
        		$arr_images[] =  $image;
        	}
        	$arr_columns['other_image'] = serialize($arr_images);
        }
		$arr_columns['description'] = Input::has('description')?Input::get('description'):'';
        $arr_columns['publish'] = Input::has('publish')?Input::get('publish'):0;
        $arr_columns['main_image'] = Input::get('main_image');
		$arr_columns['description'] = Input::has('description')?Input::get('description'):'';
        $arr_columns['publish']  = Input::has('publish')?1:0;
        $v_template = Input::has('txt_template')?Input::get('txt_template'):'';
        if(!is_array($v_template)){
            if(get_magic_quotes_gpc()) $v_template = stripslashes($v_template);
            $arr_template = json_decode($v_template, true);
            if(!is_array($arr_template)) $arr_template = array();
        }else $arr_template = $v_template;
        for($i=0; $i<sizeof($arr_template);$i++)
            $arr_template[$i] = (int) $arr_template[$i];
		$v_passes = $v->passes();
		$v_old_image = '';
		if($v_passes){
			 if($v_id > 0){
                $v_old_image = self::getScalar('main_image',array(array('field'=>'id', 'value'=>$v_id)));
            }
			$product->sku = $arr_columns['sku'];
			$product->name = $arr_columns['name'];
			$product->short_name = $arr_columns['short_name'];
			$product->bigger_price = $arr_columns['gift_card'];
			// $product->bigger_price = $arr_columns['bigger_price'];
			// $product->sell_price = $arr_columns[''];
			$product->is_sale = $arr_columns['is_sale'];
			$product->is_sold_out = $arr_columns['is_sold_out'];
	        $product->main_image = $arr_columns['main_image'];
	        $product->other_image = $arr_columns['other_image'];
			$product->color = $arr_columns['color'];
			$product->material = $arr_columns['material'];
			$product->description = $arr_columns['description'];
	        $product->category_id = $arr_columns['category_id'];
			$product->template_image = $arr_columns['template_image'];
		}else{
			$arr_post = Input::all();
			foreach($arr_post as $field=>$value){
				if($field == 'id') continue;
				$v_field_message = $v->messages()->first($field);
				if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			}
		}
		$v_session = 'ss_save_product_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			$arr_user = json_decode(Session::get('ss_admin'));
			$product->modified_by = $arr_user->id;
			$arr_columns['modified_by'] = $product->modified_by;
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
                	$product->created_by = $product->modified_by;
				    $product->save();
				    $insertId = $product->id;
				    $v_id = $insertId;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $product->condition($arr_where)->update($arr_columns);
				    //$v_result = $affectRow>0;
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				ProductTemplateController::saveProductTemplate($arr_template, $v_id);
                if($v_old_image!=''){
                    if(file_exists(public_path().'/assets/upload/'.$v_old_image)) @unlink($v_dir.$v_old_image);
                }
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/products/products/'.$v_id.'/edit');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
                $arr_columns['image_link'] = $arr_columns['main_image'];
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/products/products/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns = array_merge(self::getOneProductView($v_id,true,true),$arr_columns);
			$arr_columns['id'] = $v_id;
            $arr_columns['image_link'] = $arr_columns['main_image'];
			return Redirect::to('/admin/products/products/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public static function getPrevProduct($category_id,$product_id)
	{
		return Product::select('id','short_name','name')
							->where('id','<',$product_id)
							->where('category_id','LIKE',"%$category_id%")
							->where('publish','=',1)
							->orderBy('id','desc')
							->get()
							->first();
	}

	public static function getNextProduct($category_id,$product_id)
	{
		return Product::select('id','short_name','name')
							->where('id','>',$product_id)
							->where('category_id','LIKE',"%$category_id%")
							->where('publish','=',1)
							->orderBy('id','asc')
							->get()
							->first();
	}

	public static function getSimilarProducts($category_id,$product_id)
	{
		return Product::select('id','short_name','name','main_image','is_sale')
								->where('id','<>',$product_id)
								->where('category_id','LIKE',"%$category_id%")
								->where('category_id','LIKE',"%$category_id%")
								->where('publish','=',1)
								->where('is_sold_out','=',0)
								->orderBy(DB::raw('RAND()'))
								->take(4)
								->remember(30)
								->get();
	}

	public static function getNewProduct()
	{
		$category = ProductCategory::select('id')
						->where('short_name','LIKE','new-releases')
						->where('publish','=',1)
						->get()
						->first();
		if(!isset($category->id))
			return null;
		return Product::select('id','short_name','name','main_image','is_sale')
								->where('category_id','LIKE', "%{$category->id}%")
								->where('publish','=',1)
								->where('is_sold_out','=',0)
								->orderBy(DB::raw('RAND()'))
								->take(20)
								->remember(30)
								->get();
	}

	public static function getMainPriceByID($product_id)
	{
		$product = ProductOptionPrice::select('sell_price','bigger_price')
							->where('product_id','=',$product_id)
							->where('default','=',1)
							->get()
							->first();
		if(is_null($product))
			$product = array('sell_price'=>0,'bigger_price'=>0);
		return $product;
	}

	public static function getOptionPrice($product_id)
	{
		$prices =  ProductOptionPrice::select('sell_price','bigger_price','sizew','sizeh','id','default')
							->where('product_id','=',$product_id)
							->orderBy('default','desc')
							->orderBy('sizew','asc')
							->orderBy('sizeh','asc')
							->get();
		$arr_price = array();
		foreach($prices as $price){
			$arr_price[] = array(
			                     'id'			=>$price['id'],
			                     'default'		=>$price['default'],
			                     'size'			=>$price['sizew'] . '&nbsp;x&nbsp;' .$price['sizeh'] ,
			                     'sell_price'	=>$price['sell_price'],
			                     'bigger_price'	=>$price['bigger_price'],
			                     );
		}
		return $arr_price;
	}

	public function getAjaxProducts(){
        $v_ajax_type = Input::has('ajax_type')?Input::has('ajax_type'):'';
        $v_product_id = Input::has('id')?Input::has('id'):'0';
        settype($v_product_id, 'int');
        $arr_return = array();
        if($v_ajax_type=='load_template'){
            $api = new ImageStylor_Api(IMAGE_STYLOR_KEY);
            $arr_return = $api->get_template_list();
        }

        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }


    public static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
        $value = Product::condition($arr_where)->sort($arr_order)->pluck($field);
        return $value;
    }

    public static function listProduct()
    {
    	$product_id = Input::has('current_id') ? Input::get('current_id') : 0;
    	$product = Product::select('name','id','main_image')
    						->where('id','<>',$product_id)
							->get()
							->toArray();
		return json_encode($product);
    }

}