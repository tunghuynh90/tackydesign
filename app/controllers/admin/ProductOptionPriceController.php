<?php
class ProductOptionPriceController extends AdminController{
	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'product-option-price';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public static function create()
	{
		$arr_post = Input::all();
		if(isset($arr_post['product_id']) && is_numeric($arr_post['product_id'])){
			$arr = array();
			$arr['product_id'] = $arr_post['product_id'];
			$arr_post = $arr_post['models'];
			foreach($arr_post as $post){
				$arr['sell_price'] = (float)$post['sell_price'];
				$arr['bigger_price'] = (float)$post['bigger_price'];
				$arr['default'] = (isset($post['default']) && $post['default']=='true'? 1: 0);
				$arr['sizew'] = (isset($post['sizew'])  ? $post['sizew']: 0);
				$arr['sizeh'] = (isset($post['sizeh'])  ? $post['sizeh']: 0);
				$arr['weight'] = (isset($post['weight'])  ? $post['weight']: 0);
				break;
			}
			$arr_user = json_decode(Session::get('ss_admin'));
			$arr['created_by'] = $arr['modified_by'] = $arr_user->id;
			return ProductOptionPrice::insertGetId($arr);
			return false;
		}
	}

	public static function update()
	{
		$arr_post = Input::all();
		if(isset($arr_post['models']) ){
			$arr = array();
			$arr_post = $arr_post['models'];
			foreach($arr_post as $post){
				$id = $post['id'];
				$arr['product_id'] = $post['product_id'];
				$arr['sizew'] = (isset($post['sizew'])  ? $post['sizew']: 0);
				$arr['sizeh'] = (isset($post['sizeh'])  ? $post['sizeh']: 0);
				$arr['weight'] = (isset($post['weight'])  ? $post['weight']: 0);
				$arr['sell_price'] = (float)$post['sell_price'];
				$arr['bigger_price'] = (float)$post['bigger_price'];
				$arr['default'] = (isset($post['default']) && $post['default'] == 'true'? 1: 0);
				break;
			}
			$arr_user = json_decode(Session::get('ss_admin'));
			$arr['modified_by'] = $arr_user->id;
			if( ProductOptionPrice::where('id',$id)->update($arr))
				return json_encode($arr);
			return false;
		}
	}

	public static function delete()
	{
		$arr_post = Input::all();
		if(isset($arr_post['models']) ){
			$arr = array();
			$arr_post = $arr_post['models'];
			foreach($arr_post as $post){
				$id = $post['id'];
				ProductOptionPrice::where('id',$id)->delete();
			}
		}
	}

	public static function lists()
	{
		$arr_order = array();
		if(Input::has('product_id'))
			$arr_where[] = array('field'=>'product_id','value'=>(int)Input::get('product_id'));
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = ProductOptionPrice::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows,'total_pages'=>$v_total_pages, 'options'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array())
	{
		$arr_columns = array();
        $v_size_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_size_field==0)
		    $product = ProductOptionPrice::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $product = ProductOptionPrice::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($product){
			$i=0;
			$v_row = $offset;
			foreach($product as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['product_id'] = isset($one->product_id)?$one->product_id:'';
                    $arr_columns[$i]['sizew'] = (isset($one->sizew)  ? $one->sizew: 0);
					$arr_columns[$i]['sizeh'] = (isset($one->sizeh)  ? $one->sizeh: 0);
					$arr_columns[$i]['weight'] = (isset($one->weight)  ? $one->weight: 0);
                    $arr_columns[$i]['option_id'] = isset($one->product_option_id)?$one->product_option_id:'';
                    $arr_columns[$i]['default'] = isset($one->default)?$one->default:0;
                    $arr_columns[$i]['sell_price'] = isset($one->sell_price)?$one->sell_price:0;
                    $arr_columns[$i]['bigger_price'] = isset($one->bigger_price)?$one->bigger_price:0;
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public static function cloneOption()
	{
		if(!Input::has('current_id'))
			return false;
		if(!Input::has('product_id'))
			return false;
		$product_id = Input::get('product_id');
		$current_id = Input::get('current_id');
		$option_price = ProductOptionPrice::where('product_id','=',$product_id)
							->get();
		$arr_user = json_decode(Session::get('ss_admin'));
		foreach($option_price as $option){
			$option = $option->toArray();
			unset($option['id']);
			unset($option['modified_date']);
			$option['default'] = 0;
			$option['product_id'] = $current_id;
			$option['created_by'] = $option['modified_by'] = $arr_user->id;
			ProductOptionPrice::insert($option);
		}
	}
}