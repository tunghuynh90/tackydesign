<?php
class ProductPropertyController extends AdminController{
	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'properties';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public function addProperty()
	{
		return self::getOnePropertyView(0,true);
	}

	public static function index()
	{
		return self::getAllPropertyView();
	}

	private static function getOnePropertyView($id = 0, $p_edit = false){
		$v_title = 'Product Option - ';
		$arr_icons = array();
		$arr_icons['view'] = 'products/properties';
		$v_session = 'ss_save_product_property_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array('property_list'=>array());
			$arr_columns['property_list'] = array_merge($arr_columns['property_list'],ProductOptionController::getOption());
			$arr_columns['property_list'] = json_encode($arr_columns['property_list']);
			$property = '';
			if($id)
				$property = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($property){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($property->id)?$property->id:0;
				$arr_columns['name'] = isset($property->name)?$property->name:'';
				$arr_columns['key'] = isset($property->key)?$property->key:'';
				$arr_columns['type'] = isset($property->type)?$property->type:'';
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['key'] = '';
				$arr_columns['type'] = 'color';
			}
		}
		return View::make('admin.product_one_property')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$property = ProductProperty::condition($arr_where)->sort($arr_order)->first();
		return $property;
	}

	public static function getAllPropertyView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Product Option - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'products/properties';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		$arr_property = ProductOptionController::getOption();
		return View::make('admin.product_all_property')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit,'property' => $arr_property)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array()){
		if(empty($arr_order))
			$arr_order[] = array('field'=>'type','asc'=>true);
		$arr_columns = array();
        $v_property_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_property_field==0)
		    $property = ProductProperty::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $property = ProductProperty::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($property){
			$i=0;
			$v_row = $offset;
			foreach($property as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_property_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
                    $arr_columns[$i]['key'] = isset($one->key)?$one->key:'';
                    $arr_columns[$i]['type'] = isset($one->type)?strtoupper($one->type):'';
                }else{
                    for($j=0;$j<$v_property_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getPageProperty(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_property, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_property < 10) $v_page_property = 10;
		$v_total_rows = ProductProperty::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = array();
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'models'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

    private static function updateProperty(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = ProductProperty::condition($arr_where)->update($arr_columns);
            else
                $v_rows = ProductProperty::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editProperty($id=0){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOnePropertyView($id, true);
        return Redirect::to('/admin/error');
    }

    public function deleteProperty($property){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$property);
            $property = ProductProperty::condition($arr_where)->get();
            if($property) ProductProperty::condition($arr_where)->delete();
            return Redirect::to('/admin/products/properties');
        }else return Redirect::to('/admin/error');
	}

    public function viewProperty($id=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOnePropertyView($id);
        }else return Redirect::to('/admin/error');
    }

	public function saveProperty(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		$v_action = Input::has('action_property')?Input::get('action_property'):'';
		if($v_action=='new'){
			$property = new ProductProperty;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$property = ProductProperty::condition($arr_where)->first();
			if(!$property){
				$property = new ProductProperty;
				$v_action = 'new';
			}
		}
		$v = ProductProperty::validate(Input::all());
		$arr_columns['name'] = Input::has('name')?Input::get('name'):'';
		$arr_columns['key'] = Input::has('key')?Input::get('key'):'';
		$arr_columns['type'] = Input::has('type')?Input::get('type'):'';
		$v_passes = $v->passes();
		if($v_passes){
			$property->name = $arr_columns['name'];
			$property->key = $arr_columns['key'];
			$property->type = $arr_columns['type'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('key');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('type');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_property_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
				    $property->save();
				    $insertId = $property->id;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $property->condition($arr_where)->update($arr_columns);
				    //$v_result = $affectRow>0;
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/products/properties');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/products/properties/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/products/properties/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}
	public static function getProperties()
	{
		$arr_properties = array();
		$option = ProductOption::select('key')
								->orderBy('key','asc')
								->get();
		$properties = array();
		foreach($option as $opt)
			$properties[] = $opt->key;
		// $properties = ProductProperty::getEnumValues(true);
		$arr_order[] = array('field'=>'key', 'asc'=>true);
		foreach($properties as $property){
			$arr_where[0] = array('field'=>'type', 'operator'=>'=', 'value'=>$property);
			$property_by_type = ProductProperty::condition($arr_where)->sort($arr_order)->get();
			foreach($property_by_type as $type){
				$arr_properties[$property][] = array('name'=>$type->name,'value'=>$type->id);
			}
		}
		return $arr_properties;
	}

	public static function getPropertyById($id)
	{
		$property = ProductProperty::where('id','=',$id)
						->get()
						->first();
		return array('id'=>$property->id,'name'=>$property->name);
	}

	public static function create(){
		$arr_post = Input::all();
		$arr_columns = array();
		$arr_post = $arr_post['models'];
		foreach($arr_post as $post){
			$arr_columns['name'] = $post['name'];
			$arr_columns['key'] =  $post['key'];
			$arr_columns['type'] = $post['type'];
			break;
		}
		return ProductProperty::insertGetId($arr_columns);
		return false;
	}

	public static function update(){
		$arr_post = Input::all();
		if(isset($arr_post['models'])){
			$arr_columns = array();
			$arr_post = $arr_post['models'];
			foreach($arr_post as $post){
				$id = $post['id'];
				$arr_columns['name'] = $post['name'];
				$arr_columns['key'] = $post['key'];
				$arr_columns['type'] = $post['type'];
				break;
			}
			if(ProductProperty::where('id',$id)->update($arr_columns))
				return json_encode($arr_columns);
			return false;
		}
	}

	public static function delete(){
		$arr_post = Input::all();
		if(isset($arr_post['models'])){
			$arr_post = $arr_post['models'];
			foreach($arr_post as $post){
				$id = $post['id'];
				ProductProperty::where('id',$id)->delete();
			}
		}
	}

}