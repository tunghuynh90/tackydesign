<?php
class ProductTemplateController extends AdminController{
	/**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model AdminProductTemplate
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$product_template = ProductTemplate::condition($arr_where)->sort($arr_order)->first();
		return $product_template;
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @return array
	 */
	public function getOneProductTemplate($id = 0){
		$v_title = 'Product Template - ';
		$arr_icons = array();
		$arr_icons['view'] = '/';
		$v_session = 'ss_save_product_templates_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$product_template = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($product_template){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($product_template->id)?$product_template->id:0;
				$arr_columns['template_id'] = isset($product_template->template_id)?$product_template->template_id:0;
				$arr_columns['product_id'] = isset($product_template->product_id)?$product_template->product_id:0;
				$arr_columns['status'] = isset($product_template->status)?$product_template->status:1;
				$arr_columns['assigned_time'] = isset($product_template->assigned_time)?$product_template->assigned_time:'0000-00-00 00:00:00';
				$arr_columns['template_site'] = isset($product_template->template_site)?$product_template->template_site:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['template_id'] = 0;
				$arr_columns['product_id'] = 0;
				$arr_columns['status'] = 1;
				$arr_columns['assigned_time'] = '0000-00-00 00:00:00';
				$arr_columns['template_site'] = 0;
			}
		}
		return View::make('admin.product_template_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllProductTemplate(){
		$v_title = 'Product Template - View All';
		$arr_icons = array();
		$arr_icons['new'] = '';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.product_template_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateProductTemplate(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = ProductTemplate::condition($arr_where)->update($arr_columns);
		else
			$v_rows = ProductTemplate::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param AdminProductTemplate $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeProductTemplate(AdminProductTemplate $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
        if($limit<=0) $limit = 999999;
        $v_size_field = sizeof($arr_fields);
        if($v_size_field==0)
		    $product_template = ProductTemplate::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $product_template = ProductTemplate::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($product_template){
			$i=0;
			$v_row = $offset;
			foreach($product_template as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_size_field==0){
                    $arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['template_id'] = isset($one->template_id)?$one->template_id:0;
                    $arr_columns[$i]['product_id'] = isset($one->product_id)?$one->product_id:0;
                    $arr_columns[$i]['status'] = isset($one->status)?$one->status:1;
                    $arr_columns[$i]['assigned_time'] = isset($one->assigned_time)?$one->assigned_time:'0000-00-00 00:00:00';
                    $arr_columns[$i]['template_site'] = isset($one->template_site)?$one->template_site:0;
                }else{
                    for($j=0;$j<$v_size_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageProductTemplate(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'field_search', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = ProductTemplate::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'product_template'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = ProductTemplate::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $product_template
	 * @return Redirect
	 */
	public function getDeleteProductTemplate($product_template){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$product_template);
		$product_template = ProductTemplate::condition($arr_where)->get();
		if($product_template) ProductTemplate::condition($arr_where)->delete();
		return Redirect::to('/admin//');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array()){
		$results = ProductTemplate::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>$r->$field_text, 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

    /**
     * @param array $arr_template
     * @param $p_product
     * @return int
     */
    public static function saveProductTemplate(array $arr_template, $p_product){
        ProductTemplate::where('product_id', $p_product)->delete();
        $j=0;
        for($i=0; $i<sizeof($arr_template);$i++){
            $t = new ProductTemplate;
            $t->template_id = $arr_template[$i];
            $t->product_id = $p_product;
            $t->save();
            if($t->id>0) $j++;
        }
        return $j;
	}

}
?>