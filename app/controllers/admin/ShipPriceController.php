<?php
class ShipPriceController extends AdminController{

	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'shipping-price';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
		parent::__construct();
		$this->beforeFilter(function(){
			return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
		});
	}

	/**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model ShipPrice
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$ship_price = ShipPrice::condition($arr_where)->sort($arr_order)->first();
		return $ship_price;
	}

	/**
	 * View ShipPrice
	 * @param int $id
	 * @return array
	 */
	public function getViewShipPrice($id = 0){
		if(isset(self::$arr_permit[_R_VIEW])){
			return self::getOneShipPrice($id);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Update ShipPrice
	 * @param int $id
	 * @return array
	 */
	public function getEditShipPrice($id = 0){
		if(isset(self::$arr_permit[_R_UPDATE])){
			return self::getOneShipPrice($id, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Insert ShipPrice
	 * @param int $id
	 * @return array
	 */
	public function getAddShipPrice($id = 0){
		if(isset(self::$arr_permit[_R_INSERT])){
			return self::getOneShipPrice(0, true);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	private static function getOneShipPrice($id = 0, $p_edit = false){
		$v_title = 'Ship Price - ';
		$arr_icons = array();
		$arr_icons['view'] = self::$v_module_group_short_name.'/'.self::$v_module_short_name;
		$v_session = 'ss_save_ship_price_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$ship_price = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($ship_price){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($ship_price->id)?$ship_price->id:0;
				$arr_columns['country_id'] = isset($ship_price->country_id)?$ship_price->country_id:0;
				$arr_columns['publish'] = isset($ship_price->publish)?$ship_price->publish:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['country_id'] = 0;
				$arr_columns['publish'] = 1;
			}
			$arr_countries = Country::select('name','id')->where('publish','=',1)->get();
			$arr_columns['country_list'] = array();
			foreach ($arr_countries as $country) {
				$arr_columns['country_list'][] = array('name'=>$country->name,'value'=>$country->id);
			}
			$arr_columns['country_list'] = json_encode($arr_columns['country_list']);
		}
		return View::make('admin.ship_price_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	public static function getAllShipPrice(){
		$v_title = 'Ship Price - View all';
		$arr_icons = array();
		$arr_icons['new'] = 'products/shipping-price';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.ship_price_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}


	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateShipPrice(array $arr_columns, $arr_where){
			if(isset(self::$arr_permit[_R_UPDATE])){
			if(sizeof($arr_where)>0)
				$v_rows = ShipPrice::condition($arr_where)->update($arr_columns);
			else
				$v_rows = ShipPrice::update($arr_columns);
			return $v_rows;
		}else return 0;
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateShipPrice(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

	/**
	 * Detect Eloquent changes
	 * @param ShipPrice $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeShipPrice(ShipPrice $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){

		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$ship_price = ShipPrice::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$ship_price = ShipPrice::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($ship_price){
			$i=0;
			$v_row = $offset;
			foreach($ship_price as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['country_id'] = isset($one->country_id)?$one->country_id:0;
					$arr_columns[$i]['country'] =	Country::where('id','=',$arr_columns[$i]['country_id'])->pluck('name');
					$arr_columns[$i]['publish'] = $one->publish==1?'icon-unhide.png':'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageShipPrice(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 100) $v_page_size = 100;
		$v_total_rows = ShipPrice::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'ship_price'=>$arr_columns);
		$response = Response::json($arr_return);//->setCallback('callback');
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = ShipPrice::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $ship_price
	 * @return Redirect
	 */
	public function getDeleteShipPrice($ship_price){
		if(isset(self::$arr_permit[_R_DELETE])){			$arr_where = array();
			$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$ship_price);
			$ship_price = ShipPrice::condition($arr_where)->get();
			if($ship_price) ShipPrice::condition($arr_where)->delete();
			return Redirect::to('/admin/'.self::$v_module_group_short_name.'/'.self::$v_module_short_name);
		}else return Redirect::to('/admin/error');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = ShipPrice::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>html_entity_decode($r->$field_text), 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveShipPrice(){
		if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_category`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_ship_price')?Input::get('action_ship_price'):'';
		if($v_action=='new'){
			$ship_price = new ShipPrice;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$ship_price = ShipPrice::condition($arr_where)->first();
			if(!$ship_price){
				$ship_price = new ShipPrice;
				$v_action = 'new';
			}
		}
		$v = ShipPrice::validate(Input::all());
		$arr_columns['country_id'] = Input::has('country_id')?Input::get('country_id'):0;
        $arr_columns['publish']  = Input::has('publish')?1:0;
		$v_passes = $v->passes();
		if($v_passes){
			$ship_price->country_id = $arr_columns['country_id'];
	        $ship_price->publish = $arr_columns['publish'];
		}else{
			$arr_post = Input::all();
			foreach($arr_post as $field=>$value){
				if($field == 'id') continue;
				$v_field_message = $v->messages()->first($field);
				if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			}
		}
		$v_session = 'ss_save_ship_price_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			$arr_user = json_decode(Session::get('ss_admin'));
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
				    $ship_price->save();
				    $insertId = $ship_price->id;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $ship_price->condition($arr_where)->update($arr_columns);
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/products/shipping-price/'.($v_id ? $v_id:$insertId).'/edit');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
                $arr_columns['country'] = $arr_columns['country'];
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/products/shipping-price/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/products/shipping-price/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public static function getOptions()
	{
		$id = Input::get('id');
		$shipping_price_detail = ShipPriceDetail::where('ship_price_id','=',$id)->orderBy('id','desc')->get();
		$arr_return = array();
		if(is_null($shipping_price_detail))
			return false;
		foreach($shipping_price_detail as $value){
			$value = $value->toArray();
			$value['shipping_price'] = number_format($value['shipping_price'],2);
			$arr_return[] =$value;
		}
		return json_encode(array('options'=>$arr_return));
	}

	public static function addOptions()
	{
		$arr_post = Input::get('models');
		$arr_save = array();
		foreach($arr_post as $post){
			$arr_save = array(
			                  'shipping_method' => $post['shipping_method'],
			                  'shipping_price' => (float)$post['shipping_price'],
			                  'ship_price_id'	=> Input::get('id')
			                  );
			break;
		}
		if(!empty($arr_save))
			ShipPriceDetail::insert($arr_save);
	}

	public static function updateOptions()
	{
		$arr_post = Input::get('models');
		$arr_update = array();
		$id = 0;
		foreach($arr_post as $post){
			$id= $post['id'];
			$arr_update = array(
			                  'shipping_method' => $post['shipping_method'],
			                  'shipping_price' => (float)$post['shipping_price'],
			                  );
			break;
		}
		if( $id && !empty($arr_update))
			ShipPriceDetail::where('id','=',$id)->update($arr_update);
	}

	public static function deleteOptions()
	{
		$arr_post = Input::get('models');
		$id = 0;
		foreach($arr_post as $post){
			$id = $post['id'];
			break;
		}
		if( $id )
			ShipPriceDetail::where('id','=',$id)->delete();
	}

}
?>