<?php
class ShippingController extends AdminController{

	private static $username = 'f911015ef50edf0c';
	private static $password = '17425afcd46c38ddab8fe7';
	private static $customerNumber = '0008276169';
	private static $sandbox = true;
	private static $curl;
	private static $response;
	private static $customHeader = null;

	public function __construct()
	{
		parent::__construct();
	}

	private static function __connect($service_url, $sendRequest = '')
	{
		self::$curl = curl_init($service_url);
		curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt(self::$curl, CURLOPT_CAINFO, public_path().DS.'assets'.DS.'cert'.DS.'cacert.pem');
		if($sendRequest){
			curl_setopt(self::$curl, CURLOPT_POST, true);
			curl_setopt(self::$curl, CURLOPT_POSTFIELDS, $sendRequest);
		}
		curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(self::$curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt(self::$curl, CURLOPT_USERPWD, self::$username . ':' . self::$password);
		if(self::$customHeader)
			curl_setopt(self::$curl, CURLOPT_HTTPHEADER, self::$customHeader);
		else
			curl_setopt(self::$curl, CURLOPT_HTTPHEADER, array('Content-Type: application/vnd.cpc.ncshipment-v3+xml', 'Accept: application/vnd.cpc.ncshipment-v3+xml'));
		self::$response  = curl_exec(self::$curl);
		curl_close(self::$curl);
	}

	private static function __getData($getFile = false)
	{
		if($getFile){
			$contentType =  curl_getinfo(self::$curl,CURLINFO_CONTENT_TYPE);
			if ( strpos($contentType, 'application/pdf' ) !== FALSE ) {
				$fileName = 'artifact'.date('m-d-y-h-i').'.pdf';
				$filePath = public_path().DS.'assets'.DS.'files'.DS. $fileName;
				file_put_contents($filePath, self::$response);
				return array(
				             'file' => Request::root().'/assets/files/'.$fileName,
				             'file_path'=>$filePath
				             );
			}
		}
		libxml_use_internal_errors(true);
		$xml =  simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',self::$response) . '</root>');
		if (!$xml) {
			$arr_return['message'] = "Failed loading XML.\n";
			foreach(libxml_get_errors() as $error)
				$arr_return['message'] .= "\t{$error->message}";
			return $arr_return;
		}
		return $xml;
}

	public static function __getEndPoint()
	{
		return self::$sandbox ? 'https://ct.soa-gw.canadapost.ca/rs/'  : 'https://soa-gw.canadapost.ca/rs/';
	}

	/*
	The CreateNonContractShipment service is used to generate and pay for a
 	shipping label, including validation and payment authorization.
 	*/
	public static function createShipment($arr_data = array())
	{
		$url = self::__getEndPoint().self::$customerNumber.'/ncshipment';
		$xml = new SimpleXMLElement('<non-contract-shipment xmlns="http://www.canadapost.ca/ws/ncshipment-v3"></non-contract-shipment>');
		$xml->addChild('requested-shipping-point',$arr_data['requested_shipping_point']);
		// $xml->addChild('expected-mailing-date',$arr_data['expected_mailing_date']);
		$node = $xml->addChild('delivery-spec');
		$node->addChild('service-code', 'DOM.EP');
		$childNode = $node->addChild('sender');
		$childNode->addChild('name', $arr_data['sender']['name']);
		$childNode->addChild('company', $arr_data['sender']['company']);
		$childNode->addChild('contact-phone', $arr_data['sender']['contact_phone']);
		$grandChild = $childNode->addChild('address-details');
		$grandChild->addChild('address-line-1', $arr_data['sender']['address_line_1']);
		$grandChild->addChild('city', $arr_data['sender']['city']);
		$grandChild->addChild('prov-state', $arr_data['sender']['prov_state']);
		if(isset($arr_data['sender']['country_code']))
			$grandChild->addChild('country-code', $arr_data['sender']['country_code']);
		$grandChild->addChild('postal-zip-code', $arr_data['sender']['postal_zip_code']);

		$childNode = $node->addChild('destination');
		$childNode->addChild('name', $arr_data['destination']['name']);
		$childNode->addChild('company', $arr_data['destination']['company']);
		// $childNode->addChild('contact-phone', $arr_data['destination']['contact_phone']);
		$grandChild = $childNode->addChild('address-details');
		$grandChild->addChild('address-line-1', $arr_data['destination']['address_line_1']);
		$grandChild->addChild('city', $arr_data['destination']['city']);
		$grandChild->addChild('prov-state', $arr_data['destination']['prov_state']);
		$grandChild->addChild('country-code', $arr_data['destination']['country_code']);
		$grandChild->addChild('postal-zip-code', $arr_data['destination']['postal_zip_code']);

		$childNode = $node->addChild('options');
		$grandChild = $childNode->addChild('option');
		$grandChild->addChild('option-code','DC');

		$childNode = $node->addChild('parcel-characteristics');
		$childNode->addChild('weight',$arr_data['weight']);
		$grandChild = $childNode->addChild('dimensions');
		$grandChild->addChild('length',$arr_data['length']);
		$grandChild->addChild('width',$arr_data['width']);
		$grandChild->addChild('height',$arr_data['height']);
		$childNode->addChild('unpackaged',($arr_data['unpackaged'] ? $arr_data['unpackaged'] : 'false'));
		$childNode->addChild('mailing-tube',($arr_data['mailing_tube'] ? $arr_data['mailing_tube'] : 'false'));
		$childNode = $node->addChild('preferences');
		$childNode->addChild('show-packing-instructions',($arr_data['show_packing_instructions'] ? $arr_data['show_packing_instructions'] : 'false'));
		$childNode->addChild('show-postage-rate',($arr_data['show_postage_rate'] ? $arr_data['show_postage_rate'] : 'false'));
		$childNode->addChild('show-insured-value',($arr_data['show_insured_value'] ? $arr_data['show_insured_value'] : 'false'));

		$xmlRequest = $xml->saveXML();
		self::__connect($url,$xmlRequest);
		$xml = self::__getData();
		if (is_array($xml)) {
			return $xml;
		} else {
			if ($xml->{'non-contract-shipment-info'} ) {
				$shipment = $xml->{'non-contract-shipment-info'}->children('http://www.canadapost.ca/ws/ncshipment-v3');
				if ( $shipment->{'shipment-id'} ) {
					$arr_return['shipment_id']  =  (string)$shipment->{'shipment-id'};
					foreach ( $shipment->{'links'}->{'link'} as $link )
						$arr_return[(string)$link->attributes()->{'rel'}]  = (string)$link->attributes()->{'href'};
				}
			}
			if ($xml->{'messages'} ) {
				$arr_return['message'] = '';
				$messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
				foreach ( $messages as $message ) {
					$arr_return['message'] .= "Error Code: {$message->code} \n";
					$arr_return['message'] .= "Error Msg:  {$message->description}\n\n";
				}
			}
		}
		return $arr_return;
	}

	/*
	The GetNonContractShipmentReceipt service is used to retrieve XML details
	regarding the shipment paid for in the Create Non-Contract Shipment call.
	*/
	public static function getShipmentReceipt($receipt_url)
	{
		self::__connect($receipt_url);
		$xml = self::__getData();
		if (is_array($xml)) {
			return $xml;
		} else {
			if ($xml->{'non-contract-shipment-receipt'} ) {
				$shipment = $xml->{'non-contract-shipment-receipt'}->children('http://www.canadapost.ca/ws/ncshipment-v3');
				if ( $shipment->{'cc-receipt-details'} ) {
					$receipt_details = $shipment->{'cc-receipt-details'} ;
					$arr_return['base_amount'] =  (float)$shipment->{'base-amount'};
					$arr_return['pre_tax_amount'] =  (float)$shipment->{'pre-tax-amount'};
					$arr_return['gst_amount'] =  (float)$shipment->{'gst-amount'};
					$arr_return['pst_amount'] =  (float)$shipment->{'pst-amount'};
					$arr_return['hst_amount'] =  (float)$shipment->{'hst-amount'};
					$arr_return['total'] =  (float)$receipt_details->{'charge-amount'};
					$arr_return['weight'] =  (float)$shipment->{'rated-weigh'};
				}
			}
			if ($xml->{'messages'} ) {
				$arr_return['message'] = '';
				$messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
				foreach ( $messages as $message ) {
					$arr_return['message'] .= "Error Code: {$message->code} \n";
					$arr_return['message'] .= "Error Msg:  {$message->description}\n\n";
				}
			}
		}
		return $arr_return;
	}


	/*
	The GetArtifact service is used to retrieve a pdf of the shipping label
 	created by a prior Create Non-Contract Shipment call.
 	*/
	public static function getShipmentArtifact($label_url)
	{
		self::$customHeader = array('Accept:application/pdf', 'Accept-Language:en-CA');
		self::__connect($label_url);
		$xml = self::__getData(true);
		if (is_array($xml)) {
			return $xml;
		} else {
			if ($xml->{'messages'} ) {
				$arr_return['message'] = '';
				$messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');
				foreach ( $messages as $message ) {
					$arr_return['message'] .= "Error Code: {$message->code} \n";
					$arr_return['message'] .= "Error Msg:  {$message->description}\n\n";
				}
			}
		}
		return $arr_return;
	}

	public static function getRate($weight, $postalCode)
	{
		self::$customHeader = array('Content-Type: application/vnd.cpc.ship.rate-v3+xml', 'Accept: application/vnd.cpc.ship.rate-v3+xml');
		$customerNumber = self::$customerNumber;
		$originPostalCode = AdminConfigs::where('key_id','=','postal_code')
									->pluck('key_value');
		$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v3">
  <customer-number>{$customerNumber}</customer-number>
  <parcel-characteristics>
    <weight>{$weight}</weight>
  </parcel-characteristics>
  <origin-postal-code>{$originPostalCode}</origin-postal-code>
  <destination>
    <domestic>
      <postal-code>{$postalCode}</postal-code>
    </domestic>
  </destination>
</mailing-scenario>
XML;
		$url = self::__getEndPoint().'/ship/price';
		self::__connect($url, $xmlRequest);
		$xml = self::__getData();
		$arr_return = array();
		if (is_array($xml)) {
			return $xml;
		} else {
			if ($xml->{'price-quotes'} ) {
				$priceQuotes = $xml->{'price-quotes'}->children('http://www.canadapost.ca/ws/ship/rate-v3');
				if ( $priceQuotes->{'price-quote'} ) {
					foreach ( $priceQuotes as $priceQuote ) {
						$arr_return[] = array(
					                      	'service_name' => (string)$priceQuote->{'service-name'},
											'price' => (float)$priceQuote->{'price-details'}->{'due'}
											);
					}
				}
			}
		}
		return $arr_return;
	}


}