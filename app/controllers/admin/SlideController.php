<?php
class SlideController extends AdminController{

	public function __contruct(){
		parrent::__contruct();
	}

	public static function getAllSlide(){
		$v_title = 'Slide';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/slide';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.slide_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$slide = Slide::condition($arr_where)->sort($arr_order)->first();
		return $slide;
	}

	public function getOneSlide($id = 0){
		$v_title = 'Slide';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/slide';
		$v_session = 'ss_save_slide_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$slide = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($slide){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($slide->id)?$slide->id:0;
				$arr_columns['image_link'] = isset($slide->image)?('/assets/upload/slide/'.$slide->image):''; // image name
				if($slide->image && file_exists(public_path().$arr_columns['image_link'])){
                    $arr_columns['image_link'] = Request::root()."/". $arr_columns['image_link'];
                }
                else{
                    $arr_columns['image_link'] = '';
                }
                $arr_columns['image'] = $slide->image;
				$arr_columns['publish'] = isset($slide->publish)?$slide->publish:1;
				$arr_columns['order_no'] = isset($slide->order_no)?$slide->order_no:0;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['image'] = '';
				$arr_columns['image_link'] = Request::root()."/assets/images/no_image.jpg";
				$arr_columns['publish'] = 1;
				$arr_columns['order_no'] = 0;
			}
		}
		return View::make('admin.slide_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	public function saveSlide(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `slide`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action')?Input::get('action'):'';
		if($v_action=='new'){
			$slide = new Slide;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$slide = Slide::condition($arr_where)->first();
			$v_allow_edit = !is_null($slide);
			if(!$v_allow_edit){
				$slide = new Slide;
				$v_action = 'new';
			}
		}
        $arr_columns['image'] = Input::has('image')?Input::get('image'):'';  //   $arr_columns['image'] ='' hay  'oz.jpg'
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_order_no = Input::has('order_no')?Input::get('order_no'):0;
		settype($v_order_no,'int');
		$arr_columns['order_no'] = $v_order_no;
		$v = Slide::validate($arr_columns);
		$v_passes = $v->passes();
		if($v_passes){
			$slide->image = $arr_columns['image'];
			$slide->publish = $arr_columns['publish'];
			$slide->order_no = $arr_columns['order_no'];
		}else{
			$v_field_message = $v->messages()->first('image');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('order_no');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_slide_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$slide->save();
				$insertId = $slide->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeSlide($slide)>0){
					if($v_allow_edit){
						$affectRow = $slide->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				parent::clearCached();
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/slide');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['image_link'] =  Request::root()."/assets/upload/slide/".$arr_columns['image'];
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/slide/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['image_link'] =  '';
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/slide/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function getPageSlide(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Slide::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'slide'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		if(empty($arr_order))
			$arr_order[] = array('field'=>'order_no', 'asc'=>true);
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$slide = Slide::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$slide = Slide::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($slide){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($slide as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){

					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
                    $arr_columns[$i]['image'] = $one->image != ''?(Request::root().'/assets/upload/slide/'.$one->image):Request::root().'/assets/images/no_image.jpg';
					$arr_columns[$i]['description'] = isset($one->description)?$one->description:'';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
					$arr_columns[$i]['order_no'] = isset($one->order_no)?$one->order_no:0;
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getDeleteSlide($slide){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$slide);
		$slide = Slide::condition($arr_where)->get();
		if($slide) Slide::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/slide');
	}

	private static function detectChangeSlide(Slide $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateSlide(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateSlide(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = Slide::condition($arr_where)->update($arr_columns);
		else
			$v_rows = Slide::update($arr_columns);
		return $v_rows;
	}

}