<?php
class SocialNetworkController extends AdminController{
	static $v_module_group_short_name = 'static-content';
	static $v_module_short_name = 'social-network';
	static $arr_permit = array();

	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public static function getAllSocialNetwork(){
		$v_title = 'Social Network';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/social-network';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.social_network_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'permit'=>self::$arr_permit, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$social_network = SocialNetwork::condition($arr_where)->sort($arr_order)->first();
		return $social_network;
	}

	public static function index()
	{
		return self::getAllSocialNetwork();
	}

	public function addSocialNetwork()
	{
		return self::getOneSocialNetwork(0,true);
	}

	public function editSocialNetwork($id)
	{
		return self::getOneSocialNetwork($id,true);
	}

	public function getOneSocialNetwork($id = 0, $edit = false){
		$v_title = 'Slide';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/social-network';
		$v_session = 'ss_save_social_network_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$social_network = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($social_network){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($social_network->id)?$social_network->id:0;
                $arr_columns['name'] = $social_network->name;
                $arr_columns['short_name'] = $social_network->short_name;
                $arr_columns['link'] = $social_network->link;
				$arr_columns['icon'] = $social_network->icon;
				$arr_columns['icon_link'] = '';
				if($social_network->icon && file_exists(public_path().DS.'assets'.DS.'images'.DS.'social_icon'.DS.$social_network->icon))
					$arr_columns['icon_link'] = Request::root().'/assets/images/social_icon/'.$social_network->icon;
                $arr_columns['view_widget'] = $social_network->view_widget;
                $arr_columns['share_widget'] = $social_network->share_widget;
                $arr_columns['on_home'] = (isset($social_network->on_home) ? 1 : 0);
				$arr_columns['publish'] = isset($social_network->publish)?$social_network->publish:1;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['link'] = '';
				$arr_columns['view_widget'] = '';
				$arr_columns['icon_link'] = '';
				$arr_columns['icon'] = '';
				$arr_columns['share_widget'] = '';
				$arr_columns['on_home'] = 0;
				$arr_columns['publish'] = 1;
			}
		}
		return View::make('admin.social_network_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'permit'=>self::$arr_permit, 'title'=>$v_title, 'edit' => $edit
			)
		);
	}

	public function saveSocialNetwork(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `slide`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action')?Input::get('action'):'';
		if($v_action=='new'){
			$social_network = new SocialNetwork;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$social_network = SocialNetwork::condition($arr_where)->first();
			$v_allow_edit = !is_null($social_network);
			if(!$v_allow_edit){
				$social_network = new SocialNetwork;
				$v_action = 'new';
			}
		}
		$arr_columns['name'] = Input::get('name');
		$arr_columns['short_name'] = Input::get('short_name');
		$arr_columns['link'] = Input::get('link');
		$arr_columns['icon'] = Input::get('icon');
		$arr_columns['view_widget'] = htmlspecialchars(Input::get('view_widget'));
		$arr_columns['share_widget'] = htmlspecialchars(Input::get('share_widget'));
		$v_on_home = Input::has('on_home')?1:0;
		$arr_columns['on_home'] = (int)$v_on_home;
		$v_on_home = Input::has('publish')?1:0;
		$arr_columns['publish'] = (int)$v_on_home;
		$v = SocialNetwork::validate($arr_columns);
		$v_passes = $v->passes();
		if($v_passes){
			$social_network->name = $arr_columns['name'];
			$social_network->short_name = $arr_columns['short_name'];
			$social_network->link = $arr_columns['link'];
			$social_network->icon = $arr_columns['icon'];
			$social_network->view_widget = $arr_columns['view_widget'];
			$social_network->share_widget = $arr_columns['share_widget'];
			$social_network->on_home = $arr_columns['on_home'];
			$social_network->publish = $arr_columns['publish'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('link');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('icon');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('view_widget');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('share_widget');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('on_home');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_social_network_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$social_network->save();
				$insertId = $social_network->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeSocialNetwork($social_network)>0){
					if($v_allow_edit){
						$affectRow = $social_network->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				parent::clearCached();
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/social-network');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/social-network/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/social-network/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function getPageSocialNetwork(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = SocialNetwork::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'social_network'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		if(empty($arr_order))
			$arr_order[] = array('field'=>'id', 'asc'=>true);
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$social_network = SocialNetwork::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$social_network = SocialNetwork::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($social_network){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($social_network as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){

					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
					$arr_columns[$i]['on_home'] = isset($one->on_home)?($one->on_home==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getDeleteSocialNetwork($social_network){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$social_network);
		$social_network = SocialNetwork::condition($arr_where)->get();
		if($social_network) SocialNetwork::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/social-network');
	}

	private static function detectChangeSocialNetwork(SocialNetwork $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function updateStatus(){
        $id = 	Input::has('txt_id')  ?  (int)Input::get('txt_id') : 0;
        $new_value = Input::has('txt_value')  ?  (int) Input::get('txt_value') : 0;
        $field = Input::has('txt_name')  ?  Input::get('txt_name') : 'publish';
        $arr_return = array('error'=>1);
        if($id!=0){
            if(isset(self::$arr_permit[_R_UPDATE])){
                $rows = self::updateSocialNetwork(array($field=>$new_value),array(array('field'=>'id','operator'=>'=','value'=>$id)));
                if($rows) $arr_return['error'] = 0;
                parent::clearCached();
            }
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateSocialNetwork(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = SocialNetwork::condition($arr_where)->update($arr_columns);
		else
			$v_rows = SocialNetwork::update($arr_columns);
		return $v_rows;
	}

}