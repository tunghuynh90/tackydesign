<?php
class TaxController extends AdminController{

	static $v_module_group_short_name = 'products';
	static $v_module_short_name = 'tax';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
		parent::__construct();
		$this->beforeFilter(function(){
			return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
		});
	}

	public function getAllTax(){
		if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Tax - View All';
		$arr_icons = array();
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';

        $cities = City::where('publish', 1)->orderBy('orderno')->get();
        $arr_temp = array();
        foreach($cities as $c){
            $v_city_id = $c->id;
            $arr_temp[] = $v_city_id;
            $tax = Tax::where('city_id', $v_city_id)->first();
            if(is_null($tax)){
                $tax = new Tax();
                $tax->city_id = $v_city_id;
                $tax->save();
            }
        }
		return View::make('admin.tax_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}


	public static function getOneTax($id = 0, $p_edit = false){
		$v_title = 'Tax - ';
		$arr_icons = array();
		$arr_icons['view'] = self::$v_module_group_short_name.'/'.self::$v_module_short_name;
		$v_session = 'ss_save_tax_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$tax = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($tax){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($tax->id)?$tax->id:0;
				$arr_columns['city_id'] = isset($tax->city_id)?$tax->city_id:0;
				$arr_columns['tax'] = isset($tax->tax)?$tax->tax:0;
				$arr_columns['description'] = isset($tax->description)?$tax->description:'';
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['city_id'] = 0;
				$arr_columns['tax'] = 0;
				$arr_columns['description'] = '';
			}
		}
		return View::make('admin.tax_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$tax = Tax::condition($arr_where)->sort($arr_order)->first();
		return $tax;
	}

	public function getPageTax(){
        $cities = City::where('publish', 1)->orderBy('orderno')->get();
        $arr_temp = array();
        foreach($cities as $c){
            $v_city_id = $c->id;
            $arr_temp[] = $v_city_id;
            $ship = Tax::where('city_id', $v_city_id)->first();
            if(is_null($ship)){
                $ship = new Tax();
                $ship->city_id = $v_city_id;
                $ship->save();
            }
        }


		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			//$arr_where[] = array('field'=>'field_search', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
        $arr_where[] = array('field'=>'city_id', 'operator'=>'in', 'value'=>$arr_temp);
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 100) $v_page_size = 100;
		$v_total_rows = Tax::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$response = Response::json($arr_columns);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
        $arr_city = array();
        $cities = City::where('publish', 1)->orderBy('orderno')->get();
        foreach($cities as $c){
            $v_city_id = $c->id;
            $arr_city[$v_city_id] = array(
                'zipcode'=>$c->zipcode, 'name'=>$c->name
            );
        }

		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$tax = Tax::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$tax = Tax::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($tax){
			$i=0;
			$v_row = $offset;
			foreach($tax as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['city_id'] = isset($one->city_id)?$one->city_id:0;
					$arr_columns[$i]['city_name'] = $arr_city[$one->city_id]['name'];
					$arr_columns[$i]['zip_code'] = $arr_city[$one->city_id]['zipcode'];
					$arr_columns[$i]['tax'] = isset($one->tax)?$one->tax:0;
					$arr_columns[$i]['description'] = isset($one->description)?$one->description:'';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function saveTax(){
        $v_data = $_POST['models'];
        if(get_magic_quotes_gpc()) $v_data = stripslashes($v_data);
        $arr_data = json_decode($v_data, true);
        if(!is_array($arr_data)) $arr_data = array();
        for($i=0; $i<sizeof($arr_data);$i++){
            $v_id = intval($arr_data[$i]['id']);
            $tax = Tax::where('id', $v_id)->first();
            if($tax){
                $tax->tax = floatval($arr_data[$i]['tax']);
                $tax->description = $arr_data[$i]['description'];
                $tax->save();
            }
        }

        $response = Response::json($arr_data);
        $response->header('Content-Type', 'application/json');
        return $response;
	}

}