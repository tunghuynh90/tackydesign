<?php
class UserMemberController extends AdminController{

    static $v_module_group_short_name = 'customers';
    static $v_module_short_name = 'registered-members';
    static $arr_permit = array();

    /**
     * constructor function
     */
    public function __construct(){
        parent::__construct();
        $this->beforeFilter(function(){
            return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
        });
    }

    /**
	 * Get first row by condition and order
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return model UserMember
	 */
	private static function getOne(array $arr_where, array $arr_order = array()){
		$user_member = UserMember::condition($arr_where)->sort($arr_order)->first();
		return $user_member;
	}

    /**
     * View Promotion
     * @param int $id
     * @return array
     */
    public function getViewUserMember($id = 0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneUserMember($id);
        }else return Redirect::to('/admin/error');
    }

    /**
     * Update Promotion
     * @param int $id
     * @return array
     */
    public function getEditUserMember($id = 0){
        if(isset(self::$arr_permit[_R_UPDATE])){
            return self::getOneUserMember($id, true);
        }else return Redirect::to('/admin/error');
    }

    /**
     * Insert Promotion
     * @param int $id
     * @return array
     */
    public function getAddUserMember($id = 0){
        if(isset(self::$arr_permit[_R_INSERT])){
            return self::getOneUserMember(0, true);
        }else return Redirect::to('/admin/error');
    }

	/**
	 * Get array by field key
	 * @param int $id
	 * @param boolean $p_edit
	 * @return array
	 */
	public function getOneUserMember($id = 0, $p_edit=false){
		$v_title = 'User member - ';
		$arr_icons = array();
		$arr_icons['view'] = 'customers/registered-members';
		$v_session = 'ss_save_user_member_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$user_member = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($user_member){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($user_member->id)?$user_member->id:0;
				$arr_columns['email'] = isset($user_member->email)?$user_member->email:'';
				$arr_columns['username'] = isset($user_member->username)?$user_member->username:'';
				$arr_columns['password'] = '';
				$arr_columns['firstname'] = isset($user_member->firstname)?$user_member->firstname:'';
				$arr_columns['lastname'] = isset($user_member->lastname)?$user_member->lastname:'';
				$arr_columns['confirmed'] = isset($user_member->confirmed)?$user_member->confirmed:0;
				$v_lastest_login = isset($user_member->lastest_login)?$user_member->lastest_login:'0000-00-00 00:00:00';
				$v_lastest_login =  date('d-M-Y H:i:s',strtotime($v_lastest_login));
				$arr_columns['lastest_login'] = $v_lastest_login;
				$v_created_at = isset($user_member->created_at)?$user_member->created_at:'0000-00-00 00:00:00';
				$v_created_at =  date('d-M-Y H:i:s',strtotime($v_created_at));
				$arr_columns['created_at'] = $v_created_at;

				$v_updated_at = isset($user_member->updated_at)?$user_member->updated_at:'0000-00-00 00:00:00';
				$v_updated_at =  date('d-M-Y H:i:s',strtotime($v_updated_at));
				$arr_columns['updated_at'] = $v_updated_at;
				$arr_columns['address'] = $user_member->address;
				$arr_columns['address_default_key'] = $user_member->address_default_key;

			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['email'] = '';
				$arr_columns['username'] = '';
				$arr_columns['password'] = '';
				$arr_columns['firstname'] = '';
				$arr_columns['lastname'] = '';
				$arr_columns['confirmed'] = 0;
				$arr_columns['lastest_login'] = '00-00-0000 00:00:00';
				$arr_columns['created_at'] = '00-00-0000 00:00:00';
				$arr_columns['updated_at'] = '00-00-0000 00:00:00';
			}
		}
		return View::make('admin.user_member_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	/**
	 * Get all
	 * @return View
	 */
	public function getAllUserMember(){
		$v_title = 'User_member - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'customers/registered-members';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.user_member_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title)
		);
	}

	/**
	 * update
	 * @param array $arr_columns: key is field's name, value is value of field's column
	 * @param array $arr_where: contains array of conditions, example $arr_where[] = array('field'=>'name', 'operator'=>'=', 'value'=>'John')
	 * @return int: number of rows affected
	 */
	private static function updateUserMember(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = UserMember::condition($arr_where)->update($arr_columns);
		else
			$v_rows = UserMember::update($arr_columns);
		return $v_rows;
	}

	/**
	 * Detect Eloquent changes
	 * @param UserMember $model
	 * @return int: greater than zero -> change, otherwise -> not change
	 */
	private static function detectChangeUserMember(UserMember $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	/**
	 * Get limit row
	 * @param int $offset: stated row will be selected
	 * @param int $limit: limited rows will be selected
	 * @param string $p_where: content raw condition
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param array $arr_fields: array of selected fields array(field1,field2,...)
	 * @return array
	 */
	private static function getLimit($offset = 0, $limit = 0,  $p_where='', array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
            if($p_where!='')
                $user_member = UserMember::whereRaw($p_where)->sort($arr_order)->skip($offset)->take($limit)->get();
            else
			    $user_member = UserMember::sort($arr_order)->skip($offset)->take($limit)->get();
		else
            if($p_where!='')
			    $user_member = UserMember::whereRaw($p_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
            else
                $user_member = UserMember::sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($user_member){
			$i=0;
			$v_row = $offset;
			foreach($user_member as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['email'] = isset($one->email)?$one->email:'';
					$arr_columns[$i]['password'] = isset($one->password)?$one->password:'';
					$arr_columns[$i]['firstname'] = isset($one->firstname)?$one->firstname:'';
					$arr_columns[$i]['lastname'] = isset($one->lastname)?$one->lastname:'';

					$arr_columns[$i]['confirmed'] = isset($one->confirmed)?$one->confirmed:0;
					$arr_columns[$i]['lastest_login'] = isset($one->lastest_login)?$one->lastest_login:'';
					$arr_columns[$i]['created_at'] = isset($one->created_at)?$one->created_at:'';
					$arr_columns[$i]['updated_at'] = isset($one->updated_at)?$one->updated_at:'';

					$arr_columns[$i]['fullname'] = $arr_columns[$i]['firstname'].' '.$arr_columns[$i]['lastname'];
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	/**
	 * Get page for json
	 * @return string json
	 */
	public function getPageUserMember(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
        $v_where = '';
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'username', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
			$arr_where[] = array('field'=>'firstname', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%', 'or');
			//$arr_where[] = array('field'=>'lastname', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%', 'or');
            $v_where = " `username` LIKE '%".$v_quick_search."%' OR CONCAT(CONCAT(`firstname`, ' '), `lastname`) LIKE '%".$v_quick_search."%'";
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
            $arr_order[] = array('field'=>'created_at', 'asc'=>false);
        }
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
        if($v_where!='')
            $v_total_rows = UserMember::whereRaw($v_where)->sort($arr_order)->count();
        else
		    $v_total_rows = UserMember::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $v_where, $arr_order);
		for($i=0; $i<sizeof($arr_columns);$i++){
			  $v_lastest_login = $arr_columns[$i]['lastest_login'];
			  $v_created_at = $arr_columns[$i]['created_at'];
			  $v_updated_at = $arr_columns[$i]['updated_at'];

	          if($v_lastest_login!=''){
	                $v_lastest_login = date('d-M-Y H:i:s', strtotime($v_lastest_login));
	          }
	          if($v_created_at!=''){
	                $v_created_at = date('d-M-Y H:i:s', strtotime($v_created_at));
	          }
	          if($v_updated_at!=''){
	                $v_updated_at = date('d-M-Y H:i:s', strtotime($v_updated_at));
	          }
	          $arr_columns[$i]['lastest_login'] = $v_lastest_login;
	          $arr_columns[$i]['created_at'] = $v_created_at;
	          $arr_columns[$i]['updated_at'] = $v_updated_at;
		}
		$arr_return = array('total_rows'=>$v_total_rows, 'user_member'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	/**
	 * Get scalar
	 * @param string $field
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @return mixed
	 */
	private static function getScalar($field, array $arr_where = array(), array $arr_order = array()){
		$value = UserMember::condition($arr_where)->sort($arr_order)->pluck($field);
		return $value;
	}

	/**
	 * Delete record
	 * @param int $user_member
	 * @return Redirect
	 */
	public function getDeleteUserMember($user_member){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$user_member);
		$user_member = UserMember::condition($arr_where)->get();
		if($user_member) UserMember::condition($arr_where)->delete();
		return Redirect::to('/admin/customers/registered-members');
	}

	/**
	 * Get for options
	 * @param string $field_value
	 * @param string $field_text
	 * @param mixed $p_selected
	 * @param array $arr_where: content condition array[](field, operator, value)
	 * @param array $arr_order: content order by array[](field, asc==true)
	 * @param boolean $p_include_first_row
	 * @return mixed
	 */
	private static function getForOption($field_value, $field_text, $p_selected, array $arr_where = array(), array $arr_order = array(), $p_include_first_row = true){
		$results = UserMember::condition($arr_where)->sort($arr_order)->select($field_value, $field_text)->get();
		$arr_return = array();
		if($p_include_first_row) $arr_return[] = array($field_value=>0, $field_text=>'--------', 'selected'=>false);
		foreach($results as $r){
			$arr_return[] = array($field_value=>$r->$field_value, $field_text=>$r->$field_text, 'selected'=>$r->$field_value==$p_selected);
		}
		return $arr_return;
	}

	/**
	 * Insert or Update
	 */
	public function saveUserMember(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_user_member`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_user_member')?Input::get('action_user_member'):'';
		if($v_action=='new'){
			$user_member = new UserMember;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$user_member = UserMember::condition($arr_where)->first();
			$v_allow_edit = !is_null($user_member);
			if(!$v_allow_edit){
				$user_member = new UserMember;
				$v_action = 'new';
			} else
				UserMember::$rules['email'] .= ',email,'.$v_id;
		}
		$v = UserMember::validate(Input::all());
		$v_email = Input::has('email')?Input::get('email'):'';
		$arr_columns['email'] = $v_email;
		$v_password = Input::has('password')?Input::get('password'):'';
		$arr_columns['password'] = $v_password;
		$v_firstname = Input::has('firstname')?Input::get('firstname'):'';
		$arr_columns['firstname'] = $v_firstname;
		$v_lastname = Input::has('lastname')?Input::get('lastname'):'';
		$arr_columns['lastname'] = $v_lastname;
		if(Input::has('address')){
			$address_array = Input::get('address');
			if(!empty($address_array)){
				foreach($address_array as $key=>$address){
					if(isset($address['default']) && $address['default'] == 1){
						$user_member->address_default_key = $arr_columns['address_default_key'] = $key;
						unset($address_array[$key]['default']);
					}
				}
				if(!isset($arr_columns['address_default_key']))
					$user_member->address_default_key = $arr_columns['address_default_key'] = 0;
				$user_member->address = $arr_columns['address'] = serialize($address_array);
			}
		}

		$v_confirmed = Input::has('confirmed')?Input::get('confirmed'):0;
		settype($v_confirmed,'int');
		$arr_columns['confirmed'] = $v_confirmed;
		$arr_columns['updated_at'] = new \DateTime;
		$v_passes = $v->passes();

		if($v_passes){
			$user_member->email = $arr_columns['email'];
			$user_member->firstname = $arr_columns['firstname'];
			$user_member->lastname = $arr_columns['lastname'];
			$user_member->confirmed = $arr_columns['confirmed'];
			$user_member->updated_at = $arr_columns['updated_at'];
		}else{
			$v_field_message = $v->messages()->first('email');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('firstname');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('lastname');
		}
		$v_session = 'ss_save_user_member_'.$v_id.'_';
		if($v_passes && $v_message==''){
			if($v_action=='new'){
				$user_member->created_at = $user_member->updated_at;
				$user_member->password = Hash::make($arr_columns['password']);
				$user_member->save();
				$insertId = $user_member->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeUserMember($user_member)>0){
					if($v_allow_edit){
						if($arr_columns['password'] != '')
							$arr_columns['password'] = Hash::make($arr_columns['password']);
						else
							unset($arr_columns['password']);
						$affectRow = $user_member->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/customers/registered-members');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;


				$arr_columns['lastest_login'] = date('d-M-Y H:i:s', strtotime($arr_columns['lastest_login']));

				$arr_columns['created_at'] = date('d-M-Y H:i:s', strtotime($arr_columns['created_at']));
				$arr_columns['updated_at'] = date('d-M-Y H:i:s', strtotime($arr_columns['updated_at']));


				return Redirect::to('/admin/customers/registered-members/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			$v_lastest_login = isset($user_member->lastest_login)?$user_member->lastest_login:'0000-00-00 00:00:00';
			$v_lastest_login =  date('d-M-Y H:i:s',strtotime($v_lastest_login));
			$arr_columns['lastest_login'] = $v_lastest_login;
			$v_created_at = isset($user_member->created_at)?$user_member->created_at:'0000-00-00 00:00:00';
			$v_created_at =  date('d-M-Y H:i:s',strtotime($v_created_at));
			$arr_columns['created_at'] = $v_created_at;

			$v_updated_at = isset($user_member->updated_at)?$user_member->updated_at:'0000-00-00 00:00:00';
			$v_updated_at =  date('d-M-Y H:i:s',strtotime($v_updated_at));
			$arr_columns['updated_at'] = $v_updated_at;

			return Redirect::to('/admin/customers/registered-members/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

}
?>