<?php
class VoucherController extends AdminController{

	public function __contruct(){
		parrent::__contruct();
	}

	public static function getAllVoucher(){
		$v_title = 'Voucher';
		$arr_icons = array();
		$arr_icons['new'] = 'products/voucher';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.voucher_all')->with(array('session'=>$v_session_id,'quick'=>$v_quick,'icon'=>$arr_icons,'title'=>$v_title));
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$blog = Voucher::condition($arr_where)->sort($arr_order)->first();
		return $blog;
	}

	public function getOneVoucher($id=0){
		$v_title = 'Voucher';
		$arr_icons = array();
		$arr_icons['view'] = 'products/voucher';
		$v_session = 'ss_save_voucher_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .='Saved with error';
		}else{
			$arr_columns = array();
			$blog = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)),array(array('field'=>'id','asc'=>true)));
			if($blog){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($blog->id)?$blog->id:0;
				$arr_columns['key'] = isset($blog->key)?$blog->key:'';
				$arr_columns['value'] = isset($blog->value)?$blog->value:'';
				$arr_columns['is_shipping_discount'] = isset($blog->is_shipping_discount)?$blog->is_shipping_discount:0;
				$arr_columns['valid_from'] = isset($blog->valid_from)? date('m/d/Y',strtotime($blog->valid_from)):'';
				$arr_columns['valid_to'] = isset($blog->valid_to)?date('m/d/Y',strtotime($blog->valid_to)):'';

				$arr_columns['active'] = isset($blog->active)?$blog->active:1;
				$arr_columns['used'] = isset($blog->used)?$blog->used:0;


			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['key'] = '';
				$arr_columns['is_shipping_discount'] = 0;
				$arr_columns['value'] = 0;
				$arr_columns['valid_from'] = '';
				$arr_columns['valid_to'] = '';
				$arr_columns['active'] = 1;
				$arr_columns['used'] = 0;
			}
		}
		return View::make('admin.voucher_one')->with(array('columns'=>$arr_columns,'message'=>$v_message,'icon'=>$arr_icons,'title'=>$v_title));
	}

	public function saveVoucher(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_help`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_voucher')?Input::get('action_voucher'):'';
		if($v_action=='new'){
			$voucher = new Voucher;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$voucher = Voucher::condition($arr_where)->first();
			$v_allow_edit = !is_null($voucher);
			if(!$v_allow_edit){
				$voucher = new Voucher;
				$v_action = 'new';
			} else
				Voucher::$rules['key'] .= ',key,'.$v_id;
		}
		$v = Voucher::validate(Input::all());
		$arr_columns['key'] = Input::has('key')?Input::get('key'):'';
		$arr_columns['value'] = Input::has('value')?Input::get('value'):0;
		$arr_columns['is_shipping_discount'] = Input::has('is_shipping_discount')?1:0;
		$arr_columns['valid_from'] = Input::has('valid_from')? date('Y-m-d',strtotime(Input::get('valid_from'))):'';
		$arr_columns['valid_to'] = Input::has('valid_to')?date('Y-m-d',strtotime(Input::get('valid_to'))):'';
		$arr_columns['active'] = Input::has('active')?1:0;
		$arr_columns['used'] = Input::has('used')?1:0;
		$v_passes = $v->passes();
		if($v_passes){
			$voucher->key = $arr_columns['key'];
			$voucher->value = $arr_columns['value'];
			$voucher->is_shipping_discount = $arr_columns['is_shipping_discount'];
			$voucher->valid_from = $arr_columns['valid_from'];
			$voucher->valid_to = $arr_columns['valid_to'];
			$voucher->active = $arr_columns['active'];
			$voucher->used = $arr_columns['used'];
		}else{
			$v_field_message = $v->messages()->first('key');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('value');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('is_shipping_discount');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('valid_from');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('valid_to');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('active');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('used');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_voucher_'.$v_id.'_';
		if($v_passes && $v_message==''){
			$admin = json_decode(Session::get('ss_admin'));
			if($v_action=='new'){
				$voucher->created_by = $voucher->modified_by = $admin->id;
				$voucher->save();
				$insertId = $voucher->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeVoucher($voucher)>0){
					if($v_allow_edit){
					$arr_columns['modified_by'] = $admin->id;
						$affectRow = $voucher->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/products/voucher');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/products/voucher/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/products/voucher/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public static function getPageVoucher(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			//$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			//$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Voucher::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'voucher'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$help = Voucher::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$help = Voucher::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($help){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($help as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
                    if(!isset($arr_content_group_name[$one->ghelp_id])){
                        $arr_where = array();
                        $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$one->ghelp_id);
                    }
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['key'] = isset($one->key)?$one->key:'';
					$arr_columns[$i]['value'] = isset($one->value)?$one->value:'';
					$arr_columns[$i]['valid_from'] = isset($one->valid_from)?$one->valid_from:'';
					$arr_columns[$i]['valid_to'] = isset($one->valid_to)?$one->valid_to:'';
                    $arr_columns[$i]['active'] = isset($one->active)?($one->active==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
                    $arr_columns[$i]['used'] = isset($one->used)?($one->used==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	private static function detectChangeVoucher(Voucher $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function getDeleteVoucher($blog){
		$arr_where = array();
		$arr_where[] = array('field'=>'id','operator'=>'=','value'=>$blog);
		$blog = Voucher::condition($arr_where)->get();
		if($blog)
			Voucher::condition($arr_where)->delete();
		return Redirect::to('/admin/products/voucher');
	}

	public function updateStatus(){
        $id = 	Input::has('txt_id')  ?  (int)Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  (int) Input::get('txt_value') : 0;
        $field = Input::has('txt_name')  ?  Input::get('txt_name') : 'publish';
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateVoucher(array($field=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateVoucher(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = Voucher::condition($arr_where)->update($arr_columns);
		else
			$v_rows = Voucher::update($arr_columns);
		return $v_rows;
	}

}