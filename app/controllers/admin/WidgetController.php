<?php
class WidgetController extends  AdminController{

/*	static $arr_setting = array();
	static $v_module_short_name = 'widget';


	public static function __setting(){
		Widget::LoadSetting();
		self::$arr_setting = Widget::$field_setting;
	}*/

	public function __contruct(){
		parrent::__contruct();
	}

	public static function getAllWiget(){
		$v_title = 'Widget - View all';
		$arr_icons = array();
		$arr_icons['new'] = 'static-content/widget';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('/admin/widget_all')->with(array('session'=>$v_session_id,'quick'=>$v_quick,'icon'=>$arr_icons,'title'=>$v_title,));
	}

	public static function getOne(array $arr_where, array $arr_order = array()){
		$widget = Widget::condition($arr_where)->sort($arr_order)->first();
		return $widget;
	}

	public function getOneWidget($id = 0){
		$v_title = 'Help - ';
		$arr_icons = array();
		$arr_icons['view'] = 'static-content/widget';
		$v_session = 'ss_save_help_'.$id.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$arr_columns = array();
			$help = self::getOne(array(array('field'=>'id','operator'=>'=','value'=>$id)), array(array('field'=>'id','asc'=>true)));
			if($help){
				$v_title .= 'Edit';
				$arr_columns['id'] = isset($help->id)?$help->id:0;
				$arr_columns['name'] = isset($help->name)?$help->name:'';
				$arr_columns['short_name'] = isset($help->short_name)?$help->short_name:'';
				$arr_columns['sub_title'] = isset($help->sub_title)?$help->sub_title:'';
				$arr_columns['link'] = isset($help->link)?$help->link:'';

				$arr_columns['image_link'] = isset($help->image)?('/assets/upload/'.$help->image):''; // image name
				if($help->image && file_exists(public_path().$arr_columns['image_link'])){
                    $arr_columns['image_link'] = Request::root()."/". $arr_columns['image_link'];
                }
                else{
                    $arr_columns['image_link'] = '';
                }
                $arr_columns['image'] = $help->image;
				$arr_columns['publish'] = isset($help->publish)?$help->publish:1;
			}else{
				$v_title .= 'New';
				$arr_columns['id'] = 0;
				$arr_columns['name'] = '';
				$arr_columns['short_name'] = '';
				$arr_columns['sub_title'] = '';
				$arr_columns['link'] = '';
				$arr_columns['image'] = '';
				$arr_columns['image_link'] = Request::root()."/assets/images/no_image.jpg";
				$arr_columns['publish'] = 1;
			}
		}
		return View::make('/admin/widget_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title
			)
		);
	}

	public function saveWidget(){
		$arr_columns = array();
		$v_message = '';
		$v_allow_edit = false;
		$v_id = 0;
		$arr_where = array();
		$v_index = 0;
		//You must remove row below, if there is not field `publish` in table `upt_help`
		//$arr_where[$v_index++] = array('field'=>'publish', 'operator'=>'=', 'value'=>1);
		$v_action = Input::has('action_help')?Input::get('action_help'):'';
		if($v_action=='new'){
			$help = new Widget;
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>0);
		}else{
			$v_id = Input::has('id')?Input::get('id'):'0';
			settype($v_id, 'int');
			$arr_where[$v_index] = array('field'=>'id', 'operator'=>'=', 'value'=>$v_id);
			$help = Widget::condition($arr_where)->first();
			$v_allow_edit = !is_null($help);
			if(!$v_allow_edit){
				$help = new Widget;
				$v_action = 'new';
			}
		}
		$v = Widget::validate(Input::all());
		$v_name = Input::has('name')?Input::get('name'):'';
		$arr_columns['name'] = $v_name;
		$v_short_name = Input::has('short_name')?Input::get('short_name'):'';
		$arr_columns['short_name'] = $v_short_name;
		$v_sub_title = Input::has('sub_title')?Input::get('sub_title'):'';
		$arr_columns['sub_title'] = $v_sub_title;
		$v_link = Input::has('link')?Input::get('link'):'';
		$arr_columns['link'] = $v_link;
        $arr_columns['image'] = Input::has('image')?Input::get('image'):'';  //   $arr_columns['image'] ='' hay  'oz.jpg'
        if(Input::hasFile('image_file')){
            $file = Input::file('image_file');
            $destinationPath = public_path().'/assets/upload/footer/';
            $filename = $file->getClientOriginalName();
            $uploadSuccess = Input::file('image_file')->move($destinationPath, $filename);
            if( $uploadSuccess ) {
                $arr_columns['image'] = $filename;
            }
        }
		$v_publish = Input::has('publish')?1:0;
		settype($v_publish,'int');
		$arr_columns['publish'] = $v_publish;
		$v_passes = $v->passes();
		if($v_passes){
			$help->name = $arr_columns['name'];
			$help->short_name = $arr_columns['short_name'];
			$help->link = $arr_columns['link'];
			$help->sub_title = $arr_columns['sub_title'];
			$help->image = $arr_columns['image'];
			$help->publish = $arr_columns['publish'];
		}else{
			$v_field_message = $v->messages()->first('name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('short_name');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('link');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('sub_title');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
			$v_field_message = $v->messages()->first('publish');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_help_'.$v_id.'_';
		if($v_passes && $v_message==''){
			parent::clearCached();
			if($v_action=='new'){
				$help->save();
				$insertId = $help->id;
				$v_result = $insertId > 0;
			}else{
				if(self::detectChangeHelp($help)>0){
					if($v_allow_edit){
						$affectRow = $help->condition($arr_where)->update($arr_columns);
						$v_result = $affectRow>0;
					}else{
						$v_message .= '<li>Current record not found!</li>';
					}
				}else{
					$v_result = true;
				}
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/static-content/widget');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/static-content/widget/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/static-content/widget/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}

	public function getPagWidget(){
		$v_quick_search = Input::has('quick')?Input::get('quick'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'name', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}else{
			//$arr_order[] = array('field'=>'orderno', 'asc'=>true);
			//$arr_order[] = array('field'=>'id', 'asc'=>false);
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_size, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_size < 10) $v_page_size = 10;
		$v_total_rows = Widget::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'help'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where, array $arr_order = array(), array $arr_fields = array()){
		$arr_columns = array();
		if($limit <= 0) $limit = 999999;
		$v_size_field = sizeof($arr_fields);
		if($v_size_field == 0)
			$help = Widget::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
		else
			$help = Widget::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($help){
			$i=0;
			$v_row = $offset;
            $arr_content_group_name = array();
			foreach($help as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
				if($v_size_field==0){
                    if(!isset($arr_content_group_name[$one->ghelp_id])){
                        $arr_where = array();
                        $arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$one->ghelp_id);
                    }
					$arr_columns[$i]['id'] = isset($one->id)?$one->id:0;
					$arr_columns[$i]['name'] = isset($one->name)?$one->name:'';
					$arr_columns[$i]['short_name'] = isset($one->short_name)?$one->short_name:'';
					$arr_columns[$i]['sub_title'] = isset($one->sub_title)?$one->sub_title:'';
					$arr_columns[$i]['link'] = isset($one->link)?$one->link:'';
                    $arr_columns[$i]['image'] = $one->image != ''?(Request::root().'/assets/upload/'.$one->image):Request::root().'/assets/images/no_image.jpg';
                    $arr_columns[$i]['publish'] = isset($one->publish)?($one->publish==1?'icon-unhide.png':'icon-hide.png'):'icon-hide.png';
				}else{
					for($j=0; $j<$v_size_field;$j++)
						$arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
				}
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getDeleteWidget($help){
		$arr_where = array();
		$arr_where[] = array('field'=>'id', 'operator'=>'=', 'value'=>$help);
		$help = Widget::condition($arr_where)->get();
		if($help) Widget::condition($arr_where)->delete();
		return Redirect::to('/admin/static-content/widget');
	}

	private static function detectChangeHelp(Widget $model){
		$v_return = 0;
		foreach($model->getDirty() as $attr=>$value){
			if($model->getOriginal($attr)!=$value) $v_return++;
		}
		return $v_return;
	}

	public function updatePublish(){
        $id = Input::has('txt_id')  ?  Input::get('txt_id') : 0;
        $new_ = Input::has('txt_value')  ?  Input::get('txt_value') : 0;
        settype($id,"int");
        $arr_return = array('error'=>1);
        if($id!=0){
            $rows = self::updateHelp(array('publish'=>$new_),array(array('field'=>'id','operator'=>'=','value'=>$id)));
            if($rows) $arr_return['error'] = 0;
        }
        $response = Response::json($arr_return);
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    private static function updateHelp(array $arr_columns, $arr_where){
		if(sizeof($arr_where)>0)
			$v_rows = Widget::condition($arr_where)->update($arr_columns);
		else
			$v_rows = Widget::update($arr_columns);
		return $v_rows;
	}

}