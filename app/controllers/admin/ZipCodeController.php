<?php
class ZipCodeController extends AdminController{
	static $v_module_group_short_name = 'system';
	static $v_module_short_name = 'zip-postal';
	static $arr_permit = array();

	/**
	 * constructor function
	 */
	public function __construct(){
	    parent::__construct();
	    $this->beforeFilter(function(){
	        return parent::checkModule(self::$v_module_group_short_name, self::$v_module_short_name, self::$arr_permit);
	    });
	}

	public function addZipPostal()
	{
		return self::getOneZipPostalView(0,true);
	}

	public static function index()
	{
		return self::getAllZipPostalView();
	}

	private static function getOneZipPostalView($zip = '', $p_edit = false){
		$v_title = 'Zip Postal - ';
		$arr_icons = array();
		$arr_icons['view'] = 'system/zip-postal';
		$v_session = 'ss_save_zip-postal_'.$zip.'_';
		$v_message = '';
		if(Session::has($v_session.'input') && Session::has($v_session.'message')){
			$v_columns = Session::get($v_session.'input');
			$arr_columns = json_decode($v_columns, true);
			$v_message = Session::get($v_session.'message');
			$v_title .= 'Saved with errors';
		}else{
			$option = '';
			if($zip)
				$option = self::getOne(array(array('field'=>'zip','operator'=>'=','value'=>$zip)), array(array('field'=>'zip','asc'=>true)));
			if($option){
				$v_title .= 'Edit';
				$arr_columns['zip'] = isset($option->zip)?$option->zip:0;
				$arr_columns['city'] = isset($option->city)?$option->city:'';
				$arr_columns['state'] = isset($option->state)?$option->state:'';
				$arr_columns['country'] = isset($option->country)?$option->country:'';
			}else{
				$v_title .= 'New';
				$arr_columns['zip'] = '';
				$arr_columns['city'] = '';
				$arr_columns['state'] = '';
				$arr_columns['country'] = '';
			}
		}
		return View::make('admin.zip_postal_one')->with(
			array(
				'columns'=>$arr_columns, 'message'=>$v_message, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit, 'edit'=>$p_edit
			)
		);
	}

	private static function getOne(array $arr_where, array $arr_order = array()){
		$option = ZipCode::condition($arr_where)->sort($arr_order)->first();
		return $option;
	}

	public static function getAllZipPostalView(){
        if(!isset(self::$arr_permit[_R_VIEW])) return Redirect::to('/admin/error');
		$v_title = 'Zip Postal - View All';
		$arr_icons = array();
		$arr_icons['new'] = 'system/zip-postal';
		$v_session_id = session_id();
		$v_quick = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		return View::make('admin.zip_postal_all')->with(
			array('session'=>$v_session_id, 'quick'=>$v_quick, 'icon'=>$arr_icons, 'title'=>$v_title, 'permit'=>self::$arr_permit)
		);
	}

	private static function getLimit($offset = 0, $limit = 0, array $arr_where = array(), array $arr_order = array(), array $arr_fields = array()){
		if(empty($arr_order))
			$arr_order[] = array('field'=>'zip','asc'=>true);
		$arr_columns = array();
        $v_property_field = sizeof($arr_fields);
        if($limit<=0) $limit = 999999;
        if($v_property_field==0)
		    $option = ZipCode::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get();
        else
            $option = ZipCode::condition($arr_where)->sort($arr_order)->skip($offset)->take($limit)->get($arr_fields);
		if($option){
			$i=0;
			$v_row = $offset;
			foreach($option as $one){
				$arr_columns[$i]['row_order'] = ++$v_row;
                if($v_property_field==0){
                   	$arr_columns[$i]['zip'] = isset($one->zip)?$one->zip:0;
					$arr_columns[$i]['city'] = isset($one->city)?$one->city:'';
					$arr_columns[$i]['state'] = isset($one->state)?$one->state:'';
					$arr_columns[$i]['country'] = 'Canada';//isset($one->country)?$one->country:'';
                }else{
                    for($j=0;$j<$v_property_field;$j++)
                        $arr_columns[$i][$arr_fields[$j]] = $one->$arr_fields[$j];
                }
				$i++;
			}
		}
		return $arr_columns;
	}

	public function getPageZipPostal(){
		$v_quick_search = Input::has('txt_quick_search')?Input::get('txt_quick_search'):'';
		//Create for where clause
		$arr_where = array();
		if($v_quick_search != ''){
			//Please replace 'field_search' by 'field' you want
			$arr_where[] = array('field'=>'zip', 'operator'=>'LIKE', 'value'=>'%'.$v_quick_search.'%');
		}
		//Create for order by
		$arr_order = array();
		$arr_tmp = Input::has('sort')?Input::get('sort'):array();
		if(is_array($arr_tmp) && sizeof($arr_tmp)>0){
			for($i=0; $i<sizeof($arr_tmp); $i++){
				$arr_order[] = array('field'=>$arr_tmp[$i]['field'], 'asc'=>$arr_tmp[$i]['dir']=='asc');
			}
		}
		//Create for page limit
		$v_page = Input::has('page')?Input::get('page'):1;
		$v_page_size = Input::has('pageSize')?Input::get('pageSize'):10;
		settype($v_page, 'int');
		settype($v_page_property, 'int');
		if($v_page < 1) $v_page = 1;
		if($v_page_property < 10) $v_page_property = 10;
		$v_total_rows = ZipCode::condition($arr_where)->sort($arr_order)->count();
		$v_total_pages = ceil($v_total_rows / $v_page_size);
		if($v_total_pages < 1) $v_total_pages = 1;
		if($v_total_pages < $v_page) $v_page = $v_total_pages;
		$v_skip = ($v_page - 1) * $v_page_size;
		$arr_columns = self::getLimit($v_skip, $v_page_size, $arr_where, $arr_order);
		$arr_return = array('total_rows'=>$v_total_rows, 'zip_postal'=>$arr_columns);
		$response = Response::json($arr_return);
		$response->header('Content-Type', 'application/json');
		return $response;
	}

    private static function updateZipPostal(array $arr_columns, $arr_where){
        if(isset(self::$arr_permit[_R_UPDATE])){
            if(sizeof($arr_where)>0)
                $v_rows = ZipCode::condition($arr_where)->update($arr_columns);
            else
                $v_rows = ZipCode::update($arr_columns);
            return $v_rows;
        }
        return 0;
    }

    public function editZipPostal($zip=''){
        if(isset(self::$arr_permit[_R_UPDATE]))
            return self::getOneZipPostalView($zip, true);
        return Redirect::to('/admin/error');
    }

    public function deleteZipPostal($option){
        if(isset(self::$arr_permit[_R_DELETE])){
            $arr_where = array();
            $arr_where[] = array('field'=>'zip', 'operator'=>'=', 'value'=>$option);
            $option = ZipCode::condition($arr_where)->get();
            if($option) ZipCode::condition($arr_where)->delete();
            return Redirect::to('/admin/system/zip-postal');
        }else return Redirect::to('/admin/error');
	}

    public function viewZipPostal($zip=0){
        if(isset(self::$arr_permit[_R_VIEW])){
            return self::getOneZipPostalView($zip);
        }else return Redirect::to('/admin/error');
    }

	public function saveZipPostal(){
        if(!isset(self::$arr_permit[_R_UPDATE]) || !isset(self::$arr_permit[_R_INSERT])) return Redirect::to('/admin/error');
		$arr_columns = array();
		$v_message = '';
		$zip = '';
		$arr_where = array();
		$v_index = 0;
		$v_action = Input::has('action_zip_postal')?Input::get('action_zip_postal'):'';
		if($v_action=='new'){
			$option = new ZipCode;
			$arr_where[$v_index] = array('field'=>'zip', 'operator'=>'=', 'value'=>0);
		}else{
			$zip = Input::has('zip')?Input::get('zip'):'0';
			$arr_where[$v_index] = array('field'=>'zip', 'operator'=>'=', 'value'=>$zip);
			$option = ZipCode::condition($arr_where)->first();
			if(!$option){
				$option = new ZipCode;
				$v_action = 'new';
			}
		}
		$v = ZipCode::validate(Input::all());
		$arr_columns['zip'] = Input::has('zip')?Input::get('zip'):'';
		$arr_columns['city'] = Input::has('city')?Input::get('city'):'';
		$arr_columns['state'] = Input::has('state')?Input::get('state'):'';
		$arr_columns['latitude'] = Input::has('latitude')?Input::get('latitude'):'';
		$arr_columns['longitude'] = Input::has('longitude')?Input::get('longitude'):'';
		$arr_columns['country'] = Input::has('country')?Input::get('country'):'';
		$v_passes = $v->passes();
		if($v_passes){
			$option->zip = $arr_columns['zip'];
			$option->city = $arr_columns['city'];
			$option->state = $arr_columns['state'];
			$option->latitude = $arr_columns['latitude'];
			$option->longitude = $arr_columns['longitude'];
			$option->country = $arr_columns['country'];
		}else{
			$v_field_message = $v->messages()->first('zip');
			if($v_field_message!='') $v_message .= '<li>'.$v_field_message.'</li>';
		}
		$v_session = 'ss_save_zip_postal_'.$v_id.'_';
		if($v_passes  && $v_message==''){
			if($v_action=='new'){
                if(isset(self::$arr_permit[_R_INSERT])){
				    $option->save();
				    $insertId = $option->id;
				    $v_result = $insertId > 0;
                }else $v_result = false;
			}else{
                if(isset(self::$arr_permit[_R_UPDATE])){
				    $affectRow = $option->condition($arr_where)->update($arr_columns);
				    //$v_result = $affectRow>0;
                    $v_result = true;
                }else $v_result = false;
			}
			if($v_result){
				if(Session::has($v_session.'input')) Session::forget($v_session.'input');
				if(Session::has($v_session.'message')) Session::forget($v_session.'message');
				return Redirect::to('/admin/system/zip-postal');
			}else{
				$v_message .= '<li>Cannot save (update). Unknown error!</li>';
				$arr_columns['id'] = $v_id;
				return Redirect::to('/admin/system/zip-postal/'.($v_id>0?$v_id.'/edit':'add'))->with(
					array($v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message)
				);
			}
		}else{
			$arr_columns['id'] = $v_id;
			return Redirect::to('/admin/system/zip-postal/'.($v_id>0?$v_id.'/edit':'add'))->with(
				array(
					$v_session.'input'=>json_encode($arr_columns), $v_session.'message'=>$v_message
				)
			);
		}
	}
}