<?php
$arr_setting = array();
$arr_setting['title'] = 'Configs';

$arr_setting['field'] = array(
	'id' => array(
		'title' =>'ID',
		'type' =>'int',
	),
	'name' => array(
		'list_view'=>'1',
		'display_type'=>'text',
		'title' =>'Name / Description',
		'type' =>'int',
		'width' =>'50px',
		'sortable' =>'true',
	),
	'key_id' => array(
		'display_type'=>'text',
		'list_view'=>'1',
		'title' =>'Key',
		'type' =>'int',
		'width' =>'20px',
		'sortable' =>'true',
		'template' =>'<span style="float:right">#= key_id #</span>',
	),
	'key_value' => array(
		'list_view'=>'1',
		'display_type'=>'text',
		'title' =>'Value',
		'type' =>'string',
		'width' =>'50px',
		'sortable' =>'true',
		'encoded' => 'false',
	),
	'type' => array(
		'list_view'=>'1',
		'display_type'=>'select',
		'title' =>'Type',
		'type' =>'string',
		'width' =>'50px',
		'sortable' =>'true',
		'encoded' => 'false',
	),
	'active' => array(
		'list_view'=>'1',
		'display_type'=>'checkbox',
		'title' =>'Active',
		'type' =>'int',
		'width' =>'20px',
		'sortable' =>'true',
		'template' =>'<p style="text-align:center; margin:0"><img class="change_status" @if(isset($permit[_R_UPDATE])) onclick=updatePublish(this,'+'"#= id#"'+') @endif id="icon_#= id#_publish" data-status="#= publish#" data-id="#= id#"  src="assets/images/icons/#= publish==1?\"icon-unhide.png\":\"icon-hide.png\"#" style="max-width:50px; cursor:pointer" /></p>',
	),
);

$AdminConfigsField = $arr_setting;


