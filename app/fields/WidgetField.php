<?php
$arr_setting = array();
$arr_setting['title'] = 'Widget';

$arr_setting['field'] = array(
	'No' => array(
		'list_view'=>'1',
		'title' =>'ID',
		'type' =>'int',
		'width' =>'20px',
		'sortable' =>'true',
		'template' =>'<span style="float:right">#= key_id #</span>',
	),
	'name' => array(
		'list_view'=>'1',
		'display_type'=>'text',
		'title' =>'Name / Description',
		'type' =>'int',
		'width' =>'50px',
		'sortable' =>'true',
	),
	'image' => array(
		'list_view'=>'1',
		'title' =>'Image',
		'type' =>'string',
		'width' =>'50px',
		'sortable' =>'true',
		'template' =>'<span style="float:right"><img src="#= image #" style="max-width:100px;" /></span>',
	),
	'name' => array(
		'list_view'=>'1',
		'display_type'=>'text',
		'title' =>'Name',
		'type' =>'int',
		'width' =>'50px',
		'sortable' =>'true',
	),
	'publish' => array(
	     'list_view' => '1',
	     'display_type' => 'text',
	     'title' => 'Publish',
	     'type' => 'string',
	     'width' => '50px',
	     'sortable' => 'true',
	     'template' => '<p style="text-align:center; margin:0"><img class="change_status" onclick=updatePublish('+'"#= id#"'+','+'"#= publish#"'+')  id="icon_#= id#" data-status="#= publish#" data-id="#= id#"  src="/assets/images/icons/#= publish#"'
	),
);


$WidgetField = $arr_setting;


