<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
/*
 * ------------------------------------------
 *  User Filter
 *  ------------------------------------------
 */
// Route::when('account', 'account');
// Route::when('account/*', 'account');
// Route::filter('account', function(){
//     if(!UserController::check()){
//         return Redirect::to('account/login');
//     }
// });
// Route::when('account/login', 'account_login');
// Route::filter('account_login', function(){
//     if(UserController::check())
//         return Redirect::to('/account');
// });
/*
 * ------------------------------------------
 *  Admin Filter
 *  ------------------------------------------
 */

Route::when('admin', 'admin');
Route::when('admin/*', 'admin');
Route::filter('admin', function(){
    if(strpos(Request::path(), 'login') === false && !AdminController::check())
        return Redirect::to('admin/login');
});
Route::when('admin/login', 'admin_login');
Route::filter('admin_login', function(){
    if(AdminController::check())
        return Redirect::to('/admin');
});

