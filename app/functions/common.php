<?php
	function pr($var)
	{
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}
	function setUnSignedName($p_fragment , $p_lower = true){
	    $translite_simbols = array (
	        '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
	        '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
	        '#(ì|í|ị|ỉ|ĩ)#',
	        '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
	        '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
	        '#(ỳ|ý|ỵ|ỷ|ỹ)#',
	        '#(đ)#',
	        '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
	        '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
	        '#(Ì|Í|Ị|Ỉ|Ĩ)#',
	        '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
	        '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
	        '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
	        '#(Đ)#',
	        "/[^a-zA-Z0-9\-\_\'\"\ ]/",
	    ) ;
	    $replace = array (
	        'a',
	        'e',
	        'i',
	        'o',
	        'u',
	        'y',
	        'd',
	        'A',
	        'E',
	        'I',
	        'O',
	        'U',
	        'Y',
	        'D',
	        '-',
	    ) ;
	    $v_fragment = preg_replace($translite_simbols, $replace, $p_fragment);
	    //$v_fragment = preg_replace("/(-)+'&/", '-', $v_fragment);
	    $v_fragment = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $v_fragment);
	    $v_temp = $p_lower?strtolower($v_fragment):$v_fragment;
	    $v_temp = str_replace(" ","-",$v_temp);
	    return $v_temp;
	}
	function convert_object_to_array($object,$field_name,$field_value){
	    $arr_return = array();
	    $arr_return[] = array(
	        'name'=>'Select'
	        ,'value'=>0
	    );
	    foreach($object as $key=>$arr){
	        $arr_return[] = array(
	            'name'=>$arr[$field_name]
	            ,'value'=>$arr[$field_value]
	        );
	    }
	    return $arr_return;
	}
	function convert_object_to_array_dropdown($object,$field_name,$field_value){
	    $arr_return = array();
	    foreach($object as $key=>$arr){
	        $arr_return[] = array(
	            'name'=>$arr[$field_name]
	            ,'value'=>$arr[$field_value]
	        );
	    }
	    return $arr_return;
	}
	function sortByOption($a, $b) {
   		return strcmp($a['name'], $b['name']);
 	}
 	function getToken( $type = 'alnum', $length = 20 ){
		switch ( $type ) {
			case 'alnum':
				$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'alpha':
				$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'hexdec':
				$pool = '0123456789abcdef';
				break;
			case 'numeric':
				$pool = '0123456789';
				break;
			case 'nozero':
				$pool = '123456789';
				break;
			case 'distinct':
				$pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
				break;
			default:
				$pool = (string) $type;
				break;
		}


		$crypto_rand_secure = function ( $min, $max ) {
			$range = $max - $min;
			if ( $range < 0 ) return $min; // not so random...
			$log    = log( $range, 2 );
			$bytes  = (int) ( $log / 8 ) + 1; // length in bytes
			$bits   = (int) $log + 1; // length in bits
			$filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
			do {
				$rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
				$rnd = $rnd & $filter; // discard irrelevant bits
			} while ( $rnd >= $range );
			return $min + $rnd;
		};

		$token = "";
		$max   = strlen( $pool );
		for ( $i = 0; $i < $length; $i++ ) {
			$token .= $pool[$crypto_rand_secure( 0, $max )];
		}
		return $token;
	}
?>