<?php
class Admin extends BaseModel{
	/*
	List of columns from table: "admin"
	id                   int(11)
	username             varchar(50)
	password             varchar(50)
	fullname             varchar(255)
	phone                text
	mobile               text
	email                text
	role_id              int(11)
	created_date         datetime
	modified_date        datetime
	is_active            tinyint(1)
	lastest_login        datetime
	*/
	protected $table = 'admin';
	public $timestamps = false;
	public $login = false;
    public $ip = '';
    public $restricted = true;

	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'username' => 'Required',
			'fullname' => 'Required',
            'email'    => 'Required|Email'
		);
		return Validator::make($input, $rules);
	}
}
?>