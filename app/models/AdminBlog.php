<?php
class AdminBlog extends BaseModel{
	/*
	List of columns from table: "blog"
	id                 int(11)
	name               varchar(255)
	short_name         varchar(255)
	publish            tinyint(0:1)
	title   			varchar(255)
	content 			text
	*/
	protected $table = 'blog';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
		    'name' => 'Required',
		    'short_name' => 'Required',
		    'title' =>'Required',
			'publish' => 'Integer',
			'order_no' => 'Integer'
		);
		return Validator::make($input, $rules);
	}
}
?>