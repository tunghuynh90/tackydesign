<?php
class AdminConfig extends BaseModel{
	/*
	List of columns from table: "upt_config"
	id                      int(11)
	title_site              text
	meta_keywords           text
	meta_description        text
	front_theme             varchar(255)
	created_date            datetime
	modified_date           datetime
	created_by              int(11)
	modified_by             int(11)
	*/
	protected $table = 'admin_config';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'created_by' => 'Integer|Min:0',
			'modified_by' => 'Integer|Min:0'
		);
		return Validator::make($input, $rules);
	}
}
?>