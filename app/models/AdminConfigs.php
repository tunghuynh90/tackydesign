<?php
class AdminConfigs extends BaseModel{
	
	protected $table = 'admin_configs';
	protected $guarded = array('id');
	public $timestamps = false;
	public static $field_setting = array();
	
	public static function LoadSetting($module_name=''){
		$_this = new self;
		$module_name = get_class($_this);
		$filename = $module_name.'Field';
		include(app_path().'/fields/'.$filename.'.php');
		self::$field_setting = $$filename;
	}

	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'active' => 'Required',
			'key_id' => 'Required',
		);
		return Validator::make($input, $rules);
	}
}
?>