<?php
class AdminContact extends BaseModel{
	protected $table = 'contact';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
		);
		return Validator::make($input, $rules);
	}
}
?>