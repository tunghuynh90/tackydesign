<?php
class AdminHelp extends BaseModel{
	/*
	List of columns from table: "static_content"
	id                 int(11)
	name               varchar(255)
	description        text
	publish            tinyint(1)
	*/
	protected $table = 'static_content';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'ghelp_id' => 'Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'orderno' => 'Integer|Min:0'
		);
		return Validator::make($input, $rules);
	}
}
?>