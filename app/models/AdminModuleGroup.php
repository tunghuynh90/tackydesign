<?php
class AdminModuleGroup extends BaseModel{
	/*
	List of columns from table: "admin_module_group"
	id                    int(11)
	module_type_id        int(11)
	name                  text
	short_name            text
	publish               tinyint(1)
	orderno               int(11)
	*/
	protected $table = 'admin_module_group';
	protected $guarded = array('id');
	public $timestamps = false;

	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'module_type_id' => 'Integer|Min:0',
			'name' => 'Required',
			'publish' => 'Integer',
			'orderno' => 'Integer',
		);
		return Validator::make($input, $rules);
	}
}
?>