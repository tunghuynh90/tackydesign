<?php
class AdminRole extends BaseModel{
	/*
	List of columns from table: "admin_role"
	id                   int(11)
	name                 varchar(255)
	short_name           varchar(50)
	permit_grant         text
	is_restricted        tinyint(1)
	publish              tinyint(1)
	orderno              int(11)
	*/
	protected $table = 'admin_role';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'is_restricted' => 'Integer|Min:0',
            'name'      =>  'Required',
			'orderno' => 'Integer'
		);
		return Validator::make($input, $rules);
	}
}
?>