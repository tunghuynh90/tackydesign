<?php
class BaseModel extends \Eloquent{
	protected $connection = SERVER;

	/**
	 * Scope condition
	 * @param $query
	 * @param array $arr_where
	 * @example for $arr_where
	 * 		$arr_where = array();
	 * 		$arr_where[0] = array('field'=>'field1', 'value'=>1);
	 * 		$arr_where[1] = array('field'=>'field2','operator'=>'>' 'value'=>10);
	 * 		$arr_where[2] = array('field'=>'field3','operator'=>'LIKE' 'value'=>'%abc%', 'or');
	 * 		$arr_where[3] = array('field'=>'field4','operator'=>'in' 'value'=>array(2,4,6));
	 * @return query
	 */
	public function scopeCondition($query, array $arr_where = array()){
		for($i=0;$i<sizeof($arr_where);$i++){
			if(isset($arr_where[$i]['or'])){
				if(!isset($arr_where[$i]['operator'])){
					$query = $query->orWhere($arr_where[$i]['field'],'=',$arr_where[$i]['value']);
				}else{
					if(strtolower($arr_where[$i]['operator'])=='in'){
						$where = $arr_where[$i];
						$query = $query->orWhere(function($query) use($where){
							$query->whereIn($where['field'], is_array($where['value'])?$where['value']:array($where['value']));
						});
					}else if(strtolower($arr_where[$i]['operator'])=='notin'){
						$where = $arr_where[$i];
						$query = $query->orWhere(function($query) use($where){
							$query->whereNotIn($where['field'], is_array($where['value'])?$where['value']:array($where['value']));
						});
					}else{
						$query = $query->orWhere($arr_where[$i]['field'],$arr_where[$i]['operator'],$arr_where[$i]['value']);
					}
				}
			}else{
				if(!isset($arr_where[$i]['operator'])){
					$query = $query->where($arr_where[$i]['field'],'=',$arr_where[$i]['value']);
				}else{
					if(strtolower($arr_where[$i]['operator'])=='in')
						$query = $query->whereIn($arr_where[$i]['field'],is_array($arr_where[$i]['value'])?$arr_where[$i]['value']:array($arr_where[$i]['value']));
					else if(strtolower($arr_where[$i]['operator'])=='notin')
						$query = $query->whereNotIn($arr_where[$i]['field'],is_array($arr_where[$i]['value'])?$arr_where[$i]['value']:array($arr_where[$i]['value']));
					else
						$query = $query->where($arr_where[$i]['field'],$arr_where[$i]['operator'],$arr_where[$i]['value']);
					}
				}
			}
		return $query;
	}

    /**
     * Scope sort
     * @param $query
     * @param array $arr_order
     * @return query
     */
    public function scopeSort($query, array $arr_order = array()){
        for($i=0;$i<sizeof($arr_order);$i++){
            $query = $query->orderBy($arr_order[$i]['field'],$arr_order[$i]['asc']?'asc':'desc');
        }
        return $query;
    }
}
?>