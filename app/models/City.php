<?php
class City extends BaseModel{
	/*
	List of columns from table: "upt_city"
	id                int(11)
	name              varchar(255)
	short_name        varchar(50)
	zipcode           varchar(10)
	country_id        int(11)
	publish           tinyint(1)
	orderno           int(11)
	*/
	protected $table = 'city';
	protected $guarded = array('id');
	public $timestamps = false;

	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'country_id' => 'Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'orderno' => 'Integer|Min:0'
		);
		return Validator::make($input, $rules);
	}
}
?>