<?php
class Country extends BaseModel{
	/*
	List of columns from table: "country"
	id                int(11)
	name              varchar(255)
	short_name        varchar(50)
	publish           tinyint(1)
	orderno           int(11)
	*/
	protected $table = 'country';
	protected $guarded = array('id');
	public $timestamps = false;

	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'orderno' => 'Integer|Min:0'
		);
		return Validator::make($input, $rules);
	}
}
?>