<?php
class CustomerService extends BaseModel{
	/*
	List of columns from table: "static_content"
	id                 int(11)
	name               varchar(255)
	description        text
	publish            tinyint(1)
	*/
	protected $table = 'customer_service';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'name' => 'Required',
			'short_name' => 'Required',
		);
		return Validator::make($input, $rules);
	}
}
?>