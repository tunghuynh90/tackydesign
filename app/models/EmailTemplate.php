<?php
class EmailTemplate extends BaseModel{
	/*
	List of columns from table: "email_template"
	id                 int(5)
	type               varchar(255)
	content        		text
	publish            tinyint(1)
	*/
	protected $table = 'email_template';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
		);
		return Validator::make($input, $rules);
	}
}
?>