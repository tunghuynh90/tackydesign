<?php
class Lookbook extends BaseModel{
	/*
	List of columns from table: "product_category"
	id				int(15)
	name			varchar(150)
	short_name		varchar(150)
	main_image		text
	more_image		text
	publish			tinyint(1)
	*/
	protected $table = 'lookbook';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'name'=>'Required',
			'short_name'=>'Required',
			'publish' => 'Integer|Min:0',
		);
		return Validator::make($input, $rules);
	}
}
?>