<?php
class Newsletter extends BaseModel{
	/*
	List of columns from table: "newsletter"
	id                      int(5)
	content              	text
	*/
	protected $table = 'newsletter';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
		);
		return Validator::make($input, $rules);
	}
}