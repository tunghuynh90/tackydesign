<?php
class Order extends BaseModel{
	/*
	List of columns from table: "order"
	id                 	int(5)
	user_id     		int(5)
	email        		varchar(250)
	billing_address  	text
	shipping_address  	text
	status 				tinyint(1)
	total            	float
	voucher            	varchar(250)
	token            	varchar(250)
	created_date    	datetime
	modified_date    	timestamp
	*/
	protected $table = 'order';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
		    'email' => 'Required',
		);
		return Validator::make($input, $rules);
	}
}
?>