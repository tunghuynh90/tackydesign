<?php
class OrderDetail extends BaseModel{
	/*
	List of columns from table: "order"
	id                 	int(5)
	order_id     		int(5)
	product_id        	int(5)
	quantity  			int(11)
	sell_price  		float
	image				text
	*/
	protected $table = 'order_detail';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
		    'order_id' => 'Interger|Required',
		    'product_id' => 'Interger|Required',
		);
		return Validator::make($input, $rules);
	}
}
?>