<?php
class Product extends BaseModel{
	/*
	List of columns from table: "product_category"
	id				int(15)
	sku				varchar(150)
	name			varchar(150)
	short_name		varchar(150)
	gift_card		tinyint(1)
	bigger_price	float
	sell_price		float
	is_sale			tinyint(1)
	is_sold_out		tinyint(1)
	main_image		varchar(255)
	more_image		varchar(255)
	size			text
	material		text
	color			text
	weight			float
	description		text
	publish			tinyint(1)
	category_id		int(15)
	*/
	protected $table = 'product';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'sku'=>'Required',
			'name'=>'Required',
			'short_name'=>'Required',
			'publish' => 'Integer|Min:0',
		);
		return Validator::make($input, $rules);
	}
}
?>