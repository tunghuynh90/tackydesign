<?php
class ProductCategory extends BaseModel{
	/*
	List of columns from table: "product_category"
	id                 int(11)
	name               varchar(255)
	short_name         varchar(50)
	image              text
	description        text
	publish            tinyint(1)
	parent_id          int(11)
	*/
	protected $table = 'product_category';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'orderno' => 'Integer|Min:0'
		);
		return Validator::make($input, $rules);
	}
}
?>