<?php
class ProductOption extends BaseModel{
	/*
	List of columns from table: "product_option"
	id                	int(11)
	name               	varchar(50)
	key         		varchar(50)
	*/
	protected $table = 'product_option';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'name' => 'Required',
			// 'key' => 'Required',
		);
		return Validator::make($input, $rules);
	}
}
?>