<?php
class ProductOptionPrice extends BaseModel{
	/*
	List of columns from table: "product_option_price"
	id                 int(5)
	product_id         int(5)
	product_option_id  int(5)
	default            int(5)
	sell_price	       float
	bigger_price	   float
	created_by         int(5)
	modified_by        int(5)
	modified_date      timestamp
	*/
	protected $table = 'product_option_price';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			// 'id' => 'Required',
			'product_id' => 'Required|Integer',
			'product_option_id' => 'Integer',
		);
		return Validator::make($input, $rules);
	}
}
?>