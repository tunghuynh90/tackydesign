<?php
class ProductProperty extends BaseModel{
	/*
	List of columns from table: "product_property"
	id                	int(11)
	name               	varchar(50)
	key         		varchar(50)
	type 				varchar(50)
	*/
	protected $table = 'product_property';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			// 'name' => 'Required',
			// 'key' => 'Required',
			// 'type' => 'Required',
		);
		return Validator::make($input, $rules);
	}

	public static function getEnumValues($origin = false)
	{
        $query = 'SHOW COLUMNS FROM product_property WHERE Field = \'type\'';
		$type = DB::select($query);
		$type = $type[0]->Type;
	    preg_match('/^enum\((.*)\)$/', $type, $matches);
	    if($origin)
	    	foreach( explode(',', $matches[1]) as $value ){
		        $enum[] = $value = trim( $value, "'" );
		    }
	    else
		    foreach( explode(',', $matches[1]) as $value ){
		    	$value = trim( $value, "'" );
		        $enum[] = array('value'=>$value,'name'=>strtoupper($value));
		    }
	    return $enum;
	}
}
?>