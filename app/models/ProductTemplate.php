<?php

class ProductTemplate extends BaseModel {
	/*
		id
		template_id
		product_id
		publish
		assigned_time
		template_site
	*/

	protected $table = 'product_template';
	public $timestamps = false;
}
