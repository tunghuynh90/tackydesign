<?php
class ShipPrice extends BaseModel{
	/*
	List of columns from table: "ship_price"
	id                  int(10) unsigned
	country             int(10)
	publish         	int(1)
	*/
	protected $table = 'ship_price';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'country_id' => 'Required|Integer',
		);
		return Validator::make($input, $rules);
	}
}
?>