<?php
class ShipPriceDetail extends BaseModel{
	/*
	List of columns from table: "ship_price_detail"
	id                 int(5)
	shipping_method	   varchar(50)
	shipping_price	   float
	ship_price_id      int(5)
	*/
	protected $table = 'ship_price_detail';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required',
			'ship_price_id' => 'Integer|Required',
		);
		return Validator::make($input, $rules);
	}
}
?>