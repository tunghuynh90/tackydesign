<?php
class Slide extends BaseModel{
	/*
	List of columns from table: "slide"
	id                 int(11)
	image               varchar(255)
	publish            tinyint(1)
	publish            int(5)
	*/
	protected $table = 'slide';
	protected $guarded = array('id');
	public $timestamps = false;


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			// 'id' => 'Required',
			'image' => 'Required',
			'publish' => 'Integer',
			'order_no' => 'Integer'
		);
		return Validator::make($input, $rules);
	}
}
?>