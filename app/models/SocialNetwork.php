<?php
class SocialNetwork extends BaseModel{
	/*
	List of columns from table: "social_network"
	id                 int(5)
	name               varchar(50)
	short_name         varchar(50)
	view_widget         	text
	share_widget         	text
	on_home 			tinyint(1)
	publish            tinyint(1)
	*/
	protected $table = 'social_network';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
		    'name' => 'Required',
		    'short_name' => 'Required',
			'publish' => 'Integer',
		);
		return Validator::make($input, $rules);
	}
}
?>