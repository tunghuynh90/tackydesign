<?php
class Tax extends BaseModel{
	/*
	List of columns from table: "ship_price"
	id                  int(10) unsigned
	city_id             int(10) unsigned
	tax          decimal(10,2) unsigned
	description         text
	*/
	protected $table = 'tax';
	protected $guarded = array('id');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'city_id' => 'Required|Integer|Min:0',
			'tax' => 'Required|Numeric|Min:0',
		);
		return Validator::make($input, $rules);
	}
}
?>