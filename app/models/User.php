<?php
use Zizaco\Confide\ConfideUser;

class User extends ConfideUser {
	 public static $rules = array(
        'email' => 'required|email|unique:user',
        'firstname' => 'required',
        'lastname' => 'required',
        'password' => 'required|Min:4',
        'password_confirmation' => 'Min:4',
    );
	protected $table = 'user';

	 public function beforeSave($forced = false){
        if ( empty($this->id) )
        {
           // $this->confirmation_code = md5( uniqid(mt_rand(), true) );
        }

        /*
         * Remove password_confirmation field before save to
         * database.
         */

        return true;
    }
}