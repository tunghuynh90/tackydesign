<?php
class UserMember extends BaseModel{
	/*
	List of columns from table: "upt_user_member"
	id                       int(11)
	email                    varchar(50)
	password                 varchar(100)
	firstname                varchar(255)
	lastname                 varchar(255)
	confirmed                tinyint(1)
	lastest_login            datetime
	created_at               datetime
	updated_at               datetime
	*/
	protected $table = 'user';
	protected $guarded = array('id');
	public $timestamps = false;
	public static $rules = array();

	public function __construct()
	{
		self::$rules = array(
			'id' => 'Required|Integer',
			'email' => 'Required|email|unique:user',
			'firstname' => 'required',
        	'lastname' => 'required',
		);
		parent::__construct();
	}

	public static function validate($input)
	{

		return Validator::make($input, self::$rules);
	}
}
?>