<?php
class Voucher extends BaseModel{
	/*
	List of columns from table: "voucher"
	id                 	int(4)
	key               	varchar(250)
	value            	float
	valid_from       	datetime
	valid_to         	datetime
	active            	tinyint(1)
	used            	tinyint(1)
	created_by        	int(5)
	modified_by       	int(5)
	modified_date   	timestamp
	*/
	protected $table = 'voucher';
	protected $guarded = array('id');
	public $timestamps = false;
	public static $rules = array();

	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public function __construct()
	{
		self::$rules = array(
			'id' => 'Required',
			'key' => 'Required|unique:voucher',
		);
		parent::__construct();
	}

	public static function validate($input){
		return Validator::make($input, self::$rules);
	}
}
?>