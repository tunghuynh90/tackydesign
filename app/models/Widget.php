<?php
class Widget extends BaseModel{
	/*
	List of columns from table: "static_content"
	id                 int(11)
	name               varchar(255)
	description        text
	publish            tinyint(1)
	*/
	protected $table = 'widget';
	protected $guarded = array('id');
	public $timestamps = false;

	public static $field_setting = array();
	public static function LoadSetting($module_name=''){
		$_this = new self;
		$module_name = get_class($_this);
		$filename = $module_name.'Field';
		include(app_path().'/fields/'.$filename.'.php');
		self::$field_setting = $$filename;
	}


	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
			'id' => 'Required|Integer|Min:0',
			'publish' => 'Integer|Min:0',
			'name' => 'Required',
			'short_name' => 'Required',
		);
		return Validator::make($input, $rules);
	}
}
?>