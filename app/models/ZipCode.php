<?php
class ZipCode extends BaseModel{
	/*
	List of columns from table: "blog"
	zip                varchar(7)
	city               varchar(50)
	state         		varchar(50)
	latitude            varchar(50)
	longtitude   		varchar(50)
	country 			int(5)
	*/
	protected $table = 'zipcodes';
	protected $guarded = array('zip');
	public $timestamps = false;
	/**
	 * Check validation input, see more at: http://laravel.com/docs/validation
	 * @param mixed $input
	 * @return Validator
	 */
	public static function validate($input){
		$rules = array(
		    'zip' => 'Required',
		    'city' => 'Required',
		    'state' =>'Required',
			'country' => 'Integer',
		);
		return Validator::make($input, $rules);
	}
}
?>