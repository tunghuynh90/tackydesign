<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
//==================================FRONTEND========================================
Route::pattern('page', '[0-9]+');
Route::get('/',                                     'HomeController@index');
Route::get('/404',                                  'HomeController@pageNotFound');
Route::get('/clear',                                'AdminController@clearCached');
Route::post('/newsletter',                          'HomeController@newsletter');
Route::get('/confirm-order/{confirm_code}',         'CartController@confirmCode');
Route::get('/unsubcriber/{user_member_id}',        'HomeController@unsubcriber');
/*
 * ------------------------------------------
 *  Collection Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'collections'), function()
{
    Route::pattern('page', '[0-9]+');
    Route::pattern('collection_name', '[\w\-]+');
    Route::pattern('collection_name2', '[\w\-]+');

    Route::get('/',                                         'HomeController@collectionAll');
    Route::get('/{collection_name}/products/{product_id}',  'HomeController@collectionDetail');
    Route::get('/{collection_name}/{collection_name2}/page/{page}',array(
                                                'as'    =>  'pageWithCollection',
                                                'uses'  =>  'HomeController@collections') );
    Route::get('/{collection_name}/{collection_name2}',     'HomeController@collections');
    Route::get('/{collection_name}/page/{page}',array(
                                                'as'    =>  'page',
                                                'uses'  =>  'HomeController@collections') );
    Route::get('/{collection_name}', 'HomeController@collections');
});
Route::get('/products/{product_id}',  'HomeController@products');
/*
 * ------------------------------------------
 *  Cart Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'cart'), function()
{
    Route::match(array('GET','POST'),'/',               'CartController@cart');
    Route::post('/add',                                 'CartController@add');
    Route::get('/getcart',                              'CartController@getCart');
    Route::get('/delete-cart/{row_id}',                 'CartController@deleteCart');
    Route::post('/get-ship-price',                      'CartController@getShipPrice');
    Route::post('/get-promo-code',                      'CartController@getPromoCode');
    Route::get('/add-design',                           'CartController@addDesign');
    Route::post('/shipping-price',                      'CartController@changeShippingPrice');
    Route::post('/change-shipping-method',              'CartController@changeShippingMethod');

});


/*
 * ------------------------------------------
 *  User Routes
 *  ------------------------------------------
 */
Route::get('/account',function(){
    return Redirect::to('/user');
});
Route::group(array('prefix' => 'user'), function()
{
    Route::match(array('GET','POST'),'/',                 'UserController@login');
    Route::get('/details',                                'UserController@details');
    Route::match(array('GET','POST'),'/addresses',        'UserController@addresses');
    Route::get('/delete-address/{address_key}',           'UserController@deleteAddress');
    Route::match(array('GET','POST'),'/create',           'UserController@create');
    Route::get('/confirm/{code}',                         'UserController@confirm');
    Route::match(array('GET','POST'),'/login',            'UserController@login');
    Route::post('/login',                                 'UserController@do_login');
    Route::get( '/forgot_password',                       'UserController@forgot_password');
    Route::post('/forgot_password',                       'UserController@do_forgot_password');
    Route::get( '/reset/{token}',                         'UserController@reset_password');
    Route::post('/reset',                                 'UserController@do_reset_password');
    Route::get('/logout',                                 'UserController@logout');
    Route::get('/logout',                                 'UserController@logout');
});
/*
 * ------------------------------------------
 *  Design Online Routes
 *  ------------------------------------------
 */
Route::match(array('GET','POST'),'/design-online/template', 'HomeController@loadTemplate');
Route::get('/add-image', 'HomeController@loadAddImage');
/*
 * ------------------------------------------
 *  Page static
 *  ------------------------------------------
 */
Route::pattern('page_name', '[\w\-]+');
Route::match(array('GET','POST'),'/pages/{page_name}',  'HomeController@getStaticPage');
Route::get('/blog','HomeController@getAllBlog');
Route::get('blog/page/{page}','HomeController@getAllBlog');
Route::get('/blog-detail/{blog_id}',            'HomeController@getBlogDetail');
Route::get('/lookbooks',                        'HomeController@getFirstLookbook');
Route::get('/lookbooks/{lookbook_name}',        'HomeController@lookbooks');
Route::get('/moodboard',                        'HomeController@moodboard');
Route::get('/moodboard/{moodboard_name}',       'HomeController@moodboard');


Route::get('/checkout',                                     'CartController@checkout');
Route::post('/checkout', array('before'=>'csrf', 'uses' =>  'CartController@checkout'));

//Route::match(array('GET','POST'),'/search/{name}', 'HomeController@search');
Route::match(array('GET','POST'),'/search', 'HomeController@getAllSearch');
Route::match(array('GET','POST'),'/search/{page}', 'HomeController@getAllSearch');
Route::match(array('GET','POST'),'/search/{page}/{name}', 'HomeController@getAllSearch');

//===================================BACKEND========================================
/*
 * ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin'), function()
{
    Route::get('/', 'AdminController@index');
    Route::match(array('GET', 'POST'),'login',                  'AdminController@login');
    Route::match(array('GET', 'POST'),'logout',                 'AdminController@logout');
    /*
     * -------------------------------------------
     *  Image Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/images/listImages',     'AdminImageController@listImages');
    Route::post('/images/deleteFile',                           'AdminImageController@deleteFile');
    Route::match(array('GET', 'POST'),'/images/uploadImage',    'AdminImageController@uploadImage');
    Route::match(array('GET', 'POST'),'/images/thumbImage',     'AdminImageController@thumbImage');
    /*
     * -------------------------------------------
     *  Account Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/account',               'AdminController@account');
     /*
     * ------------------------------------------
     *  Config Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/system/configuration',  'AdminConfigController@getEditConfig');
	/*
     * ------------------------------------------
     *  Config Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/system/configs',         'AdminConfigsController@ListView');
    Route::get('/system/configs/add',                            'AdminConfigsController@AddItem');
    Route::post('/system/configs/save',                          'AdminConfigsController@saveConfigs');
    Route::post('/system/configs/json',                          'AdminConfigsController@getPageConfigs');
    Route::post('/system/configs/ajax',                          'AdminConfigsController@getAjaxConfigs');
    #
    Route::pattern('configs_id', '[0-9]+');
    Route::get('/system/configs/{configs_id}/edit',               'AdminConfigsController@AddItem');
    Route::get('/system/configs/{configs_id}/delete',             'AdminConfigsController@getDeleteConfigs');

	 /*
     * ------------------------------------------
     *  Module Group Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/system/module-group',   'AdminModuleGroupController@getAllModuleGroup');
    Route::get('/system/module-group/add',                      'AdminModuleGroupController@getOneModuleGroup');
    Route::post('/system/module-group/save',                    'AdminModuleGroupController@saveModuleGroup');
    Route::post('/system/module-group/json',                    'AdminModuleGroupController@getPageModuleGroup');
    Route::post('/system/module-group/ajax',                    'AdminModuleGroupController@getAjaxModuleGroup');
    #
    Route::pattern('module_group_id', '[0-9]+');
    Route::get('/system/module-group/{module_group}/delete',    'AdminModuleGroupController@getDeleteModuleGroup');
    Route::get('/system/module-group/{module_group_id}/edit',   'AdminModuleGroupController@getOneModuleGroup');
    /*
     * ------------------------------------------
     *  Module Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/system/module',         'AdminModuleController@getAllModule');
    Route::get('/system/module/add',                            'AdminModuleController@getOneModule');
    Route::post('/system/module/save',                          'AdminModuleController@saveModule');
    Route::post('/system/module/json',                          'AdminModuleController@getPageModule');
    Route::post('/system/module/ajax',                          'AdminModuleController@getAjaxModule');
    #
    Route::pattern('module_id', '[0-9]+');
    Route::get('/system/module/{module_id}/edit',               'AdminModuleController@getOneModule');
    Route::get('/system/module/{module_id}/delete',             'AdminModuleController@getDeleteModule');
    /*
     * -------------------------------------------
     *  Country Category Content
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/system/country',     'CountryController@getAllCountry');
    Route::get('/system/country/add',                       'CountryController@getAddCountry');
    Route::post('/system/country/save',                     'CountryController@saveCountry');
    Route::post('/system/country/json',                     'CountryController@getPageCountry');
     #
    Route::pattern('country_id', '[0-9]+');
    Route::get('/system/country/{country_id}/delete',             'CountryController@getDeleteCountry');
    Route::get('/system/country/{country_id}/edit',               'CountryController@getOneCountry');
    Route::post('/system/country/updatePublish',            'CountryController@updatePublish');
    /*
     * -------------------------------------------
     *  City Category Content
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/system/province-state',     'CityController@getAllCity');
    Route::get('/system/province-state/add',                       'CityController@getAddCity');
    Route::post('/system/province-state/save',                     'CityController@saveCity');
    Route::post('/system/province-state/json',                     'CityController@getPageCity');
     #
    Route::pattern('city_id', '[0-9]+');
    Route::get('/system/province-state/{city_id}/delete',             'CityController@getDeleteCity');
    Route::get('/system/province-state/{city_id}/edit',               'CityController@getEditCity');
    Route::post('/system/province-state/updatePublish',               'CityController@updatePublish');

    /*
     * -------------------------------------------
     *  Zip Postal Route
     *  -----------------------------------------
     */
    Route::match(array('GET', 'POST'),'/system/zip-postal',   'ZipCodeController@index');
    Route::get('/system/zip-postal/add',                      'ZipCodeController@create');
    Route::post('/system/zip-postal/update',                    'ZipCodeController@update');
    Route::post('/system/zip-postal/delete',                    'ZipCodeController@delete');
    Route::post('/system/zip-postal/json',                    'ZipCodeController@getPageZipPostal');
    #
    Route::get('/system/zip-postal/{zip}/view',       'ZipCodeController@viewZipPostal');
    Route::get('/system/zip-postal/{zip}/edit',       'ZipCodeController@editZipPostal');
    Route::get('/system/zip-postal/{zip}/delete',     'ZipCodeController@deleteZipPostal');
    /*
     * -------------------------------------------
     *  Product Category Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/products/categories',   'ProductCategoryController@index');
    Route::get('/products/categories/add',                      'ProductCategoryController@addCategory');
    Route::post('/products/categories/save',                    'ProductCategoryController@saveCategory');
    Route::post('/products/categories/json',                    'ProductCategoryController@getPageCategory');
    Route::post('/products/categories/update-status',           'ProductCategoryController@updateStatus');
    #
    Route::pattern('category_id', '[0-9]+');
    Route::get('/products/categories/{category_id}/view',       'ProductCategoryController@viewCategory');
    Route::get('/products/categories/{category_id}/edit',       'ProductCategoryController@editCategory');
    Route::get('/products/categories/{category_id}/delete',     'ProductCategoryController@deleteCategory');
    /*
     * -------------------------------------------
     *  Product Property Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/products/properties',   'ProductPropertyController@index');
    Route::get('/products/properties/add',                      'ProductPropertyController@create');
    Route::post('/products/properties/update',                    'ProductPropertyController@update');
    Route::post('/products/properties/delete',                    'ProductPropertyController@delete');
    Route::post('/products/properties/json',                    'ProductPropertyController@getPageProperty');
    #
    Route::pattern('property_id', '[0-9]+');
    Route::get('/products/properties/{property_id}/view',       'ProductPropertyController@viewProperty');
    Route::get('/products/properties/{property_id}/edit',       'ProductPropertyController@editProperty');
    Route::get('/products/properties/{property_id}/delete',     'ProductPropertyController@deleteProperty');
    /*
     * -------------------------------------------
     *  Product Option Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/products/option',   'ProductOptionController@index');
    Route::get('/products/option/add',                      'ProductOptionController@addOption');
    Route::post('/products/option/save',                    'ProductOptionController@saveOption');
    Route::post('/products/option/json',                    'ProductOptionController@getPageOption');
    #
    Route::pattern('option_id', '[0-9]+');
    Route::get('/products/option/{option_id}/view',       'ProductOptionController@viewOption');
    Route::get('/products/option/{option_id}/edit',       'ProductOptionController@editOption');
    Route::get('/products/option/{option_id}/delete/',     'ProductOptionController@deleteOption');
    /*
     * -------------------------------------------
     *  Product Option Routes
     *  ------------------------------------------
     */
    Route::post('/products/product-option-price/list',              'ProductOptionPriceController@lists');
    Route::post('/products/product-option-price/add',               'ProductOptionPriceController@create');
    Route::post('/products/product-option-price/update',               'ProductOptionPriceController@update');
    Route::get('/products/product-option-price/delete',               'ProductOptionPriceController@delete');
    Route::post('/products/product-option-price/clone-option',         'ProductOptionPriceController@cloneOption');
    /*
     * -------------------------------------------
     *  Product Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/products/products',     'ProductController@index');
    Route::get('/products/products/add',                        'ProductController@addProduct');
    Route::post('/products/products/save',                      'ProductController@saveProduct');
    Route::post('/products/products/json',                      'ProductController@getPageProduct');
    Route::post('/products/products/update-status',             'ProductController@updateStatus');
    Route::post('/products/products/ajax',                      'ProductController@getAjaxProducts');
    Route::post('/products/products/list-products',             'ProductController@listProduct');

    #
    Route::pattern('product_id', '[0-9]+');
    Route::get('/products/products/{product_id}/view',          'ProductController@viewProduct');
    Route::get('/products/products/{product_id}/edit',          'ProductController@editProduct');
    Route::get('/products/products/{product_id}/delete',        'ProductController@deleteProduct');
    /*
     * -------------------------------------------
     *  Product Shiping Price Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/products/shipping-price',       'ShipPriceController@getAllShipPrice');
    Route::get('/products/shipping-price/add',                          'ShipPriceController@getAddShipPrice');
    Route::post('/products/shipping-price/save',                        'ShipPriceController@saveShipPrice');
    Route::post('/products/shipping-price/json',                        'ShipPriceController@getPageShipPrice');
    Route::post('/products/shipping-price/updatePublish',               'ShipPriceController@updatePublish');
    #
    Route::pattern('ship_price', '[0-9]+');
    Route::get('/products/shipping-price/{ship_price}/edit',            'ShipPriceController@getEditShipPrice');
    Route::post('/products/shipping-price/{ship_price}/get-options',     'ShipPriceController@getOptions');
    Route::post('/products/shipping-price/{ship_price}/add-options',     'ShipPriceController@addOptions');
    Route::post('/products/shipping-price/{ship_price}/update-options',  'ShipPriceController@updateOptions');
    Route::post('/products/shipping-price/{ship_price}/delete-options',  'ShipPriceController@deleteOptions');
    Route::get('/products/shipping-price/{ship_price}/view',            'ShipPriceController@getViewShipPrice');
    Route::get('/products/shipping-price/{ship_price}/delete',          'ShipPriceController@getDeleteShipPrice');
     /*
     * -------------------------------------------
     *  Voucher Routes
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/products/voucher',     'VoucherController@getAllVoucher');
    Route::get('/products/voucher/add',                       'VoucherController@getOneVoucher');
    Route::post('/products/voucher/save',                     'VoucherController@saveVoucher');
    Route::post('/products/voucher/json',                     'VoucherController@getPageVoucher');
    Route::get('/products/voucher/{voucher_id}/delete',       'VoucherController@getDeleteVoucher');
    Route::get('/products/voucher/{voucher_id}/edit',         'VoucherController@getOneVoucher');
    Route::post('/products/voucher/update-status',            'VoucherController@updateStatus');
    /*
     * -------------------------------------------
     *  Infomation Footer
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/static-content/information',     'StaticContentController@getAllHelp');
    Route::get('/static-content/information/add',                       'StaticContentController@getOneHelp');
    Route::post('/static-content/information/save',                     'StaticContentController@saveHelp');
    Route::post('/static-content/information/json',                     'StaticContentController@getPageHelp');
    Route::get('/static-content/information/{help}/delete',             'StaticContentController@getDeleteHelp');
    Route::get('/static-content/information/{help}/edit',               'StaticContentController@getOneHelp');
    Route::post('/static-content/information/updatePublish',            'StaticContentController@updatePublish');

    /*
     * -------------------------------------------
     *  Email Template Route
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/static-content/email-template',      'EmailTemplateController@getAllEmailTemplate');
    Route::get('/static-content/email-template/add',                        'EmailTemplateController@getOneEmailTemplate');
    Route::post('/static-content/email-template/save',                      'EmailTemplateController@saveEmailTemplate');
    Route::post('/static-content/email-template/json',                      'EmailTemplateController@getPageEmailTemplate');
    Route::get('/static-content/email-template/{template_id}/delete',       'EmailTemplateController@getDeleteEmailTemplate');
    Route::get('/static-content/email-template/{template_id}/edit',         'EmailTemplateController@getOneEmailTemplate');
    Route::post('/static-content/email-template/updatePublish',             'EmailTemplateController@updatePublish');

    /*
     * -------------------------------------------
     *  Customer Footer
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/static-content/customer-service',     'CustomerServiceController@getAllCustomer');
    Route::get('/static-content/customer-service/add',                       'CustomerServiceController@getOneCustomer');
    Route::post('/static-content/customer-service/save',                     'CustomerServiceController@saveCustomer');
    Route::post('/static-content/customer-service/json',                     'CustomerServiceController@getPageCustomer');
    Route::get('/static-content/customer-service/{help}/delete',             'CustomerServiceController@getDeleteCustomer');
    Route::get('/static-content/customer-service/{help}/edit',               'CustomerServiceController@getOneCustomer');
    Route::post('/static-content/customer-service/updatePublish',            'CustomerServiceController@updatePublish');

    /*
     * -------------------------------------------
     *  Order Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/customers/orders',     'OrderController@index');
    Route::get('/customers/orders/add',                        'OrderController@addOrder');
    Route::post('/customers/orders/save',                      'OrderController@saveOrder');
    Route::post('/customers/orders/json',                      'OrderController@getPageOrder');
    Route::post('/customers/orders/change',                    'OrderController@updateStatus');

    #
    Route::pattern('order_id', '[0-9]+');
    Route::get('/customers/orders/{order_id}/view',          'OrderController@viewOrder');
    Route::get('/customers/orders/{order_id}/edit',          'OrderController@editOrder');
    Route::get('/customers/orders/{order_id}/delete',        'OrderController@deleteOrder');
    /*
     * -------------------------------------------
     *  Flash Sale Footer
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/static-content/flash-sale',           'FlashSaleController@getAllFlashSale');
    Route::get('/static-content/flash-sale/add',                       'FlashSaleController@getOneFlashSale');
    Route::post('/static-content/flash-sale/save',                     'FlashSaleController@saveFlashSale');
    Route::post('/static-content/flash-sale/json',                     'FlashSaleController@getPagFlashSale');
    Route::get('/static-content/flash-sale/{help}/delete',             'FlashSaleController@getDeleteCustomer');
    Route::get('/static-content/flash-sale/{help}/edit',               'FlashSaleController@getOneFlashSale');
    Route::post('/static-content/flash-sale/updatePublish',            'FlashSaleController@updatePublish');
    /*
     * -------------------------------------------
     *  Widget Footer
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/static-content/widget',          'WidgetController@getAllWiget');
    Route::get('/static-content/widget/add',                       'WidgetController@getOneWidget');
    Route::post('/static-content/widget/save',                     'WidgetController@saveWidget');
    Route::post('/static-content/widget/json',                     'WidgetController@getPagWidget');
    Route::get('/static-content/widget/{help}/delete',             'WidgetController@getDeleteWidget');
    Route::get('/static-content/widget/{help}/edit',               'WidgetController@getOneWidget');
    Route::post('/static-content/widget/updatePublish',            'WidgetController@updatePublish');
    /*
     * -------------------------------------------
     *  Country Category Content
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/categories/country-category',     'CountryController@getAllCountry');
    Route::get('/categories/country-category/add',                       'CountryController@getAddCountry');
    Route::post('/categories/country-category/save',                     'CountryController@saveCountry');
    Route::post('/categories/country-category/json',                     'CountryController@getPageCountry');
     #
    Route::pattern('country_id', '[0-9]+');
    Route::get('/categories/country-category/{country_id}/delete',             'CountryController@getDeleteCountry');
    Route::get('/categories/country-category/{country_id}/edit',               'CountryController@getOneCountry');
    Route::post('/categories/country-category/updatePublish',            'CountryController@updatePublish');
    /*
     * -------------------------------------------
     *  City Category Content
     *  -----------------------------------------
     */
    Route::match(array('GET','POST'),'/categories/state-category',     'CityController@getAllCity');
    Route::get('/categories/state-category/add',                       'CityController@getAddCity');
    Route::post('/categories/state-category/save',                     'CityController@saveCity');
    Route::post('/categories/state-category/json',                     'CityController@getPageCity');
     #
    Route::pattern('city_id', '[0-9]+');
    Route::get('/categories/state-category/{city_id}/delete',             'CityController@getDeleteCity');
    Route::get('/categories/state-category/{city_id}/edit',               'CityController@getOneCity');
    Route::post('/categories/state-category/updatePublish',            'CityController@updatePublish');
    /*
     * ------------------------------------------
     *  Customer Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/customers/registered-members',      'UserMemberController@getAllUserMember');
    Route::post('/customers/registered-members/json',                       'UserMemberController@getPageUserMember');
    Route::get('/customers/registered-members/add',                         'UserMemberController@getAddUserMember');
    Route::post('/customers/registered-members/save',          'UserMemberController@saveUserMember');
    #
    Route::pattern('member_id', '[0-9]+');
    Route::get('/customers/registered-members/{member_id}/delete',          'UserMemberController@getDeleteUserMember');
    Route::get('/customers/registered-members/{member_id}/edit',          'UserMemberController@getEditUserMember');
    #
    Route::get('/customers/contact-us/{contact}/edit',                      'AdminContactController@getEditContact');
    Route::get('/customers/contact-us/{contact}/view',                      'AdminContactController@getViewContact');
    Route::get('/customers/contact-us/add',                                 'AdminContactController@getAddContact');
    Route::post('/customers/contact-us/save',                               'AdminContactController@saveContact');
    Route::post('/customers/contact-us/json',                               'AdminContactController@getPageContact');
    Route::match(array('GET','POST'),'/customers/contact-us',               'AdminContactController@getAllContact');
    Route::get('/customers/contact-us/{contact}/delete',                    'AdminContactController@getDeleteContact');
    Route::post('/customers/contact-us/ajax',                               'AdminContactController@getAjaxContact');
    /*
     * ------------------------------------------
     *  Slide Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/static-content/slide',      'SlideController@getAllSlide');
    Route::post('/static-content/slide/json',                       'SlideController@getPageSlide');
    Route::get('/static-content/slide/add',                         'SlideController@getOneSlide');
    Route::post('/static-content/slide/save',                       'SlideController@saveSlide');
    #
    Route::pattern('slide_id', '[0-9]+');
    Route::get('/static-content/slide/{slide_id}/delete',           'SlideController@getDeleteSlide');
    Route::get('/static-content/slide/{slide_id}/edit',             'SlideController@getOneSlide');
   /*
     * ------------------------------------------
     *  Dynamic Content
     *  ------------------------------------------
    */
    Route::match(array('GET','POST'),'/static-content/blog',        'AdminBlogController@adminAllBlog');
    Route::get('/static-content/blog/add',                          'AdminBlogController@adminOneBlog');
    Route::post('/static-content/blog/save',                        'AdminBlogController@saveBlog');
    Route::post('/static-content/blog/json',                        'AdminBlogController@getPageBlog');
    Route::get('/static-content/blog/{blog_id}/edit',               'AdminBlogController@adminOneBlog');
    Route::get('/static-content/blog/{blog_id}/delete',             'AdminBlogController@getDeleteBlog');
    Route::post('/static-content/blog/updatePublish',               'AdminBlogController@updatePublish');
    /*
     * -------------------------------------------
     *  Lookbook Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/static-content/lookbook',     'LookbookController@index');
    Route::get('/static-content/lookbook/add',                        'LookbookController@addLookbook');
    Route::post('/static-content/lookbook/save',                      'LookbookController@saveLookbook');
    Route::post('/static-content/lookbook/json',                      'LookbookController@getPageLookbook');
    Route::post('/static-content/lookbook/update-status',             'LookbookController@updateStatus');
    #
    Route::pattern('lookbook_id', '[0-9]+');
    Route::get('/static-content/lookbook/{lookbook_id}/view',          'LookbookController@viewLookbook');
    Route::get('/static-content/lookbook/{lookbook_id}/edit',          'LookbookController@editLookbook');
    Route::get('/static-content/lookbook/{lookbook_id}/delete',        'LookbookController@deleteLookbook');
    /*
     * -------------------------------------------
     *  Moodboard Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/static-content/moodboard',     'MoodboardController@index');
    Route::get('/static-content/moodboard/add',                        'MoodboardController@addMoodboard');
    Route::post('/static-content/moodboard/save',                      'MoodboardController@saveMoodboard');
    Route::post('/static-content/moodboard/json',                      'MoodboardController@getPageMoodboard');
    Route::post('/static-content/moodboard/update-status',             'MoodboardController@updateStatus');
    #
    Route::pattern('moodboard_id', '[0-9]+');
    Route::get('/static-content/moodboard/{moodboard_id}/view',          'MoodboardController@viewMoodboard');
    Route::get('/static-content/moodboard/{moodboard_id}/edit',          'MoodboardController@editMoodboard');
    Route::get('/static-content/moodboard/{moodboard_id}/delete',        'MoodboardController@deleteMoodboard');
    /*
     * -------------------------------------------
     *  Social Network Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/static-content/social-network',     'SocialNetworkController@index');
    Route::get('/static-content/social-network/add',                        'SocialNetworkController@addSocialNetwork');
    Route::post('/static-content/social-network/save',                      'SocialNetworkController@saveSocialNetwork');
    Route::post('/static-content/social-network/json',                      'SocialNetworkController@getPageSocialNetwork');
    Route::post('/static-content/social-network/update-status',             'SocialNetworkController@updateStatus');
    #
    Route::pattern('social_id', '[0-9]+');
    Route::get('/static-content/social-network/{social_id}/view',          'SocialNetworkController@viewSocialNetwork');
    Route::get('/static-content/social-network/{social_id}/edit',          'SocialNetworkController@editSocialNetwork');
    Route::get('/static-content/social-network/{social_id}/delete',        'SocialNetworkController@deleteSocialNetwork');
    /*
     * -------------------------------------------
     *  Admin User Routes
     *  ------------------------------------------
     */
    Route::get('/account/user/add',                     'AdminUserController@getOneUserAdmin');
    Route::post('/account/user/save',                   'AdminUserController@saveUserAdmin');
    Route::post('/account/user/json',                   'AdminUserController@getPageUserAdmin');
    Route::match(array('GET', 'POST'),'/account/user',  'AdminUserController@getAllUserAdmin');
    #
    Route::pattern('user_admin', '[0-9]+');
    Route::get('/account/user/{user_admin}/delete',     'AdminUserController@getDeleteUserAdmin');
    Route::get('/account/user/{user_admin}/edit',       'AdminUserController@getOneUserAdmin');
   /*
     * ------------------------------------------
     *  Newsletter Routes
     *  ------------------------------------------
     */
    Route::match(array('GET', 'POST'),'/customers/newsletter',  'NewsletterController@getEditNewsletter');
    Route::post('/customers/newsletter/send-newsletter',  'NewsletterController@sendNewsletter');
    /*
     * ------------------------------------------
     *  TAX
     *  ------------------------------------------
     */
    Route::match(array('GET','POST'),'/products/tax', 'TaxController@getAllTax');
    Route::post('/products/tax/json', 'TaxController@getPageTax');
    Route::get('/products/tax/add', 'TaxController@getOneTax');
    Route::post('/products/tax/save', 'TaxController@saveTax');
});
Route::controller('testtoken', 'TestTokenController');
