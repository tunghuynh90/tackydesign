@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Contact</h3>

            <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/customers/contact-us">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by Contact Name" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                function updatePublish(obj, id) {
                      var sid = obj.id;
                      var $this = $('img#'+sid);
                      var status = $this.attr('data-status');
                      status = parseInt(status, 10);
                      if(status!=1) status = 0;
                      var icons = ['icon-hide.png', 'icon-unhide.png'];
                      $.ajax({
                          url : '/admin/customers/contact-us/ajax',
                          type    : 'POST',
                          dataType: 'json',
                          data    :   {id: id, status: status,ajax_type:'change_publish'},
                          beforeSend: function(){
                          },
                          success: function(data, status){
                              if(data.error==0){
                                  if(data.success==1){
                                      $this.attr('src',"/assets/images/icons/"+icons[data.status]).attr('data-status', data.status);
                                  }
                              }
                          },error :function(data){
                              alert(data.responseText);
                          }
                      });
                  }
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>
        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Contact</h2>
          </div>
          <script type="text/javascript">
          var window_search;
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/customers/contact-us/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "contact"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "contact_name", title: "Contact Name", type:"string", width:"75px", sortable: true },
                      {field: "contact_phone", title: "Contact Phone", type:"string", width:"50px", sortable: true },
                      {field: "contact_email", title: "Contact Email", type:"string", width:"70px", sortable: true },

                      {field: "contact_message", title: "Contact Message", type:"string", width:"70px", sortable: true },
                      {field: "posted_date", title: "Posted Date", type:"string", width:"65px", sortable: true },
                     /* {field: "publish", title: "Publish", type:"int", width:"40px", sortable: true,template:'<p style="text-align:center; margin:0"><img class="change_status" @if(isset($permit[_R_UPDATE])) onclick=updatePublish(this,'+'"#= id#"'+') @endif id="icon_#= id#_publish" data-status="#= publish#" data-id="#= id#"  src="/assets/images/icons/#= publish==1?\"icon-unhide.png\":\"icon-hide.png\"#" style="max-width:50px; cursor:pointer" /></p>'},*/

                      { command:  [
                        { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' }
                        /*@if(isset($permit[_R_UPDATE]))
                        ,{ name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        @endif*/
                        @if(isset($permit[_R_DELETE]))
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        @endif
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/customers/contact-us/"+dataItem.id+"/view";
                }
                @if(isset($permit[_R_UPDATE]))
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/customers/contact-us/"+dataItem.id+"/edit";
                }
                @endif
                @if(isset($permit[_R_DELETE]))
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete contact name : '+dataItem.contact_name+'?')){
                        document.location = "/admin/customers/contact-us/"+dataItem.id+"/delete";
                    }
                }
                @endif

            });
        </script>
    </div>
@stop