@extends('layouts.admin.admin_layout')

@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Information</h3>
            <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/static-content/information">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by name" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>

        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Help</h2>
          </div>
          <script type="text/javascript">
          var window_search;

          function updatePublish(id,status) {
              var status_ = $("#icon_"+id).attr('data-status');
              var val_ = 0;
              if(status_=='icon-unhide.png') val_ = 0;
              else val_ = 1;
              $.ajax({
                  url : '/admin/static-content/information/updatePublish',
                  type    : 'POST',
                  data    :   {txt_id: id, txt_value: val_
                  },
                  beforeSend: function(){
                  },
                  success: function(data, type){
                      if(data.error==0){
                          if(status_=='icon-unhide.png'){
                              $("#icon_"+id).attr('src',"/assets/images/icons/icon-hide.png");
                              $("#icon_"+id).attr('data-status',"icon-hide.png");
                          }else{
                              $("#icon_"+id).attr('src',"/assets/images/icons/icon-unhide.png");
                              $("#icon_"+id).attr('data-status',"icon-unhide.png");
                          }
                      }
                  },error :function(data){
                      alert(data.responseText);
                  }
              });
          }
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/static-content/information/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "help"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "name", title: "Name", type:"string", width:"75px", sortable: true },
                      {field: "short_name", title: "Short name", type:"string", width:"75px", sortable: true },
                      {field: "description", title: "Title", type:"string", width:"75px", sortable: true },
                      {field: "group_name", title: "Group", type:"string", width:"30px", sortable: true },
                      {field: "orderno", title: "Orderno", type:"int", width:"20px", sortable: true },
                      {field: "publish", title: "Publish", type:"string", width:"50px", sortable: true , template:'<p style="text-align:center; margin:0"><img class="change_status" onclick=updatePublish('+'"#= id#"'+','+'"#= publish#"'+')  id="icon_#= id#" data-status="#= publish#" data-id="#= id#"  src="/assets/images/icons/#= publish#" ' },
                      { command:  [
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/static-content/information/"+dataItem.id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/static-content/information/"+dataItem.id+"/edit";
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete help : '+dataItem.name+'?')){
                        document.location = "/admin/static-content/information/"+dataItem.id+"/delete";
                    }
                }

            });
        </script>
    </div>
@stop