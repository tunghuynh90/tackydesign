@extends('layouts.admin.admin_layout')

@section('content')
<script>
    $(document).ready(function(){
        var editor = $('textarea#content,textarea#application,textarea#technical,textarea#articles').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                dataType:'json',
                transport: {
                    read: {
                        url: "/admin/images/listImages",
                        type: "POST",
                        dataType:'json'
                    },
                    destroy: {
                        url: "/admin/images/deleteFile",
                        type: "POST",
                        dataType:'json'
                    },
                    uploadUrl: "/admin/images/uploadImage",
                    thumbnailUrl: "/admin/images/thumbImage",
                    imageUrl: "{{$url}}/assets/upload/{0}"
                }
            }
        }).data("kendoEditor");
        // auto upload anh
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
        // end
    });
</script>

  <div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Static content</h3>
    </div>
    <div id="div_quick_search"></div>

    <form id="frm_help" action="/admin/{{$icon['view']}}/save" method="post">
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_help" name="action_help" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>Content</li>
            </ul>

            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>


                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>
                    <tr>
                        <td>Title</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="title" name="title" value="{{$columns['title']}}" /></td>
                    </tr>

                   <tr>
                        <td>Posted Date</td>
                        <td>&nbsp;</td>
                        <td>
                            <input  type="text" id="posted_date" name="posted_date" value="{{$columns['posted_date']}}" />
                            <!-- <input type="hidden" id="hide_posted_date" name="hide_posted_date" value="" /> -->
                        </td>
                    </tr>

                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>

                </table>
            </div>

            <div class="other">
                <textarea id="content" name="content" style="width:90%; height:300px; padding:5px">
                    {{$columns['content']}}
                </textarea>
            </div>
        </div>

        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
        <ul>
        {{$message}}
        </ul>
        </div>
        @endif

        <div class="k-block k-widget css_buttons">
        <input type="submit" id="btn_submit_help" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
    </form>
  </div>
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $('input#btn_submit_help').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");
		$('input#ghelp_id').kendoNumericTextBox({
        	min:0, step:1, format:'n0'
        });
		$('input#orderno').kendoNumericTextBox({
        	min:0, step:1, format:'n0'
        });

        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
            open: {
                effects: "fadeIn"
                }
        }
        }).data("kendoTabStrip");
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
		$('input#posted_date').kendoDateTimePicker({
    		format: 'dd-MMM-yyyy HH:mm:ss'
        }).data('kendoDateTimePicker');
    });
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
    </script>
@stop
