@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Contact</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_contact" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_contact" name="action_contact" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="contact_name" name="contact_name" value="{{$columns['contact_name']}}" /></td>
                        </tr>

                        <tr>
                            <td>Phone</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="contact_phone" name="contact_phone" value="{{$columns['contact_phone']}}" /></td>
                        </tr>

                        <tr>
                            <td>Email</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="contact_email" name="contact_email" value="{{$columns['contact_email']}}" /></td>
                        </tr>

                        <tr>
                            <td>Message</td>
                            <td>&nbsp;</td>
                            <td>
                                <textarea class="k-textbox" id="contact_message" name="contact_message" disabled readonly rows="7" style="width: 300px">{{$columns['contact_message']}}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td>Posted Date</td>
                            <td>&nbsp;</td>
                            <td>
                                <input readonly type="text" id="posted_date" name="posted_date" value="{{$columns['posted_date']}}" />
                                <input type="hidden" id="hide_posted_date" name="hide_posted_date" value="" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_contact" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
            @endif
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            @if(isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE]))
            $('input#btn_submit_contact').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
                $('input#hide_posted_date').val(kendo.toString(posted_date.value(),'yyyy-MM-dd HH:mm:ss'));
            });
            @endif
            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
			var posted_date = $('input#posted_date').kendoDateTimePicker({
            	format: 'dd-MMM-yyyy HH:mm:ss'
            }).data('kendoDateTimePicker');
			$('input#orderno').kendoNumericTextBox({
            	min:0, step:1, format:'n0'
            });

            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        </script>
    @stop
