@extends('layouts.admin.admin_layout')
@section('content')
  <div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Account: {{$admin['fullname']}}</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_user_admin" method="post">
        <input type="hidden" id="id" name="id" value="{{$admin['id']}}" />
        <input type="hidden" id="action_admin_account" name="action_admin_account" value="edit" />
        <div id="div_tab">
            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Username</td>
                        <td>&nbsp;</td>
                        <td>{{$admin['username']}}</td>
                    </tr>

                    <tr>
                        <td>Current password</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="cpassword" name="cpassword" value="" required validationMessage="Please input current password" /> <span class="k-required">(*)</span></td>
                    </tr>

                    <tr>
                        <td>New password</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="npassword" name="npassword" value="" /><br />
                        <label class="k-required">If you want to change your password</label> </td>
                    </tr>
                    <tr>
                        <td>Repeat password</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="rpassword" name="rpassword" value="" /><br />
                            <label class="k-required">If you want to change your password</label></td>
                    </tr>

                    <tr>
                        <td>Fullname</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="fullname" name="fullname" value="{{$admin['fullname']}}" required validationMessage="Please input fullname" /> <span class="k-required">(*)</span></td>
                    </tr>

                    <tr>
                        <td>Phone</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="phone" name="phone" value="{{$admin['phone']}}" /></td>
                    </tr>

                    <tr>
                        <td>Mobile</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="mobile" name="mobile" value="{{$admin['mobile']}}" /></td>
                    </tr>

                    <tr>
                        <td>Email</td>
                        <td>&nbsp;</td>
                        <td><input type="email" class="k-textbox" id="email" name="email" value="{{$admin['email']}}" required data-required-msg="Input valid email" data-email-msg="Invalid email" /> <span class="k-required">(*)</span></td>
                    </tr>

                    <tr>
                        <td>Last Login</td>
                        <td>&nbsp;</td>
                        <td>{{date('d-M-Y H:i:s',strtotime($admin['lastest_login']))}}</td>
                    </tr>
                        </table>
            </div>
        </div>
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
        <ul>
        {{$message}}
        </ul>
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
        <input type="submit" id="btn_submit_user_admin" name="submit" value="Submit" class="k-button button_css" />
        </div>
    </form>
  </div>
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $('input#btn_submit_user_admin').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        $("#phone").kendoMaskedTextBox({
            mask: "(999) 000-0000"
        });
        $("#mobile").kendoMaskedTextBox({
            mask: "(999) 000-0000"
        });
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        /*
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
            open: {
                effects: "fadeIn"
                }
        }
        }).data("kendoTabStrip");
        */
    });
    </script>
@stop
