@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>User Admin</h3>
             <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/account/user">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by username or fullname" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>

        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for User Admin</h2>
          </div>
          <script type="text/javascript">
          var window_search;
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/account/user/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "user_admin"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "username", title: "Username", type:"string", width:"50px", sortable: true },
                      {field: "fullname", title: "Full Name", type:"string", width:"70px", sortable: true },
                      {field: "phone", title: "Phone", type:"string", width:"40px", sortable: true },
                      {field: "mobile", title: "Mobile", type:"string", width:"40px", sortable: true },
                      {field: "email", title: "Email", type:"string", width:"50px", sortable: true },
                      {field: "created_date", title: "Created Date", type:"string", width:"50px", sortable: true },
                      {field: "is_active", title: "Active", type:"int", width:"20px", sortable: true, template:'#= is_active?"Active":"Inactive"#' },
                      {field: "lastest_login", title: "Last Login", type:"string", width:"50px", sortable: true },

                      { command:  [
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/account/user/"+dataItem.id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/account/user/"+dataItem.id+"/edit";
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete user: "'+dataItem.fullname+'"?')){
                        document.location = "/admin/account/user/"+dataItem.id+"/delete";
                    }
                }

            });
        </script>
    </div>
@stop