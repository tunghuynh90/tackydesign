@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>User Admin</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_user_admin" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_user_admin" name="action_user_admin" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Username</td>
                            <td>&nbsp;</td>
                            <td>
                                @if($columns['id']>0)
                                {{$columns['username']}}
                                <input type="hidden" class="k-textbox" id="username" name="username" value="{{$columns['username']}}" />
                                @else
                                <input type="text" class="k-textbox" id="username" name="username" value="{{$columns['username']}}" required data-required-msg="Input unique username" />
                                <span class="k-required">(*)</span>
                                @endif
                            </td>
                        </tr>
        
                        <tr>
                            <td>Password</td>
                            <td>&nbsp;</td>
                            <td><input type="password" class="k-textbox" id="password" name="password" value="" {{$columns['id']<=0?'required data-required-msg="Input password for this user"':''}} />
                            {{$columns['id']<=0?'<span class="k-required">(*)</span>':''}}
                            </td>
                        </tr>
        
                        <tr>
                            <td>Fullname</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="fullname" name="fullname" value="{{$columns['fullname']}}" /></td>
                        </tr>
        
                        <tr>
                            <td>Phone</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="phone" name="phone" value="{{$columns['phone']}}" /></td>
                        </tr>
        
                        <tr>
                            <td>Mobile</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="mobile" name="mobile" value="{{$columns['mobile']}}" /></td>
                        </tr>
        
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;</td>
                            <td><input type="email" class="k-textbox" id="email" name="email" value="{{$columns['email']}}" required data-required-msg="Input valid email" data-email-msg="Invalid email" />
                            <span class="k-required">(*)</span>
                            </td>
                        </tr>
        
                        <!-- <tr>
                            <td>Group (Role)</td>
                            <td>&nbsp;</td>
                            <td><select id="role_id" name="role_id"></select></td>
                        </tr> -->
                        <tr>
                            <td>Is Active</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" id="is_active" name="is_active" value="{{$columns['is_active']}}"{{$columns['is_active']==1?' checked="checked"':''}} /></td>
                        </tr>
        
                            </table>
                </div>
            </div>
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_user_admin" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            $('input#btn_submit_user_admin').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
        $("#phone").kendoMaskedTextBox({
            mask: "(999) 000-0000"
        });
        $("#mobile").kendoMaskedTextBox({
            mask: "(999) 000-0000"
        });
            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        </script>
    @stop
    