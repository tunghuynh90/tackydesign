@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Country</h3>
            <div id="div_quick_search">
            <form id="frm_quick_search" method="post" action="/admin/system/country">
                <span id="txt_quick_search" class="k-textbox k-space-left">
                <input type="text" value="{{$quick}}" placeholder="Search by name" name="txt_quick_search">
                <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                </span>
            </form>
            <script type="text/javascript">
            $(document).ready(function(e){
                $('a#a_quick_search').click(function(e){
                    $('form#frm_quick_search').submit();
                });
            });
            </script>
        </div>
        </div>
        
        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Country</h2>
          </div>
          <script type="text/javascript">
          var window_search;
          function testing(id,status) {
              var status_ = $("#icon_"+id).attr('data-status');
              var val_ = 0;
              if(status_=='icon-unhide.png') val_ = 0;
              else val_ = 1;
              $.ajax({
                  url : '/admin/system/country/updatePublish',
                  type    : 'POST',
                  data    :   {txt_id: id, txt_value: val_
                  },
                  beforeSend: function(){
                  },
                  success: function(data, type){
                      if(data.error==0){
                          if(status_=='icon-unhide.png'){
                              $("#icon_"+id).attr('src',"{{$url}}/assets/images/icons/icon-hide.png");
                              $("#icon_"+id).attr('data-status',"icon-hide.png");
                          }else{
                              $("#icon_"+id).attr('src',"{{$url}}/assets/images/icons/icon-unhide.png");
                              $("#icon_"+id).attr('data-status',"icon-unhide.png");
                          }
                      }
                  },error :function(data){
                      alert(data.responseText);
                  }
              });
          }
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/system/country/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "country"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "name", title: "Name", type:"string", width:"80px", sortable: true },
                      {field: "short_name", title: "Short Name", type:"string", width:"80px", sortable: true },
                      {field: "publish", title: "Publish", type:"int", width:"50px", sortable: true , template:'<p style="text-align:center; margin:0"><img class="change_status" @if(isset($permit[_R_UPDATE])) onclick=testing('+'"#= id#"'+','+'"#= publish#"'+') @endif id="icon_#= id#" data-status="#= publish#" data-id="#= id#"  src="{{$url}}/assets/images/icons/#= publish#" ' },
                      {field: "orderno", title: "Orderno", type:"int", width:"50px", sortable: true },

                      { command:  [
                        // { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' },
                        @if(isset($permit[_R_UPDATE]))
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        @endif
                    @if(isset($permit[_R_DELETE]))
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                    @endif
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/country/"+dataItem.id+"/view";
                }
          @if(isset($permit[_R_UPDATE]))
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/country/"+dataItem.id+"/edit";
                }
          @endif
          @if(isset($permit[_R_DELETE]))
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete country : '+dataItem.id+'?')){
                        document.location = "/admin/system/country/"+dataItem.id+"/delete";
                    }
                }
          @endif
            });
        </script>
    </div>
@stop