@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Country{{$columns['id']>0?': '.$columns['name']:''}}</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_country" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_country" name="action_country" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                    <li>Other</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                        </tr>

                        <tr>
                            <td>Short Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                        </tr>

                        <tr>
                            <td>Publish</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                        </tr>

                        <tr>
                            <td>Orderno</td>
                            <td>&nbsp;</td>
                            <td><input type="number" id="orderno" name="orderno" value="{{$columns['orderno']}}" /></td>
                        </tr>
                            </table>
                </div>
                <div class="other"></div>
            </div>
            @if((isset($permit[_R_UPDATE]) || isset($permit[_R_INSERT])) && $edit)
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_country" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
            @endif
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            @if((isset($permit[_R_UPDATE]) || isset($permit[_R_INSERT])) && $edit)
            $('input#btn_submit_country').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            @endif
            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
			$('input#orderno').kendoNumericTextBox({
            	min:0, step:1, format:'n0'
            });

            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        </script>
    @stop
