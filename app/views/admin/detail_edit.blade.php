@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>{{$title}}</h3>
            <div id="div_quick_search">
            </div>
        </div>
        <form id="frm_module" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_module" name="action_module" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        @if(!empty($arr_setting['field']))
                        @foreach($arr_setting['field'] as $field => $field_value)
                            @if(isset($field_value['display_type']))
                                <tr>
                                    <td>{{$field_value['title']}}</td>
                                    <td>&nbsp;</td>
                                    <td>
                                        @if($field_value['display_type']=='text')
                                        	<input type="text" class="k-textbox" id="field_{{$field}}" name="{{$field}}" value="{{$columns[$field]}}" {{ '';if(isset($lock[$field])) echo 'readonly="readonly" style="background:#ddd;"'}} />
                                        @elseif($field_value['display_type']=='checkbox')
                                        	<input type="checkbox" id="field_{{$field}}" name="{{$field}}" value="{{$columns[$field]}}" {{$columns[$field]==1?' checked="checked"':''}} />
                                        @elseif($field_value['display_type']=='select')
                                        	<select id="field_{{$field}}" name="{{$field}}">
                                    			<option value="">--------</option>
                                    			<option value="1"{{$columns[$field]==1?' selected="selected"':''}}>
                                                	Back End
                                                </option>
                                    			<option value="2"{{$columns[$field]==2?' selected="selected"':''}}>
                                                	Member
                                                </option>
											</select>
                                           <script language="javascript" type="text/javascript">
												$(document).ready(function(e){
													$('select#field_{{$field}}').width(300).kendoDropDownList();
													var module_group_data = {{$dropdown[$field.'_json']}};
													var module_group = $('select#field_{{$field}}').width(300).kendoDropDownList({
														dataSource: module_group_data,
														dataValueField:'id',
														dataTextField:'name',
														encoded: false
													}).data('kendoDropDownList');
													module_group.value({{$columns[$field]}});
												});
											</script>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        @endif 
                    </table>
                </div>
            </div>
            
            <div class="k-block k-widget css_buttons">
            	<input type="submit" id="btn_submit_module" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
        </form>
      </div>



<script language="javascript" type="text/javascript">
	$(document).ready(function(e){
		//Even save
		$('input#btn_submit_module').click(function(e){
			if(!validator.validate()){
				e.preventDefault();
				if(tab_strip.select().index()!=0) tab_strip.select(0);
				return false;
			}
		});
		
		var tooltip = $("span.tooltips").kendoTooltip({
			filter: 'a',
			width: 120,
			position: "top"
		}).data("kendoTooltip");
		
		// Vien box
		var validator = $('div.information').kendoValidator().data("kendoValidator");
		var tab_strip = $("#div_tab").kendoTabStrip({
				animation:{
					open: {effects: "fadeIn"}
				}
			}).data("kendoTabStrip");
	});
</script>


@stop
