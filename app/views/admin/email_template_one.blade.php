@extends('layouts.admin.admin_layout')
@section('content')
<link href="http://tackydesign.local/assets/css/email_template.css" rel="stylesheet" type="text/css">
<script src="{{$url}}/assets/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    CKEDITOR.config.contentsCss = '{{$url}}/assets/css/email_template.css';
    CKEDITOR.config.enterMode=CKEDITOR.ENTER_DIV;
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.replace('content',
    {
        toolbar : 'EmailTemplate',
        resize_enabled : false,
        removePlugins : 'elementspath',
        filebrowserImageUploadUrl : '{{$url}}/assets/js/kcfinder/upload.php?type=images',
        filebrowserImageBrowseUrl : '{{$url}}/assets/js/kcfinder/browse.php?type=images',
        height : 500,
        enterMode:CKEDITOR.ENTER_BR,
    });
    spanclick();
})
function spanclick(){
    $("span",".field_button").click(function(){
        var tmp_html = $(this);
        console.log(tmp_html);
        tmp_html.addClass("field_span");
        CKEDITOR.instances['content'].insertHtml(tmp_html[0].outerHTML);
        return false;
    });
}
</script>

  <div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Static content</h3>
    </div>
    <div id="div_quick_search"></div>

    <form id="frm_help" action="/admin/{{$icon['view']}}/save" method="post">
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_email_template" name="action_email_template" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
            </ul>

            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Type</td>
                        <td>&nbsp;</td>
                        <td>
                            <select id="type" name="type" ></select>
                            <script type="text/javascript">
                                $("#type").width(300).kendoDropDownList({
                                    contentType: "charset=utf-8",
                                    placeholder: "Select type...",
                                    dataTextField: "name",
                                    dataValueField: "value",
                                    dataSource: {{json_encode($columns['type_list'])}},
                                    value: "{{$columns['type']}}"
                                }).data("kendoDropDownList");
                            </script>
                        <td>
                    </tr>
                    <?php
                    $field_button = array('CONFIRM_LINK','CUSTOMER_NAME','ORDER_DATE','ORDER_NUMBER','SHIPPING_ADDRESS','TOKEN','BILLING_ADDRESS','CARD_TYPE','CARD_NUMBER','PRODUCT_CONTENT','SUB_TOTAL','SHIPPING_PRICE','DISCOUNT_PERCENT','DISCOUNT','TAX','TOTAL');

                    ?>
                    <tr id="field_content">
                        <td>Field</td>
                        <td>&nbsp;</td>
                        <td >
                            <div class="field_button" style="padding-top: 15px;width: 450px"  >
                            <?php foreach($field_button as $key => $v_field_button): ?>
                            <span contenteditable="false" rel="<?php echo '[['.$v_field_button.']]'; ?>"  unselectable="on"><?php echo $v_field_button; ?></span>
                            <?php endforeach ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td>&nbsp;</td>
                        <td><input type="text" id="subject" style="width: 500px" name="subject" value="{{@$columns['subject']}}" class="k-textbox"/></td>
                    </tr>
                    <tr>
                        <td>Content</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea id="content" name="content" style="width:90%; height:500px; padding:5px">{{$columns['content']}}</textarea>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
        <ul>
        {{$message}}
        </ul>
        </div>
        @endif

        <div class="k-block k-widget css_buttons">
        <input type="submit" id="btn_submit_email_template" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
    </form>
  </div>
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $('input#btn_submit_email_template').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");

        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
            open: {
                effects: "fadeIn"
                }
        }
        }).data("kendoTabStrip");
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
    });
    </script>
@stop
