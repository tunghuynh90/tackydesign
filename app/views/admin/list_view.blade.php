@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>{{$title}}</h3>
            <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/system/{{$module_name}}">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by config's name" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>
        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Config</h2>
          </div>
          <script type="text/javascript">
              function updatePublish(obj, id) {
                  var sid = obj.id;
                  var $this = $('img#'+sid);
                  var status = $this.attr('data-status');
                  status = parseInt(status, 10);
                  if(status!=1) status = 0;
                  var icons = ['icon-hide.png', 'icon-unhide.png'];
                  $.ajax({
                      url : "/admin/{{$icon['module_group']}}/{{$module_name}}/ajax",
                      type    : 'POST',
                      dataType: 'json',
                      data    :   {id: id, status: status,ajax_type:'change_publish'},
                      beforeSend: function(){
                      },
                      success: function(data, status){
                          if(data.error==0){
                              if(data.success==1){
                                  $this.attr('src',"assets/images/icons/"+icons[data.status]).attr('data-status', data.status);
                                  //$(this).attr('data-status', data.status);
                              }
                          }
                      },error :function(data){
                          alert(data.responseText);
                      }
                  });
              }

          var window_search;
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/system/{{$module_name}}/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "{{$module_name}}"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [

					@if(!empty($arr_setting['field']))
            		@foreach($arr_setting['field'] as $field => $field_value)

						@if(isset($field_value['list_view']) && $field_value['list_view']=='1')
							{field: "{{$field}}",
							@foreach($field_value as $element => $value)
								@if($element=='sortable' || $element=='encoded' || $element=='lockable' || $element=='locked')
									{{$element}}: {{$value}},
								@elseif($element!='list_view' && $element!='display_type')
									{{$element}}: '{{$value}}',
								@endif
							@endforeach
							},
                    	@endif

					@endforeach
            		@endif

                      { command:  [
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/{{$module_name}}/"+dataItem.id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/{{$module_name}}/"+dataItem.id+"/edit";
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete module: "'+dataItem.name+'"?')){
                        document.location = "/admin/system/{{$module_name}}/"+dataItem.id+"/delete";
                    }
                }

            });
        </script>
    </div>
@stop