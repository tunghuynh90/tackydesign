<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admin Login</title>
    <base href="{{$url}}" />
    <link href="{{$url}}/assets/css/login.css" rel="stylesheet" type="text/css" />
    <link href="{{$url}}/assets/css/kendo.common.min.css" rel="stylesheet" />
    <script src="{{$url}}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.web.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.core.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.maskedtextbox.min.js" type="text/javascript"></script>
</head>

<body>
<div id="div_wrapper">
    <div id="div_content">
        <section class="container">
            <div class="login">
                <h1>Administrator</h1>
                <div style="text-align: center">
                </div>
                <form id="frm_login" method="post" action="{{$url}}/admin/login">
                    <p><input type="text" name="username" placeholder="Username" required></p>
                    <p><input type="password" name="password" placeholder="Password" required></p>
                    <input type="hidden" name="token" value="{{$token}}" />
                    <p class="submit"><input type="submit" name="submit" value="Login" /></p>
                </form>
            </div>
            @if($message!='')
            <div style="height: 20px"></div>
            <div style="width: 310px !important; margin: auto !important" class="k-block k-widget k-error-colored">
                <ul>
                {{$message}}
                </ul>
                @if($second>0)
                <script type="text/javascript">
                    var timer = null;
                    var second = {{$second}};
                    function show_time(){
                        second--;
                        if(second>=0){
                            timer = setTimeout('show_time()', 1000);
                            document.getElementById('lbl_time').innerHTML = convert_string(second);
                        }else{
                            clearTimeout(timer);
                        }
                    }
                    function convert_string(second){
                        var s = second % 60;
                        var m = (second - s)/60;
                        s = '0'+s; m = '0'+m;
                        s = s.substring(s.length-2);
                        m = m.substring(m.length - 2);
                        return m+':'+s;
                    }
                    $(document).ready(function(e){
                        $('form#frm_login').submit(function(e){
                            if(second>0){
                                alert('Please wait in: '+convert_string(second));
                                // return false;
                            }
                            return true;
                        })
                        show_time();
                    });
                </script>
                @endif
            </div>
            @endif
        </section>
    </div>
</div>
</body>
</html>