@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Gallery</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_product" action="admin/static-content/lookbook/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_lookbook" name="action_lookbook" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>Image</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Name</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>

                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>

                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>

                </table>
            </div>
            <div class="Image">
                <table id="tbl_single">
                     <tr>
                        <td>Main Image</td>
                        <td>&nbsp;</td>
                        <td>
                            <img id="main_image_link" style="max-width: 150px;" src="{{$columns['image_link']}}" />
                            <input <?php if($columns['main_image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="main_image" readonly data-id="main_image_link" name="main_image" value="{{$columns['main_image']}}" />
                            <span id="main_image_link_delete" <?php if($columns['main_image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg(this,'main_image_link')"><img src="{{$url}}/assets/images/icons/delete.png" /></span>
                            <script type="text/javascript">
                                $(function(){
                                    $("#main_image").kendoEditor({
                                        tools: [
                                            "insertImage"
                                        ],
                                        imageBrowser: {
                                            dataType:'json',
                                            transport: {
                                                read: {
                                                    url: "/admin/images/listImages",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path : "upload/lookbook"}
                                                },
                                                destroy: {
                                                    url: "/admin/images/deleteFile",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path : "upload/lookbook"}
                                                },
                                                uploadUrl: "/admin/images/uploadImage?image_path=lookbook",
                                                thumbnailUrl: "/admin/images/thumbImage?image_path=lookbook",
                                                imageUrl: function(imagename){
                                                    $("#main_image").val(imagename).attr('type','hidden');
                                                    var img_id = $("#main_image").attr("data-id");
                                                    $("#"+img_id).attr("src","{{$url}}/assets/upload/lookbook/"+imagename);
                                                    $("#"+img_id+"_delete").show();
                                                    event.preventDefault();
                                                }
                                            }
                                        }
                                    });
                                })
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>Other Image</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="other_image" value="{{$columns['hidden_other_image']}}" />
                             <div class="dropzone dz-clickable" id="other_image" >
                                <div class="dz-default dz-message" data-dz-message="">
                                    <span>Drop files here to upload</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_lookbook" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
        @endif
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $('input#btn_submit_lookbook').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        Dropzone.autoDiscover = false;
        @if($edit)
        @if(strpos($_SERVER['REQUEST_URI'],'edit') === false)
        $("#frm_product").dropzone({
            init: function() {
                var myDropzone = this; //closure
                this.on("complete", function(){
                });
                this.on("success", function(file, responseText) {
                    var file_name = file.name;
                    var other_image = $("input[name=other_image]").val();
                    $("input[name=other_image]").val(other_image+"@_@"+file_name);
                });
                this.on("removedfile",function(file){
                    var file_name = file.name;
                    removeFile(file_name);
                });
             },
            url: '{{$url}}/admin/images/uploadImage',
            addRemoveLinks: true,
            dictDefaultMessage: "Drag or Click",
            clickable: true,
            method: 'POST',
            paramName: "other_image_lookbook",
            maxFiles:{{MAX_UPLOAD_FILE}},
            parallelUploads: 10,
            maxFilesize:2,
            previewsContainer:'#other_image',
            autoProcessQueue: true,
            uploadMultiple: true
        });
        @else
        $("#frm_product").dropzone({
            init: function() {
                var other_image = {{$columns['other_image']}};
                for(i in other_image){
                    var mockFile = { name: other_image[i].name, size: other_image[i].size };
                    this.options.addedfile.call(this, mockFile);
                    this.options.thumbnail.call(this, mockFile, "{{$url.'/'}}"+other_image[i].link);

                }
                this.on("success", function(file, responseText) {
                    var file_name = file.name;
                    var other_image = $("input[name=other_image]").val();
                    $("input[name=other_image]").val(other_image+"@_@"+file_name);
                });
                this.on("removedfile",function(file){
                    var file_name = file.name;
                    removeFile(file_name);
                });
             },
            url: '{{$url}}/admin/images/uploadImage',
            addRemoveLinks: true,
            dictDefaultMessage: "Drag or Click",
            clickable: true,
            method: 'POST',
            paramName: "other_image_lookbook",
            maxFiles:{{MAX_UPLOAD_FILE}},
            parallelUploads: 25,
            maxFilesize:2,
            previewsContainer:'#other_image',
            autoProcessQueue: true,
            uploadMultiple: true
        });
        @endif
        @endif
    });
    function removeFile(file_name){
        $.ajax({
            url : "/admin/images/deleteFile",
            type: "POST",
            data: {name: file_name, image_path: 'upload/lookbook'},
            success: function(){
                var other_image = $("input[name=other_image]").val();
                other_image = other_image.replace(file_name,'');
                $("input[name=other_image]").val(other_image);
            }
        })
    }
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
    function removeImg(obj,img_id){
        $("#image").val("");
        $("#"+img_id).attr("src","");
        $(obj).hide();
        var input_id = img_id.replace("_link","");
        $("#"+input_id).val("").attr("type","text");
    }
</script>
@stop
