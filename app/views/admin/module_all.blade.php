@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Module / Child Category </h3>

            <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/system/module">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by module's name" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>
        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Module</h2>
          </div>
          <script type="text/javascript">
              function updatePublish(obj, id) {
                  var sid = obj.id;
                  var $this = $('img#'+sid);
                  var status = $this.attr('data-status');
                  status = parseInt(status, 10);
                  if(status!=1) status = 0;
                  var icons = ['icon-hide.png', 'icon-unhide.png'];
                  $.ajax({
                      url : '/admin/system/module/ajax',
                      type    : 'POST',
                      dataType: 'json',
                      data    :   {id: id, status: status,ajax_type:'change_publish'},
                      beforeSend: function(){
                      },
                      success: function(data, status){
                          if(data.error==0){
                              if(data.success==1){
                                  $this.attr('src',"assets/images/icons/"+icons[data.status]).attr('data-status', data.status);
                                  //$(this).attr('data-status', data.status);
                              }
                          }
                      },error :function(data){
                          alert(data.responseText);
                      }
                  });
              }

          var window_search;
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/system/module/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "module"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "name", title: "Name", type:"string", width:"50px", sortable: true },
                      {field: "short_name", title: "Short Name", type:"string", width:"50px", sortable: true },
                      {field: "module_type", title: "Module Type", type:"int", width:"50px", sortable: false },
                      {field: "module_group", title: "Module Group", type:"int", width:"50px", sortable: false, encoded: false },
                      //{field: "module_path", title: "Module Path", type:"string", width:"50px", sortable: true },
                      {field: "publish", title: "Publish", type:"int", width:"50px", sortable: true,template:'<p style="text-align:center; margin:0"><img class="change_status" @if(isset($permit[_R_UPDATE])) onclick=updatePublish(this,'+'"#= id#"'+') @endif id="icon_#= id#_publish" data-status="#= publish#" data-id="#= id#"  src="assets/images/icons/#= publish==1?\"icon-unhide.png\":\"icon-hide.png\"#" style="max-width:50px; cursor:pointer" /></p>'},
                      {field: "orderno", title: "Orderno", type:"int", width:"50px", sortable: true },

                      { command:  [
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/module/"+dataItem.id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/system/module/"+dataItem.id+"/edit";
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete module: "'+dataItem.name+'"?')){
                        document.location = "/admin/system/module/"+dataItem.id+"/delete";
                    }
                }

            });
        </script>
    </div>
@stop