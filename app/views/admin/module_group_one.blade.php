@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Module Group{{$columns['id']>0?': '.$columns['name']:''}}</h3>

            <div id="div_quick_search">

            </div>
        </div>
        <form id="frm_module_group" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_module_group" name="action_module_group" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Module Type</td>
                            <td>&nbsp;</td>
                            <td>
                                <select id="module_type_id" name="module_type_id" required data-require-msg="Please choose group's type">
                                    <option value=""{{$columns['module_type_id']==0?' selected="selected"':''}}>--------</option>
                                    <option value="1"{{$columns['module_type_id']==1?' selected="selected"':''}}>Back End</option>
                                    <option value="2"{{$columns['module_type_id']==2?' selected="selected"':''}}>Member</option>
                                    <option value="3"{{$columns['module_type_id']==3?' selected="selected"':''}}>Front End</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Group Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" required data-required-msg="Please input name" />
                                <input type="hidden" id="hidden_name" name="hidden_name" value="{{$columns['name']!=''?'Y':'N'}}" required data-required-msg="Duplicate name" />
                            <span class="k-required">(*)</span>
                                <span id="sp_name"></span>
                                <span class="tooltips"><a title="Group Name is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Group Short Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" />
                        </tr>
                        <tr>
                            <td>Publish</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                        </tr>

                        <tr>
                            <td>Orderno</td>
                            <td>&nbsp;</td>
                            <td><input type="number" id="orderno" name="orderno" value="{{$columns['orderno']}}" /></td>
                        </tr>
                            </table>
                </div>
            </div>
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_module_group" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            $('input#btn_submit_module_group').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
			$('input#module_type_id').kendoNumericTextBox({
            	min:0, step:1, format:'n0'
            });
			$('input#orderno').kendoNumericTextBox({
            	min:0, step:1, format:'n0'
            });
            $('#module_type_id').width(200).kendoDropDownList();
            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");

            $('input#name').focusout(function(){
                var name = $.trim($(this).val());
                var $this = $(this);
                if(name==''){
                    $(this).val('');
                    $('input#hidden_name').val('N');
                    validator.validate();
                    return false;
                }
                $.ajax({
                    url :'/admin/{{$icon["view"]}}/ajax',
                    type:'POST',
                    dataType: 'json',
                    data: {session_id: '{{$session}}', name: name, ajax_type: 'check_unique_name', id:'{{$columns["id"]}}'}
                    ,beforeSend:function(){
                        $this.prop('disabled', true);
                        $('span#sp_name').addClass('k-info-colored').html('Checking...');
                    },
                    success: function(data, status){
                        $('span#sp_name').html('');
                        if(data.success==1){
                            $('input#hidden_name').val('Y');
                        }else{
                            $('input#hidden_name').val('');
                        }
                        validator.validate();
                        $this.prop('disabled', false);
                    }
                });

            });

        });
        </script>
    @stop
