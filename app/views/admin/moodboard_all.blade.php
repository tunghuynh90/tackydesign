@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Moodboard</h3>

        <div id="div_quick_search">
            <form id="frm_quick_search" method="post">
                <span id="txt_quick_search" class="k-textbox k-space-left">
                <input type="text" value="{{$quick}}" placeholder="Search by name" name="txt_quick_search">
                <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                </span>
            </form>
            <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
            </script>
        </div>
    </div>
    <div id="grid"></div>
    <script type="text/javascript">
        function change_status(Obj) {
            var id = $(Obj).attr("data-id");
            var status_ = $(Obj).attr('data-status');
            var val = 1;
            if(status_=='icon-unhide.png') val = 0;
            var name = $(Obj).attr('data-name');
            $.ajax({
                url : '/admin/static-content/moodboard/update-status',
                type    : 'POST',
                data    :   {txt_id: id, txt_name: name,txt_value: val
                },
                success: function(data){
                    if(data.error==0){
                        if(status_=='icon-unhide.png'){
                            $(Obj).attr('src',"{{$url}}/assets/images/icons/icon-hide.png");
                            $(Obj).attr('data-status',"icon-hide.png");
                        }else{
                            $(Obj).attr('src',"{{$url}}/assets/images/icons/icon-unhide.png");
                            $(Obj).attr('data-status',"icon-unhide.png");
                        }
                    }
                }
            });
        }
        $(document).ready(function() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    pageSize: 20,
                    serverPaging: true,
                    serverSorting: true,
                    transport: {
                        read: {
                            url: "/admin/static-content/moodboard/json",
                            type: "POST",
                            data: {session:"{{$session}}", quick:'{{$quick}}'}
                        }
                    },
                    schema: {
                        data: "moodboard"
                        ,total: function(data){
                            return data.total_rows;
                        }
                    },
                    type: "json"
                },
                pageSize: 20,
                height: 430,
                scrollable: true,
                sortable: true,
                //selectable: "single",
                pageable: {
                    input: true,
                    refresh: true,
                    pageSizes: [10, 20, 30, 40, 50],
                    numeric: false
                },
                columns: [
                    {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:center">#= row_order #</span>'},
                    {field: "name", title: "Name", type:"string", width:"100px", sortable: true },
                    {field: "short_name", title: "Short Name", type:"string", width:"50px", sortable: true },
                    {field: "image", title: "Image", type:"string", width:"50px", sortable: true, template:'<p style="text-align:center; margin:0"><img src="#= main_image#" style="max-width:150px" /></p>'},
                    {field: "", title: "", type:"string", width:"50px", sortable: true, template:'<p style="text-align:center; margin:0"><img src="#= name_image#" style="max-width:150px" /></p>'},
                    {field: "publish", title: "Publish", type:"string", width:"40px", sortable: true ,template:'<p style="text-align:center; margin:0"><img class="change_status" @if(isset($permit[_R_UPDATE])) onclick=change_status(this) @endif id="icon_#= id#" data-status="#= publish#" data-id="#= id#"  src="{{$url}}/assets/images/icons/#= publish#" data-name="publish" style="max-width:50px" /></p>'},
                    { command:  [
                        // { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' },
                        @if(isset($permit[_R_UPDATE]))
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        @endif
                        @if(isset($permit[_R_DELETE]))
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        @endif
                    ],
                        title: " ", width: "70px" }
                ]
            }).data("kendoGrid");
            function view_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/static-content/moodboard/"+dataItem.id+"/view";
            }
        @if(isset($permit[_R_UPDATE]))
            function edit_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/static-content/moodboard/"+dataItem.id+"/edit";
            }
        @endif
        @if(isset($permit[_R_DELETE]))
            function delete_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                if(confirm('Do you want to delete lookbook: "'+dataItem.name+'"?')){
                    document.location = "/admin/static-content/moodboard/"+dataItem.id+"/delete";
                }
            }
        @endif
        });
    </script>
</div>
@stop