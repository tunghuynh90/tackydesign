@extends('layouts.admin.admin_layout')
@section('content')
<style type="text/css">
    .selected-value {
        float: left;
        width: 200px;
        margin: 0 4px;
    }
</style>
<script>
    $(document).ready(function(){
        var editor = $('textarea').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                dataType:'json',
                transport: {
                    read: {
                        url: "/admin/images/listImages",
                        type: "POST",
                        dataType:'json'
                    },
                    destroy: {
                        url: "/admin/images/deleteFile",
                        type: "POST",
                        dataType:'json'
                    },
                    uploadUrl: "/admin/images/uploadImage",
                    thumbnailUrl: "/admin/images/thumbImage",
                    imageUrl: "{{$url}}/assets/upload/{0}"
                }
            }
        }).data("kendoEditor");
        // auto upload anh
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
        // end
    });
</script>
  <div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Newsletter</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_newsletter" method="POST" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="submit" name="submit" value="submit" />
        <input type="hidden" id="action_newsletter" name="action_newsletter" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li id="auto_send" class="k-state-active">Setup auto send</li>
                <li id="event_send">Send Event email</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Period for check</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="number" style="width:75px"  id="interval_check" name="interval_check" value="{{$columns['interval_check']}}" /> hour(s)
                        </td>
                    </tr>
                    <tr>
                        <td>Minimum to send</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="number" style="width:75px"  id="min_product" name="min_product" value="{{@$columns['min_product']}}" /> product(s)
                        </td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" class="k-textbox" name="subject" value="{{$columns['subject']}}" />
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td>Field</td>
                        <td>&nbsp;</td>
                        <td>
                            [USER] : User's full name<br />
                            [PRODUCT_CONTENT] : Info about products<br />
                        </td>
                    </tr>
                    <tr>
                        <td>Content</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea id="content" name="content" style="width:90%; height:500px; padding:5px">
                                {{$columns['content']}}
                            </textarea>
                        </td>
                    </tr>
                </table>
            </div>
        <input type="submit" id="submit"  style="display: none;" />
    </form>
            <div class="event_email">
                <table id="tbl_single">
                    <tr>
                        <td>Send to</td>
                        <td>&nbsp;</td>
                        <td>
                            <select id="event_send_to" name="event_send_to" ></select>
                            <script type="text/javascript">
                                $("#event_send_to").width(300).kendoDropDownList({
                                    contentType: "charset=utf-8",
                                    dataTextField: "name",
                                    dataValueField: "value",
                                    dataSource: [{"name":"All user","value":"all_user"},{"name":"One User","value":"one_user"}],
                                }).data("kendoDropDownList");
                                $("#event_send_to").change(function(){
                                    var value = $(this).val();
                                    $("#one_user").html("");
                                    if(value == "one_user"){
                                        var html = "<td>Email</td>";
                                        html += "<td>&nbsp;</td>";
                                        html += "<td><input type=\"email\" class=\"k-textbox\"  name=\"event_email\" value=\"\" /></td>";
                                        $("#one_user").html(html);
                                    }
                                });
                            </script>
                        </td>
                    </tr>
                    <tr id="one_user"></tr>
                    <tr>
                        <td>Subject</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" class="k-textbox" id="event_subject" name="event_subject" value="" />
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td>Field</td>
                        <td>&nbsp;</td>
                        <td>
                            [USER] : User's full name<br />
                        </td>
                    </tr>
                    <tr>
                        <td>Content</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea id="event_content" name="event_content" style="width:90%; height:500px; padding:5px"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
        <ul>
        {{$message}}
        </ul>
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
        <input type="button" id="btn_submit_newsletter" value="Save" class="k-button button_css" />
        <input type="button" id="btn_send_event" value="Send" class="k-button button_css" style="display: none" />
        </div>
        @endif
  </div>
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $("#interval_check,#min_product").kendoNumericTextBox({
            min: -1,
            step: 1,
            format:'n0'
        });

        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
            open: {
                effects: "fadeIn"
                }
        }
        }).data("kendoTabStrip");
        $("#auto_send,#event_send").click(function(){
            $("#btn_submit_newsletter").toggle();
            $("#btn_send_event").toggle();
        })
        $('#btn_submit_newsletter').click(function(){
            $("input[type=submit]#submit").click();
        });
        $("#btn_send_event").click(function(){
            $(this).attr({"type":"image","src":"{{$url}}/assets/images/ajax-loader.gif"}).css("height","25px");
            var data = $("input,select,textarea",".event_email").serialize();
            $.ajax({
                url: "{{$url}}/admin/customers/newsletter/send-newsletter",
                type: "POST",
                data: data,
                success: function(result){
                    alert(result.toUpperCase());
                    $("#btn_send_event").attr("type","button").removeAttr("src");
                }
            });
            event.preventDefault();
        });
    });
    </script>
@stop
