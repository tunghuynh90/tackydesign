@extends('layouts.admin.admin_layout')
@section('content')
<style type="text/css">
    .selected-value {
        float: left;
        width: 200px;
        margin: 0 4px;
    }
</style>
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Config</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_config" method="post" enctype="multipart/form-data" >
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_config" name="action_config" value="{{$columns['id']==0?'new':'edit'}}" />
            <input type="hidden" id="created_date" name="created_date" value="{{$columns['created_date']}}" />
            <input type="hidden" id="created_by" name="created_by" value="{{$columns['created_by']}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Title Site</td>
                            <td>&nbsp;</td>
                            <td><input type="text" style="width: 500px" class="k-textbox" id="title_site" name="title_site" value="{{$columns['title_site']}}" /></td>
                        </tr>

                        <tr>
                            <td>Meta Keywords</td>
                            <td>&nbsp;</td>
                            <td>
                                <textarea style="width: 500px; height: 100px; resize: none" class="k-textbox" id="meta_keywords" name="meta_keywords">{{$columns['meta_keywords']}}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td>Meta Description</td>
                            <td>&nbsp;</td>
                            <td>
                                <textarea style="width: 500px; height: 100px; resize: none" class="k-textbox" id="meta_description" name="meta_description">{{$columns['meta_description']}}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>Main Logo</td>
                            <td>&nbsp;</td>
                            <td>
                                <img id="logo_image_link"  style="max-width: 250px" src="{{$columns['logo_image_link']}}" />
                                <input <?php if($columns['logo_image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="logo_image" readonly data-id="logo_image_link" name="logo_image" value="{{$columns['logo_image']}}" />
                                <span  id="logo_image_link_delete" <?php if($columns['logo_image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg('logo_image_link')"><img style="width: 25px;" src="{{$url}}/assets/images/icons/delete.png" /></span>
                                <script type="text/javascript">
                                    $(function(){
                                        $("#logo_image").kendoEditor({
                                            tools: [
                                                "insertImage"
                                            ],
                                            imageBrowser: {
                                                dataType:'json',
                                                transport: {
                                                    read: {
                                                        url: "/admin/images/listImages",
                                                        type: "POST",
                                                        data:{image_path : 'images/logos'},
                                                        dataType:'json'
                                                    },
                                                    destroy: {
                                                        url: "/admin/images/deleteFile",
                                                        type: "POST",
                                                        dataType:'json',
                                                        data:{image_path : 'images/logos'}
                                                    },
                                                    uploadUrl: "/admin/images/uploadImage?image_path=logo",
                                                    thumbnailUrl: "/admin/images/thumbImage/?image_path=logo",
                                                    imageUrl: function(imagename){
                                                        $("#logo_image").val(imagename).attr('type','hidden');
                                                        var img_id = $("#logo_image").attr("data-id");
                                                        $("#"+img_id).attr("src","{{$url}}/assets/images/logos/"+imagename);
                                                        $("#"+img_id+"_delete").show();
                                                        event.preventDefault();
                                                    }
                                                }
                                            }
                                        });
                                    })
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td>Logo in Mobile</td>
                            <td>&nbsp;</td>
                            <td>
                                <img id="logo_retina_image_link" style="max-width: 250px" src="{{$columns['logo_retina_image_link']}}" />
                                <input <?php if($columns['logo_retina_image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="logo_retina_image" readonly data-id="logo_retina_image_link" name="logo_retina_image" value="{{$columns['logo_retina_image']}}" />
                                <span id="logo_retina_image_link_delete" <?php if($columns['logo_retina_image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg('logo_retina_image_link')"><img src="{{$url}}/assets/images/icons/delete.png" style="width: 25px;" /></span>
                                <script type="text/javascript">
                                    $(function(){
                                        $("#logo_retina_image").kendoEditor({
                                            tools: [
                                                "insertImage"
                                            ],
                                            imageBrowser: {
                                                dataType:'json',
                                                transport: {
                                                    read: {
                                                        url: "/admin/images/listImages",
                                                        type: "POST",
                                                        data:{image_path : 'images/logos'},
                                                        dataType:'json'
                                                    },
                                                    destroy: {
                                                        url: "/admin/images/deleteFile",
                                                        type: "POST",
                                                        dataType:'json',
                                                        data:{image_path : 'images/logos'}
                                                    },
                                                    uploadUrl: "/admin/images/uploadImage?image_path=logo",
                                                    thumbnailUrl: "/admin/images/thumbImage/?image_path=logo",
                                                    imageUrl: function(imagename){
                                                        $("#logo_retina_image").val(imagename).attr('type','hidden');
                                                        var img_id = $("#logo_retina_image").attr("data-id");
                                                        $("#"+img_id).attr("src","{{$url}}/assets/images/logos/"+imagename);
                                                        $("#"+img_id+"_delete").show();
                                                        event.preventDefault();
                                                    }
                                                }
                                            }
                                        });
                                    })
                                </script>
                            </td>
                        </tr>
                         <tr>
                            <td>Favicon</td>
                            <td>&nbsp;</td>
                            <td>
                                <img id="favicon_link" style="max-width: 50px" src="{{$columns['favicon_link']}}" />
                                <input <?php if($columns['favicon'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="favicon" readonly data-id="favicon_link" name="favicon" value="{{$columns['favicon']}}" />
                                <span id="favicon_link_delete" <?php if($columns['favicon'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg('favicon_link')"><img src="{{$url}}/assets/images/icons/delete.png" style="width: 25px;" /></span>
                                <script type="text/javascript">
                                    $(function(){
                                        $("#favicon").kendoEditor({
                                            tools: [
                                                "insertImage"
                                            ],
                                            imageBrowser: {
                                                dataType:'json',
                                                transport: {
                                                    read: {
                                                        url: "/admin/images/listImages",
                                                        type: "POST",
                                                        data:{image_path : 'images/logos'},
                                                        dataType:'json'
                                                    },
                                                    destroy: {
                                                        url: "/admin/images/deleteFile",
                                                        type: "POST",
                                                        dataType:'json',
                                                        data:{image_path : 'images/logos'}
                                                    },
                                                    uploadUrl: "/admin/images/uploadImage?image_path=logo",
                                                    thumbnailUrl: "/admin/images/thumbImage/?image_path=logo",
                                                    imageUrl: function(imagename){
                                                        $("#favicon").val(imagename).attr('type','hidden');
                                                        var img_id = $("#favicon").attr("data-id");
                                                        $("#"+img_id).attr("src","{{$url}}/assets/images/logos/"+imagename);
                                                        $("#"+img_id+"_delete").show();
                                                        event.preventDefault();
                                                    }
                                                }
                                            }
                                        });
                                    })
                                </script>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_config" name="submit" value="Submit" class="k-button button_css" />
            </div>
            @endif
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            @if(isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE]))
            $('input#btn_submit_config').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            @endif

            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");

            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        function removeImg(img_id){
            $("#"+img_id).attr("src","");
            $("#"+img_id+"_delete").hide();
            var input_id = img_id.replace("_link","");
            $("#"+input_id).val("").attr("type","text");
        }
        </script>
    @stop
