@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Orders</h3>

    </div>
    <div id="grid"></div>
    <script type="text/javascript">
        $(document).ready(function() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    pageSize: 20,
                    serverPaging: true,
                    serverSorting: true,
                    transport: {
                        read: {
                            url: "/admin/customers/orders/json",
                            type: "POST",
                            data: {session:"{{$session}}", quick:'{{$quick}}'}
                        }
                    },
                    schema: {
                        data: "order"
                        ,total: function(data){
                            return data.total_rows;
                        }
                    },
                    type: "json"
                },
                pageSize: 20,
                height: 430,
                scrollable: true,
                sortable: true,
                //selectable: "single",
                pageable: {
                    input: true,
                    refresh: true,
                    pageSizes: [10, 20, 30, 40, 50],
                    numeric: false
                },
                columns: [
                    {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:center">#= row_order #</span>'},
                    {field: "email", title: "Email", type:"string", width:"100px", sortable: true },
                    {field: "order_status", title: "Order Status", type:"string", width:"150px", sortable: true },
                    {field: "payment_status", title: "Payment Status", type:"string", width:"150px", sortable: true },
                    {field: "total", title: "Total", type:"string", width:"250px", sortable: true },
                    {field: "created_date", title: "Date", type:"string", width:"150px", sortable: true},
                    { command:  [
                        // { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' },
                        @if(isset($permit[_R_UPDATE]))
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        @endif
                        @if(isset($permit[_R_DELETE]))
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        @endif
                    ],
                        title: " ", width: "70px" }
                ]
            }).data("kendoGrid");
            function view_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/customers/orders/"+dataItem.id+"/view";
            }
        @if(isset($permit[_R_UPDATE]))
            function edit_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/customers/orders/"+dataItem.id+"/edit";
            }
        @endif
        @if(isset($permit[_R_DELETE]))
            function delete_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                if(confirm('Do you want to delete order: "'+dataItem.name+'"?')){
                    document.location = "/admin/customers/orders/"+dataItem.id+"/delete";
                }
            }
        @endif
        });
    </script>
</div>
@stop