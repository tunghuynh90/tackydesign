@extends('layouts.admin.admin_layout')
@section('content')
<style>
    #tbl_single td:first-child{
        width:200px !important;
    }
    .fielddis{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .fielddis label {
        width: 100px;
        margin: 0;
        float: left;
        text-align: right;
        padding-right: 5px;
        line-height: 26px;
    }
    .white {
        background-color: #fff !important;
    }
    .center-text{
        text-align: center;
    }
    .right-text{
        text-align: right;
    }
</style>
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Order</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_product" action="admin/products/products/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_product" name="action_product" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>Detail</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Customer</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="user" disabled value="{{$columns['user']}}" /></td>
                    </tr>

                    <tr>
                        <td>Email</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="email" disabled value="{{$columns['email']}}" /></td>
                    </tr>
                    <tr>
                        <td>Order Status</td>
                        <td>&nbsp;</td>
                        <td>
                            <select id="order_status" name="order_status"></select>
                        </td>
                        <script type="text/javascript">
                            $("#order_status").width(300).kendoDropDownList({
                                contentType: "charset=utf-8",
                                placeholder: "Select status...",
                                dataTextField: "name",
                                dataValueField: "value",
                                dataSource: {{json_encode($columns['status_list'])}},
                                value : {{json_encode($columns['order_status'])}}
                            }).data("kendoDropDownList");
                        </script>
                    </tr>
                    <tr>
                        <td>Payment Status</td>
                        <td>&nbsp;</td>
                        <td>
                            <select id="payment_status" name="payment_status"></select>
                        </td>
                        <script type="text/javascript">
                            $("#payment_status").width(300).kendoDropDownList({
                                contentType: "charset=utf-8",
                                placeholder: "Select status...",
                                dataTextField: "name",
                                dataValueField: "value",
                                dataSource: {{json_encode($columns['payment_status_list'])}},
                                value : {{json_encode($columns['payment_status'])}}
                            }).data("kendoDropDownList");
                        </script>
                    </tr>
                    <tr>
                        <td>Billing Address</td>
                        <td>&nbsp;</td>
                        <td>
                            <?php
                                $billing_address = unserialize($columns['billing_address']);
                                if(isset($billing_address['country']) && is_numeric($billing_address['country']))
                                    $billing_address['country'] = Country::where('id','=',$billing_address['country'])->pluck('name');
                                if(isset($billing_address['province']) && is_numeric($billing_address['province']))
                                    $billing_address['province'] = City::where('id','=',$billing_address['province'])->pluck('name');
                                $billing_address = implode("\n", $billing_address);
                            ?>
                            <textarea id="billing_address" disabled readonly class="k-textbox" rows="7" style="width: 300px">{{$billing_address}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Shipping Address</td>
                        <td>&nbsp;</td>
                        <td>
                            <?php
                                $shipping_address = unserialize($columns['shipping_address']);
                                if(isset($shipping_address['country']) && is_numeric($shipping_address['country']))
                                    $shipping_address['country'] = Country::where('id','=',$shipping_address['country'])->pluck('name');
                                if(isset($shipping_address['province']) && is_numeric($shipping_address['country']))
                                    $shipping_address['province'] = City::where('id','=',$shipping_address['province'])->pluck('name');
                                $shipping_address = implode("\n", $shipping_address);
                            ?>
                            <textarea id="shipping_address" disabled readonly class="k-textbox" rows="7" style="width: 300px">{{$shipping_address}}</textarea>
                        </td>
                    </tr>

                     <tr>
                        <td>Customer notes</td>
                        <td>&nbsp;</td>
                        <td>
                            <textarea id="note" disabled readonly class="k-textbox" rows="7" style="width: 300px">{{$columns['note']}}</textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>Created date</td>
                        <td>&nbsp;</td>
                        <td>
                            <input id="created_date" name="created_date" value="{{$columns['created_date']}}" disabled readonly />
                        </td>
                        <script type="text/javascript">
                           $("#created_date").kendoDatePicker({
                            }).data("kendoDatePicker");
                        </script>
                    </tr>
                    <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>
                            <input id="save" name="save" value="Save" type="button" class="k-button button_css" />
                        </td>
                        <script type="text/javascript">
                            $("#save").click(function(){
                                $(this).attr({"type":"image","src":"{{$url}}/assets/images/ajax-loader.gif","disabled":true}).css("height","25px");
                                $.ajax({
                                    url: "{{$url}}/admin/customers/orders/change",
                                    type: "POST",
                                    data: {order_status : $("#order_status").val(), payment_status : $("#payment_status").val(), order_id : {{$columns['id']}}},
                                    success: function(result){
                                        alert("Saved");
                                        $("#save").attr({"type":"text","disabled":false}).removeAttr("src");
                                    }
                                });

                            });
                        </script>
                    </tr>
                </table>
            </div>
            <div class="detail">
                <table class="tbl_order">
                    <tr>
                        <th>Product Info</th>
                        <th>Uploaded Image / Design</th>
                        <th>Quantity</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <?php $total = 0; ?>
                    @foreach($product_details as $details)
                    <tr>
                        <td>
                            {{$details['name']}}
                        </td>
                        <td>
                            <img src="{{$details['image']}}" align="absmiddle" style="max-width: 150px" />
                        </td>
                        <td style="text-align: right">{{$details['quantity']}}</td>
                        <td style="text-align: right">{{$details['size']}}</td>
                        <td style="text-align: right">{{number_format($details['sell_price'],2)}}</td>
                        <td style="text-align: right">{{number_format($details['total'],2)}}</td>
                        <?php $total+= $details['total']; ?>
                    </tr>
                    @endforeach
                </table>
                <table class="tbl_price" style="width: 100%">
                    <tr>
                        <th style="width: 50%; text-align: right"></th>
                        <td style="width: 50%; text-align: right">
                            <table style="width: 500px; float: right; font-weight: 700" class="tbl_price">

                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        $ {{number_format($total,2)}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Voucher: {{$columns['voucher']}}<br/>
                                        Discount {{$columns['discount'] ? $columns['discount'].'%' : ''}}<br/>
                                    </td>
                                    <td>
                                        <?php
                                            $discount = $total*$columns['discount']/ 100;
                                        ?>
                                        $ {{$discount ? number_format(-$discount,2) : number_format(-$discount,2)}}<br/>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        Shipping price:
                                    </td>
                                    <td>$ {{number_format($columns['shipping_price'],2)}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr class="price" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total:
                                    </td>
                                    <td>
                                        $ {{number_format($columns['total'],2)}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @endif
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");

    });
</script>
@stop
