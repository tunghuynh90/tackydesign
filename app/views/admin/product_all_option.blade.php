@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Option</h3>
        <div id="div_quick_search">
            <form id="frm_quick_search" method="post">
                <span id="txt_quick_search" class="k-textbox k-space-left">
                <input type="text" value="{{$quick}}" placeholder="Search by name" name="txt_quick_search">
                <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                </span>
            </form>
            <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
            </script>
        </div>
    </div>
    <div id="grid"></div>
    <script type="text/javascript">
        $(document).ready(function() {
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    pageSize: 20,
                    serverPaging: true,
                    serverSorting: true,
                    transport: {
                        read: {
                            url: "/admin/products/option/json",
                            type: "POST",
                            data: {session:"{{$session}}", quick:'{{$quick}}'}
                        }
                    },
                    schema: {
                        data: "option"
                        ,total: function(data){
                            return data.total_rows;
                        }
                    },
                    type: "json"
                },
                pageSize: 20,
                height: 430,
                scrollable: true,
                sortable: true,
                //selectable: "single",
                pageable: {
                    input: true,
                    refresh: true,
                    pageSizes: [10, 20, 30, 40, 50],
                    numeric: false
                },
                columns: [
                    {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:center">#= row_order #</span>'},
                    {field: "name", title: "Name", type:"string", width:"100px", sortable: true },
                    {field: "key", title: "Key", type:"string", width:"50px", sortable: true },
                    { command:  [
                        // { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' },
                        @if(isset($permit[_R_UPDATE]))
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        @endif
                        @if(isset($permit[_R_DELETE]))
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        @endif
                    ],
                        title: " ", width: "70px" }
                ]
            }).data("kendoGrid");
            function view_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/products/option/"+dataItem.id+"/view";
            }
        @if(isset($permit[_R_UPDATE]))
            function edit_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                document.location = "/admin/products/option/"+dataItem.id+"/edit";
            }
        @endif
        @if(isset($permit[_R_DELETE]))
            function delete_row(e) {
                e.preventDefault();
                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                if(confirm('Do you want to delete option: "'+dataItem.name+'"?')){
                    document.location = "/admin/products/option/"+dataItem.id+"/delete";
                }
            }
        @endif
        });
    </script>
</div>
@stop