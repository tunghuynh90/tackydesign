@extends('layouts.admin.admin_layout')
@section('content')
<script>
    $(document).ready(function(){
        var editor = $('textarea#description,textarea#application,textarea#technical,textarea#articles').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
//                    "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
        }).data("kendoEditor");
        // auto upload anh
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
        // end
    });
</script>
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Product Category</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_category" action="admin/products/categories/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_category" name="action_category" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>Description</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Name</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>

                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>
                    <tr>
                        <td>Parent Category</td>
                        <td>&nbsp;</td>
                        <td><select id="parent_category" name="parent_category" ></select></td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td>&nbsp;</td>
                        <td>
                            <img id="image_link" style="max-width: 150px;" src="{{$columns['image_link']}}" />
                            <input <?php if($columns['image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="image" readonly data-id="image_link" name="image" value="{{$columns['image']}}" />
                            <span id="image_link_delete" <?php if($columns['image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg(this,'image_link')"><img src="{{$url}}/assets/images/icons/delete.png" /></span>
                            <script type="text/javascript">
                                $(function(){
                                    $("#image").kendoEditor({
                                        tools: [
                                            "insertImage"
                                        ],
                                        imageBrowser: {
                                            dataType:'json',
                                            transport: {
                                                read: {
                                                    url: "/admin/images/listImages",
                                                    type: "POST",
                                                    dataType:'json'
                                                },
                                                destroy: {
                                                    url: "/admin/images/deleteFile",
                                                    type: "POST",
                                                    dataType:'json'
                                                },
                                                uploadUrl: "/admin/images/uploadImage",
                                                thumbnailUrl: "/admin/images/thumbImage",
                                                imageUrl: function(imagename){
                                                    $("#image").val(imagename).attr('type','hidden');
                                                    var img_id = $("#image").attr("data-id");
                                                    $("#"+img_id).attr("src","{{$url}}/assets/upload/"+imagename);
                                                    $("#"+img_id+"_delete").show();
                                                    event.preventDefault();
                                                }
                                            }
                                        }
                                    });
                                })
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>On Shop</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="on_shop" name="on_shop" value="{{$columns['on_shop']}}"{{$columns['on_shop']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>On Collection</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="on_collection" name="on_collection" value="{{$columns['on_collection']}}"{{$columns['on_collection']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Order</td>
                        <td>&nbsp;</td>
                        <td><input type="number" id="order_no" name="order_no" value="{{$columns['order_no']}}" /></td>
                    </tr>
                </table>
            </div>
            <div class="Description">
                <textarea id="description" name="description" style="width:90%; height:300px; padding:5px">
                    {{$columns['description']}}
                </textarea>
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_category" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
        @endif
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $('input#order_no').kendoNumericTextBox({
            min:0, step:1, format:'n0'
        });
        var category_list = {{$columns['category_list']}};
        var parent_category = $('select#parent_category').width(300).kendoComboBox({
            contentType: "charset=utf-8",
            dataSource: category_list,
            dataTextField:'name',
            dataValueField:'value',
            suggest: true,
        }).data("kendoComboBox");
        parent_category.value({{$columns['parent_category']}});
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $('input#btn_submit_category').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        $('input#orderno').kendoNumericTextBox({
            min:0, step:1
        });
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
    });
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
    function removeImg(obj,img_id){
        $("#image").val("");
        $("#"+img_id).attr("src","");
        $(obj).hide();
        var input_id = img_id.replace("_link","");
        $("#"+input_id).val("").attr("type","text");
    }
</script>
@stop
