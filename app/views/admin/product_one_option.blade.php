@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Option</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_category" action="admin/products/option/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_option" name="action_option" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Name</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>
                    <tr>
                        <td>Key</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="key" name="key" value="{{$columns['key']}}" /></td>
                    </tr>
                </table>
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_option" name="btn_submit_option" value="Submit" class="k-button button_css" />
        </div>
        @endif
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $('input#btn_submit_option').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#name").keypress(function(){
            $("#key").val(toKey($(this).val()));
        });
        $("#name").change(function(){
            $("#key").val(toKey($(this).val()));
        });
    });
    function toKey(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '_');
        return value;
    }
</script>
@stop
