@extends('layouts.admin.admin_layout')
@section('content')
<style>
    #tbl_single td:first-child{
        width:200px !important;
    }
    .fielddis{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .fielddis label {
        width: 100px;
        margin: 0;
        float: left;
        text-align: right;
        padding-right: 5px;
        line-height: 26px;
    }
    .white {
        background-color: #fff !important;
    }
    .center-text{
        text-align: center;
    }
    .right-text{
        text-align: right;
    }
    .dropdown-header {
                    font-size: 1.2em;
    }

    .dropdown-header > span {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        text-align: left;
        display: inline-block;
        border-style: solid;
        border-width: 0 0 1px 1px;
        padding: .3em .6em;
        width: 79%;
    }

    .dropdown-header > span:first-child {
        width: 82px;
        border-left-width: 0;
    }

    .choose-product-container {
        width: 400px;
        padding: 30px;
    }
    .choose-product-container h2 {
        text-transform: uppercase;
        font-size: 1.2em;
        margin-bottom: 10px;
    }

    .product-preview{
        width: 70px;
    }
    .selected-value {
        float: left;
        width: 16px;
        margin: 3px 4px;
    }

    #choose-product-list .k-item > span{
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        display: inline-block;
        border-style: solid;
        border-width: 0 0 1px 1px;
        vertical-align: top;
        min-height: 95px;
        width: 79%;
        padding: .6em 0 0 .6em;
    }

    #choose-product-list .k-item > span:first-child{
        width: 77px;
        border-left-width: 0;
        padding: .6em 0 0 0;
    }

    #choose-product-list img {
        -moz-box-shadow: 0 0 2px rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.4);
        box-shadow: 0 0 2px rgba(0,0,0,.4);
        width: 70px;
        height: 77px;
    }

    #choose-product-list h3 {
        font-size: 1.6em;
        margin: 0;
        padding: 0;
    }

    #choose-product-list p {
        margin: 0;
        padding: 0;
    }
</style>
<script>
    $(document).ready(function(){
        var editor = $('textarea#description').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                dataType:'json',
                transport: {
                    read: {
                        url: "/admin/images/listImages",
                        type: "POST",
                        dataType:'json'
                    },
                    destroy: {
                        url: "/admin/images/deleteFile",
                        type: "POST",
                        dataType:'json'
                    },
                    uploadUrl: "/admin/images/uploadImage",
                    thumbnailUrl: "/admin/images/thumbImage",
                    imageUrl: "{{$url}}/assets/upload/{0}"
                }
            }
        }).data("kendoEditor");
        // auto upload anh
        $('input#txt_image_file').kendoUpload({
            multiple:false
        });
        // end
    });
</script>
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Products</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_product" action="admin/products/products/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_product" name="action_product" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>Image</li>
                <li>Description</li>
                <li>Templates</li>
                <li>Other</li>
                <li>Price</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Name</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>

                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>
                    <tr>
                        <td>SKU</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="sku" name="sku" value="{{$columns['sku']}}" /></td>
                    </tr>
                    <tr>
                        <td>Category</td>
                        <td>&nbsp;</td>
                        <td>
                            <select id="category_id" name="category_id[]"></select>
                        </td>
                        <script type="text/javascript">
                            $("#category_id").width(300).kendoMultiSelect({
                                contentType: "charset=utf-8",
                                placeholder: "Select category...",
                                dataTextField: "name",
                                dataValueField: "value",
                                dataSource: {{json_encode($columns['category_list'])}},
                                value : {{json_encode($columns['category_id'])}}
                            }).data("kendoMultiSelect");
                        </script>
                    </tr>

                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>

                </table>
            </div>
            <div class="Image">
                <table id="tbl_single">
                     <tr>
                        <td>Main Image</td>
                        <td>&nbsp;</td>
                        <td>
                            <img id="main_image_link" style="max-width: 150px;" src="{{$columns['image_link']}}" />
                            <input <?php if($columns['main_image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="main_image" readonly data-id="main_image_link" name="main_image" value="{{$columns['main_image']}}" />
                            <span id="main_image_link_delete" <?php if($columns['main_image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg(this,'main_image_link')"><img src="{{$url}}/assets/images/icons/delete.png" /></span>
                            <script type="text/javascript">
                                $(function(){
                                    $("#main_image").kendoEditor({
                                        tools: [
                                            "insertImage"
                                        ],
                                        imageBrowser: {
                                            dataType:'json',
                                            transport: {
                                                read: {
                                                    url: "/admin/images/listImages",
                                                    type: "POST",
                                                    dataType:'json'
                                                },
                                                destroy: {
                                                    url: "/admin/images/deleteFile",
                                                    type: "POST",
                                                    dataType:'json'
                                                },
                                                uploadUrl: "/admin/images/uploadImage",
                                                thumbnailUrl: "/admin/images/thumbImage",
                                                imageUrl: function(imagename){
                                                    $("#main_image").val(imagename).attr('type','hidden');
                                                    var img_id = $("#main_image").attr("data-id");
                                                    $("#"+img_id).attr("src","{{$url}}/assets/upload/"+imagename);
                                                    $("#"+img_id+"_delete").show();
                                                    event.preventDefault();
                                                }
                                            }
                                        }
                                    });
                                })
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>Other Image</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="other_image" value="{{$columns['hidden_other_image']}}" />
                             <div class="dropzone dz-clickable" id="other_image" >
                                <div class="dz-default dz-message" data-dz-message="">
                                    <span>Drop files here to upload</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="Description">
                <textarea id="description" name="description" style="width:90%; height:300px; padding:5px">
                    {{$columns['description']}}
                </textarea>
            </div>
            <div class="template div_details">
                <table id="tbl_single">
                    <!-- <tr>
                        <td>Design Online</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="design_online" name="design_online" value="{{$columns['design_online']}}"{{$columns['design_online']==1?' checked="checked"':''}} /></td>
                    </tr> -->
                    <tr valign="top">
                        <td align="right" style="width: 200px">Filter</td>
                        <td  style="width: 2px" align="right">&nbsp;</td>
                        <td align="left">
                            <select id="txt_filter_template">
                                <option value="startswith">Starts with</option>
                                <option value="contains">Contains</option>
                                <option value="eq">Equal</option>
                            </select>
                            <input type="text" class="k-textbox" id="txt_filter_template_value" />
                            <input type="button" id="btn_filter_template" class="k-button" value="Find template" />
                        </td>
                    </tr>
                    <tr align="right" valign="top">
                        <td style="width:200px">Template</td>
                        <td  style="width: 2px" align="right">&nbsp;</td>
                        <td align="left">
                            <select id="txt_template" name="txt_template[]" multiple="multiple"> </select>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="Other">
                <table id="tbl_single">
                    <tr>
                        <td>Is Sale</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="is_sale" name="is_sale" value="{{$columns['is_sale']}}"{{$columns['is_sale']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Is Sold Out</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="is_sold_out" name="is_sold_out" value="{{$columns['is_sold_out']}}" {{$columns['is_sold_out']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Is Gift card</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="gift_card" name="gift_card" value="{{$columns['gift_card']}}" {{$columns['gift_card']==1?' checked="checked"':''}} /></td>
                    </tr>
                    @foreach($columns['property_list'] as $property_name => $property_value)
                    <?php if($property_name == 'size') continue; ?>
                    <tr>
                        <td>{{ucfirst($property_name)}}</td>
                        <td>&nbsp;</td>
                        <td><select id="{{$property_name}}" name="{{$property_name}}[]"></select></td>
                        <script type="text/javascript">
                            $("#{{$property_name}}").width(300).kendoMultiSelect({
                                contentType: "charset=utf-8",
                                placeholder: "Select {{$property_name}}...",
                                dataTextField: "name",
                                dataValueField: "value",
                                dataSource: {{json_encode($property_value)}},
                                value : {{json_encode($columns[$property_name])}}
                            }).data("kendoMultiSelect");
                        </script>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div class="price">
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_product" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
        @endif
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
            var optionPriceUrl = "{{$url}}/admin/products/product-option-price",
                dataSource = new kendo.data.DataSource({
                    pageSize: 20,
                    serverPaging: true,
                    transport: {
                        read:  {
                            url: optionPriceUrl + "/list",
                            type: "POST",
                            data: { product_id : {{$columns['id']}}}
                        },
                        update: {
                            url: optionPriceUrl + "/update",
                            type: "POST"
                        },
                        destroy:{
                            url: optionPriceUrl + "/delete/",
                        },
                        create: {
                            url: optionPriceUrl + "/add",
                            type: "POST",
                            data: { product_id : {{$columns['id']}}},
                            complete: function(e) {
                                $(".price").data("kendoGrid").dataSource.read();
                            }
                        },
                    },
                    type: "json",
                    batch: true,
                    pageSize: 20,
                    pageable: {
                        input: true,
                        refresh: true,
                        pageSizes: [10, 20, 30, 40, 50],
                        numeric: false
                    },
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { editable: false, nullable: true },
                                // option_id: { validation: { required: true } },
                                sizew: { type: "number", validation: { min: 0, required: true } },
                                sizeh: { type: "number", validation: { min: 0, required: true } },
                                weight: { type: "number", validation: { min: 0, required: true } },
                                default: { type: "boolean" },
                                sell_price: { type: "number", validation: { min: 0, required: true } },
                                bigger_price: { type: "number", validation: { min: 0, nullable: true } },
                            }
                        },
                        data: "options"
                        ,total: function(data){
                            return data.total_rows;
                        }
                    },
                });

        $(".price").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            height: 450,
            toolbar: ["create"],
            columns: [
                { field: "product_id",  hidden: true  },
                { field: "default",  title: "Default", width: "120px", attributes: { class: "center-text"}},
                { field: "sizew",  title: "Size W", width: "120px", attributes: { class: "right-text"}},
                { field: "sizeh",  title: "Size H", width: "120px", attributes: { class: "right-text"}},
                { field: "weight",  title: "Weight (kg)", width: "120px", attributes: { class: "right-text"}},
                // { field: "option_id", title: "Size", width: "200px",  editor: optionDropDownEditor, template: "#=getOptionName(option_id)#" },
                { field: "sell_price", title:"Sell Price", width: "120px" , attributes: { class: "right-text"}},
                { field: "bigger_price", title:"Bigger Price", width: "120px" , attributes: { class: "right-text"}},
                { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }],
            editable: "inline"
        });
        $(".k-grid-add",".price").after(" or <span id=\"choose-product-container\"><select style=\"width: 400px\" id=\"choose-product\"></select></span>");
        $("#choose-product").kendoDropDownList({
            optionLabel: {
                id: 0,
                name: "Clone option from...",
                main_image: "no-image-large.gif"
            },
            dataTextField: "sell_price",
            dataValueField: "id",
            headerTemplate: '<div class="dropdown-header">' +
                        '<span class="k-widget k-header">Image</span>' +
                        '<span class="k-widget k-header">Name</span>' +
                '</div>',
            valueTemplate: '<img class="selected-value product-preview" src=\"{{$url}}/assets/upload/#:data.main_image#\" alt=\"#:data.main_image#\" /><span>#:data.name#</span>',
            template: '<span class="k-state-default"><img class="product-preview" src=\"{{$url}}/assets/upload/#:data.main_image#\" alt=\"#:data.main_image#\" /></span>' +
                    '<span class="k-state-default"><p>#: data.name #</p></span>',
            dataSource: {
                transport: {
                    read: {
                        dataType: "json",
                        type: "POST",
                        data: {current_id : "{{$columns['id']}}"},
                        url: "{{$url}}/admin/products/products/list-products"
                    }
                }
            },
            select : cloneOption
        });
        var product_id = {{$columns['id']}};
        if(product_id == 0){
            $(".k-grid-add").hide();
        }
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $("#weight").kendoNumericTextBox({
            min: 0,
            step: 0.01
        });
        $('input#btn_submit_product').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        Dropzone.autoDiscover = false;
        @if($edit)
        @if(strpos($_SERVER['REQUEST_URI'],'edit') === false)
        $("#frm_product").dropzone({
            init: function() {
                var myDropzone = this; //closure
                this.on("complete", function(){
                });
                this.on("success", function(file, responseText) {
                    var file_name = file.name;
                    var other_image = $("input[name=other_image]").val();
                    $("input[name=other_image]").val(other_image+"@_@"+file_name);
                });
                this.on("removedfile",function(file){
                    var file_name = file.name;
                    removeFile(file_name);
                });
             },
            url: '{{$url}}/admin/images/uploadImage',
            addRemoveLinks: true,
            dictDefaultMessage: "Drag or Click",
            clickable: true,
            method: 'POST',
            paramName: "other_image",
            maxFiles:{{MAX_UPLOAD_FILE}},
            parallelUploads: 10,
            maxFilesize:2,
            previewsContainer:'#other_image',
            autoProcessQueue: true,
            uploadMultiple: true
        });
        @else

        $("#frm_product").dropzone({
            init: function() {
                var other_image = {{$columns['other_image']}};
                for(i in other_image){
                    var mockFile = { name: other_image[i].name, size: other_image[i].size };
                    this.options.addedfile.call(this, mockFile);
                    this.options.thumbnail.call(this, mockFile, "{{$url.'/'}}"+other_image[i].link);

                }
                this.on("success", function(file, responseText) {
                    var file_name = file.name;
                    var other_image = $("input[name=other_image]").val();
                    $("input[name=other_image]").val(other_image+"@_@"+file_name);
                });
                this.on("removedfile",function(file){
                    var file_name = file.name;
                    removeFile(file_name);
                });
             },
            url: '{{$url}}/admin/images/uploadImage',
            addRemoveLinks: true,
            dictDefaultMessage: "Drag or Click",
            clickable: true,
            method: 'POST',
            paramName: "other_image",
            maxFiles:{{MAX_UPLOAD_FILE}},
            parallelUploads: 10,
            maxFilesize:2,
            previewsContainer:'#other_image',
            autoProcessQueue: true,
            uploadMultiple: true
        });
        @endif
        @endif
        var multi_template = $('select#txt_template').width(500).kendoMultiSelect({
            dataTextField: "name",
            dataValueField: "id",
            // define custom template
            itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="width:150px; float:left; margin-right: 5px" src=\"${data.image}\" alt=\"${data.name}\" />' +
                '<h3>${ data.name }</h3>' +
                '<p>#= data.width # &times; #= data.height#"</p></div>',
            tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"${data.image}\" alt=\"${data.name}\" />' +
                '#: data.name #',
            dataSource:{
                transport:{
                    read:{
                        url     :'/admin/products/products/ajax',
                        dataType:'json',
                        type    : 'POST',
                        data    :   {ajax_type: 'load_template', id: '{{$columns["id"]}}'}
                    }
                }
            }
        }).data("kendoMultiSelect");
        multi_template.value({{json_encode($template)}});

        $("select#txt_filter_template").kendoDropDownList({
            change: filter_site_changed
        });
        function filter_site_changed(){
            multi_template.options.filter = $("#txt_filter_template_value").val();
        }
        var set_search_site = function (e) {
            if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
                multi_template.search($("#txt_filter_template_value").val());
                e.preventDefault();
            }
        };
        $("input#btn_filter_template").click(set_search_site);
        $("#txt_filter_template_value").keypress(set_search_site);

    });
    function cloneOption(e) {
        var dataItem = this.dataItem(e.item.index());
        if(dataItem.id == 0)
            return false;
        if(!confirm("Do you want clone option from "+dataItem.name+"?"))
            return false;
        $.ajax({
            url : "{{$url}}/admin/products/product-option-price/clone-option",
            type: "POST",
            data : {product_id: dataItem.id, current_id: "{{$columns['id']}}"},
            success: function(){
                $(".price").data("kendoGrid").dataSource.read();
            }
        });
    };
    function getOptionName(option_id) {
        var options = {{json_encode($columns['property_list']['size'])}};
        for (var idx = 0, length = options.length; idx < length; idx++) {
            if (options[idx].value == option_id) {
                return options[idx].name;
            }
        }
        return option_id;
    }
    function optionDropDownEditor(container, options) {
        $('<input name="option_id" required data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                placeholder: "Select size...",
                dataTextField: "name",
                dataValueField: "value",
                <?php $columns['property_list']['size'] = array_merge(array(array('name'=>'Select size...','value'=>1)),$columns['property_list']['size']);
                    $columns['property_list']['size'] = array_values( $columns['property_list']['size']);
                ?>
                dataSource: {{json_encode($columns['property_list']['size'])}}
                });
    }
    function removeFile(file_name){
        $.ajax({
            url : "/admin/images/deleteFile",
            type: "POST",
            data: {name: file_name, include_thumb : 1},
            success: function(){
                var other_image = $("input[name=other_image]").val();
                other_image = other_image.replace(file_name,'');
                $("input[name=other_image]").val(other_image);
            }
        })
    }
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
    function removeImg(obj,img_id){
        $("#image").val("");
        $("#"+img_id).attr("src","");
        $(obj).hide();
        var input_id = img_id.replace("_link","");
        $("#"+input_id).val("").attr("type","text");
    }
</script>
@stop
