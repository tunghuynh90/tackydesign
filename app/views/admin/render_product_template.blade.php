<table cellspacing='0'>
	<thead>
		<tr style="text-align: left;padding-left:20px;border-left:0;">
			<th style="padding:21px 25px 22px 25px;border-top:1px solid #fafafa;border-bottom:1px solid #e0e0e0;background: #ededed;background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));background: -moz-linear-gradient(top,#ededed,#ebebeb);text-align: left;padding-left:20px;-moz-border-radius-topleft:3px;-webkit-border-top-left-radius:3px;border-top-left-radius:3px;">Name/Description</th>
			<th style="padding:21px 25px 22px 25px;border-top:1px solid #fafafa;border-bottom:1px solid #e0e0e0;background: #ededed;background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));background: -moz-linear-gradient(top,#ededed,#ebebeb);">Price(CAD)</th>
			<th style="padding:21px 25px 22px 25px;border-top:1px solid #fafafa;border-bottom:1px solid #e0e0e0;background: #ededed;background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));background: -moz-linear-gradient(top,#ededed,#ebebeb);">Category</th>
            <th style="padding:21px 25px 22px 25px;border-top:1px solid #fafafa;border-bottom:1px solid #e0e0e0;background: #ededed;background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));background: -moz-linear-gradient(top,#ededed,#ebebeb);-moz-border-radius-topright:3px;-webkit-border-top-right-radius:3px;border-top-right-radius:3px;">Thumnail</th>
         </tr>
	</thead>
	<tbody>
		@foreach($products as $key => $product)
		<?php
			$color = '#fafafa';
			if($key % 2 != 0)
				$color = '#f6f6f6';
		?>
		<tr>
			<td style="padding:18px;border-top: 1px solid #ffffff;border-bottom:1px solid #e0e0e0;background: {{$color}};background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to({{$color}}));background: -moz-linear-gradient(top,  #f8f8f8,  {{$color}});text-align: left;padding-left:20px;border-left:0;">
            	<a href="{{$url}}/collections/{{$product['category_short_name']}}/products/{{$product['short_name']}}-{{$product['id']}}">{{$product['name']}}</a>
            	{{$product['description']}}
            </td>
			<td style="padding:18px;border-top:1px solid #ffffff;border-bottom:1px solid #e0e0e0;border-left: 1px solid #e0e0e0;background: {{$color}};background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to({{$color}}));	background: -moz-linear-gradient(top,  #f8f8f8,  {{$color}});" align="right">
				<a href="{{$url}}/collections/{{$product['category_short_name']}}/products/{{$product['short_name']}}-{{$product['id']}}">{{number_format($product['sell_price'],2)}}</a>
			</td>
			<td style="padding:18px;border-top:1px solid #ffffff;border-bottom:1px solid #e0e0e0;border-left: 1px solid #e0e0e0;background: {{$color}};background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to({{$color}}));	background: -moz-linear-gradient(top,  #f8f8f8,  {{$color}});" align="center">
				{{$product['category']}}
			</td>
           	<td style="padding:18px;border-top:1px solid #ffffff;border-bottom:1px solid #e0e0e0;border-left: 1px solid #e0e0e0;background: {{$color}};background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to({{$color}}));	background: -moz-linear-gradient(top,  #f8f8f8,  {{$color}});" align="center">
            	<img src="{{urlencode($product['main_image'])}}" style="max-height: 215px" alt="{{$product['name']}}" />
            </td>
		</tr>
		@endforeach
	</tbody>
</table>
