@extends('layouts.admin.admin_layout')
@section('content')
<style type="text/css">
    .right-text{
        text-align: right;
    }
    td.shipping-price-td{
        background-color:#eee;
        padding-right:1%;
        line-height:30px;
        vertical-align:top;
        width: 200px !important;
        text-align: right;
    }
</style>
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Ship Price</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_ship_price" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_ship_price" name="action_ship_price" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                </ul>
                <div class="information">
                    <table>
                        <tr>
                            <td class="shipping-price-td">Country</td>
                            <td>&nbsp;</td>
                            <td><select id="country_id"  name="country_id" /></select>
                            <script type="text/javascript">
                                var country_list = {{$columns['country_list']}};
                                var country = $('select#country_id').width(300).kendoComboBox({
                                    contentType: "charset=utf-8",
                                    dataSource: country_list,
                                    dataTextField:'name',
                                    dataValueField:'value',
                                    suggest: true,
                                }).data("kendoComboBox");
                                country.value({{$columns['country_id']}});
                            </script>
                        </tr>
                        <tr>
                            <td class="shipping-price-td">Publish</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                        </tr>
                        @if($columns['id'])
                        <tr>
                            <td class="shipping-price-td">Shipping Price</td>
                            <td></td>
                            <td>
                                <div id="shiping-price-content"></div>
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_ship_price" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
            @endif
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            @if(isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE]))
            $('input#btn_submit_ship_price').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            @endif
            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
            var url = "{{$url}}/admin/products/shipping-price/{{$columns['id']}}",
            dataSource = new kendo.data.DataSource({
                transport: {
                    read:  {
                        url: url + "/get-options",
                        type: "POST",
                        dataType: "json",
                        data: {id : {{$columns['id']}}}
                    },
                    update: {
                        url: url + "/update-options",
                        type: "POST",
                        data: {id : {{$columns['id']}}}
                    },
                    destroy: {
                        url: url + "/delete-options",
                        type: "POST",
                        data: {id : {{$columns['id']}}}
                    },
                    create: {
                        url: url + "/add-options",
                        type: "POST",
                        data: {id : {{$columns['id']}}},
                        complete: function(e) {
                            $("#shiping-price-content").data("kendoGrid").dataSource.read();
                        }
                    }
                },
                batch: true,
                pageSize: 40,
                pageable: {
                    input: true,
                    refresh: true,
                    pageSizes: [10, 20, 30, 40, 50],
                    numeric: false
                },
                schema: {
                    data: "options",
                    model: {
                        id: "id",
                        fields: {
                             shipping_method: { validation: { required: true } },
                             shipping_price: { type: "number", validation: { min: 0, required: true } },
                        }
                    },total: function(data){
                            return data.total_rows;
                        }
                }
            });

        $("#shiping-price-content").kendoGrid({
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            height: 350,
            toolbar: ["create"],
            columns: [
                { field: "shipping_method", title: "Shipping Method",  width: "120px" },
                { field: "shipping_price", title:"Shipping Price", width: "120px" ,attributes: { class: "right-text"}},
                { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }],
            editable: "inline"
        });
        });
        </script>
    @stop
