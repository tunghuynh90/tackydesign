@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Social Network</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_category" action="admin/static-content/social-network/save" method="post" enctype="multipart/form-data" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action" name="action" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
                <li>View Widget</li>
                <li>Share Widget</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr style="width: 100%">
                        <td style="width: 200px;">Name</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>

                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>
                    <tr>
                        <td>Link</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="link" name="link" value="{{$columns['link']}}" /></td>
                    </tr>
                    <tr>
                        <td>Icon</td>
                        <td>&nbsp;</td>
                        <td>
                            <img id="icon_link" style="max-width: 150px;" src="{{$columns['icon_link']}}" />
                            <input <?php if($columns['icon'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="icon" readonly data-id="icon_link" name="icon" value="{{$columns['icon']}}" />
                            <span id="icon_link_delete" <?php if($columns['icon'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg(this,'icon_link')"><img src="{{$url}}/assets/images/icons/delete.png" /></span>
                            <script type="text/javascript">
                                $(function(){
                                    $("#icon").kendoEditor({
                                        tools: [
                                            "insertImage"
                                        ],
                                        imageBrowser: {
                                            dataType:'json',
                                            transport: {
                                                read: {
                                                    url: "/admin/images/listImages",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path: 'images/social_icon'}
                                                },
                                                destroy: {
                                                    url: "/admin/images/deleteFile",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path: 'images/social_icon'}
                                                },
                                                uploadUrl: "/admin/images/uploadImage?image_path=social_icon",
                                                thumbnailUrl: "/admin/images/thumbImage?image_path=social_icon",
                                                imageUrl: function(imagename){
                                                    $("#icon").val(imagename).attr('type','hidden');
                                                    var img_id = $("#icon").attr("data-id");
                                                    $("#"+img_id).attr("src","{{$url}}/assets/images/social_icon/"+imagename);
                                                    $("#"+img_id+"_delete").show();
                                                    event.preventDefault();
                                                }
                                            }
                                        }
                                    });
                                })
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>

                </table>
            </div>
            <div class="view-widget">
                <table>
                    <tr>
                        <td>On home</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="on_home" name="on_home" value="{{$columns['on_home']}}"{{$columns['on_home']==1?' checked="checked"':''}} /></td>
                    </tr>
                </table>
                <textarea class="k-textbox" id="view_widget" name="view_widget" style="width:90%; height:300px; padding:5px">{{$columns['view_widget']}}</textarea>
                <span class="tooltips" style="margin-left: 50px"><a title="Change this at your own risk">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
            </div>
            <div class="share-widget">
                <span>[PAGE_URL] : Url of current page</span><br />
                <textarea class="k-textbox" id="share_widget" name="share_widget" style="width:90%; height:300px; padding:5px;margin-top: 15px;">{{$columns['share_widget']}}</textarea>
                <span class="tooltips" style="margin-left: 50px"><a title="Change this at your own risk">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
            </div>
        </div>
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_social_network" name="btn_submit_social_network" value="Submit" class="k-button button_css" />
        </div>
        @endif
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $('input#btn_submit_social_network').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
         var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
    });
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
    function removeImg(obj,img_id){
        $("#icon").val("");
        $("#"+img_id).attr("src","");
        $(obj).hide();
        var input_id = img_id.replace("_link","");
        $("#"+input_id).val("").attr("type","text");
    }
</script>
@stop
