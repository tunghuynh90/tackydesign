@extends('layouts.admin.admin_layout')
@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Tax</h3>
        </div>
        <div id="div_quick_search">

        </div>

        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Tax</h2>
          </div>
          <script type="text/javascript">
          var window_search;
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });

            var option_data_source = new kendo.data.DataSource({
              transport: {
                  read: {
                      url: '/admin/products/tax/json',
                      type: 'post',
                      dataType:'json'
                  }
                  @if(isset($permit[_R_UPDATE]))
                  ,update:{
                        url: '/admin/products/tax/save',
                        type: 'post',
                        dataType:'json'
                  }
                  ,
                  parameterMap: function(options, operation) {
                      if (operation !== "read" && options.models) {
                          return {models: kendo.stringify(options.models)};
                      }
                  }
                  @endif
              },
              batch: true,
              pageSize: 100,
              schema: {
                  model: {
                      id: "id",
                      fields: {
                          id: { editable: false, nullable: true},
                          city_id: { editable: false, nullable: true},
                          row_order: { editable: false, nullable: true},
                          zip_code: { editable: false},
                          city_name: { editable: false},
                          tax: {type:"number", validation: { min: 0, step: 1, format:'p0'}},
                          description: { type: "string"}
                      }
                  }
              }
            });
            //option_data_source.read();
            var grid = $("#grid").kendoGrid({
                dataSource: option_data_source,
                scrollable: true,
                sortable: false,
                filterable: false,
                pageable:false,
                height: 500,
                navigatable: true,
                editable: true
                @if(isset($permit[_R_UPDATE]))
                ,toolbar: [{name:"save", text:"Save Changes"},{name:"cancel", text:"Cancel Changes"}]
                @endif
                ,columns: [
                  {field: "row_order", title: "&nbsp;", type:"int", width:"20px", editable:false, sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                  {field: "zip_code", title: "Zip Code", type:"int", width:"50px", sortable: true, editable:false },
                  {field: "city_name", title: "City Name", type:"int", width:"50px", sortable: true, editable:false },
                  {field: "tax", title: "Tax", type:"double", width:"50px", template: '#= tax #%'},
                  {field: "description", title: "Description", type:"string", width:"50px" }
                ]
          }).data("kendoGrid");
        });
        </script>
    </div>
@stop