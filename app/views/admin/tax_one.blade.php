@extends('layouts.admin.admin_layout')
@section('content')
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Tax</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_ship_price" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_ship_price" name="action_ship_price" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                    <li>Other</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>City Id</td>
                            <td>&nbsp;</td>
                            <td><input type="number" id="city_id" name="city_id" value="{{$columns['city_id']}}" /></td>
                        </tr>

                        <tr>
                            <td>Tax</td>
                            <td>&nbsp;</td>
                            <td><input type="number" id="tax" name="tax" value="{{$columns['tax']}}" /></td>
                        </tr>

                        <tr>
                            <td>Description</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="description" name="description" value="{{$columns['description']}}" /></td>
                        </tr>
                            </table>
                </div>
                <div class="other"></div>
            </div>
            @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_tax" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
            @endif
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            @if(isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE]))
            $('input#btn_submit_tax').click(function(e){
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            @endif
            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
            $('input#city_id').kendoNumericTextBox({
                min:0, step:1, format:'p'
            });
            $('input#tax').kendoNumericTextBox({
                min:0, step:0.01, format:'p0'
            });

            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        </script>
    @stop
