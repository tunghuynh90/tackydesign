@extends('layouts.admin.admin_layout')
@section('content')
<style>
    #tbl_single td:first-child{
        width:200px !important;
    }
    .fielddis{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .fielddis label {
        width: 100px;
        margin: 0;
        float: left;
        text-align: right;
        padding-right: 5px;
        line-height: 26px;
    }
    .white {
        background-color: #fff !important;
    }
</style>
      <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>User Member</h3>
        </div>
        <div id="div_quick_search">

        </div>
        <form id="frm_user_member" action="/admin/{{$icon['view']}}/save" method="post">
            <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
            <input type="hidden" id="action_user_member" name="action_user_member" value="{{$columns['id']==0?'new':'edit'}}" />
            <div id="div_tab">
                <ul>
                    <li class="k-state-active">Information</li>
                    <li>Address</li>
                </ul>
                <div class="information">
                    <table id="tbl_single">
                        <tr>
                            <td>Email</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="email" name="email" value="{{$columns['email']}}" /></td>
                        </tr>

                        <tr>
                            <td>Password</td>
                            <td>&nbsp;</td>
                            <td><input type="password" class="k-textbox" id="password" name="password" value="{{$columns['password']}}" /></td>
                        </tr>

                        <tr>
                            <td>Firstname</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="firstname" name="firstname" value="{{$columns['firstname']}}" /></td>
                        </tr>

                        <tr>
                            <td>Lastname</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="lastname" name="lastname" value="{{$columns['lastname']}}" /></td>
                        </tr>


                        <tr>
                            <td>Is Active</td>
                            <td>&nbsp;</td>
                            <td><input type="checkbox" id="confirmed" name="confirmed" value="{{$columns['confirmed']}}"{{$columns['confirmed']==1?' checked="checked"':''}} /></td>
                        </tr>
                        @if($columns['id'])
                        <tr>
                            <td>Lastest Login</td>
                            <td>&nbsp;</td>
                            <td>
                                <input disabled type="text" id="lastest_login" name="lastest_login" value="{{$columns['lastest_login']}}" />
                            </td>
                        </tr>
                        <tr>
                            <td>Created At</td>
                            <td>&nbsp;</td>
                            <td>
                                <input disabled type="text" id="created_at" name="created_at" value="{{$columns['created_at']}}" />
                            </td>
                        </tr>

                        <tr>
                            <td>Updated At</td>
                            <td>&nbsp;</td>
                            <td>
                                <input disabled type="text" id="updated_at" name="updated_at" value="{{$columns['updated_at']}}" />
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div class="address">
                    <table id="tbl_single" >
                        <tr>
                            <td>First Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="first_name" value="" /></td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="last_name" value="" /></td>
                        </tr>
                        <tr>
                            <td>Company</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="company" value="" /></td>
                        </tr>
                        <tr>
                            <td>Address 1</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="address_1" value="" /></td>
                        </tr>
                        <tr>
                            <td>Address 2</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="address_2" value="" /></td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="city" value="" /></td>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="country" value="" /></td>
                        </tr>
                        <tr>
                            <td>Province</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="province" value="" /></td>
                        </tr>
                        <tr>
                            <td>Zip</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="zip" value="" /></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>&nbsp;</td>
                            <td><input type="text" class="k-textbox" id="phone" value="" /></td>
                        </tr>
                    </table>
                    <div class="fielddis" style="clear:both;">
                        <div class="grid_table">
                        <label for="txt_allow_size_option"> Add </label>
                            <img id="add_option_grid" src="{{$url}}/assets/images/icons/add.png" onclick="addAddress()" style="cursor:pointer" title="Add Option" width="25" />
                    </div>
                    <div id="option_grid" data-role="grid" class="k-grid k-widget k-secondary" style="height: 310px;">
                        <div class="k-grid-header" style="padding-right: 17px;">
                            <div class="k-grid-header-wrap">
                                <table role="grid">
                                    <colgroup>
                                        <col style="width:80px">
                                        <col style="width:80px">
                                        <col style="width:200px">
                                        <col style="width:200px">
                                        <col style="width:200px">
                                        <col style="width:70px">
                                        <col style="width:70px">
                                        <col style="width:40px">
                                        <col style="width:70px">
                                        <col style="width:90px">
                                        <col style="width:60px">
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th role="columnheader" data-field="first_name" data-title="First Name" class="k-header">First Name</th>
                                            <th role="columnheader" data-field="last_name" data-title="Last Name" class="k-header">Last Name</th>
                                            <th role="columnheader" data-field="company" data-title="Company" class="k-header">Company</th>
                                            <th role="columnheader" data-field="address_1" data-title="Address 1" class="k-header">Address 1</th>
                                            <th role="columnheader" data-field="address_2" data-title="Address 2" class="k-header">Address 2</th>
                                            <th role="columnheader" data-field="city" data-title="City" class="k-header">City</th>
                                            <th role="columnheader" data-field="country" data-title="Country" class="k-header">Country</th>
                                            <th role="columnheader" data-field="country" data-title="Province" class="k-header">Province</th>
                                            <th role="columnheader" data-field="zip" data-title="Zip" class="k-header">Zip</th>
                                            <th role="columnheader" data-field="phone" data-title="Phone" class="k-header">Phone</th>
                                            <th role="columnheader" data-field="default" data-title="Default" class="k-header">Default</th>
                                            <th class="k-header">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($columns['address']))
                                        <?php $columns['address'] = unserialize($columns['address']); ?>
                                        @if(!empty($columns['address']))
                                        @foreach($columns['address'] as $key => $address)
                                        <tr class="address_row <?php echo $key%2==0 ?  ' white' : ' k-alt'; ?>" role="row">
                                            @foreach($address as $field=>$value)
                                            <?php if($field== 'default') continue; ?>
                                            <td role="gridcell">{{$value}}<input type="hidden" name="address[{{$key}}][{{$field}}]" value="{{$value}}" /></td>
                                            @endforeach
                                            @if(isset($columns['address_default_key']) && $columns['address_default_key']==$key)
                                            <td role="gridcell"><input type="checkbox" name="address[{{$key}}][default]" checked value="1"></td>
                                            @else
                                            <td role="gridcell"><input type="checkbox" name="address[{{$key}}][default]" value="0"></td>
                                            @endif
                                            <td role="gridcell"><a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)" onclick="deleteRow(this)"><span class="k-icon k-delete"></span>Delete</a></td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            @if($message!='')
            <div class="k-block k-widget k-error-colored css_error">
            <ul>
            {{$message}}
            </ul>
            </div>
            <script type="text/javascript">
                if(localStorage["address"]!=undefined && localStorage["address"]!='')
                    $("tbody","#option_grid").html(localStorage["address"]);
            </script>
            @else
            <script type="text/javascript">
                localStorage.setItem("address","");
            </script>
            @endif
            <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_user_member" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
            </div>
        </form>
      </div>
        <script language="javascript" type="text/javascript">
        $(document).ready(function(e){
            $("#option_grid").on("click","input[type=checkbox]",function(){
                $("input[type=checkbox]","#option_grid").prop("checked",false).val(0);
                $(this).prop("checked",true).val(1);
            });
            $('input#btn_submit_user_member').click(function(e){
                @if(!$columns['id'])
                localStorage.setItem("address",$("tbody","#option_grid").html());
                @endif
                if(!validator.validate()){
                    e.preventDefault();
                    if(tab_strip.select().index()!=0) tab_strip.select(0);
                    return false;
                }
            });
            var tooltip = $("span.tooltips").kendoTooltip({
                filter: 'a',
                width: 120,
                position: "top"
            }).data("kendoTooltip");
            @if($columns['id'])
			var lastest_login = $('input#lastest_login').kendoDateTimePicker({
            	format: 'dd-MMM-yyyy HH:mm:ss',
            }).data('kendoDateTimePicker');
			var created_at = $('input#created_at').kendoDateTimePicker({
            	format: 'dd-MMM-yyyy HH:mm:ss'
            }).data('kendoDateTimePicker');
			var updated_at = $('input#updated_at').kendoDateTimePicker({
            	format: 'dd-MMM-yyyy HH:mm:ss'
            }).data('kendoDateTimePicker');
            @endif
            var validator = $('div.information').kendoValidator().data("kendoValidator");
            var tab_strip =$("#div_tab").kendoTabStrip({
                animation:  {
                open: {
                    effects: "fadeIn"
                    }
            }
            }).data("kendoTabStrip");
        });
        function addAddress(){
            var i = 0;
            $("input",".address > #tbl_single").each(function(){
                if($(this).val() == "")
                    i++;
            });
            if(i == $("input",".address > #tbl_single").length){
                alert("All input are blank.");
                $("input",".address > #tbl_single").focus();
                return false;
            }
            var array = ['first_name','last_name','company','address_1','address_2','city','country','province','zip','phone'];
            var row = $(".address_row","#option_grid").length;
            var html = '<tr class="address_row'+(row%2==0 ? ' white' : ' k-alt')+'" role="row">';
            var input = '';
            for(var i in array){
                var value = $("#"+array[i],".address").val() != undefined ? $("#"+array[i],".address").val() : '';
                input = '<input type="hidden" name="address['+row+']['+array[i]+']" value="'+value+'" />';
                html += '<td role="gridcell">'+value+input+'</td>';
            }
            if(row == 0){
                html += '<td role="gridcell"><input type="checkbox" name="address[0][default]" value="1" checked /></td>';
            } else
                html += '<td role="gridcell"><input type="checkbox" name="address['+row+'][default]" value="0"  /></td>';
            html += '<td role="gridcell"><a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)" onclick="deleteRow(this)"><span class="k-icon k-delete"></span>Delete</a></td>';
            html += '</tr>';
            $("tbody","#option_grid").append(html);
            $("input",".address").val("");

        }
        function deleteRow(obj){
            $(obj).parent().parent().remove();
            var i = 0;
            $("tr.address_row","#option_grid").each(function(){
                $(this).removeClass("white k-alt").addClass((i%2==0 ? ' white' : ' k-alt'));
                i++;
            });
        }
        </script>
    @stop
