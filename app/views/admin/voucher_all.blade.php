@extends('layouts.admin.admin_layout')

@section('content')
    <div id="div_content">
        <div id="div_main_title" class="k-block k-widget">
            <h3>Voucher</h3>
            <div id="div_quick_search">
                <form id="frm_quick_search" method="post" action="/admin/products/voucher">
                    <span id="txt_quick_search" class="k-textbox k-space-left">
                    <input type="text" value="{{$quick}}" placeholder="Search by name" name="txt_quick_search">
                    <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                    </span>
                </form>
                <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
                </script>
            </div>
        </div>

        <div id="grid"></div>
          <div id="advanced_search_window" style="display:none">
              <h2>Advanced Search for Help</h2>
          </div>
          <script type="text/javascript">
          var window_search;

          function updateStatus(Obj) {
              var id = $(Obj).attr("data-id");
              var status_ = $(Obj).attr('data-status');
              var val = 1;
              if(status_=='icon-unhide.png') val = 0;
              var name = $(Obj).attr('data-name');
              $.ajax({
                  url : '/admin/products/voucher/update-status',
                  type    : 'POST',
                  data    :   {txt_id: id, txt_name: name,txt_value: val
                  },
                  success: function(data){
                      if(data.error==0){
                          if(status_=='icon-unhide.png'){
                              $(Obj).attr('src',"{{$url}}/assets/images/icons/icon-hide.png");
                              $(Obj).attr('data-status',"icon-hide.png");
                          }else{
                              $(Obj).attr('src',"{{$url}}/assets/images/icons/icon-unhide.png");
                              $(Obj).attr('data-status',"icon-unhide.png");
                          }
                      }
                  }
              });
          }
          $(document).ready(function() {
              window_search = $('div#advanced_search_window');
              $('li#icons_advanced_search').bind("click", function() {
                  if (!window_search.data("kendoWindow")) {
                      window_search.kendoWindow({
                          width: "600px",
                          actions: ["Maximize", "Close"],
                          modal: true,
                          title: "Advanced Search"
                      });
                  }
                  window_search.data("kendoWindow").center().open();
              });
              var grid = $("#grid").kendoGrid({
                  dataSource: {
                      pageSize: 20,
                      serverPaging: true,
                      serverSorting: true,
                      transport: {
                        read: {
                            url: "/admin/products/voucher/json",
                              type: "POST",
                              data: {session:"{{$session}}", quick:'{{$quick}}'}
                          }
                        },
                        schema: {
                            data: "voucher"
                          ,total: function(data){
                                return data.total_rows;
                            }
                        },
                        type: "json"
                    },
                    pageSize: 20,
                    height: 430,
                    scrollable: true,
                    sortable: true,
                    //selectable: "single",
                    pageable: {
                      input: true,
                      refresh: true,
                      pageSizes: [10, 20, 30, 40, 50],
                      numeric: false
                    },
                    columns: [
                      {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                      {field: "key", title: "Voucher code", type:"string", width:"75px", sortable: true },
                      {field: "value", title: "Discount", type:"string", width:"75px", sortable: true },
                      {field: "valid_from", title: "Valid from", type:"string", width:"75px", sortable: true },
                      {field: "valid_to", title: "Valid to", type:"string", width:"75px", sortable: true },
                      {field: "active", title: "Active", type:"string", width:"50px", sortable: true , template:'<p style="text-align:center; margin:0"><img class="change_status" onclick=updateStatus(this) data-name="active" id="icon_#= id#" data-status="#= active#" data-id="#= id#"  src="/assets/images/icons/#= active#" ' },
                      {field: "used", title: "Used", type:"string", width:"50px", sortable: true , template:'<p style="text-align:center; margin:0"><img class="change_status" onclick=updateStatus(this)  id="icon_#= id#" data-status="#= used#" data-id="#= id#" data-name="used" src="/assets/images/icons/#= used#" ' },
                      { command:  [
                        { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                        ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                        ],
                            title: " ", width: "70px" }
                    ]
              }).data("kendoGrid");
                function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/products/voucher/"+dataItem.id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "/admin/products/voucher/"+dataItem.id+"/edit";
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete help : '+dataItem.name+'?')){
                        document.location = "/admin/products/voucher/"+dataItem.id+"/delete";
                    }
                }

            });
        </script>
    </div>
@stop