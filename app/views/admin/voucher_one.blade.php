@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Voucher</h3>
    </div>
    <div id="div_quick_search">

    </div>
    <form id="frm_category" action="admin/products/voucher/save" method="post">
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_voucher" name="action_voucher" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
            </ul>
            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Voucher code</td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" class="k-textbox" id="key" name="key" value="{{$columns['key']}}" /> <input type="button" class="k-button" onclick="$('#key').val(randomString())" value="Generate" />
                        </td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 200px;">Discount</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="number" id="value" name="value" value="{{$columns['value']}}" /> %</td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 200px;">Valid from</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="date" id="valid_from" name="valid_from" value="{{$columns['valid_from']}}" /></td>
                    </tr>
                    <tr style="width: 100%">
                        <td style="width: 200px;">Valid to</td>
                        <td style="width: 1px;">&nbsp;</td>
                        <td><input type="date" id="valid_to" name="valid_to" value="{{$columns['valid_to']}}" /></td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="active" name="active" value="{{$columns['active']}}" {{$columns['active']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Shipping Discount</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="is_shipping_discount" name="is_shipping_discount" value="{{$columns['is_shipping_discount']}}" {{$columns['is_shipping_discount']==1?' checked="checked"':''}} /></td>
                    </tr>
                    <tr>
                        <td>Used</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="used" name="used" value="{{$columns['used']}}" {{$columns['used']==1?' checked="checked"':''}} /></td>
                    </tr>
                </table>
            </div>
        </div>
        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
            {{$message}}
        </div>
        @endif
        <div class="k-block k-widget css_buttons">
            <input type="submit" id="btn_submit_voucher" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
    </form>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(e){

        function startChange() {
            var startDate = valid_from.value(),
            endDate = valid_to.value();

            if (startDate) {
                startDate = new Date(startDate);
                startDate.setDate(startDate.getDate());
                valid_to.min(startDate);
            } else if (endDate) {
                valid_from.max(new Date(endDate));
            } else {
                endDate = new Date();
                valid_from.max(endDate);
                valid_to.min(endDate);
            }
        }

        function endChange() {
            var endDate = valid_to.value(),
            startDate = valid_from.value();

            if (endDate) {
                endDate = new Date(endDate);
                endDate.setDate(endDate.getDate());
                valid_from.max(endDate);
            } else if (startDate) {
                valid_to.min(new Date(startDate));
            } else {
                endDate = new Date();
                valid_from.max(endDate);
                valid_to.min(endDate);
            }
        }
        var valid_from = $("#valid_from").kendoDatePicker({
            change: startChange,
        }).data("kendoDatePicker");

        var valid_to = $("#valid_to").kendoDatePicker({
            change: endChange,
        }).data("kendoDatePicker");

        valid_from.max(valid_to.value());
        valid_to.min(valid_from.value());
        $("#value").kendoNumericTextBox({
            min: 0,
            step: 0.5,
        });
        @if((isset($permit[_R_INSERT]) || isset($permit[_R_UPDATE])) && $edit)
        $('input#btn_submit_voucher').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        @endif
        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
    });
    function randomString() {
        return 'xxxx-xxxx-{{rand(0,9)}}xxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
</script>
@stop
