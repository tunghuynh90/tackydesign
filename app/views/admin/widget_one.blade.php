@extends('layouts.admin.admin_layout')

@section('content')

  <div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Widget</h3>
    </div>
    <div id="div_quick_search"></div>

    <ul>
        - Field link: example - you want show collection PAPER PRINTS on home page, you must fill: /collections/paper-prints
    </ul>

    <form id="frm_help" action="/admin/{{$icon['view']}}/save" method="post" >
        <input type="hidden" id="id" name="id" value="{{$columns['id']}}" />
        <input type="hidden" id="action_help" name="action_help" value="{{$columns['id']==0?'new':'edit'}}" />
        <div id="div_tab">
            <ul>
                <li class="k-state-active">Information</li>
            </ul>

            <div class="information">
                <table id="tbl_single">
                    <tr>
                        <td>Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="name" name="name" value="{{$columns['name']}}" /></td>
                    </tr>


                    <tr>
                        <td>Short Name</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="short_name" name="short_name" value="{{$columns['short_name']}}" /></td>
                    </tr>

                     <tr>
                        <td>Sub Title</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="sub_title" name="sub_title" value="{{$columns['sub_title']}}" /></td>
                    </tr>

                    <tr>
                        <td>Link</td>
                        <td>&nbsp;</td>
                        <td><input type="text" class="k-textbox" id="link" name="link" value="{{$columns['link']}}" /></td>
                    </tr>

                    <tr>
                    <td>Image</td>
                    <td>&nbsp;</td>
                        <td>
                            <img id="image_link" style="max-width: 150px;" src="{{$columns['image_link']}}" />
                            <input <?php if($columns['image'] == '') {?>type="text"<?php }else{ ?>type="hidden"<?php } ?> class="logo k-textbox"  id="image" readonly data-id="image_link" name="image" value="{{$columns['image']}}" />
                            <span id="image_link_delete" <?php if($columns['image'] == '') { ?>style="display: none"<?php } ?> onclick="removeImg(this,'image_link')"><img src="{{$url}}/assets/images/icons/delete.png" /></span>
                            <script type="text/javascript">
                                $(function(){
                                    $("#image").kendoEditor({
                                        tools: [
                                            "insertImage"
                                        ],
                                        imageBrowser: {
                                            dataType:'json',
                                            transport: {
                                                read: {
                                                    url: "/admin/images/listImages",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path : 'upload'}
                                                },
                                                destroy: {
                                                    url: "/admin/images/deleteFile",
                                                    type: "POST",
                                                    dataType:'json',
                                                    data: {image_path : 'upload'}
                                                },
                                                uploadUrl: "/admin/images/uploadImage?image_path=upload",
                                                thumbnailUrl: "/admin/images/thumbImage?image_path=upload",
                                                imageUrl: function(imagename){
                                                    $("#image").val(imagename).attr('type','hidden');
                                                    var img_id = $("#image").attr("data-id");
                                                    $("#"+img_id).attr("src","{{$url}}/assets/upload/"+imagename);
                                                    $("#"+img_id+"_delete").show();
                                                    event.preventDefault();
                                                }
                                            }
                                        }
                                    });
                                })
                            </script>
                        </td>
                    </tr>

                    <tr>
                        <td>Publish</td>
                        <td>&nbsp;</td>
                        <td><input type="checkbox" id="publish" name="publish" value="{{$columns['publish']}}"{{$columns['publish']==1?' checked="checked"':''}} /></td>
                    </tr>

                </table>
            </div>
        </div>

        @if($message!='')
        <div class="k-block k-widget k-error-colored css_error">
        <ul>
        {{$message}}
        </ul>
        </div>
        @endif

        <div class="k-block k-widget css_buttons">
        <input type="submit" id="btn_submit_help" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
        </div>
    </form>
  </div>
    <script language="javascript" type="text/javascript">
    $(document).ready(function(e){
        $('input#btn_submit_help').click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
        });
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");
		$('input#orderno').kendoNumericTextBox({
        	min:0, step:1, format:'n0'
        });

        var validator = $('div.information').kendoValidator().data("kendoValidator");
        var tab_strip =$("#div_tab").kendoTabStrip({
            animation:  {
            open: {
                effects: "fadeIn"
                }
        }
        }).data("kendoTabStrip");
        $('input#image_file').kendoUpload({
            multiple:false
        });
        $("#name").keypress(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
        $("#name").change(function(){
            $("#short_name").val(toSlugger($(this).val()));
        });
    });
    function toSlugger(value){
        value = $.trim(value);
        value = value.toLowerCase();
        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
        return value;
    }
     function removeImg(obj,img_id){
        $("#image").val("");
        $("#"+img_id).attr("src","");
        $(obj).hide();
        var input_id = img_id.replace("_link","");
        $("#"+input_id).val("").attr("type","text");
    }
    </script>
@stop
