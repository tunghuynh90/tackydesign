@extends('layouts.admin.admin_layout')
@section('content')
<div id="div_content">
    <div id="div_main_title" class="k-block k-widget">
        <h3>Zip Postal</h3>
        <div id="div_quick_search">
            <form id="frm_quick_search" method="post">
                <span id="txt_quick_search" class="k-textbox k-space-left">
                <input type="text" value="{{$quick}}" placeholder="Search by zip" name="txt_quick_search">
                <a id="a_quick_search" class="k-icon k-i-search" style="cursor: pointer"></a>
                </span>
            </form>
            <script type="text/javascript">
                $(document).ready(function(e){
                    $('a#a_quick_search').click(function(e){
                        $('form#frm_quick_search').submit();
                    });
                });
            </script>
        </div>
    </div>
    <div id="grid"></div>
    <script type="text/javascript">
     $(document).ready(function() {
        var url = "{{$url}}/admin/system/zip-postal",
            dataSource = new kendo.data.DataSource({
                transport: {
                    read:  {
                        url: url + "/json",
                        type: "POST",
                        dataType: "json"
                    },
                    update: {
                        url: url + "/update",
                        type: "POST"
                    },
                    destroy: {
                        url: url + "/delete",
                        type: "POST"
                    },
                    create: {
                        url: url + "/add",
                        type: "get",
                        dataType: "json"
                    }
                },
                batch: true,
                pageSize: 40,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                pageable: {
                    input: true,
                    refresh: true,
                    pageSizes: [10, 20, 30, 40, 50],
                    numeric: false
                },
                schema: {
                    data: "zip_postal",
                    model: {
                        id: "zip",
                        fields: {
                             zip: { validation: { required: true } },
                             city: { validation: { required: true } },
                             state: { validation: { required: true } },
                             country: { validation: { required: true } },
                        }
                    },total: function(data){
                            return data.total_rows;
                        }
                }
            });

        $("#grid").kendoGrid({
            dataSource: dataSource,
            sortable: true,
            pageable: true,
            height: 550,
            toolbar: ["create"],
            columns: [
                { field: "zip", title: "Zip",  width: "120px" },
                { field: "city", title:"City", width: "120px" },
                { field: "state", title:"State", width: "120px" },
                { field: "country", title:"Country", width: "120px"/*, editor: optionDropDownEditor*/ },
                { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }],
            editable: "inline"
        });
    });
    </script>
</div>
@stop