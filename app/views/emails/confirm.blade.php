<?php
	$template = EmailTemplate::where('type','=','user_confirm')
					->get()
					->first();
?>
@if(is_null($template))
<h1>{{ Lang::get('confide::confide.email.account_confirmation.subject') }}</h1>

<p>{{ Lang::get('confide::confide.email.account_confirmation.greetings', array( 'name' => $user->firstname.' '.$user->lastname)) }},</p>
<p>{{ Lang::get('confide::confide.email.account_confirmation.unsubscribe', array( 'id' => $user->id)) }},</p>

<p>{{ Lang::get('confide::confide.email.account_confirmation.body') }}</p>
<a href='{{{ URL::to("user/confirm/{$user->confirmation_code}") }}}'>
    {{{ URL::to("user/confirm/{$user->confirmation_code}") }}}
</a>

<p>{{ Lang::get('confide::confide.email.account_confirmation.farewell') }}</p>
@else
<?php
	$confirm_link = URL::to("user/confirm/{$user->confirmation_code}");;
	$content = html_entity_decode($template->content);
	$customer_name = $user->firstname.' '.$user->lastname;
	$token =  $user->confirmation_code;
	if(strpos($content, '[TOKEN]') !== false)
		$content = str_replace('[TOKEN]',$token,$content);
	if(strpos($content, '[CUSTOMER_NAME]')!==false)
		$content = str_replace('[CUSTOMER_NAME]',$customer_name,$content);
	if(strpos($content, '[CONFIRM_LINK]')!==false)
		$content = str_replace('[CONFIRM_LINK]',$confirm_link,$content);
	else
		$content .= $confirm_link;
	echo $content;
?>
@endif

