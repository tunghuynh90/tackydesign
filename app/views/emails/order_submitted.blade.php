<?php
	$content = html_entity_decode($config['content']);
	preg_match_all("!<span[^>]+>(.*?)</span>!", $content, $matches);
	$all_span = $matches[0];

	foreach($all_span as $span){
		preg_match_all("/<span [^>]+ rel=\"\[\[(.+?)\]\]\" [^>]+>[^>]+<\/span>/",$span,$content_matches);
		foreach($content_matches[1] as $val){
			$temp_field = strtolower($val);
			if(isset($arr_data[$temp_field])){
				if($temp_field == 'product_content'){
					$arr_data[$temp_field] = $__env->make('emails.order_submitted_products', array($temp_field=>$arr_data[$temp_field]), array_except(get_defined_vars(), array('__data', '__path')))->render();
				}
				$content = str_replace($span, $arr_data[$temp_field], $content);
			} else
				$content = str_replace($span, '', $content);
		}
	}
	echo $content;
?>