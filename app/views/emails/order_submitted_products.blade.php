<!-- PRODUCT START -->
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td align="left" height="56" style="font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:13px;line-height:13px;text-transform:uppercase;color:#333333;" valign="middle" width="232">item</td>
            <td align="right" style="font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:13px;line-height:13px;text-transform:uppercase;color:#333333;" valign="middle" width="51">size</td>
            <td align="right" style="font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:13px;line-height:13px;text-transform:uppercase;color:#333333;" valign="middle" width="54">QTY</td>
            <td align="right" style="font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:13px;line-height:13px;text-transform:uppercase;color:#333333;" valign="middle" width="88">price</td>
            <td align="right" style="font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:13px;line-height:13px;text-transform:uppercase;color:#333333;padding-right:20px;" valign="middle" width="">subtotal</td>
        </tr>
    </tbody>
</table>
@foreach($product_content as $product)
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td bgcolor="#CCCCCC" height="1"><img height="1" src="https://ci5.googleusercontent.com/proxy/aiG2f2TfVYPyC_mUusohDLXTm7h971biFJNOlWearEdNwcD8zakXlfKigElb5BKbT1aNEeUTom9GOx3ROAy1eR8f-srJNt34dfQInsW8=s0-d-e1-ft#http://f.e.aritzia.com/i/36/2089433611/120713_spacer.gif" style="display:block;" width="1" /></td>
        </tr>
        <tr>
            <td height="10"><img height="10" src="https://ci5.googleusercontent.com/proxy/aiG2f2TfVYPyC_mUusohDLXTm7h971biFJNOlWearEdNwcD8zakXlfKigElb5BKbT1aNEeUTom9GOx3ROAy1eR8f-srJNt34dfQInsW8=s0-d-e1-ft#http://f.e.aritzia.com/i/36/2089433611/120713_spacer.gif" style="display:block;" width="1" /></td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <?php $image = str_replace(' ', '%20', $product['image']); ?>
            <td width="40"><img height="51" src="{{$image}}" style="display:block;" width="40" /></td>
            <td width="16"><img height="1" src="http://f.e.aritzia.com/i/36/2089433611/120713_spacer.gif" style="display:block;" width="16" /></td>
            <td align="left" style="font-family:Helvetica,Arial,Helvetica,sans-serif;font-size:11px;line-height:11px;color:#333333;" valign="top" width="135">
                <a href="{{$product['options']['url']}}" style="color:#333333;line-height:22px;" target="_blank">{{$product['name']}}</a><br />
            </td>
            <td align="center" style="font-family:Helvetica,Arial,Helvetica,sans-serif;font-size:11px;line-height:22px;color:#333333;" valign="top" width="31">{{$product['options']['size']}}</td>
            <td align="center" style="font-family:Helvetica,Arial,Helvetica,sans-serif;font-size:11px;line-height:22px;color:#333333;" valign="top" width="56">{{$product['quantity']}}</td>
            <td align="center" style="font-family:Helvetica,Arial,Helvetica,sans-serif;font-size:11px;line-height:22px;color:#333333;" valign="top" width="74"><span>${{number_format($product['sell_price'],2)}}</span> </td>
            <?php $sub_total =  $product['sell_price']*$product['quantity'];?>
            <td align="right" style="font-family:Helvetica,Arial,Helvetica,sans-serif;font-size:11px;line-height:22px;color:#333333;padding-right:20px;" valign="top" width="88">{{number_format($sub_total,2)}}</td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td height="9"><img height="9" src="http://f.e.aritzia.com/i/36/2089433611/120713_spacer.gif" style="display:block;" width="1" /></td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC" height="1"><img height="1" src="http://f.e.aritzia.com/i/36/2089433611/120713_spacer.gif" style="display:block;" width="1" /></td>
        </tr>
    </tbody>
</table>
@endforeach
<!-- END PRODUCT -->