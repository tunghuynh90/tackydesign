<div id="div_header">
    <div id="div_header_logo">
        @if($admin)
        	<a href="#"><img src="{{$url}}/assets/images/logos/logo_backend.png" title="Home" /></a>
        @endif
    </div>
    <div id="div_header_info">
        @if($admin)
        <div id="div_account">Hello <a href="{{$url}}/admin/account">{{$admin->fullname}}</a>! | <a href="{{$url}}/admin/logout">Logout</a></div>
        <div id="div_icon">
            <ul class="icons">
                @if(isset($icon['new']))
                <li>
                    <a href="{{$url}}/admin/{{$icon['new']}}/add">
                        <p>
                            <img id="icons_add_new" title="Add New" src="{{$url}}/assets/images/icons/add.png" />
                        </p>
                        <p>Add New</p>
                    </a>
                </li>
                @endif
                @if(isset($icon['view']))
                <li>
                    <a href="{{$url}}/admin/{{$icon['view']}}">
                        <p>
                            <img id="icons_add_new" title="View All" src="{{$url}}/assets/images/icons/all.png">
                        </p>
                        <p>View All</p>
                    </a>
                </li>
                @endif
            </ul>
        </div>
        @endif
    </div>
</div>
@if($admin)
<div id="div_header_menu">
    <div id="menu"></div>
</div>
@endif