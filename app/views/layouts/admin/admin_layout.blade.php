<!-- app/views/adminLayout.blade.php -->
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>tackydesign - {{$title}}</title>
    <base href="{{$url}}" />
    <link href="{{$url}}/assets/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="{{$url}}/assets/css/basic.css" rel="stylesheet" type="text/css" />
    <link href="{{$url}}/assets/css/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="{{$url}}/assets/css/special.css" rel="stylesheet" type="text/css" />
    <link href="{{$url}}/assets/css/kendo.common.min.css" rel="stylesheet" />
    <link href="{{$url}}/assets/css/theme.blueopal.css" rel="stylesheet" type="text/css" />
    <script src="{{$url}}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.web.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.core.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/kendo.maskedtextbox.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/dropzone.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(e){
            $("#menu").kendoMenu({
                //dataUrlField: "LinksTo",
                //animation: { open: { effects: "expand: vertical slideIn:down fadeIn" } },
                //hoverDelay: 100,
                dataSource:[
                    @include('layouts.admin.admin_menu')
                ]
            });

        });
    </script>
</head>

<body>
<div id="div_wrapper">
    @include('layouts.admin.admin_header')
    <!-- Body -->
    @yield('content')
    <!-- Body -->
    @include('layouts.admin.admin_footer')
</div>
</body>
</html>