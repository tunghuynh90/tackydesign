{text: "System", expanded: true, url: "", encoded:false, imageUrl:"",
    items: [
            {text: "Account", encoded:false, url: "/admin/account", imageUrl: ""},
@if($admin)
    @if(!$admin->restricted)
            {text: "Configuration", encoded:false, url: "/admin/system/configuration", imageUrl: ""},
            {text: "Configs", encoded:false, url: "/admin/system/configs", imageUrl: ""},
            {text: "User", encoded:false, url: "/admin/account/user", imageUrl: ""},
            <!-- {text: "User's Group", encoded:false, url: "/admin/account/user-group", imageUrl: ""}, -->
            {text: "Module", encoded:false, url: "/admin/system/module", imageUrl: ""},
            {text: "Module's Group", encoded:false, url: "/admin/system/module-group", imageUrl: ""},
    @endif
@endif
            {text: "Country", encoded:false, url: "/admin/system/country", imageUrl: ""},
            {text: "Province / State", encoded:false, url: "/admin/system/province-state", imageUrl: ""},
            {text: "Zip Postal", encoded:false, url: "/admin/system/zip-postal", imageUrl: ""},
            {text: "<hr style='border: solid 1px #31708f; height: 0px' />", encoded:false, url: "", enabled:false, imageUrl: ""},
            {text: "Log Out", encoded:false, url: "/admin/logout", imageUrl: ""},
    ]
}
@for($i=0; $i<sizeof($menus);$i++)
    <?php $subs = $menus[$i]['sub'];?>
    ,{text: "{{$menus[$i]['name']}}", expanded: true, url: "", encoded:false, imageUrl:"",
    items: [
    @for($j=0; $j<sizeof($menus[$i]['sub']);$j++)
        {text: "{{$menus[$i]['sub'][$j]['name']}}", encoded:false, url: "/admin/{{$menus[$i]['key']}}/{{$menus[$i]['sub'][$j]['key']}}", imageUrl: ""}{{sizeof($menus[$i]['sub'])>$j+1?',':''}}
    @endfor
    ]
}
@endfor
