@extends($theme.'.common.default')
@section('body')
<body class="page-news template-blog">
    @stop
    @section('content')
    <section class="main-content">
    <div class="row blog-title">
        <div class="columns large-12">
        	<h1 class="text">News</h1>
        </div>
    </div>

	<div class="left-two-columns-wrapper row">
  		<!-- Begin content -->
  		<div class="articles columns large-9">
  			@if(!empty($blog))
            @foreach($blog['blog'] as $key => $blog_detail)
                <div class="article clearfix">
                    <div class="left-sidebar columns large-3 show-for-large-up">
                        <div class="date block">
                            <h3 class="title">Date</h3>
                            <p class="content">{{date('M d Y',strtotime($blog_detail['posted_date']))}}</p>
                        </div>
                        <div class="author block">
                            <h3 class="title">Written By</h3>
                            <p class="content">{{$blog_detail['created_by']}}</p>
                        </div><!--
                        <div class="comments block">
                            <h3 class="title">Comments</h3>
                            <p class="content">0 Comments</p>
                        </div> -->
                    </div>
                    <div class="middle-column columns large-9">
                        <h1 class="title">
                             <a href="{{$url}}/blog-detail/{{$blog_detail['short_name']}}">
                                {{$blog_detail['title']}}
                            </a>
                        </h1>
                        <div class="content rte-content colored-links">
                            {{html_entity_decode($blog_detail['content'])}}
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
		</div>
    	<!-- END content -->
  		<!-- Begin sidebar -->
		<div class="side-bar columns large-3">
			<div class="divider"></div>
			<div class="recent-articles block clearfix colored-links">
				<h3 class="title">Recent Articles</h3>
				<ul>
                     @if(!empty($blog))
                     @foreach($blog['recent_article'] as $key => $recent_article)
                        <li class="item">
                            <a href="{{$url}}/blog-detail/{{$recent_article['short_name']}}">
                                {{$recent_article['title']}}
                            </a>
                            <br><em>Posted on {{date('M d Y',strtotime($recent_article['posted_date']))}}</em>
                        </li>
                    @endforeach
                    @endif
				</ul>
			</div>
            <div class="tags block clearfix colored-links">
            </div>
        </div>
  		<!-- End sidebar -->
	</div>



	<div class="row">
        @if($blog['total_pages'] > 1)
		<div class="right columns large-6">
			<ul class="pagination">
                <?php
                $page= $blog['page'];
                    if($page-3 > 0)
                        $min = $page-3;
                    else
                        $min=1;
                    if($page+3 <= $blog['total_pages'])
                        $max=$page+3;
                    else
                        $max=$blog['total_pages'];
                ?>
                @if($page-1 >= 1)
				<li class="arrow left"><a href="{{$url}}/blog/page/{{$page-1}}">Previous<span class="glyph arrow-left" aria-hidden="true"></span></a></li>
                @endif
                @for($i=$min; $i<$max;$i++)
                @if($i==$page)
				<li class="current"><a href="">{{$i}}</a></li>
                @endif
                @endfor
                @if(@page + 1 <=$blog['total_pages'])
				<li><a href="{{$url}}/blog/page/{{$i}}">{{$i}}</a></li>
                @endif
                @if($page + 1 <=$blog['total_pages'])

				<li class="arrow right"><a href="{{$url}}/blog/page/{{$page+1}}">Next <span class="glyph arrow-right" aria-hidden="true"></span></a></li>
                @endif
			</ul>
		</div>
        @endif
	</div>

</section>

    @stop