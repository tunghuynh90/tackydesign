@extends($theme.'.common.default')
@section('body')
	<body class="page-news template-blog">
@stop

@section('content')
<section class="main-content">
    <div class="row blog-title">
        <div class="columns large-12">
        	<h1 class="text">News</h1>
        </div>
    </div>

	<div class="left-two-columns-wrapper row">
  		<!-- Begin content -->
  		<div class="articles columns large-9">
            <?php for($m=0;$m<5;$m++){?>
                <div class="article clearfix">
                    <div class="left-sidebar columns large-3 show-for-large-up">
                        <div class="date block">
                            <h3 class="title">Date</h3>
                            <p class="content">May 09 2014</p>
                        </div>
                        <div class="author block">
                            <h3 class="title">Written By</h3>
                            <p class="content">Kevin Gerard</p>
                        </div>
                        <div class="comments block">
                            <h3 class="title">Comments</h3>
                            <p class="content">0 Comments</p>
                        </div>
                    </div>
                    <div class="middle-column columns large-9">
                        <h1 class="title">
                            <a href="/blogs/ids">
                                Portland Rose Instagram Contest
                            </a>
                        </h1>
                        <p class="mobile-meta show-for-medium-down">
                            Written by Kevin Gerard - May 09 2014
                        </p>
                        <div class="content rte-content colored-links">
                          We are proud to announce that the Portland Rose Five Panel hat has reached a milestone and has been one of our best selling items between our site and Urban Outfitters.<span>&nbsp;&nbsp;</span>A very big thank you is in order and we truly value the support we have received from all over the globe. To celebrate this monumental achievement we are having another contest!<span>&nbsp;&nbsp;</span>If you are a proud owner of one of these now infamous Portland Rose hats please partake in our new instagram contest.<span>&nbsp;&nbsp;</span>Snap a profound picture of you in your Portland Rose hat and hashtag #profoundco &amp; #myportlandrose to enter to win a $100 gift card to our new and improved website!<span>&nbsp;&nbsp;</span>The contest will run until Monday so you'll have plenty of time to snap a perfect Portland rose shot.<span>&nbsp;&nbsp;</span>And if you don't have one yet, join the fam!<span>&nbsp;&nbsp;</span>They are now restocked on our website and can also be purchased at Urban Outfitters online and in stores across the U.S.<span>&nbsp;&nbsp;</span>Good luck!
                          <img src="{{$url}}/assets/themes/default/images/blogs.jpg" alt="" />
                        </div>
                    </div>
                </div>
            <?php }?>
		</div>
    	<!-- END content -->


  		<!-- Begin sidebar -->
		<div class="side-bar columns large-3">
			<div class="divider"></div>
			<div class="recent-articles block clearfix colored-links">
				<h3 class="title">Recent Articles</h3>
				<ul>
                	 <?php for($m=0;$m<5;$m++){?>
                        <li class="item">
                            <a href="/blogs/ids">
                                Portland Rose Instagram Contest
                            </a>
                            <br><em>Posted on May 09, 2014</em>
                        </li>
                     <?php }?>
				</ul>
			</div>
            <div class="tags block clearfix colored-links">
            </div>
        </div>
  		<!-- End sidebar -->
	</div>



	<div class="row">
		<div class="right columns large-6">
			<ul class="pagination">
				<li class="arrow left unavailable"><a><span class="glyph arrow-left" aria-hidden="true"></span> Previous</a></li>
				<li class="current"><a href="/blogs/ids">1</a></li>
				<li><a href="http://profoundco.com/blogs/news?page=2">2</a></li>
				<li class="arrow right"><a href="/blogs/ids">Next <span class="glyph arrow-right" aria-hidden="true"></span></a></li>
			</ul>
		</div>
	</div>

</section>

@stop