@extends($theme.'.common.default')
@section('body')
	<body class="page-spring-instagram-giveaway template-article" data-twttr-rendered="true">
@stop

@section('content')
<section class="main-content">

    <div class="row show-for-medium-up">
		<div class="columns large-12">
			<ul class="breadcrumbs colored-links">
				<li><a href="{{$url}}">Home</a></li>
				<li><a href="{{$url}}/blog" title="">Blog</a></li>
				<li>{{$blog->title}}</li>
			</ul>
		</div>
	</div>


    <div class="previous-next row">
		<div class="previous columns large-2 small-6">
			@if(isset($prev_blog->short_name))
			<span class="left"><a href="{{$url}}/blog-detail/{{$prev_blog->short_name}}" title="">
            	<span aria-hidden="true" class="glyph arrow-left"></span> Previous </a>
            </span>
            @endif
    		<div class="border-decoration show-for-large-up"></div>
  		</div>
		<div class="next columns large-3 large-offset-7 small-6">
			@if(isset($next_blog->short_name))
			<span class="right">
            	<a href="{{$url}}/blog-detail/{{$next_blog->short_name}}" title="">Next <span aria-hidden="true" class="glyph arrow-right"></span></a>
           	</span>
           	@endif
			<div class="border-decoration show-for-large-up"></div>
		</div>
	</div>


	<div class="left-two-columns-wrapper row">

		<div class="article columns large-9">
			<div class="left-sidebar columns large-3 show-for-large-up">
				<div class="date block">
					<h3 class="title">Date</h3>
					<p class="content">{{date('M d Y',strtotime($blog->posted_date))}}</p>
				</div>

				<div class="author block">
					<h3 class="title">Written By</h3>
					<p class="content">{{$blog->created_by}}</p>
				</div>

			<!-- 	<div class="comments block">
					<h3 class="title">Comments</h3>
					<p class="content">0 Comments</p>
				</div> -->
			</div>

			<div class="middle-column columns large-9">
				<h1 class="title"><a href="">{{$blog->title}}</a></h1>
                <div class="content colored-links rte-content">
                	{{html_entity_decode($blog->content)}}
                </div>

				<div class="share block clearfix">
					<h2 class="title">Share this</h2>
					<div class="item google-plus"></div>
                    <div class=" item tweet-btn"></div>
					<div class="item pinterest"></div>
					<div class="item facebook-like"></div>
				</div>
			</div>
		</div>



  		<!-- Begin sidebar -->
		<div class="side-bar columns large-3">
			<div class="divider"></div>
			<div class="recent-articles block clearfix colored-links">
				<h3 class="title">Recent Articles</h3>
				<ul>
                    @foreach($recent_articles as $key => $recent_article)
                    <li class="item">
                        <a href="{{$url}}/blog-detail/{{$recent_article['short_name']}}">
                            {{$recent_article['title']}}
                        </a>
                        <br><em>Posted on {{date('M d Y',strtotime($recent_article['posted_date']))}}</em>
                    </li>
                    @endforeach
				</ul>
			</div>
            <div class="tags block clearfix colored-links">
            </div>
        </div>
  		<!-- End sidebar -->

    </div>

</section>

@stop