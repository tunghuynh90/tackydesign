@extends($theme.'.common.default')
@section('body')
<body class="page-your-shopping-cart template-cart">
    @stop
@section('content')
<section class="main-content">
    <header class="row">
        <div class="columns">
            <h1 class="page-title">Your Cart</h1>
        </div>
    </header>
    @if(Session::has('order_message'))
    <section class="empty-cart row colored-links">
        <div class="columns">
            <h1>{{Session::get('order_message')}}</h1>
        </div>
        <script type="text/javascript">
            setTimeout(function(){
                location.reload();
            },3000);
        </script>
    </section>
    @elseif(Session::has('order_confirm'))
    <section class="empty-cart row colored-links">
        <div class="columns">
            <h1>{{Session::get('order_confirm')}}</h1>
            <h2>You can continue browsing <a href="{{$url}}/collections/">here</a>.</h2>
        </div>
    </section>
    @elseif(empty($carts))
    <section class="empty-cart row colored-links">
        @if(Session::has('order_success'))
        <div class="columns">
            <h1>{{Session::get('order_success')}}</h1>
            <h2>You can continue browsing <a href="{{$url}}/collections/">here</a>.</h2>
        </div>
        @else
        <div class="columns">
            <h1>It appears that your cart is currently empty!</h1>
            <h2>You can continue browsing <a href="{{$url}}/collections/">here</a>.</h2>
        </div>
        @endif
    </section>
    @else
    @if(Session::has('order_message'))
    <section class="empty-cart row colored-links">
        <div class="columns">
            <h1>{{Session::get('order_message')}}</h1>
        </div>
    </section>
    @endif
    <form action="{{$url}}/cart" method="post" class="custom">
        <div class="row">
            <div class="columns">
                <table width="100%" class="cart-table">
                    <thead>
                        <tr>
                            <th class="image">&nbsp;</th>
                            <th class="title">&nbsp;</th>
                            <th class="quantity">Quantity</th>
                            <th class="total">Total</th>
                            <th class="remove">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($carts as $rowId=>$cart)
                        <tr>
                            <td class="image">
                                <a class="image_product" href="{{$cart['url']}}">
                                <img class="item-cart-image"  style="width: 68px" src="{{$cart['image']}}" alt="{{$cart['title']}}">
                                </a>
                                <p class="mobile-title"><a href="{{$cart['url']}}">{{$cart['title']}}</a></p>
                            </td>
                            <td class="title">
                                <p><a href="{{$cart['url']}}">{{$cart['title']}}</a></p>
                            </td>
                            <td class="quantity"><input type="text" class="field styled-input" name="quantity[{{$rowId}}]" id="{{$rowId}}" value="{{$cart['quantity']}}"></td>
                            <td class="total">$ {{number_format($cart['subtotal'],2)}}</td>
                            <td class="remove"><a title="Remove" href="{{$url}}/cart/delete-cart/{{$rowId}}" aria-hidden="true" class="glyph cross"></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- .row -->
        <div class="row">
            <div class="columns large-5 show-for-medium-up">
                <div class="shipping-rates-calculator">
                    <div id="shipping-calculator" class="shipping-calculator">
                        <h3>Shipping Calculator</h3>
                        <div>
                            <p class="country-select-container">
                                <?php
                                    $row_id = Cart::search(array('id'=>'shipping_method'));
                                    $shipping_method = null;
                                    if($row_id)
                                        $shipping_method = Cart::get($row_id[0]);
                                ?>
                                <label for="address_country">Country</label>
                                <select id="address_country" >
                                    <option value="" data-value="">-- Please Select --</option>
                                    @foreach($arr_shipping_price as $shipping_price_id => $shipping)
                                    <option value="{{$shipping_price_id}}" data-value="{{htmlentities(json_encode($shipping['shipping_price']))}}" @if(!is_null($shipping_method) && $shipping_method->options->ship_price_id == $shipping_price_id) selected @endif>{{$shipping['country']}}</option>
                                    @endforeach
                                </select>
                            </p>
                        </div>
                        <div id="country-shipping-price">
                        @if(!is_null($shipping_method)  )
                        @if(isset($arr_shipping_price[$shipping_method->options->ship_price_id]))
                            @foreach($arr_shipping_price[$shipping_method->options->ship_price_id]['shipping_price'] as $method_id => $method)
                            <input type="radio" class="shipping_method" id="shipping_method_{{$method_id}}" name="shipping_method" value="{{$method_id}}" @if($shipping_method->options->ship_price_detail_id == $method_id) checked @endif /> <label style="display: inline" for="shipping_method_{{$method_id}}">$ {{ $method['shipping_price']}} - {{ $method['shipping_method']}}</label><br/>
                            @endforeach
                        @endif
                        @endif
                        </div>
                        <div id="wrapper-response" class="hidden-field">
                            <p id="shipping-rates-feedback" class="error"></p>
                            <p id="discount-promo-code" class="error"></p>
                        </div>
                    </div>
                </div>
                <div class="continue-shopping show-for-medium-up">
                    <span><a href="{{$url}}/collections/"><span aria-hidden="true" class="glyph arrow-left"></span> Continue Shopping</a></span>
                </div>
            </div>
            <div class="columns large-7">
                <div class="totals columns">
                    <?php $total =  $total_price; ?>
                    <?php
                        $discount_price = 0;
                        if(isset($discount)){
                            $discount = (float)str_replace('%', '', $discount);
                            if($is_shipping_discount){
                                if(!is_null($shipping_method)){
                                   $discount_price =  $shipping_method->options->shipping_price * ($discount / 100);
                                }
                            } else
                                $discount_price = $total_price*($discount/100);
                        }
                    ?>
                    <h3><strong>SUBTOTAL $ {{number_format($total_price,2)}}</strong></h3>
                    <h4 id="estimated-discount" <?php if(!$discount){ ?>style="display:none" <?php } ?>>- <span id="discount-label"><?php if($is_shipping_discount){ ?>Shipping <?php } ?>Discount</span> <em id="discount-percent">{{isset($discount) ? $discount.'%' : ''}}</em>: <em id="discount-price"><?php echo '- $ '.number_format($discount_price,2); ?></em></h4>
                    <h4 id="estimated-shipping" @if(is_null($shipping_method)) style="display:none" @endif>+ Estimated shipping <em>@if(!is_null($shipping_method)) {{'$ '.$shipping_method->options->shipping_price}} @endif</em></h4>
                    <?php
                        $total -= $discount_price;
                        if(!is_null($shipping_method)){
                            $total += $shipping_method->options->shipping_price;
                        }
                    ?>
                    <h3><strong>TOTAL $ <span id="total-cart">{{number_format($total,2)}}</span></strong></h3>
                    <input type="submit" name="update" class="button" value="Update cart">
                    <?php
                    /*
                    <span class="or">or</span>
                    <input class="button" type="submit" name="checkout" value="Check out">
                    */
                    ?>
                    <p class="additional-checkout-buttons">
                        <img id="paypal_express" style="cursor: pointer;" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckoutsm.gif">
                        <script type="text/javascript">
                            $("#paypal_express").click(function(){
                                location.assign("{{$url}}/checkout");
                            })
                        </script>
                    </p>
                </div>
                <div class="order-notes columns">
                    <div class="container">
                        <h3>Promo Code</h3>
                        <input type="text" id="cart-promo-code" name="promo_code" value="{{$promo_code}}" />
                        <input class="button" type="button" id="check-promo-code" value="Apply" />
                    </div>
                </div>
            </div>
            <div class="continue-shopping columns show-for-small columns">
                <span><a onclick="{{$url}}/collections/"><span aria-hidden="true" class="glyph arrow-left"></span> Continue Shopping</a></span>
            </div>
        </div>
    </form>
        <!-- .row -->
    <script type="text/javascript">
        // $("#cart-promo-code").mask('AAAA-AAAA-AAAA');
        $("input",".quantity").keydown(function(e){
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $("input",".quantity").focus(function(){
            $(this).attr("data-old-value", $(this).val() );
        });
        $("input",".quantity").change(function(){
            if( $(this).val() <= 0){
                $(this).val( $(this).attr("data-old-value") );
                event.preventDefault();
            }
        });
        $("#check-promo-code").click(function(){
            $("#estimated-discount").hide();
            $("em","#estimated-discount").html("");
            $(this).val("Searching...");
            $.ajax({
                url: "{{$url}}/cart/get-promo-code",
                type: "POST",
                data: {promo_code : $("#cart-promo-code").val()},
                success: function(result){
                    $("input[type=button]#check-promo-code").val("Apply");
                    if(result.discount != undefined){
                        $("#estimated-discount").show();
                        if(result.is_shipping_discount != undefined)
                            $("#discount-label","#estimated-discount").html("Shipping Discount");
                        else
                            $("#discount-label","#estimated-discount").html("Discount");
                        $("em#discount-percent","#estimated-discount").html(result.discount);
                        $("em#discount-price","#estimated-discount").html(result.discount_price);
                    } else
                        alert(result.message);
                    if(result.total_price != undefined)
                        $("#total-cart").html(result.total_price);
                }
            });
        });
        $("#address_country").change(function(){
            var shipping = $("option:selected",this).attr("data-value");
            var html = "";
            if(shipping != "" && shipping != undefined){
                shipping= $.parseJSON(shipping);
                var checked = false;
                for(var i in shipping){
                    html += '<input type="radio" class="shipping_method" id="shipping_method_'+i+'" name="shipping_method" value="'+i+'" '+(!checked ? 'checked' : '')+' /> <label style="display: inline" for="shipping_method_'+i+'">$'+shipping[i].shipping_price+' '+shipping[i].shipping_method+'</label><br />';
                    checked = true;
                }
            }
            $("#country-shipping-price").html(html);
            changeShipping();
        });
        $("#country-shipping-price").on("click",".shipping_method",function(){
            changeShipping();
        })
        function changeShipping()
        {
            var radio = $(".shipping_method:checked","#country-shipping-price");
            var id = radio.val();
            $("em","#estimated-shipping").html("").parent().hide();
            $("#total-cart").html("");
            if(id == undefined)
                id = 0;
            $.ajax({
                url : "{{$url}}/cart/change-shipping-method",
                type: "POST",
                data: {method : id},
                success: function(shipping){
                    if(shipping.status == "ok"){
                        $("em","#estimated-shipping").html("$ "+shipping.shipping_price).parent().show();
                        $("#total-cart").html(shipping.cost);
                        $("em#discount-price","#estimated-discount").html(shipping.discount_price);
                    }
                }
            })
        }
    </script>
    @endif
</section>
@stop