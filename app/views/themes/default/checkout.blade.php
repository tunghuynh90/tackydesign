
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="chrome=1" />
        <title>{{$config['title_site']}} - Checkout</title>
        <link href="{{$url}}/assets/css/forms-39cd117b7ec24c130bdfef510ec431bb.css" media="screen" rel="stylesheet" />
        <link href="{{$url}}/assets/css/checkout-f8d5c8b5203361aedb3af1d372a362db.css" media="screen" rel="stylesheet" />
        <link href="{{$url}}/assets/css/forms-39cd117b7ec24c130bdfef510ec431bb.css" media="screen" rel="stylesheet" />
        <link href="{{$url}}/assets/css/checkout.css" media="screen" rel="stylesheet" type="text/css" />
        <script src="{{$url}}/assets/js/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="container" class="step{{!isset($arr_data['step_2']) ? 1 : 2}}">
            <div id="header">
                <a href="{{$url}}" role="banner" title="{{$config['title_site']}}">
                    <img src="{{$url}}/assets/images/logos/{{$config['logo_image']}}" data-retina="{{$url}}/assets/themes/default/images/logo-retina.png" alt="Tacky Design">
                </a>
                <h1 id="tagline"><span class="title">{{$config['title_site']}}</span> <span class="subtitle">Create your order.</span></h1>
            </div>
            <div id="main">
                <!-- begin overview -->
                <div id="overview" class="clearfix">
                    <div id="products">
                        <h2>You&#39;re purchasing this&hellip;</h2>
                        <table id="thumbs" cellspacing="0" cellpadding="0">
                            @foreach($arr_items as $item)
                            <tr>
                                @foreach($item as $item_detail)
                                <td>
                                    <div style="height:100px;width:100px;">
                                        <img style="height:100px;" alt="{{$item_detail['title']}}" class="images" src="{{$item_detail['image']}}" />
                                    </div>
                                    <h3 class="plisthead" title="{{$item_detail['title']}}">{{$item_detail['title']}}</h3>
                                    <span class="plist-variant-title">{{(isset($options['size']) ? ' - '.$options['size'].'/ ' : '').(isset($options['color']) ? $options['color'] : '')}}</span><br />
                                    <span class="plist-line-price">{{$item_detail['quantity']}}x $ {{number_format($item_detail['price'],2)}}</span>
                                </td>
                               @endforeach
                            </tr>
                            @endforeach
                            <tr>
                            </tr>
                        </table>
                    </div>
                    <div id="wallet">
                        <span id="cost">$ {{number_format($total,2)}}</span>
                        @if(isset($arr_data['step_2']))
                        <span id="tax_span" class="hint">.. including tax {{$arr_data['tax_percent']}}% for $ {{number_format($arr_data['tax'],2)}}.</span><br />
                        <span id="shipping_span" class="hint">.. including shipping for $ {{isset($arr_data['minimun_price']) ? $arr_data['minimun_price'] : number_format($arr_data['shipping_price'],2)}}.</span>
                        @endif
                        <br />
                        <span id="steps">
                        step {{!isset($arr_data['step_2']) ? 1 : 2}} of 2
                        </span>
                    </div>
                    <div style="clear:left"></div>
                </div>
                <!-- /end overview -->
                <div id="content">
                    @if(isset($arr_data['step_2']))
                    @include('themes.default.checkout_step_2',array('arr_data'=>$arr_data))
                    @else
                    {{Form::open(['url' => '/checkout','class'=>'new_order','id'=>'new_order'])}}
                        @if(!$email)
                        <div id="email">
                          <label for="order_email">Your Contact Email</label>
                                @if(Session::has('email_error'))
                                <div class="field-with-errors">
                                    <span class="error-message">{{Session::get('email_error')}}</span>
                                    <br />
                                @endif
                                    {{Form::email('checkout[email]', null,
                                                            array(
                                                                'class'=>'medium',
                                                                'id'=>'order_email',
                                                                'size'=>'30',
                                                                'tabindex'=>'1',
                                                                'x-autocompletetype'=>'email',
                                                                )
                                                            )
                                            }}
                                    <span class="sign-in-message">Already have an account? <a href="{{$url}}/user/login?redirect_url=checkout" id="customer-login-link">Sign in now</a></span>
                                @if(Session::has('email_error'))
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endif
                        <!-- begin addresses -->
                        <div class="group clearfix" id="addresses">
                            <div class="gleft">
                                <h3>Billing address</h3>
                                <table cellspacing="0" cellpadding="0" class="form" id="billing">
                                    <tr>
                                        <td class="lbl"><label for="billing_address_first_name">First Name</label></td>
                                        <td>
                                            {{Form::text('billing_address[first_name]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_first_name',
                                                                'size'=>'30',
                                                                'tabindex'=>'2',
                                                                'x-autocompletetype'=>'section-billing given-name',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_last_name">Last Name</label></td >
                                        <td>
                                            @if(isset($arr_post['billing_address']['last_name']) && strlen($arr_post['billing_address']['last_name']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            {{Form::text('billing_address[last_name]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_last_name',
                                                                'size'=>'30',
                                                                'tabindex'=>'3',
                                                                'x-autocompletetype'=>'section-billing surname',
                                                                )
                                                            )
                                            }}
                                            @if(isset($arr_post['billing_address']['last_name']) && strlen($arr_post['billing_address']['last_name']) == 0)
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_company">Company</label></td >
                                        <td>
                                            {{Form::text('billing_address[company]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_company',
                                                                'size'=>'30',
                                                                'tabindex'=>'4',
                                                                'x-autocompletetype'=>'section-billing organization',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_address1">Address</label></td>
                                        <td style="width: 260px">
                                            @if(isset($arr_post['billing_address']['address_1']) && strlen($arr_post['billing_address']['address_1']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                             {{Form::text('billing_address[address_1]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_address1',
                                                                'size'=>'30',
                                                                'tabindex'=>'5',
                                                                'x-autocompletetype'=>'section-billing address-line1',
                                                                )
                                                            )
                                            }}
                                            @if(isset($arr_post['billing_address']['address_1']) && strlen($arr_post['billing_address']['address_1']) == 0)
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_address2">Address2</label></td>
                                        <td style="width: 260px">
                                            {{Form::text('billing_address[address_2]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_address2',
                                                                'size'=>'30',
                                                                'tabindex'=>'6',
                                                                'x-autocompletetype'=>'section-billing address-line2',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_city">City</label></td>
                                        <td>
                                            @if(isset($arr_post['billing_address']['city']) && strlen($arr_post['billing_address']['city']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            {{Form::text('billing_address[city]', null,
                                                            array(
                                                                'class'=>'medium',
                                                                'id'=>'billing_address_city',
                                                                'size'=>'30',
                                                                'tabindex'=>'7',
                                                                'x-autocompletetype'=>'section-billing city',
                                                                )
                                                            )
                                            }}
                                            @if(isset($arr_post['billing_address']['city']) && strlen($arr_post['billing_address']['city']) == 0)
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_zip">Postal/ZIP Code</label></td>
                                        <td>
                                            @if(isset($arr_post['billing_address']['zip']) && strlen($arr_post['billing_address']['zip']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            @if(isset($arr_post['billing_address']['not_match_zip']) )
                                            <div class="field-with-errors">
                                                <span class="error-message">is not valid</span>
                                                <br />
                                            @endif
                                            {{Form::text('billing_address[zip]', null,
                                                            array(
                                                                'class'=>'medium',
                                                                'id'=>'billing_address_zip',
                                                                'size'=>'30',
                                                                'tabindex'=>'8',
                                                                'x-autocompletetype'=>'section-billing postal-code',
                                                                )
                                                            )
                                            }}
                                            @if(isset($arr_post['billing_address']['zip']) && strlen($arr_post['billing_address']['zip']) == 0)
                                            </div>
                                            @endif
                                            @if(isset($arr_post['billing_address']['not_match_zip']) )
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 360px">
                                            <ul class="inline">
                                                <li style="width: 160px">
                                                    <label for="billing_address_country">Country</label>
                                                    @if(isset($arr_post['billing_address']['country']) && !$arr_post['billing_address']['country'])
                                                    <div class="field-with-errors">
                                                        <span class="error-message">can't be blank</span>
                                                        <br />
                                                    @endif
                                                    <?php
                                                        $arr_country_view = array(0=>'-- Please Select --');
                                                        foreach($countries as $country_id => $country)
                                                            $arr_country_view[$country_id] = $country['name'];
                                                    ?>
                                                    {{Form::select('billing_address[country]',
                                                                $arr_country_view,
                                                                null,
                                                                array(
                                                                    'style'=>'width:150px',
                                                                    'id'=>'billing_address_country',
                                                                    'tabindex'=>'9',
                                                                    )
                                                                )
                                                    }}
                                                    @if(isset($arr_post['billing_address']['country']) && !$arr_post['billing_address']['country'])
                                                    </div>
                                                    @endif
                                                </li>
                                                <li id="billing_address_province_li" style="width: 160px;margin-right: 5px">
                                                    <label for="billing_address_province" id="billing_zonelabel">State / Province</label>
                                                    {{Form::text('billing_address[province]', null,
                                                            array(
                                                                'style'=>'width:150px',
                                                                'id'=>'billing_address_province',
                                                                'size'=>'30',
                                                                'tabindex'=>'10',
                                                                )
                                                )
                                                    }}
                                                    <select id="billing_address_province" name="billing_address[province]" style="width:150px;display: none" disabled>
                                                    </select>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="billing_address_phone">Phone</label></td>
                                        <td>
                                            {{Form::text('billing_address[phone]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'billing_address_phone',
                                                                'size'=>'30',
                                                                'tabindex'=>'11',
                                                                'x-autocompletetype'=>'section-billing phone-full',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr id="billing-is-shipping">
                                        <td colspan="2">
                                            <input type="checkbox" name="billing_is_shipping" id="shipping-toggle"  style="width: auto" tabindex="12" @if(isset($arr_post['billing_is_shipping'])) checked @endif />
                                            <label for="shipping-toggle">Ship items to the above billing address</label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="gright">
                                <h3>Shipping address</h3>
                                <div id="shipping-same" class="address-notification" @if(!isset($arr_post['billing_is_shipping'])) style='display:none;' @endif>
                                    <span>Item(s) will be shipped to your billing address.</span>
                                </div>
                                <table id="shipping" cellspacing="0" cellpadding="0" class="form" @if(isset($arr_post['billing_is_shipping'])) style='display:none;' @endif>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_first_name">First Name</label></td>
                                        <td>
                                            {{Form::text('shipping_address[first_name]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_first_name',
                                                                'size'=>'30',
                                                                'tabindex'=>'21',
                                                                'x-autocompletetype'=>'section-shipping given-name',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_last_name">Last Name</label></td>
                                        <td>
                                            @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['last_name']) && strlen($arr_post['shipping_address']['last_name']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            {{Form::text('shipping_address[last_name]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_last_name',
                                                                'size'=>'30',
                                                                'tabindex'=>'22',
                                                                'x-autocompletetype'=>'section-shipping surname',
                                                                )
                                                            )
                                            }}
                                            @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['last_name']) && strlen($arr_post['shipping_address']['last_name']) == 0)
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_company">Company</label></td >
                                        <td>
                                            {{Form::text('shipping_address[company]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_company',
                                                                'size'=>'30',
                                                                'tabindex'=>'23',
                                                                'x-autocompletetype'=>'section-shipping organization',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_address1">Address</label></td>
                                        <td style="width: 260px">
                                            @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['address_1']) && strlen($arr_post['shipping_address']['address_1']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            {{Form::text('shipping_address[address_1]', null,
                                                            array(
                                                                'id'=>'shipping_address_address1',
                                                                'size'=>'30',
                                                                'tabindex'=>'24',
                                                                'x-autocompletetype'=>'section-shipping address-line1',
                                                                )
                                                )
                                            }}
                                            @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['address_1']) && strlen($arr_post['shipping_address']['address_1']) == 0)
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_address2">Address2</label></td>
                                        <td style="width: 260px">
                                            {{Form::text('shipping_address[address_2]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_address2',
                                                                'size'=>'30',
                                                                'tabindex'=>'25',
                                                                'x-autocompletetype'=>'section-shipping address-line2',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_city">City</label></td>
                                        <td>
                                            {{Form::text('shipping_address[city]', null,
                                                            array(
                                                                'class'=>'medium',
                                                                'id'=>'shipping_address_city',
                                                                'size'=>'30',
                                                                'tabindex'=>'26',
                                                                'x-autocompletetype'=>'section-shipping city',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_zip">Postal/ZIP Code</label></td>
                                        <td>
                                            @if(isset($arr_post['shipping_address']['zip']) && strlen($arr_post['shipping_address']['zip']) == 0)
                                            <div class="field-with-errors">
                                                <span class="error-message">can't be blank</span>
                                                <br />
                                            @endif
                                            @if(isset($arr_post['shipping_address']['not_match_zip']) )
                                            <div class="field-with-errors">
                                                <span class="error-message">is not valid</span>
                                                <br />
                                            @endif
                                            {{Form::text('shipping_address[zip]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_zip',
                                                                'size'=>'30',
                                                                'tabindex'=>'27',
                                                                'x-autocompletetype'=>'section-shipping postal-code',
                                                                )
                                                            )
                                            }}
                                            @if(isset($arr_post['shipping_address']['zip']) && strlen($arr_post['shipping_address']['zip']) == 0)
                                            </div>
                                            @endif
                                            @if(isset($arr_post['shipping_address']['not_match_zip']) )
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ul class="inline">
                                                <li style="width: 150px; margin-right: 10px">
                                                    <label for="shipping_address_country">Country</label>
                                                    @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['country'])  && !$arr_post['shipping_address']['country'])
                                                    <div class="field-with-errors">
                                                        <span class="error-message">can't be blank</span>
                                                        <br />
                                                    @endif
                                                    {{Form::select('shipping_address[country]',
                                                                    $arr_country_view,
                                                                    null,
                                                                    array(
                                                                        'style'=>'width:150px',
                                                                        'id'=>'shipping_address_country',
                                                                        'tabindex'=>'28',
                                                                        )
                                                            )
                                                    }}
                                                    @if(!isset($arr_post['billing_is_shipping']) && isset($arr_post['shipping_address']['country'])  && !$arr_post['shipping_address']['country'])
                                                    </div>
                                                    @endif
                                                </li>
                                                <li style="width: 150px" id="shipping_address_province_li">
                                                    <label for="shipping_address_province" id="shipping_zonelabel">State / Province</label>
                                                     {{Form::text('shipping_address[province]', null,
                                                            array(
                                                                'style'=>'width:150px',
                                                                'id'=>'shipping_address_province',
                                                                'size'=>'30',
                                                                'tabindex'=>'29',
                                                                )
                                                )
                                                    }}
                                                    <select id="shipping_address_province" name="shipping_address[province]" style="width:150px; display: none" disabled >
                                                    </select>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl"><label for="shipping_address_phone">Phone</label></td>
                                        <td>
                                            {{Form::text('shipping_address[phone]', null,
                                                            array(
                                                                'class'=>'short',
                                                                'id'=>'shipping_address_phone',
                                                                'size'=>'30',
                                                                'tabindex'=>'30',
                                                                'x-autocompletetype'=>'section-shipping phone-full',
                                                                )
                                                            )
                                            }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="clear: both"></div>
                        </div>
                        <!-- /end addresses -->
                        <div id="buttons">
                            <input id="commit-button" name="commit" tabindex="40" type="submit" value="Continue to next step" />
                            <small id="cancel-purchase">or <a href="{{$url}}">return to store</a></small>
                        </div>
                    {{Form::close()}}
                    @endif
                </div>
            </div>
        </div>
        <div id="wallet-highlight" class="wallet-highlight" style="display: none;"></div>
        <script type="text/javascript">
            @if(isset($message) && $message)
            alert("{{$message}}");
            @endif
            @if(!isset($arr_data['step_2']))
            $(function(){
                var arr_country = ['shipping_address_country','billing_address_country'];
                for(var i in arr_country){
                    if($("#"+arr_country[i]).val()){
                        var value = $("#"+arr_country[i]).val();
                        autoChangeProvince(value,arr_country[i]);
                    }
                }
            });
            $("#shipping-toggle").click(function(){
                $("#shipping-same").toggle();
                $("#shipping").toggle();
            })
            @foreach($countries as $country_id => $country)
            @if(!empty($country['cities']))
                var country_{{$country_id}} = {{json_encode($country['cities'])}};
            @endif
            @endforeach
            $("#shipping_address_country, #billing_address_country").change(function(){
                var value = $(this).val();
                autoChangeProvince(value,$(this).attr("id"));
            });
            function autoChangeProvince(value,key){
                var input_id = "#billing_address_province";
                if(key == "shipping_address_country")
                    input_id = "#shipping_address_province";
                var select_id = "select"+input_id;
                input_id = "input"+input_id;
                if( window["country_"+value]!=undefined){
                    var input_value = $(input_id).val();
                    $(input_id).attr("disabled",true).hide();
                    var html = "";
                    var country = window["country_"+value];
                    for(var i in country){
                        if(i == input_value)
                            html += '<option value="'+i+'" selected>'+country[i]+'</option>';
                        else
                            html += '<option value="'+i+'">'+country[i]+'</option>';
                    }
                    $(select_id).html(html).attr("disabled",false).show();
                } else {
                    $(input_id).attr("disabled",false).show();
                    $(select_id).attr("disabled",true).hide();
                }
            }
            @endif
        </script>
    </body>
</html>