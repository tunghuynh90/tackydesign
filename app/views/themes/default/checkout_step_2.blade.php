{{Form::open(['url' => '/checkout','id'=>'purchase-form'])}}
    <div id="payment">
         <div class="group clearfix">
            <h3>Tax</h3>
            <p></p>
            <p class="sst">
                Your Tax for {{$arr_data['province']}}, {{$arr_data['country']}} is {{$arr_data['tax_percent']}}% <span style="font: 200% Georgia, Times, 'Times New Roman', serif;">$ {{number_format($arr_data['tax'],2)}}</span>
            </p>
        </div>
        <div class="group clearfix">
            <h3>Shipping Price</h3>
            <p></p>
            <p class="sst">
                <?php
                    $arr_shipping = CartController::getShippingMethod();
                    $row_id = Cart::search(array('id'=>'shipping_method'));
                    $shipping_method = null;
                    if($row_id)
                        $shipping_method = Cart::get($row_id[0]);
                    $found = false;
                    foreach( $arr_shipping as $key => $value){
                        if( $value['country_id'] != $arr_data['country_id']) continue;
                        $found = true;
                        break;
                    }
                ?>
                @if($found)
                <select name="shipping_rate" id="shipping-rates">
                    @foreach( $arr_shipping[$key]['shipping_price'] as $method_id => $method )
                     <option value="{{$method_id}}" @if(!is_null($shipping_method) && $shipping_method->options->ship_price_detail_id == $method_id) selected @endif />$ {{ $method['shipping_price']}} - {{ $method['shipping_method']}}</option>
                    @endforeach
                </select>
                @endif
            </p>
        </div>
    </div>
    <div id="payment-method">
        <div class="group">
            <h3>How would you like to pay for your order?</h3>
            <p class="hint">
                All transactions are secure and encrypted, and we never store your credit card information. To learn more, please view our privacy policy.
            </p>
            <div class="sst">
                <div class="gleft ">
                    <span class="error-message"></span>
                    <ul id="payment-methods">
                        <?php
                        /*
                        <li id="credit-cards">
                            <input checked="checked" class="payment-method" id="direct-payment" name="gateway" type="radio" value="1">
                            <label for="direct-payment">
                                <span class="visually-hidden">Credit card</span>
                                <img alt="Visa" class="credit-card" id="credit-card-visa" src="{{$url}}/assets/images/visa.png" title="The credit card type is automatically detected when you enter your card number">
                                <img alt="Master" class="credit-card" id="credit-card-master" src="{{$url}}/assets/images/master.png" title="The credit card type is automatically detected when you enter your card number">
                            </label>
                        </li>
                        */
                        ?>
                        <li id="paypal-payments">
                            <input checked="checked" class="payment-method" id="paypal_express" name="gateway" type="radio" value="paypal_express">
                            <label for="paypal_express">
                                <span class="visually-hidden">PayPal Express</span>
                                <img src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" alt="PayPal Express Logo">
                            </label>
                        </li>
                    </ul>
                </div>
                <?php
                /*
                <div class="gright" id="credit-card-info">
                    <noscript>
                        &lt;p&gt;Your credit card details are only required if you have chosen to pay by credit card.&lt;/p&gt;
                    </noscript>
                    <table border="0">
                        <?php if(isset($arr_post)) $arr_post = $arr_post['credit_card']; ?>
                        <tbody>
                            <tr>
                                <td><label for="credit_card_first_name">First Name</label></td>
                                <td><label for="credit_card_last_name">Last Name</label></td>
                            </tr>
                            <tr>
                                <td>
                                    @if(isset($arr_post['first_name']) && strlen($arr_post['first_name']) == 0)
                                    <div class="field-with-errors">
                                        <span class="error-message">can't be blank</span>
                                    @endif
                                    <input id="credit_card_first_name" name="credit_card[first_name]" size="11" type="text" value="Tung" x-autocompletetype="cc-given-name">
                                    @if(isset($arr_post['first_name']) && strlen($arr_post['first_name']) == 0)
                                    </div>
                                    @endif
                                </td>
                                <td>
                                    @if(isset($arr_post['last_name']) && strlen($arr_post['last_name']) == 0)
                                    <div class="field-with-errors">
                                        <span class="error-message">can't be blank</span>
                                    @endif
                                    <input id="credit_card_last_name" name="credit_card[last_name]" size="13" type="text" value="Huynh" x-autocompletetype="cc-surname">
                                    @if(isset($arr_post['last_name']) && strlen($arr_post['last_name']) == 0)
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 7px">
                                    <label for="credit_card_number">Credit Card Number</label> <img alt="Check2" id="credit-card-valid" src="//cdn.shopify.com/s/images/admin/icons/check2.gif" style="display: none;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    @if( isset($arr_post['not_valid_number']) || isset($arr_post['number']) && strlen($arr_post['number']) == 0)
                                    <div class="field-with-errors">
                                        <span class="error-message">is not a credit card number</span>
                                    @endif
                                    {{Form::text('credit_card[number]', @$arr_post['number'],
                                                    array(
                                                        'pattern'=>'[0-9]*',
                                                        'id'=>'credit_card_number',
                                                        'size'=>'26',
                                                        'autocomplete'=>'off',
                                                        'x-autocompletetype'=>'cc-number',
                                                        )
                                                    )
                                    }}
                                    @if(isset($arr_post['not_valid_number']) || isset($arr_post['number']) && strlen($arr_post['number']) == 0)
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 7px">
                                    <label for="credit_card_month">Expiration Date<span class="visually-hidden"> (month)</span></label>
                                </td>
                            </tr>
                            <tr>
                                <?php
                                    $expired = false;
                                    if(isset($arr_post['month'])){
                                        $expired_date = $arr_post['year'].'-'.($arr_post['month'] < 10 ? '0'.$arr_post['month'] : $arr_post['month']);
                                        $current_time = date('Y-m');
                                        if($current_time > $expired_date)
                                            $expired = true;
                                    }
                                ?>
                                <td colspan="2" class="{{(isset($arr_post['expired']) || $expired) ? 'field-with-errors' : ''}}">
                                    @if(isset($arr_post['expired']) || $expired)
                                    <span class="error-message">Credit card is expired</span>
                                    @endif
                                    <?php
                                        $arr_month = array(
                                                           1=>'1 - January',
                                                           '2 - February',
                                                           '3 - March',
                                                           '4 - April',
                                                           '5 - May',
                                                           '6 - June',
                                                           '7 - July',
                                                           '8 - August',
                                                           '9 - Septemper',
                                                           '10 - October',
                                                           '11 - November',
                                                           '12 - December',
                                                           );
                                    ?>
                                    {{Form::select('credit_card[month]',
                                                $arr_month,
                                                isset($arr_post['month']) ? $arr_post['month'] : array(),
                                                array(
                                                    'x-autocompletetype'=>'cc-exp-month',
                                                    'id'=>'credit_card_month',
                                                    )
                                                )
                                    }}
                                    <label for="credit_card_year" class="visually-hidden">Expiration Date (year)</label>
                                     <?php
                                        $current_year =  date('Y');
                                        $twenty_year_later = $current_year + 20;
                                        for($year = $current_year; $year <= $twenty_year_later; $year++){
                                            $arr_year[$year] = $year;
                                        }
                                    ?>
                                    {{Form::select('credit_card[year]',
                                                $arr_year,
                                                isset($arr_post['year']) ? $arr_post['year'] : array(),
                                                array(
                                                    'x-autocompletetype'=>'cc-exp-year',
                                                    'id'=>'credit_card_year',
                                                    )
                                                )
                                    }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 20px">
                                    <label for="credit_card_verification_value">Card Security Code</label>
                                    @if(isset($arr_post['not_valid_cvv']))
                                    <div class="field-with-errors">
                                        <span class="error-message">is not valid for this card type</span>
                                    @elseif(isset($arr_post['verification_value']) && strlen($arr_post['verification_value']) == 0)
                                    <div class="field-with-errors">
                                        <span class="error-message">can't be blank</span>
                                    @endif

                                    {{Form::text('credit_card[verification_value]', @$arr_post['verification_value'],
                                                    array(
                                                        'pattern'=>'[0-9]*',
                                                        'id'=>'credit_card_verification_value',
                                                        'size'=>'4',
                                                        'maxlength'=>'4',
                                                        'autocomplete'=>'off',
                                                        'x-autocompletetype'=>'cc-csc',
                                                        )
                                                    )
                                    }}
                                    @if(isset($arr_post['not_valid_cvv']) || isset($arr_post['verification_value']) && strlen($arr_post['verification_value']) == 0)
                                    </div>
                                    @endif
                                    <span class="hint">
                                    <a class="closure-lightbox" href="https://www.paypal.com/us/cgi-bin/webscr?cmd=p/acc/cvv_info_pop-outside" target="_blank" title="Card Security Code">what is this?</a>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                */
                ?>
            </div>
            <p style="clear:both;height: 0"></p>
        </div>
    </div>
    @if(!Confide::user())
    <div id="newsletter">
        <div id="marketing-box" class="group clearfix">
            <h3>Keep me updated</h3>
            <p class="sst">
                <input type="checkbox" name="buyer_accepts_marketing" checked="checked" value="true" id="marketing">
                <label for="marketing" style="font-weight:normal">I want to receive occasional emails about new products, promotions and other news.</label>
            </p>
        </div>
    </div>
    @endif
    <table border="0" cellspacing="0" cellpadding="0" style="height: 55px; width: 100%;" id="buttons">
        <tbody>
            <tr>
                <td style="vertical-align:middle">
                    <input id="complete-purchase" name="commit" type="submit" value="Complete my purchase">
                    <small id="cancel-purchase">or <a href="{{$url}}">cancel, and return to store</a></small>
                </td>
            </tr>
        </tbody>
    </table>
</form>
{{Form::close()}}
<script type="text/javascript">
    $(".payment-method").click(function(){
        var id = $("input:checked.payment-method").attr("id");
        if(id == "direct-payment")
            $("#credit-card-info").show();
        else
            $("#credit-card-info").hide();
    });
    $("#shipping-rates").change(function(){
        var method_id = $(this).val();
        $.ajax({
            url: "{{$url}}/cart/shipping-price",
            type: "POST",
            data: {method_id:method_id},
            success: function(result){
                if(result.status == "ok"){
                    $("#cost").html("$ "+result.cost);
                    var html = ".. including shipping for $ "+result.shipping_price+".";
                    $("#shipping_span").html(html);
                }
            }
        })
    });
</script>