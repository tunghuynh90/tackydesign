@extends($theme.'.common.default')
@section('body')
<body class="page-collections template-list-collections" data-twttr-rendered="true">
    @stop
    @section('content')
    <section class="main-content">
    <header class="row">
        <div class="left columns large-6">
            <h1 class="page-title">Shop</h1>
        </div>
    </header>
    <div class="row">
        <div class="collections-grid clearfix">
            @if(!empty($arr_categories))
            @foreach($arr_categories as $category)
            <div class="collection-item columns large-4">
                <a href="{{$url}}/collections/{{$category['short_name']}}">
                    <div class="image-wrapper">
                        <div class="hover"></div>
                        <img src="{{$category['image']}}" alt="{{$category['name']}}">
                    </div>
                    <!-- .coll-image-wrap -->
                    <div class="caption" style="display: none;">
                        <div class="bg"></div>
                        <div class="inner">
                            <div class="title">
                                {{$category['name']}}
                            </div>
                            <p class="product-count">{{$category['num_of_product']}} product{{$category['num_of_product'] > 1 ? 's' : ''}}</p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
@stop
