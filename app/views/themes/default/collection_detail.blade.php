@extends($theme.'.common.default')
@section('body')
<body class="page-the-floral-paradox-tee template-product" data-twttr-rendered="true">
    @stop
    @section('content')
    <section class="main-content">
        <a name="detail"></a>
        <header>
            <div class="row show-for-medium-up">
                <div class="columns">
                    <ul class="breadcrumbs colored-links">
                        <li><a href="{{$url}}/">Home</a></li>
                        @if(!isset($from_product))
                        <li><a href="{{$url}}/collections">Shop</a></li>
                        <li><a href="{{$url}}/collections/{{$collection_name}}">{{$collection}}</a></li>
                        @else
                        <li><a href="{{$url}}/collections/">{{$collection}}</a></li>
                        @endif
                        <li>{{$product->name}}</li>
                    </ul>
                </div>
            </div>
            <div class="previous-next row">
                <div class="previous columns large-3 small-6">
                    @if(isset($prev_product->id))
                    <span class="left">
                        <a href="{{$url}}/collections/{{$collection_name}}/products/{{$prev_product->short_name}}-{{$prev_product->id}}#detail" title="{{$prev_product->name}}">
                            <span aria-hidden="true" class="glyph arrow-left"></span> Previous Item
                        </a>
                    </span>
                    @endif
                    <div class="border-decoration show-for-large-up"></div>
                </div>
                <div class="next columns large-3 large-offset-6 small-6">
                    @if(isset($next_product->id))
                    <span class="right">
                        <a href="{{$url}}/collections/{{$collection_name}}/products/{{$next_product->short_name}}-{{$next_product->id}}#detail" title="{{$next_product->name}}">
                            Next Item <span aria-hidden="true" class="glyph arrow-right"></span>
                        </a>
                    </span>
                    @endif
                    <div class="border-decoration show-for-large-up"></div>
                </div>
            </div>
        </header>
        <article class="row" itemscope="" itemtype="">
            <meta itemprop="url" content="#">
            <meta itemprop="image" content="{{$url}}/assets/themes/default/images/zoom_328x480.jpg">
            <div class="left-column right columns large-5">
                <form class="custom cart-form" action="{{$url}}/cart/add" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{$product->id}}" />
                    <input type="hidden" name="url" value="{{$url}}/collections/{{$collection_name}}/products/{{$product->short_name}}-{{$product->id}}" />
                    <h1 class="page-title" itemprop="name">{{$product->name}}</h1>
                    <div class="prices" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                        <meta itemprop="priceCurrency" content="USD">
                        <link itemprop="availability" href="http://schema.org/InStock">
                        <p>
                            <span class="actual-price" itemprop="price">$ {{number_format($product->sell_price,2)}}</span>&nbsp;
                            @if($product->bigger_price > 0)
                            <span class="compare-price">Was $ {{number_format($product->bigger_price,2)}}</span>
                            @endif
                        </p>
                    </div>
                    <div class="quanity-cart-row clearfix">
                        <div class="variants ">
                            @if(!empty($option_price))
                            <div class="selector-wrapper">
                                <label for="variant-listbox-option-0">Size</label>
                                <select class="single-option-selector size_product" name="size" data-option="option1" id="variant-listbox-option-0">
                                    @foreach($option_price as $option)
                                    <option value="{{$option['id']}}" data-value={{json_encode($option)}}>{{$option['size']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            @if(!empty($product->color))
                            <div class="selector-wrapper">
                                <label for="variant-listbox-option-1">Color</label>
                                <select class="single-option-selector color_product" name="color" data-option="option2" id="variant-listbox-option-1">
                                    @foreach($product->color as $color)
                                    <option value="{{$color['id']}}">{{$color['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                        </div>
                        <div class="quantity">
                            <label for="quantity">Quantity</label>
                            <input id="quantity" type="number" name="quantity" value="1">
                        </div>
                        <div class="add-to-cart">
                            @if($product->is_sold_out)
                            <span class="sold-out long disabled" style="display: block;">This variant is sold out please select another</span>
                            @else
                            <input type="submit" name="add" id="add" value="Add to cart" class="purchase button">
                            @if($product_template)
                            <input type="button" style="background: rgb(180, 42, 42);" name="gotodesign" id="gotodesign" onclick="go_to_design()" value="Go to Design" class="purchase button">
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="backorder">
                        <p></p>
                    </div>
                </form>
                <div itemprop="description" class="description show-for-medium-down colored-links rte-content hide-for-ie9-down">
                    {{$product->description}}
                </div>
            </div>
            <!-- .left-column -->
            <div class="middle-column photos left columns large-6 show-for-medium-up show-for-ie9-down">
                <div class="container clearfix clickable">
                    <img id="main-image" src="{{$url}}/assets/upload/{{$product->main_image}}" class="zoomImg" />
                </div>
                <!-- #product-photo-container -->
                <div class="thumbs show-for-medium-up show-for-ie9-down">
                    <ul class="clearfix row">
                        <li class="thumb other-image active">
                            <img src="{{$url}}/assets/upload/{{$product->main_image}}" alt="{{$product->name}}">
                        </li>
                        @if(!empty($product->other_image))
                        @foreach($product->other_image as $other_image)
                        <li class="thumb other-image">
                            <img src="{{$url}}/assets/upload/{{$other_image}}" alt="{{$product->name}}">
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <!-- #product-photos -->
            <div class="right-column description columns large-5">
                <div itemprop="description colored-links rte-content">
                    {{$product->description}}
                </div>
            </div>
        </article>
        <section class="row social-share">
            <div class="columns">
                <h2 class="title">Share this</h2>
                @if(!is_null($social_network) && $social_network->count())
                @foreach($social_network as $network)
                <?php
                    $product_url = "$url/collections/$collection_name/products/{$product->short_name}-{$product->id}";
                    echo $share_widget = htmlspecialchars_decode(str_replace('[PAGE_URL]',$product_url,$network->share_widget));
                ?>
                @endforeach
                @endif
                <script type="text/javascript">
                    (function() {
                      var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                      po.src = 'https://apis.google.com/js/plusone.js';
                      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                    })();
                </script>
            </div>
        </section>
        <section class="similar-products row">
            @if(isset($similar_products) && !is_null($similar_products) && $similar_products->count())
            <h2 class="title">Similar Products</h2>
            <div class="product-grid">
                <div class="clearfix"></div>
                @foreach($similar_products as $pro)
                    <div class="product-item columns large-3">
                        <div class="image-wrapper">
                            <a href="{{$url}}/collections/{{$collection_name}}/products/{{$pro->short_name}}-{{$pro->id}}">
                            <img src="{{$url}}/assets/upload/{{$pro->main_image}}" alt="{{$pro->name}}">
                            </a>
                        </div>
                        <!-- .coll-image-wrap -->
                        <div class="caption">
                            <p class="title">
                                <a href="{{$url}}/collections/{{$collection_name}}/products/{{$pro->short_name}}-{{$pro->id}}">
                                {{$pro->name}}
                                </a>
                            </p>
                            <p class="price">
                                <?php
                                    $price = ProductController::getMainPriceByID($pro->id);
                                    $pro->sell_price = $price['sell_price'];
                                ?>
                                $ {{number_format($pro->sell_price,2)}}
                            </p>
                        </div>
                        <!-- .coll-prod-caption -->
                    </div>
                @endforeach
            </div>
            @endif
        </section>
<script>
    $(function(){
        $("#quantity").keydown(function(e){
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $("#quantity").focus(function(){
            $(this).attr("data-old-value", $(this).val() );
        });
        $("#quantity").change(function(){
            if( $(this).val() <= 0){
                $(this).val( $(this).attr("data-old-value") );
                event.preventDefault();
            }
        });
        $(".other-image > img").click(function(){
            var src = $(this).attr("src");
            $("#main-image").fadeOut(100,function(){
                $(".active",".show-for-medium-up").removeClass("active");
                $(this).parent().addClass("active");
                $(".zoomImg").attr("src",src).fadeIn();
            });
        })
        $(".size_product").change(function(){
            var value = $("option:selected",this).attr("data-value");
            value = $.parseJSON(value);
            $(".actual-price").html(number_format(value.sell_price,2));
            if(value.bigger_price)
                $(".compare-price").html(number_format(value.bigger_price,2));

        })
    })
    @if($product_template)
    function go_to_design(){
        var size = $(".size_product").val() != undefined ? $(".size_product").val() : 0;
        var quantity = $("#quantity").val() != undefined ? $("#quantity").val() : 1;
        var color = $(".color_product").val() != undefined ? $(".color_product").val() : 0;
        var form = '<form id="goToDesign" action="{{$url}}/design-online/template?product_id={{$product->id}}&template_id={{$product_template->template_id}}" method="POST">';
            form += '<input type="hidden" name="size" value="'+size+'" />';
            form += '<input type="hidden" name="quantity" value="'+quantity+'" />';
            form += '<input type="hidden" name="color" value="'+color+'" />';
            form += '<input type="hidden" name="url" value="'+window.location.pathname+'" />';
        form += '</form>';
        $("body").append(form);
        $("#goToDesign").submit();
    }
    @endif
</script>
    </section>
    @stop