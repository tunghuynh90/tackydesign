@extends($theme.'.common.default')
@section('body')
<body class="page-wristwear template-collection">
    @stop
    @section('content')
    <section class="main-content">
        <header class="row">
            <div class="left columns large-6">
                <ul class="breadcrumbs colored-links">
                    <li><a href="{{$url}}">Home</a></li>
                    <li><a href="{{$url}}/collections">Shop</a></li>
                    <li> {{$collection}} </li>
                    @if($more_collection_name != '')
                    <li> {{$more_collection_name}} </li>
                    @endif
                </ul>
            </div>
            @if(!empty($arr_tag))
            <div class="right columns large-6">
                <div class="tag-filter">
                    <span class="title">Sort by Tags</span>
                    <form class="custom">
                        <select class="small tags-listbox" name="coll-filter" size="1" data-id="1401090366533-ixstR">
                            <option value="" <?php if($more_collection_name == '') echo 'selected' ?>>All</option>
                            @foreach($arr_tag as $tag)
                            <option value="{{$tag->short_name}}" <?php if($more_collection_name == $tag->name) echo 'selected'  ?>>{{$tag->name}}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
            @endif
        </header>
        <div class="row">
            <div class="columns">
                <div class="divider"></div>
            </div>
        </div>
        <div class="row">
            @if($collection_image!='')
            <div style="text-align:center">
                <img src="{{$url}}/assets/upload/{{$collection_image}}" alt="{{$collection}}">
            </div>
            @endif
            <section class="row">
                <div class="product-grid clearfix">
                    <div class="clearfix"></div>
                    @if(!empty($arr_products['product']))
                    @foreach($arr_products['product'] as $key => $product)
                    <div class="product-item columns large-3">
                        <div class="image-wrapper">
                            @if($product['is_sold_out'])
                            <div class="circle sold-out">Sold<br>Out</div>
                            @endif
                            @if($product['is_sale'])
                            <a class="circle sale" href="{{$url}}/collections/{{$collection_name}}/products/{{$product['short_name']}}-{{$product['id']}}"></a>
                            @endif
                            <a href="{{$url}}/collections/{{$collection_name}}/products/{{$product['short_name']}}-{{$product['id']}}">
                            <img src="{{$product['main_image']}}" alt="{{$product['name']}}">
                            </a>
                        </div>
                        <!-- .coll-image-wrap -->
                        <div class="caption">
                            <p class="title">
                                <a href="{{$url}}/collections/{{$collection_name}}/products/{{$product['short_name']}}-{{$product['id']}}">
                                {{$product['name']}}
                                </a>
                            </p>
                            <p class="price">
                                $ {{number_format($product['sell_price'],2)}}
                                @if($product['bigger_price'])
                                <em class="marked-down-from">Was $ {{number_format($product['bigger_price'],2)}}</em>
                                @endif
                            </p>
                        </div>
                        <!-- .coll-prod-caption -->
                    </div>
                    @if($key!= 0 && ($key+1)%5==0)
                    <div class="clearfix"></div>
                    @endif
                    @endforeach
                    @endif
                </div>
            </section>
        </div>
        <div class="row">
            <div class="columns">
                <div class="divider bottom-margin"></div>
            </div>
        </div>
        <footer class="row">
            <div class="left columns large-6">
                <ul class="breadcrumbs colored-links">
                    <li><a href="{{$url}}">Home</a></li>
                    <li><a href="{{$url}}/collections">Shop</a></li>
                    <li> {{$collection}} </li>
                </ul>
            </div>
            @if($arr_products['total_pages'] > 1)
            <div class="right columns large-6">
                <ul class="pagination">
                    <?php
                        if($page - 3 >0)
                            $min = $page - 3;
                        else
                            $min = 1;
                        if($page + 3 <= $arr_products['total_pages'])
                            $max = $page + 3;
                        else
                            $max = $arr_products['total_pages'];
                    ?>
                    @if($page - 1 >= 1)
                    <li class="arrow left"><a href="{{$url}}/collections/{{$collection_name}}/page/{{($page-1)}}">Previous <span class="glyph arrow-right" aria-hidden="true"></span></a></li>
                    @endif
                    @for($i = $min; $i < $max; $i++)
                    @if($i == $page)
                    <li class="current"><a href="">{{$i}}</a></li>
                    @else
                    <li><a href="{{$url}}/collections/{{$collection_name}}/page/{{$i}}">{{$i}}</a></li>
                    @endif
                    @endfor
                    @if($page + 1 <= $arr_products['total_pages'])
                    <li class="arrow right"><a href="{{$url}}/collections/{{$collection_name}}/page/{{($page+1)}}">Next <span class="glyph arrow-right" aria-hidden="true"></span></a></li>
                    @endif
                </ul>
            </div>
            @endif
        </footer>
        <script>
            /* Product Tag Filters - Good for any number of filters on any type of collection pages */
            /* Brought to you by Caroline Schnapp */
            var collFilters = jQuery('.tags-listbox');
            collFilters.change(function() {
              var newTags = [];
              collFilters.each(function() {
                if (jQuery(this).val()) {
                  newTags.push(jQuery(this).val());
                }
              });
              if (newTags.length) {
                var query = newTags.join('+');
                window.location.href = jQuery('<a title="Show products matching tag tag" href="/collections/{{$collection_name}}/tag">tag</a>').attr('href').replace('tag', query);
              }
              else {

                window.location.href = '/collections/{{$collection_name}}';

              }
            });
        </script>
    </section>
    @stop