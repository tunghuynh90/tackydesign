<!DOCTYPE html>
<html class="js no-touch svg inlinesvg svgclippaths no-ie8compat wf-lato-n4-active wf-active">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{$url}}/assets/images/logos/{{$config['favicon']}}" rel="shortcut icon" type="image/x-icon">
    <title>{{ !isset($title) ? "{$config['title_site']} | Home" : "$title | {$config['title_site']}"}}</title>
    <meta name="description" content="{{$config['meta_description']}}">
    <meta name="keywords" content="{{$config['meta_keywords']}}">
    <!--============= MAIN CSS ================-->
    <link href='http://fonts.googleapis.com/css?family=Comfortaa:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{$url}}/assets/themes/default/css/style.css" type="text/css" rel="stylesheet">
    <link href="{{$url}}/assets/themes/default/css/style2.css" type="text/css" rel="stylesheet">
    <!--============= MAIN JS ================-->
    <script src="{{$url}}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{$url}}/assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="{{$url}}/assets/js/storefront360_carousel.js"></script>
    <script type="text/javascript" src="{{$url}}/assets/js/shopify_stats.js"></script>
    <script id="twitter-wjs" src="{{$url}}/assets/js/widgets.js"></script>
    <script src="{{$url}}/assets/js/custom.modernizr.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/api.jquery.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/option_selection.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/jquery.zoom.js" type="text/javascript"></script>
    <script src="{{$url}}/assets/js/columniz.js" type="text/javascript"></script>
    <script>
        home_slider_auto_enabled = true,
        home_slider_rotate_frequency = 5000,
        product_zoom_enabled = false,
        product_modal_enabled = true;
    </script>
    <script src="{{$url}}/assets/js/storefront360_lib.js" type="text/javascript"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-52739618-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

@yield('body','<body class="page-home template-index" data-twttr-rendered="true">')


    <!--============= MAIN HEADER ================-->
    @include($theme.'.common.header')


    <!--============= MAIN CONTENTS ================-->
     @yield('content')


    <!--============= MAIN FOOTER ================-->
    @include($theme.'.common.footer')

</body>


<script src="{{$url}}/assets/js/plugins.js" type="text/javascript"></script>
<script src="{{$url}}/assets/js/shop.js" type="text/javascript"></script>
</html>