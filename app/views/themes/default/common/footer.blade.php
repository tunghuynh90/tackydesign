<footer class="main-footer">
	<div class="row bottom">

    	<!--===== COLUMN 1 =====-->
        <div class="column-1 large-3 columns">
            <h2 class="title">Information</h2>
            <div class="content">
                <ul class="footer-nav plain-list" role="navigation">
                    @if(!empty($static_homes))
                    @foreach($static_homes as $key => $static_home)
                        <li><a href="{{$url}}/pages/{{$static_home['short_name']}}" title="SHIPPING INFO" style="text-transform:uppercase">{{$static_home['description']}}</a></li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>

      	<!--===== COLUMN 2 =====-->
        <div class="column-2 large-3 columns">
            <h2 class="title">Customer Service</h2>
            <div class="content">
                <ul class="footer-nav plain-list" role="navigation">
                    @if(!empty($customer_services))
                    @foreach($customer_services as $key => $customer_service)
                     <?php if($customer_service['short_name'] == 'phone'){?>
                     <li></br></li>
                        <li>{{$customer_service['description']}}</li>
                    <?php }else{?>
                        <?php
                            $tmp = $customer_service['short_name'] == 'phone';
                            unset($tmp);
                        ?>
                        <li><a href="{{$url}}/pages/{{$customer_service['short_name']}}" style="text-transform:uppercase" title="">{{$customer_service['description']}}</a>
                    <?php } ?>
                    @endforeach
                    @endif
                </ul>
              <!--   <p class="text">TEL (732) 851 7223   /  INFO@tackydesign.COM</p> -->
            </div>
        </div>

		<!--===== COLUMN 3 =====-->
        <div class="column-3 large-3 columns">
            <h2 class="title">NEWSLETTER</h2>
            <div class="content" id="mailing-list-module">
                <p>Sign up to get the latest news...</p>
                <form accept-charset="UTF-8" action="javascript:void(0)" class="contact-form" method="post">
                    <p class="success feedback"></p>
                    <p class="error feedback"></p>
                    <input type="hidden" id="newsletter-first-name" name="contact[firstname]" value="Subscriber">
                    <input type="hidden" id="newsletter-last-name" name="contact[lastname]" value="Newsletter">

                    <div class="row collapse">
                        <div class="small-10 large-10 columns" style="padding-right: 10px;">
                        	<input type="email" placeholder="Your Email" name="contact[email]">
                        </div>
                        <div class="small-2 large-2 columns">
                        	<input type="submit" class="button prefix" value="OK" name="subscribe" id="email-submit">
                        </div>
                    </div>
                </form>
                <script type="text/javascript">
                    $("form.contact-form").submit(function() {
                        $.ajax({
                            url: "{{$url}}/newsletter",
                            type: "POST",
                            data: $("input",this).serialize(),
                            success: function(result){
                                $("input[type=email]","form.contact-form").val("");
                                if(result == "ok")
                                    $(".success","form.contact-form").html("Thank you for signing up!");
                            }
                        });
                    });
                </script>

            </div>
      	</div>

        <!--===== COLUMN 3 =====-->
     <!--    <div class="column-4 large-3 columns">
            <a title="footer-banner" href="{{$url}}/collections/sale"><img src="{{$url}}/assets/themes/default/images/footer-custom.png" alt="footer banner"></a>
        </div> -->
        @if(!empty($flash_sales))
        @foreach($flash_sales as $key => $flash_sale)
        <div class="column-4 large-3 columns">
            <a title="footer-banner" href="{{$url}}/collections/sale"><img style="width:320px;height:285px" src="{{$url}}/assets/upload/{{$flash_sale['image']}}" alt="footer banner"></a>
        </div>
        @endforeach
        @endif

	</div>
    <div class="row">
        <div class="columns"></div>
    </div>
</footer>