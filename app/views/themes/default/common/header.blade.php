<?php

	$menu_top_left = array(
		array('title'=>'My Account','link'=>'/user'),
		array('title'=>'Shopping Cart','link'=>'/cart'),
		array('title'=>'Checkout','link'=>'/checkout'),
	);

	$menu_top_right = array(
		array('title'=>'Search','icon'=>'search','link'=>'#','class'=>'searchbar-open'),
		array('title'=>'Account','icon'=>'account','link'=>'#','class'=>'account-open'),
	);

?>

<header class="main-header">
    <div class="bg"></div>

    <!-- TOP -->
    <div class="row top">
		<div class="search-account large-12 columns">
        	<!-- TOP LEFT -->
            <div class="inline-list font-nav" role="navigation" style="float:left;margin-left: 1px">
                @if(!is_null($social_network) && $social_network->count())
                <div class="social-follow">
                    @foreach($social_network as $network)

                    <a title="{{$network->short_name}}" aria-hidden="true" target="_blank" href="{{$network->link}}"><img style="width: 38px;" src="{{$url}}/assets/images/social_icon/{{$network->icon}}" /></a>
                    @endforeach
                </div>
                @endif

            </div>
            <!-- TOP RIGHT -->
            <div class="menu">
                <?php foreach($menu_top_right as $key=>$value){?>
                    <a class="<?php echo $value['class'];?>" href="<?php echo $value['link'];?>">
                        <?php echo $value['title'];?>
                        <span aria-hidden="true" class="glyph <?php echo $value['icon'];?>"></span>
                    </a>
                <?php }?>
            </div>
            <div class="searchbar-container">
                <form action="javascript:void(0)"  method="get" role="search">
                    <input name="query" id="query" type="text" placeholder="Search store..." class="search-box hint text">
                    <button type="submit" class="glyph search"></button>
                </form>
                <script type="text/javascript">
                    $(".searchbar-container > form").submit(function(){
                        window.location = "{{$url}}/search/1/"+ toSlugger($("#query",this).val());
                    });

                    function toSlugger(value){
                        value = $.trim(value);
                        value = value.toLowerCase();
                        value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
                        value= value.replace('/','');
                        return value;
                    }

                </script>
                <a href="{{$url}}#" aria-hidden="true" class="searchbar-close glyph cross"></a>
            </div>
            <div class="account-container">
                <?php $user = Confide::user(); ?>
                @if($user)
                <a href="{{$url}}/user">View Account (Logged in as <?php echo $user->firstname!='' ? $user->firstname : $user->email; ?>)</a>
                <span class="separator">|</span>
                <a href="{{$url}}/user/logout" id="customer_logout_link">Logout</a>
                <a href="#" aria-hidden="true" class="account-close glyph cross"></a>
                @else
                <a href="{{$url}}/user/login" id="customer_login_link">Log in</a> or <a href="{{$url}}/user/create" id="customer_register_link">Sign Up</a>
                <a href="{{$url}}#" aria-hidden="true" class="account-close glyph cross"></a>
                @endif
            </div>
		</div>

		<!-- Cart -->
        <div class="columns cart-container">
             <div class="cart">
                <a class="cart-link" href="{{$url}}/cart">
                    Cart
                    <span class="number">
                        @if($cart_quantity)
                        ({{$cart_quantity}})
                        @endif
                    </span>
                    <span aria-hidden="true" class="glyph cart"></span>
                </a>
                <div class="recently-added">
                    <table width="100%">
                      <thead>
                        <tr>
                          <td colspan="3">Recently Added</td>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                            <td class="items-count">
                                <a href="{{$url}}/cart"><span class="number">0</span> Items</a>
                            </td>
                            <td colspan="2" class="text-right">
                                <strong>TOTAL <span class="total-price" style="margin-left:5px;">$ 0.00</span></strong>
                            </td>
                        </tr>
                      </tfoot>
                    </table>
                    <div class="row">
                      <div class="checkout columns">
                        <a class="button" href="{{$url}}/cart">Go to Checkout</a>
                      </div><!-- #cart-meta -->
                    </div>
                    <div class="error">
                      <p>The item you just added is unavailable. Please select another product or variant.</p>
                    </div>
                </div>
            </div>
		</div>
        <!-- #end cart -->

        <!-- Logo -->
        <div class="large-12 columns">
            <h1 class="title clearfix" role="banner">
                <a href="{{$url}}" role="banner" title="{{$config['title_site']}}">
                    <img src="{{$url}}/assets/images/logos/{{$config['logo_image']}}" data-retina="{{$url}}/assets/images/logos/{{$config['logo_retina_image']}}" alt="{{$config['title_site']}}">
                </a>
            </h1>
        </div>

	</div>
 	<!-- #end TOP -->



    @if(!isset($no_menu))
    <div class="bottom-row">
        <div class="row">
            <div class="columns menu-container">
                <!--============= MAIN MENU ================-->
                 @include($theme.'.common.menu')
            </div>
            <div class="mobile-tools">
                <a class="glyph menu" href=""></a>
                <a href="{{$url}}/search" class="glyph search"></a>
                <a href="{{$url}}/account" class="glyph account"></a>
                <a href="{{$url}}/cart" class="glyph cart"></a>
            </div>
        </div>
    </div>

    <div class="main-menu-dropdown-panel">
    	<div class="row"></div>
    </div>

    <!--============= MAIN MENU ================-->
    @include($theme.'.common.menu_mobile')
    <div class="row">
    	<div class="header-divider"></div>
    </div>
    @endif
</header>