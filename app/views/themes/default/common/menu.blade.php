
<div class="main-menu">
    <nav role="navigation" class="widescreen clearfix" style="position:relative; float:right; left:-50%;">
        <ul class="inline-list font-nav" style="position:relative; left:50%;">
            <?php foreach($menu as $key=>$value){
                    $dropdown = '';
                    if(isset($value['submenu'])){
                        $dropdown = 'dropdown';
                    }
            ?>
            <li class="nav-item <?php echo $dropdown;?>">
                <a class="nav-item-link" href="<?php echo $value['link'];?>">
                <?php echo $value['title'];?>

                <?php if($dropdown!=''){ // icon arrow down?>
                    <span aria-hidden="true" class="glyph arrow-down"></span>
                <?php }?>
                </a>

				<?php if(isset($value['submenu']) && count($value['submenu'])>0){ // submenu?>
                    <ul class="sub-nav catalog">
                        <?php foreach($value['submenu'] as $lable=>$arrsub){?>
                        <li class="sub-nav-item row">
                            <div class="columns large-<?php if($key=='collections') echo '4';else echo '3';?>">
                                <?php if(!is_numeric($lable)){ $str = ' ';?>
                                    <h3 class="title"><?php echo $lable;?></h3>
                                <?php }else $str = ' sub-nav-item';?>
                                <ul>
                                    <?php foreach($arrsub as $keysub=>$valuesub){?>
                                        <li<?php echo $str;?>>
                                            <a href="<?php echo $valuesub['link'];?>" title="">
                                                <?php echo $valuesub['title'];?>
                                            </a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </li>
                        <?php }?>
                    </ul>
                <?php } // end submenu?>

            </li>

            <?php }?>
        </ul>
    </nav>
</div>



