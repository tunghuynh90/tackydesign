<div class="mobile-menu">
      <nav role="navigation">
  			<ul class="font-nav">
            <?php foreach($menu as $key=>$value){
                    $dropdown = '';
					$class = 'nav-item-link';
                    if(isset($value['submenu'])){
                        $dropdown = 'dropdown';
						$class = 'dropdown-link';
                    }
            ?>
            <li class="nav-item <?php echo $dropdown;?>">
                <a class="<?php echo $class;?>" href="<?php echo $value['link'];?>">
                <?php echo $value['title'];?>

                <?php if($dropdown!=''){ // icon arrow down?>
                    <span aria-hidden="true" class="glyph plus"></span>
                    <span aria-hidden="true" class="glyph minus"></span>
                <?php }?>
                </a>

				<?php if(isset($value['submenu']) && count($value['submenu'])>0){ // submenu?>
                    <ul class="sub-nav <?php if(!isset($value['submenu'][0])) echo 'catalog';?>">
                    	<li class="bg"></li>
						<?php if(!isset($value['submenu'][0])){ // type 3 level
							foreach($value['submenu'] as $lable=>$arrsub){?>
                                <li class="sub-nav-item">
                                    <a class="dropdown-link" href="">
                                        <?php echo $lable;?>
                                            <span aria-hidden="true" class="glyph plus"></span>
                                            <span aria-hidden="true" class="glyph minus"></span>
                                    </a>
                                    <ul class="sub-nav">
                                        <li class="bg"></li>
                                        <?php foreach($arrsub as $keysub=>$valuesub){?>
                                            <li>
                                                <a href="<?php echo $valuesub['link'];?>" title="">
                                                    <?php echo $valuesub['title'];?>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                </li>
                        <?php } }else{ // type 2 level
								foreach($value['submenu'][0] as $lable=>$arrsub){?>
                        			<li class="sub-nav-item">
                                        <a href=" <?php echo $arrsub['link'];?>">
                                            <?php echo $arrsub['title'];?>
                                        </a>
                                    </li>

						<?php } }?>

                    </ul>
                <?php } // end submenu?>

            </li>

            <?php }?>
        </ul>
    </nav>
</div>



