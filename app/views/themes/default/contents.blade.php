@extends($theme.'.common.default')
@section('content')
<section class="main-content">

    <div id="slider1_container" style="position: relative; margin: 0px auto; top: 0px; left: 0px; width: 1349px; height: 726.3846153846154px;">
        <!-- Slides Container -->
        @if(!empty($slides))
        <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 1349px; height: 726.3846153846154px;">
            @foreach($slides as $slide)
            <div><img u="image" src="{{$slide}}" /></div>
            @endforeach
    	</div>
        @endif
		<style>
            .jssord21l, .jssord21r, .jssord21ldn, .jssord21rdn{
            	position: absolute;
            	cursor: pointer;
            	display: block;
            background: url({{$url}}/assets/themes/default/images/d21.png) center center no-repeat;
                overflow: hidden;
            }
            .jssord21l { background-position: -3px -33px; }
            .jssord21r { background-position: -63px -33px; }
            .jssord21l:hover { background-position: -123px -33px; }
            .jssord21r:hover { background-position: -183px -33px; }
            .jssord21ldn { background-position: -243px -33px; }
            .jssord21rdn { background-position: -303px -33px; }
        </style>
        <span u="arrowleft" class="jssord21l" style="width: 55px; height: 55px; top: 322.5px; left: 8px;"></span>
        <span u="arrowright" class="jssord21r" style="width: 55px; height: 55px; top: 322.5px; right: 8px;"></span>
</div>

<script src="{{$url}}/assets/js/jssor.core.js" type="text/javascript"></script>
<script src="{{$url}}/assets/js/jssor.utils.js" type="text/javascript"></script>
<script src="{{$url}}/assets/js/jssor.slider.min.js" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function ($) {
            var options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actuall size, default value is 0
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 3,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuart,
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $DirectionNavigatorOptions: {                       //[Optional] Options to specify and enable direction navigator or not
                    $Class: $JssorDirectionNavigator$,              //[Requried] Class to create direction navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
        });
    </script>

    <!-- #product-slider in shop.js -->
    <div class="product-slider dark">
        @if(!empty($new_products))
        <div class="row header">
            <div class="columns">
                <h2 class="font-headings clearfix">
                    <a href="{{$url}}/collections/new-releases" title="">
                        NEW / VISIONS &amp; VICTORIES COLLECTION
                    </a>
                </h2>
            </div>
        </div>
        <div class="row content">
            <div class="product-grid clearfix">
                @foreach($new_products as $product)
                <div class="product-item ">
                	<div class="image-wrapper">
                    	<a href="{{$url}}/collections/new-releases/products/{{$product['short_name']}}-{{$product['id']}}">
                        	<img class="" src="{{$product['main_image']}}" alt="{{$product['name']}}">
                        </a>
                    </div>
                    <div class="caption">
                      <p class="title">
                        <a href="{{$url}}/collections/new-releases/products/{{$product['short_name']}}-{{$product['id']}}">
                            {{$product['name']}}
                        </a>
                      </p>
                      <p class="price">$ {{number_format($product['sell_price'],2)}}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
    <!-- #product-slider -->






<div class="widgets">

  <!-- 2 picture -->
  <div class="small-promos">
    <div class="row">
<!--       <div class="columns large-6">
		<div class="image-text-widget">
	    <a href="{{$url}}/collections/new-releases">
	  		<img src="{{$url}}/assets/themes/default/images/home-widget-image-text-1.png" alt="NEW ARRIVALS">
            <div class="caption">
                <div class="bg"></div>
                <div class="inner">
                  <h1>NEW ARRIVALS</h1>
                  <h2>Shop New</h2>
              	</div>
	    	</div>
	    </a>
	    </div>
      </div> -->
      @if(!is_null($widget_homes))
      @foreach($widget_homes as $key => $widget_home)
          <div class="columns large-6">
    		<div class="image-text-widget">
    	    <a href="{{$url}}{{$widget_home['link']}}">
    	  	<img style="width:726px; height: 469px" src="{{$url}}/assets/upload/{{$widget_home['image']}}" alt="{{$widget_home['name']}}">
    	    <div class="caption">
    	    	<div class="bg"></div>
    	      <div class="inner">
    	          <h1>{{$widget_home['name']}}</h1>
                  <h2>{{$widget_home['sub_title']}}</h2>
    	      </div>
    	    </div>
    	    </a>
    		</div>
          </div>
        @endforeach
        @endif
    </div>
  </div>



   <!-- Twitter, Instagram, From The Blog   -->
  <div class="social-feeds">
        <div class="row">
            @if(!is_null($home_social_network) && $home_social_network->count())
            @foreach($home_social_network as $network)
                {{htmlspecialchars_decode($network->view_widget)}}
            @endforeach
            @endif

            @if(!is_null($two_articles) && $two_articles->count() && 1>3)
                <div class="columns large-4">
                    <div class="blog-widget">
                        <h3 class="title"><a href="#">From The Blog</a></h3>
                            <ul class="colored-links">
                                @foreach($two_articles as $key => $article)
                                <li>
                                    <div class="item-title">{{$article['title']}}</div>
                                    <div class="date">{{date('M d Y',strtotime($article['posted_date']))}}</div>
                                    <div class="content" style="height:80px; overflow-y:hidden;">{{html_entity_decode($article['content'])}}
                                    </div>
                                    <div class="read-more">
                                        <a href="{{$url}}/blog-detail/{{$article['short_name']}}">
                                            Read more <span aria-hidden="true" class="glyph arrow-right"></span>
                                        </a>
                                    </div>
                                </li>
                                @endforeach
                                <li></li>
                            </ul>
                    </div>
                </div>
              @endif

        </div>
    </div>
    <!-- .social-feeds -->


</div>
<!-- .widgets -->




</section>
@stop