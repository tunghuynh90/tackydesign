<!DOCTYPE html>
<html>
@include($theme.'.design.headdesign')
<body>
  <div id="user-background">
    <div id="wrapper">
        <h1 class="title clearfix" role="banner">
            <a href="#" role="banner" title="{{$config['title_site']}}">
                <img src="{{$url}}/assets/images/logos/{{$config['logo_image']}}" data-retina="{{$url}}/assets/themes/default/images/logo-retina.png" alt="{{$config['title_site']}}">
            </a>
        </h1>
        @yield('content')
    </div> <!-- end wrapper-->
  </div><!-- user-background-->
</body>
</html>