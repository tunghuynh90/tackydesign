@extends($theme.'.design.design_layout')
@section('content')
<script type="text/javascript" >
    var products = {{json_encode($template_product)}};
    var current_product = {{json_encode($current_product)}};
    var user = {{json_encode($current_user)}};
    var config = {};
    config.framework = 'Laravel';
    config.designURL = '{{$design_url}}/';
    config.addImageURL = '{{$url}}/{{$add_image}}';
    config.fonts = {{json_encode($font)}} ;
    config.homeUrl = '{{$url}}' ;
    config.share_link = '{{$url}}/design-online/[@DESIGN_ID]/[@PRODUCT_ID]' ;
    config.nextPageUrl =  '{{$url}}/{{$next_url}}';
    config.popupLoginUrl = '{{$login_url}}' ;
    config.maxSvgColorSupport = 300;
    config.siteKey = '{{$site_key}}';
    config.urlProfileEQ = {"id":"0","description":"{{$url}}","show_text_panel":"0","show_ruler":"0"} ;
</script>
<script type="text/javascript" src="{{$design_url}}lib/js/jquery.1.10.2.min.js"></script>
<script type="text/javascript" src="{{$design_url}}lib/js/jquery.xdomainrequest.min.js"></script>
<script type="text/javascript" src="{{$design_url}}lib/design/js/helpers.js"></script>
<script type="text/javascript" src="{{$design_url}}lib/design/js/jquery-ui.1.9.2.min.js"></script>
<link href="{{$url}}/assets/css/design_tool.css" rel="stylesheet" type="text/css" />
<!-- <link href="{{$design_url}}lib/design/css/theme_design.css" rel="stylesheet" type="text/css" /> -->

<div id="main_loading" style="font-size: 15px ; font-weight: bold ; position:fixed; top: 0 ; left: 0 ; width: 100% ; height: 100% ; background-color: #fff ; z-index: 100000 ; padding: 25px 30px;">Loading...</div>
<div id="preview_design"></div>
<div id="gray_box" style="display:none" ></div>
<div id="notice_bar" style="display:none"><div id="notice_box"></div></div>

<!-- Code Minh -->
<div id="workspace" class="one des_ol_1">
    <div class="toolbar ztoolbar firstTool" id="nav_toolbar">
        <div class="top-nav-left" >
            <div id="theme_options" class="toolbar-button type5 has-menu has-menu menu-opener" style="line-height:normal ; display: none">
                <div class="button-icon button-arrow" style="vertical-align:text-bottom"></div>
                <div class="button-menu with-divider" id="theme_menu"></div>
            </div>

            <div id="info_ph">
                <div id="info_design_title"></div>
                <div id="info_product_title"><a href="" onClick="history.back();" >{{$current_product['name']}}</a></div>
            </div>
        </div>

        <div class="top-nav-right" >
            <div id="btn_rotate_layout" data-rotate="0" class="toolbar-button type5 preview_icon_1 active" style="width:120px">
                <img id="icon" src="{{$url}}/assets/themes/default/images/document.png" style="margin-left: -30px; float:left; width: 32px; margin-top: 0px;"/>
                <div class="button-text">Vertical</div>
            </div>
            @if($permission['allow_preview'])
                <div id="btn_preview" class="toolbar-button type5 preview_icon_1 active" style="width:123px">
                    <img src="{{$url}}/assets/themes/default/images/preview.png" style="margin-left: -20px; float:left; width: 35px; margin-top:1px;"/>
                    <div class="button-text">Preview</div>
                </div>
            @endif
            @if($permission['allow_share'])
            <div id="btn_share_email" class="toolbar-button type5 has-menu menu-opener share_icon_1">
                    <img src="{{$url}}/assets/themes/default/images/share.png" style="margin-left: -20px; float:left; width: 35px; margin-top: 2px;"/>
                    <div class="button-text">Share</div>
            </div>
            @endif

            <div id="btn_save" class="toolbar-button type5 has-menu has-optional-menu save_icon_1">
                <img src="{{$url}}/assets/themes/default/images/save.png" style="margin-left: -20px; float:left; width: 35px; margin-top: 2px;"/>
                <div class="button-text">Save</div>
                <div class="button-arrow-full-height menu-opener">
                    <div class="button-icon button-arrow"></div>
                </div>
                <div class="button-menu with-divider thick" >
                    <div id="btn_save_2" class="toolbar-button type4" >
                        <div class="button-menu-icon" style="background-position: -592px 0px"></div>
                        <div class="button-menu-text">Save</div>
                    </div>
                    <div id="btn_save_pdf" class="toolbar-button type4">
                        <div class="button-menu-icon" style="background-position: -384px 0px"></div>
                        <div class="button-menu-text">Download PDF</div>
                    </div>
                </div>
            </div>
            @if($permission['allow_proceed'])
                @if(isset($current_product['id']) && $current_product['id']>0)
                    <div id="btn_proceed_order" class="toolbar-button type5 action-button red order_icon_1">
                        <img src="{{$url}}/assets/themes/default/images/order.png" style="margin-left: -20px; float:left; width: 35px; margin-top: 2px;"/>
                        <div class="button-text">to Order</div>
                    </div>
                @endif
            @endif
        </div>
        <div class="back_fronts">
            <!--
            <div class="button_txt front" id="page_navigation_0" style="display: none">Front</div>
            <div class="button_txt front" id="page_navigation_1" style="display: none">Back</div>
            -->
            <div id="page_navigation" style="position: static !important"></div>
        </div>
        <div class="clearer"></div>
    </div>
    <div id="btn_top_toolbar_toggle" class="toolbar-button" ></div>
        <div id="btn_top_toolbar_product" class="toolbar-button">
            <ul class="idTabs">
                <li><a class="active" href="#Designer">Designer</a></li>
                <li><a href="#Product">Product</a></li>
            </ul>
        </div>
        <div id="btn_top_toolbar_toggle" class="toolbar-button" >Hide Options</div>
    <div id="top_toolbar_placeholder_wrapper" class="ztoolbar secondTool" >
        <div id="Designer">
            <div id="top_toolbar_placeholder" class="ztoolbar control" >
                <div id="top_toolbar" class="toolbar ztoolbar">
                    <!-- Button Insert -->
                    <div class="toolbar-section">
                        <div class="toolbar-section-title">Insert</div>
                        <div class="toolbar-section-body">
                            @if($permission['allow_add_text'])
                            <div id="btn_insert_text" class="toolbar-button type1 has-menu menu-opener">
                                <div class="button-icon" style="background-position: -170px -137px"></div>
                                <div class="button-text">Text</div>
                                <div class="button-menu with-divider insert-text-menu">
                                    <div class="menu-label">Select a text type:</div><br/>
                                    <div class="toolbar-button type2 btn-insert-text" id="insert_user_text" data-body="New Text Box..." data-title="Enter text here">
                                        <div class="insert-text-item-icon"></div>
                                        <div class="insert-text-item-title">Insert Text Box</div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($permission['allow_add_image'])
                                <div id="btn_insert_upload" class="toolbar-button type1">
                                    <div class="button-icon" style="background-position: -230px -137px"></div>
                                    <div class="button-text">Image</div>
                                </div>
                            @endif
                            @if($permission['allow_add_shape'])
                                <div id="btn_insert_shape" class="toolbar-button type1 has-menu menu-opener">
                                    <div class="button-icon" style="background-position: -288px -134px"></div>
                                    <div class="button-text">Shape</div>
                                    <div class="button-menu with-divider">
                                        <div id="btn_shape_hline" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -816px 0px"></div>
                                            <div class="button-menu-text">Horizontal Line</div>
                                        </div>
                                        <!-- new  -->
                                        <div id="btn_shape_vline" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -800px 0px"></div>
                                            <div  class="button-menu-text">Vertical Line</div>
                                        </div>
                                        <div id="btn_shape_rect" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -832px 0px"></div>
                                            <div class="button-menu-text">Rectangle</div>
                                        </div>
                                        <div id="btn_shape_circle" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -848px 0px"></div>
                                            <div class="button-menu-text">Circle</div>
                                        </div>
                                        <div id="btn_shape_triangle" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -864px 0px"></div>
                                            <div class="button-menu-text">Triangle</div>
                                        </div>
                                        <!-- new  -->
                                        <div id="btn_rounded_rectangle" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -153px -100px"></div>
                                            <div class="button-menu-text">Rounded Rectangle</div>
                                        </div>
                                        <div id="btn_ellipse_tool" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -127px -102px"></div>
                                            <div class="button-menu-text">Ellipse Tool</div>
                                        </div>
                                        <div id="btn_polygon_tool" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -104px -99px"></div>
                                            <div class="button-menu-text">Polygon Tool</div>
                                        </div>
                                        <!-- end new  -->
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- Them_default -->
                    <div id="remove_them_default" class="them_default">
                        <div class="toolbar-section">
                            <div class="toolbar-section-title">Text</div>
                            <div class="toolbar-section-divider"></div>
                            <div class="toolbar-section" id="text_tools_section">
                                <div class="toolbar-section-body">
                                    <div class="toolbar-section-row">
                                        <div id="btn_text_font_face" class="toolbar-button type3 has-menu menu-opener" >
                                            <div class="button-text">Font</div>
                                            <div class="bordered">
                                                <div id="selected_font_face" class="button-selected-text" style="width:80px" ></div>
                                                <div class="button-arrow" ></div>
                                            </div>
                                            <div id="font_face_menu" class="button-menu"></div>
                                        </div>
                                        <div id="btn_text_font_size" class="toolbar-button type3 has-menu menu-opener">
                                            <div class="button-text">Size</div>
                                            <div class="bordered">
                                                <input id="selected_font_size" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div>
                                            </div>
                                            <div id="font_size_menu" class="button-menu"></div>
                                        </div>
                                        <div id="btn_text_font_color" class="toolbar-button type3 has-menu menu-opener">
                                            <div class="button-text">Color</div>
                                            <div id="selected_font_color"></div>
                                            <!--
                                            <div class="button-text">Color</div>
                                            <div class="bordered">
                                                <div id="selected_font_color" class="selected-color"></div>
                                                <div class="button-arrow" ></div>
                                            </div>
                                            <div id="font_color_menu" class="button-menu color-palette"></div>
                                            -->
                                        </div>
                                    </div>
                                    <div class="toolbar-section-row">
                                        <div  id="btn_text_bold" class="toolbar-button type2" >
                                            <div class="button-icon" style="background-position: -32px 0px"></div>
                                        </div>
                                        <div  id="btn_text_italic" class="toolbar-button type2" >
                                            <div class="button-icon" style="background-position: -48px 0px"></div>
                                        </div>
                                        <div  id="btn_text_line_underline" class="toolbar-button type2" >
                                            <div class="button-icon" style="background-position: -192px -17px"></div>
                                        </div>
                                        <div  id="btn_text_line_strike" class="toolbar-button type2" >
                                            <div class="button-icon" style="background-position: -208px -17px"></div>
                                        </div>
                                        <div id="btn_text_gravity_west" class="toolbar-button type2 grouped " >
                                            <div class="button-icon" style="background-position: -64px 0px"></div>
                                        </div>
                                        <div id="btn_text_gravity_center" class="toolbar-button type2 grouped " >
                                            <div class="button-icon" style="background-position: -80px 0px"></div>
                                        </div>
                                        <div id="btn_text_gravity_east" class="toolbar-button type2 grouped " >
                                            <div class="button-icon" style="background-position: -96px 0px"></div>
                                        </div>
                                        <div id="btn_text_bullet" class="toolbar-button type2 has-menu menu-opener" >
                                            <div class="button-icon" style="background-position: -96px -16px"></div>
                                            <div class="button-menu">
                                                <div id="btn_text_bullet_none" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: 0 0"></div>
                                                </div>

                                                <div id="btn_text_bullet_black_circle" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: 0 -60px"></div>
                                                </div>
                                                <div id="btn_text_bullet_white_circle" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -60px -60px"></div>
                                                </div>
                                                <br />
                                                <div id="btn_text_bullet_black_square" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -120px -60px"></div>
                                                </div>
                                                <div id="btn_text_bullet_white_square" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -180px -60px"></div>
                                                </div>
                                                <div id="btn_text_bullet_dash" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -240px -60px"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="btn_text_number" class="toolbar-button type2 has-menu menu-opener" >
                                            <div class="button-icon" style="background-position: -112px -16px"></div>
                                            <div class="button-menu">
                                                <div id="btn_text_number_none" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: 0 0"></div>
                                                </div>
                                                <div id="btn_text_bullet_number_dot" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -60px 0"></div>
                                                </div>
                                                <br />
                                                <div id="btn_text_bullet_number_dash" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -120px 0"></div>
                                                </div>
                                                <div id="btn_text_bullet_number_parenth" class="toolbar-button type2">
                                                    <div class="bullet-text-item" style="background-position: -180px 0"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="btn_text_auto_fit" class="toolbar-button type2 admin-only" >
                                            <div class="button-text">Auto Fit</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="toolbar-section">
                            <div class="toolbar-section-title">Edit</div>
                            <div class="toolbar-section-divider"></div>
                            <div id="view_tools_section" class="toolbar-section" >
                                <div class="toolbar-section-body">
                                    <div id="btn_edit_undo" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: 0px 0px"></div>
                                        <div class="button-text">Undo</div>
                                    </div>
                                    <div id="btn_edit_redo" class="toolbar-button type2" >
                                        <div class="button-icon"  style="background-position: -16px 0px"></div>
                                        <div class="button-text">Redo</div>
                                    </div>
                                    <div id="btn_edit_lock" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: -720px 0px"></div>
                                        <div class="button-text">Lock</div>
                                    </div>
                                    <hr />
                                    <div id="btn_edit_copy" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: -480px 0px"></div>
                                        <div class="button-text">Copy</div>
                                    </div>
                                    <div id="btn_edit_paste" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: -496px 0px"></div>
                                        <div class="button-text">Paste</div>
                                    </div>
                                    <div id="btn_edit_delete" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: -400px 0px"></div>
                                        <div class="button-text">Delete</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="toolbar-section">
                            <div class="toolbar-section-title">Advanced</div>
                            <div class="toolbar-section-divider"></div>
                            <div class="toolbar-section">
                                <div class="toolbar-section-body">
                                    <div id="btn_advance_align" class="toolbar-button type2 has-menu menu-opener">
                                    <div class="button-icon" style="background-position: -144px 0px"></div>
                                    <div class="button-text">Align</div>
                                    <div class="button-menu with-divider" >
                                        <div id="btn_align_left" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -144px 0px"></div>
                                            <div class="button-menu-text">Align Left</div>
                                        </div>
                                        <div id="btn_align_center" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -192px 0px"></div>
                                            <div class="button-menu-text">Align Center</div>
                                        </div>
                                        <div id="btn_align_right" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -176px 0px"></div>
                                            <div class="button-menu-text">Align Right</div>
                                        </div>
                                        <hr />
                                        <div id="btn_align_top" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -128px 0px"></div>
                                            <div class="button-menu-text">Align Top</div>
                                        </div>
                                        <div id="btn_align_middle" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -208px 0px" ></div>
                                            <div class="button-menu-text">Align Middle</div>
                                        </div>
                                        <div id="btn_align_bottom" class="toolbar-button type4"  >
                                            <div class="button-menu-icon" style="background-position: -160px 0px"></div>
                                            <div class="button-menu-text">Align Bottom</div>
                                        </div>
                                        <hr />
                                        <div id="btn_dist_ver" class="toolbar-button type4" >
                                            <div class="button-menu-icon" style="background-position: -896px 0px" ></div>
                                            <div class="button-menu-text">Distribute Vertically</div>
                                        </div>
                                        <div id="btn_dist_hor" class="toolbar-button type4" >
                                            <div class="button-menu-icon"  style="background-position: -880px 0px" ></div>
                                            <div class="button-menu-text">Distribute Horizontally</div>
                                        </div>
                                    </div>
                                    </div>
                                    <div id="btn_advance_layers" class="toolbar-button type2 has-menu menu-opener">
                                        <div class="button-icon" style="background-position: -320px 0px"></div>
                                        <div class="button-text">Layers</div>
                                        <div class="button-menu with-divider" >
                                            <div id="btn_arrange_bring_front" class="toolbar-button type4" >
                                                <div class="button-menu-icon" style="background-position: -432px 0px"></div>
                                                <div class="button-menu-text">Bring to Front</div>
                                            </div>
                                            <div id="btn_arrange_send_back" class="toolbar-button type4"  >
                                                <div class="button-menu-icon" style="background-position: -464px 0px"></div>
                                                <div class="button-menu-text">Send to Back</div>
                                            </div>
                                            <div id="btn_arrange_bring_forward" class="toolbar-button type4" >
                                                <div class="button-menu-icon"  style="background-position: -416px 0px"></div>
                                                <div class="button-menu-text">Bring Forward</div>
                                            </div>
                                            <div id="btn_arrange_send_backward" class="toolbar-button type4" >
                                                <div class="button-menu-icon"  style="background-position: -448px 0px"></div>
                                                <div class="button-menu-text">Send Backward</div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div id="btn_advance_flip" class="toolbar-button type2 has-menu menu-opener">
                                        <div class="button-icon" style="background-position: -8px -129px"></div>
                                        <div class="button-text">Flip</div>
                                        <div class="button-menu with-divider" >
                                            <div id="btn_flip_horizontal" class="toolbar-button type4" >
                                                <div class="button-menu-icon" style="background-position: -24px -129px"></div>
                                                <div class="button-menu-text">By Horizontal</div>
                                            </div>
                                            <div id="btn_flip_vertical" class="toolbar-button type4"  >
                                                <div class="button-menu-icon" style="background-position: -40px -129px"></div>
                                                <div class="button-menu-text">By Vertical</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="btn_advance_crop" class="toolbar-button type2 indent_lefd">
                                        <div class="button-icon" style="background-position: -336px 0px"></div>
                                        <div class="button-text">Crop</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="toolbar-section">
                            <div class="toolbar-section-title">Info & Options</div>
                            <div class="toolbar-section-divider"></div>
                            <div class="toolbar-section">
                                <div class="toolbar-section-body">
                                    <div id="btn_advance_rotate_shape" class="toolbar-button type3 has-menu menu-opener" >
                                        <div class="button-text different_txt block_col">Background<br/>Color</div>
                                        <div class="left_div">
                                            <!--
                                            <div class="bordered"><input id="selected_rotate_degree_shape" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div></div>
                                            <div id="rotate_degree_menu_shape" class="button-menu"></div>
                                            -->
                                            <div id="selected_bg_color"></div>
                                        </div>
                                    </div>
                                    <div id="btn_theme_x" class="toolbar-button type3 has-menu menu-opener" >
                                        <div class="button-text block_col_2 indent_div">X</div>
                                        <div class="left_div">
                                            <!--
                                            <div class="bordered"><input id="selected_rotate_degree_shape" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div></div>
                                            <div id="rotate_degree_menu_shape" class="button-menu"></div>
                                            -->
                                            <input id="txt_left" type="text" style="width: 30px; height: 20px; font-size:11px" value="0" readonly />
                                        </div>
                                    </div>
                                    <hr />
                                    <div id="btn_advance_rotate_shape" class="toolbar-button type3 has-menu menu-opener" >
                                        <!--
                                        <div class="button-text block_col indent_div">Rotation</div>
                                        <div class="left_div">
                                            <div class="bordered"><input id="selected_rotate_degree_shape" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div>
                                            </div>
                                            <div id="rotate_degree_menu_shape" class="button-menu"></div>
                                        </div>
                                        -->
                                        <div class="button-text block_col indent_div">Rotate</div>
                                        <div id="btn_advance_rotate" class="toolbar-button type3 has-menu menu-opener" >

                                            <div class="bordered">
                                                <input id="selected_rotate_degree" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div>
                                            </div>
                                            <div id="rotate_degree_menu" class="button-menu"></div>
                                        </div>

                                    </div>
                                    <div id="btn_theme_y" class="toolbar-button type3 has-menu menu-opener" >
                                        <div class="button-text block_col_2 indent_div">Y</div>
                                        <div class="left_div">
                                            <!--
                                            <div class="bordered"><input id="selected_rotate_degree_shape" class="button-textbox" type="text"  />
                                                <div class="button-arrow" ></div></div>
                                            <div id="rotate_degree_menu_shape" class="button-menu"></div>
                                            -->
                                            <input id="txt_top" type="text" style="width: 30px; height: 20px; font-size:11px; margin-top: 5px" value="0" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="toolbar-section">
                            <div class="toolbar-section-title">View</div>
                            <div class="toolbar-section-divider"></div>
                            <div id="view_tools_section" class="toolbar-section right_div" >
                                <div class="toolbar-section-body">
                                    <div id="btn_view_bleed" class="toolbar-button type2" style="width:50px">
                                        <div class="button-icon" style="background-position: -256px 0px"></div>
                                        <div class="button-text">Trim Box</div>
                                    </div>
                                    <div id="btn_view_grid" class="toolbar-button type2" style="width:45px">
                                        <div class="button-icon" style="background-position: -272px 0px"></div>
                                        <div class="button-text">Grid</div>
                                    </div>

                                    <div id="btn_view_snap" class="toolbar-button type2" >
                                        <div class="button-icon" style="background-position: -368px 0px"></div>
                                        <div class="button-text">Snap</div>
                                    </div>
                                    <hr />
                                    <div id="btn_view_zoom_out" class="toolbar-button type2" style="width:50px" >
                                        <div class="button-icon" style="background-position: -304px 0px"></div>
                                        <div class="button-text">Zoom out</div>
                                    </div>
                                    <div id="btn_view_zoom_in" class="toolbar-button type2" style="width:45px">
                                        <div class="button-icon" style="background-position: -288px 0px"></div>
                                        <div class="button-text">Zoom in</div>
                                    </div>

                                    <div id="btn_edit_lock" class="toolbar-button type2" >
                                        <div class="button-text text_put">Quick<br/> Input</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Them_default -->
                </div>
                <!-- End Them_text -->
            </div>
        </div>
        <div id="Product">
            <div id="top_toolbar_placeholder" class="ztoolbar control" >
           <div class="center_txt" style="padding: 5px; text-align: left">
                <form role="form" onsubmit="return false;">
                   <div style="margin-right:30px; float: left; height: 40px; margin-left:10px; margin-top:10px;">
                       <label for="size" style="display: inline-block;margin-bottom: 5px;font-weight: bold;">Size</label>
                       <select data-role="price" data-parent="1" class="form-control-price" id="size" data-id="size" name="size">
                            <?php $sell_price = 0; ?>
                            @foreach($product_sizes as $size)
                            <?php
                                $tmp_size = explode('x', str_replace(array(' ','&nbsp;'), '', $size['size']));
                                $sizew = (float)$tmp_size[0];
                                $sizeh = (float)$tmp_size[1];
                                if($current_size == $size['id']){
                                    $current_sizew = $sizew;
                                    $current_sizeh = $sizeh;
                                }
                            ?>
                            <option <?php if($current_size == $size['id']) { echo 'selected'; $sell_price = $size['sell_price']; } ?> data-width="{{$sizew}}" data-height="{{$sizeh}}" data-sell-price="{{$size['sell_price']}}" data-theme="0" value="{{$size['id']}}">{{number_format($sizew,2).' x '.number_format($sizeh,2).'""'}}</option>
                            @endforeach
                       </select>
                    </div>
                    <div class="form-group" style="margin-right:30px; float: left; height: 40px; margin-left:10px; margin-top:10px;">
                        <div style="font-weight: bold; margin-bottom: 5px">Your quantity</div>
                        <div>
                            <input type="number" class="form-control-price" value="{{$quantity}}" id="quantity" name="quantity" style="width:80px;" />
                        </div>
                    </div>
                    <div class="form-group" style="margin-right:30px; float: left; height: 40px; margin-left:10px; margin-top:10px;">
                        <div style="font-weight: bold; margin-bottom: 5px">Estimated Total</div>
                        <div class="form-control-price">
                            <span id="lblprice">{{number_format($sell_price * $quantity,2)}}</span>
                            <input type="hidden" id="txt_lblprice" name="txt_lblprice" value = "{{number_format($sell_price * $quantity,2)}}" />
                        </div>
                    </div>
               </form>
           </div>
        </div>
        </div>
    </div>
</div>
<!-- END Minh Code-->



<div>
<div id="left_panel">
    <div id="text_fields" ></div>
</div>


<div id="design_tool" >
    <!--
    <div id="page_navigation"></div>-->
    <div id="tool_placeholder">




    </div>
</div>

<!-- Start: Bottom toolbar placeholder -->
<div id="bottom_toolbar_placeholder">
</div>
<!-- End Bottom toolbar placeholder -->
</div>
<!-- End: workspace -->
<div class="float-toolbar ztoolbar" id="crop_toolbar">
    <div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1120px 0px ;" onclick="moveInnerImage('U')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
    </div>
    <div>
        <div class="crop-toolbar-item" style="background-position: -1104px 0px ;" onclick="moveInnerImage('L')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1136px 0px ;" onclick="moveInnerImage('R')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1168px 0px ;" onclick="zoomOutInnerImage()"><span>Zoom Out</span></div>
        <div class="crop-toolbar-item" style="width:auto; background-position: 100px 100px ; cursor:default" ><div id="crop_slider"></div></div>
        <div class="crop-toolbar-item" style="background-position: -1184px 0px ;" onclick="zoomInInnerImage()"><span>Zoom In</span></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -688px 0px ;" onclick="rotateInnerImage(90)"><span>Rotate 90&deg;</span></div>
        <div class="crop-toolbar-item" style="background-position: -704px 0px ;" onclick="rotateInnerImage(-90)"><span>Rotate 90&deg;</span></div>
    </div>
    <div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1152px 0px ;" onclick="moveInnerImage('D')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
    </div>

    <div id="extra_crop_option" >
        <div id="btn_text_remove" class="toolbar-button type6 gray-gradient" >
            <div class="button-icon" style="background-position: -400px 0 " ></div>
        </div>

        <div id="btn_text_remove" class="toolbar-button type6 gray-gradient"  onclick="replacePhImage()">
            <div class="button-text">Replace Image</div>
        </div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>

        <div id="btn_crop_done" class="toolbar-button type6 green-gradient" >
            <div class="button-text">Done</div>
        </div>

    </div>

</div>

<div class="ztoolbar close" id="textbox_tools" >
    <div id="btn_text_edit" class="toolbar-button type2 close-only">
        <div class="button-icon" style="background-position: -352px 0px"></div>
        <div class="button-text">Edit</div>
    </div>

    <div class="open-only">
        <div id="textbox_edit_area" style="display:block; margin-bottom: 10px">
            <div id="textbox_tools_text_title" ></div>
            <textarea id="inline_textarea" ></textarea>
        </div>

        <div style="border-top: 0px solid #ddd; text-align: center; padding-top: 4px">


            <div id="btn_text_remove" class="toolbar-button type6 gray-gradient" >
                <div class="button-icon" style="background-position: -400px 0 " ></div>
                <div class="button-text">Delete</div>
            </div>

            <div id="btn_text_done" class="toolbar-button type6 green-gradient" >
                <div class="button-text">Done</div>
            </div>

        </div>
    </div>

</div>


<div class="float-toolbar ztoolbar" id="image_toolbar">
    <div id="btn_image_delete" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -400px 0px"></div>
        <div class="button-text">Delete</div>
    </div>

    <div id="btn_image_crop" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -336px 0px"></div>
        <div class="button-text">Crop</div>
    </div>

    <!--
        <div id="btn_image_border_color" class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
            <div class="selected-color"></div>
            <div class="button-text">Border</div>
            <div id="image_border_color_menu" class="button-menu color-palette"></div>
        </div>
    -->
    <div id="btn_image_border_color" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="selected-image-color" id="selected_image_border_color"></div>
        <div class="button-text">Border</div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -512px 0px" ></div>
        <div class="button-text">Weight</div>
        <div id="image_border_width_menu" class="button-menu"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
        <div class="image_filter_option_parent" style="background-position: 0px 0px"></div>
        <div class="button-text">Filter</div>
        <div id="image_filter_width_menu" class="button-menu">
            <div id="image_filter_none" class="toolbar-button type4 image_filter_option" style="background-position: -16px 0px"></div>
            <div class="button-text">None</div>
            <div id="image_filter_gotham" class="toolbar-button type4 image_filter_option" style="background-position: -32px 0px"></div>
            <div class="button-text">Gotham</div>
            <div id="image_filter_kelvin" class="toolbar-button type4 image_filter_option" style="background-position: -48px 0px"></div>
            <div class="button-text">Kelvin</div>
            <div id="image_filter_lomo" class="toolbar-button type4 image_filter_option" style="background-position: -64px 0px"></div>
            <div class="button-text">Lomo</div>
            <div id="image_filter_nashville" class="toolbar-button type4 image_filter_option" style="background-position: -80px 0px"></div>
            <div class="button-text">Nashville</div>
            <div id="image_filter_tiltshift" class="toolbar-button type4 image_filter_option" style="background-position: -96px 0px"></div>
            <div class="button-text">TiltShift</div>
            <div id="image_filter_toaster" class="toolbar-button type4 image_filter_option" style="background-position: -112px center"></div>
            <div class="button-text">Toaster</div>

        </div>
    </div>


    <div id="svg_tools" class="toolbar-button type3 has-menu">
        <div id="svg_colors"></div>
        <div id="svg_color_menu" class="button-menu color-palette with-transparent"></div>
    </div>

</div>

<div class="float-toolbar ztoolbar" id="shape_toolbar">
    <div id="btn_shape_delete" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -400px 0px"></div>
        <div class="button-text">Delete</div>
    </div>

    <div id="btn_shape_fill_color" class="toolbar-button type2 has-menu menu-opener" >
        <!--
        <div class="selected-color"></div>
        -->
        <div id="selected_shape_fill_color"></div>
        <div class="button-text">Fill</div>
        <!--
        <div id="shape_fill_color_menu" class="button-menu color-palette with-transparent"></div>
        -->
    </div>

    <div id="btn_shape_border_color" class="toolbar-button type2 has-menu menu-opener">
        <!--
        <div class="selected-color"></div>
        -->
        <div id="selected_shape_border_color"></div>
        <div class="button-text">Border</div>
        <!--
        <div id="shape_border_color_menu" class="button-menu color-palette"></div>
        -->
    </div>
    <div class="toolbar-button type2 has-menu menu-opener" >
        <div class="button-icon" style="background-position: -512px 0px" ></div>
        <div class="button-text">Border Size</div>
        <div id="shape_border_width_menu" class="button-menu"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener" >
        <div class="button-icon" style="background-position: -528px 0px" ></div>
        <div class="button-text">Border Style</div>
        <div id="shape_border_stroke_menu" class="button-menu">
            <div id="shape_border_stroke_solid" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -1px"></div>
            <!--
            <div id="shape_border_stroke_round_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -15px"></div>
            -->
            <div id="shape_border_stroke_square_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -29px"></div>
            <div id="shape_border_stroke_dash" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -43px"></div>
            <div id="shape_border_stroke_dash_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -58px"></div>
            <div id="shape_border_stroke_long_dash" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -72px"></div>
            <div id="shape_border_stroke_long_dash_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -86px"></div>
        </div>
    </div>

</div>

<iframe id="pdf_proof_iframe" name="pdf_proof_iframe" src="about:blank" frameborder="0"></iframe>
<form id= "pdf_proof_form" action="{{$design_url}}tools.php?action=assets&action_type=download_pdf_proof" target="pdf_proof_iframe" method="post" >
    <input id="pdf_proof_json" type="hidden" name="json" value="{}" />
    <input id="pdf_template_id" type="hidden" name="template_id" value="0" />
    <input id="pdf_design_id" type="hidden" name="design_id" value="0" />
    <input id="pdf_theme_id" type="hidden" name="theme_id" value="0" />
    <input id="pdf_dpi" type="hidden" name="dpi" value="0" />
    <input id="pdf_page" type="hidden" name="page" value="0" />
    <input type="hidden" name="license_key" value="{{$site_key}}" />
</form>


<link rel="stylesheet" href="{{$design_url}}lib/jpicker/css/jPicker-1.1.6.css" type="text/css" />
<link rel="stylesheet" href="{{$design_url}}lib/jpicker/css/jPicker.css" type="text/css" />
<script type="text/javascript" src="{{$design_url}}lib/jpicker/js/jpicker-1.1.6.min.js"></script>
<script type="text/javascript">
    $.fn.jPicker.defaults.images.clientPath='{{$design_url}}lib/jpicker/images/';
</script>
<script type="text/javascript" src="{{$design_url}}lib/design/js/stylist.js?ref={{rand(0,100000)}}"></script>
<script type="text/javascript" src="{{$url}}/assets/js/jquery.idTabs.min.js"></script>
<script type="text/javascript">
$(document).ready(function(e){
    $('select#size').change(function(e){
        var w= 0, h=0;
        var val = $(this).val();
        //var text = $(this).find('option:selected').text();
        val = parseInt(val, 10);
        if(isNaN(val)) val = 0;
        if(val==-1){
            w = $('select#width').val();
            h = $('select#height').val();
        }else{
            w = $(this).find('option:selected').attr('data-width');
            h = $(this).find('option:selected').attr('data-height');
        }
        $('div#width').css('display', val!=-1?'none':'');
        $('div#height').css('display', val!=-1?'none':'');
        w = parseFloat(w);
        h = parseFloat(h);
        var option = {'width':w, 'height':h};
        var sell_price = $(this).find('option:selected').attr('data-sell-price');
        var quantity = $("#quantity").val();
        var sum = sell_price * quantity;
        sum = number_format(sum,2);
        $("#lblprice").text(sum);
        $("#txt_lblprice").val(sum);
        reloadDesign(option);
    });
    $("#quantity").change(function(){
        var sell_price = $('select#size').find('option:selected').attr('data-sell-price');
        var quantity = $(this).val();
        var sum = sell_price * quantity;
        sum = number_format(sum,2);
        $("#lblprice").text(sum);
        $("#txt_lblprice").val(sum);
    });
});
function number_format(n,c,d,t){
  c = isNaN(c = Math.abs(c)) ? 2 : c;
  d = d == undefined ? "." : d;
  t = t == undefined ? "," : t;
  s = n < 0 ? "-" : "";
  i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
  j = (j = i.length) > 3 ? j % 3 : 0;
  return '$ '+ s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

var first_load = 1;
function loadTemplate(id, theme_id, page, option) {
    if(! view.isDesignSaved){
        if(! confirm('You have unsaved work. Continue anyway?'))
            return ;
    }
    closePopups();
    $.ajax({
        url:ajax_url+"/tools.php?action=product_design&action_type=load_template" ,
        data: {"template_id" : id  ,"theme_id" : theme_id ?  theme_id : 0, "product_id": current_product.id, "product_code": current_product.code, "product_title": current_product.title, "site_key": config.siteKey?config.siteKey:'', option: option,"license_key":license_key } ,
        dataType: 'json' ,
        crossDomain: true,
        beforeSend: function(){
            //alert(ajax_url);
        },
        error: function(){
            //alert('er');
        },
        success: function(data) {
            if (data.success != 1) {
                alert('Error loading template: ' + data.error);
                return ;
            }
            resetView() ;
            view.themes = data.themes ;
            applyThemeToToolbar(data.theme);
            var json = data.json;
            updateImageInfo(data.images);
            updateFotoliaInfo(data.fotoliaLicenses);
            view.templateId = data.id * 1 ;
            view.themeId = theme_id ;
            if(data.product_id)
                view.productId = data.product_id;
            else
                view.productId = typeof current_product.id!='undefined'?current_product.id:0;
            view.designName = data.set_title ;

            var ontop = data.ontop+'';
            ontop = parseInt(ontop, 10);
            if(isNaN(ontop) || ontop <0) ontop = 0;
            view.ontop = ontop;
            adjustProfileEQ(json.product);
            switchView(json , page) ;
            pushState() ;
            view.isDesignSaved = true ;
            if(window['first_load']){
                <?php if($current_sizew >= $current_sizeh){ ?>
                var state = true;
                <?php  } else {  ?>
                var state = false;
                <?php } ?>
                rotateLayout(state);
                window['first_load'] = 0;
            } else {
                rotateLayout(false);
            }
            /*
            resetView() ;
            view.themes = data.themes ;
            applyThemeToToolbar(data.theme);
            var json = plugRecentTexts($.parseJSON(data.json)) ;
            updateImageInfo(data.images);
            updateFotoliaInfo(data.fotoliaLicenses);
            view.templateId = data.id * 1 ;
            view.designName = data.set_title ;
            adjustProfileEQ(json.product);
            switchView(json , page) ;
            pushState() ;
            view.isDesignSaved = true ;
            */
        }
    }) ;
}
</script>
@stop