
<head>
  <title>Tacky Design | Design Online</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <base href="{{$url}}" />
  <link href="{{$url}}/assets/css/theme_designm.css" rel="stylesheet" type="text/css">
  <link href="{{$url}}/assets/css/design_layout.css" rel="stylesheet" type="text/css">
  <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  <script src="{{$url}}/assets/js/jquery.min.js" type="text/javascript"></script>
  <script src="{{$url}}/assets/js/jquery-ui.min.js" type="text/javascript"></script>
  <script src="{{$url}}/assets/js/api.jquery.js" type="text/javascript"></script>
</head>