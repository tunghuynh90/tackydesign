@extends($theme.'.common.default')
@section('body')
    <body class="page-visions-amp-victories-lookbook customer-logged-in template-page">
@stop

@section('content')
<section class="main-content">
    <div class="row full-width">
        <div class="columns" style="text-align: center;" >
            <div class="page-content">
                <h1 class="page-title">{{$this_lookbook->name}}</h1>
                <div id="slider1_container" style="position: relative; top: 0px; left: 0px; margin: 0 auto; width: 952px; height: 616px;">
                    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 952px; height: 616px; overflow: hidden;">
                        @if($this_lookbook->other_image!='')
                        <?php $other_image = unserialize($this_lookbook->other_image); ?>
                        @foreach($other_image as $image)
                        <div><img u="image" src="{{$url}}/assets/upload/lookbook/{{$image}}" /></div>
                        @endforeach
                        @endif
                    </div>
                    <span u="arrowleft" class="jssord02l" style="width: 55px; height: 55px; top: 270px; left: 8px;"></span> <span u="arrowright" class="jssord02r" style="width: 55px; height: 55px; top: 270px; right: 8px;"></span>
                </div>
                <div id="slider2_container" style="position: relative; top: 10px; left: 0px; margin: 0 auto; width: 962px; height: 308px;">
                    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 962px; height: 308px; overflow: hidden;">
                        @if(!empty($all_lookbook))
                        @foreach($all_lookbook as $lookbook)
                        <div>
                            <a href="{{$url}}/lookbooks/{{$lookbook->short_name}}">
                                <div class="caption">
                                    <h2>{{$lookbook->name}}</h2>
                                </div>
                            </a>
                            <img u="image" src="{{$url}}/assets/upload/lookbook/{{$lookbook->main_image}}" />
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <span u="arrowleft" class="jssord02l" style="width: 55px; height: 55px; top: 135px; left: 8px;"></span> <span u="arrowright" class="jssord02r" style="width: 55px; height: 55px; top: 135px; right: 8px;"></span>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.core.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.utils.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.slider.min.js?5152" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 2000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 3,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3
                $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 100,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 616,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0,                                   //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
        
                $DirectionNavigatorOptions: {
                    $Class: $JssorDirectionNavigator$,              //[Requried] Class to create direction navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };
            var options2 = {
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 1000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 0,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3
                $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 100,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: 476,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 308,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 8,                                   //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
        
                $DirectionNavigatorOptions: {
                    $Class: $JssorDirectionNavigator$,             //[Requried] Class to create direction navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
        
        }
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
        var jssor_slider2 = new $JssorSlider$("slider2_container", options2);
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$SetScaleWidth(Math.min(parentWidth, 1200));
        jssor_slider2.$SetScaleWidth(Math.min(parentWidth, 1200));
        }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
        });
    </script>
</section>

@stop