@extends($theme.'.common.default')
@section('body')
    <body class="page-moodboard template-page">
@stop

@section('content')
<section class="main-content">
    <div class="row full-width">
        <div class="columns">
            <div class="page-content">
                <h1 class="page-title" style="text-align:center;">Moodboard</h1>
                <div class="rte-content colored-links">
                    <div id="slider1_container" style="position: relative; margin: 0 auto; width: 1000px; height: 670px;">
                        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1000px; height: 670px; overflow: hidden;">
                            <div>
                                @foreach($all_moodboard as $moodboard)
                                <a class="mboard" style="float: left; padding: 5px;" href="{{$url}}/moodboard/{{$moodboard->short_name}}">
                                    <div class="preview">
                                        <img src="{{$url}}/assets/upload/moodboard/{{$moodboard->main_image}}" alt="{{$moodboard->name}}" title="{{$moodboard->name}}" style="width: 240px; height: 670px;" />
                                    </div>
                                    <div class="caption">
                                        <h2>{{$moodboard->name}}</h2>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="moodbar">MOODBOARD:<br /> A COLLAGE OF WORDS, IMAGES AND VISUALS THAT INSPIRE OUR COLLECTIONS.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .page-moodboard .moodbar {background-color: #000; color: #fff; display: block; position: relative; top: 40%; left: 30%; height: 86px; padding: 16px; width: 400px; opacity: 0.6; }
        .mboard .caption {
        position: absolute;
        top: 0;
        color:#fff;
        background-color:rgba(255,255,255,0.6);
        width: 240px;
        height: 670px;
        line-height: 80px;
        text-align: left;
        opacity: 0;
        z-index:10;
        -webkit-transition: all 0.7s ease;
        -moz-transition: all 0.7s ease;
        -o-transition: all 0.7s ease;
        transition: all 0.7s ease;
        }
        .mboard .caption:hover { opacity: 1; }
        .mboard h2 {margin-left: 10px ;}
    </style>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.core.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.utils.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.slider.min.js?5152" type="text/javascript"></script>         
    <script>
        jQuery(document).ready(function ($) {
            var options = {
                $NavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $ClassX: $JssorNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                  //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $SpacingX: 5                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    var sliderWidth = parentWidth;
                    sliderWidth = Math.min(sliderWidth, 1000);
                    jssor_slider1.$SetScaleWidth(sliderWidth);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
        });
    </script>
</section>
@stop