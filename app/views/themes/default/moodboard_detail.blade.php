@extends($theme.'.common.default')
@section('body')
    <body class="page-moodboard template-page">
@stop

@section('content')
<section class="main-content">
    <div class="row full-width">
        <div class="columns">
            <div class="page-content" style="text-align:center">
                <h1 class="page-title">{{$moodboard->name}}</h1>
                <div id="slider1_container" style="position: relative; top: 0px; left: 0; margin: 0 auto; width: 908px; height: 900px;">
                    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 908px; height: 900px; overflow: hidden;">
                        @if(!empty($arr_image))
                        @foreach($arr_image as $key=>$image)
                        <div>
                            <div id="sliderh{{($key+1)}}_container" style="position: relative; top: 0px; left: 0px; width: 900px; height: 300px;">
                                <!-- Slides Container -->
                                @if(!empty($image))
                                <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 900px; height: 300px; overflow: hidden;">
                                    @foreach($image as $img)
                                    <div><img u="image" src="{{$path.'/'.$img}}" /></div>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                    <div style="position: absolute; left: 0; top: 606px;"><img src="{{$url}}/assets/upload/moodboard/{{$moodboard->name_image}}" style="height: 294px; width: 300px;" alt="{{$moodboard->name}}" /></div>
                </div>
            </div>
        </div>
    </div>
    <link href="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jquery.lightbox-0.5.css?5152" rel="stylesheet" type="text/css"  media="all"  />
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jquery.lightbox-0.5.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.core.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.utils.js?5152" type="text/javascript"></script>
    <script src="//cdn.shopify.com/s/files/1/0389/6781/t/24/assets/jssor.slider.min.js?5152" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function ($) {
            var nestedSliders = [];
            $.each(["sliderh1_container", "sliderh2_container", "sliderh3_container", "sliderh4_container", "sliderh5_container"], function (index, value) {
                var sliderhOptions = {
                    $SlideDuration: 300,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                    $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                    $SlideWidth: 300,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                    //$SlideHeight: 150,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                    $SlideSpacing: 3, 					                //[Optional] Space between each slide in pixels, default value is 0
                    $DisplayPieces: 3,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                    $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                    $UISearchMode: 0,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
        
                    $NavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                        $Class: $JssorNavigator$,                       //[Required] Class to create navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                        $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                        $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                        $SpacingY: 0,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                        $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    }
                }
                var jssor_sliderh = new $JssorSlider$(value, sliderhOptions);
            });
        
            var options = {
                $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 0,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 3
        
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 300,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 80,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 350,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                $SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 3, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 3,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 0,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, direction navigator container, thumbnail navigator container etc).
                $PlayOrientation: 2,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
                $DragOrientation: 2,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0),
        
                $NavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $SpacingY: 5,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 2                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                }
            };
            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    jssor_slider1.$SetScaleWidth(Math.min(parentWidth, 952));
        }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();
            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
          $('#slider1_container a').lightBox();
        });
    </script>
</section>
@stop