@extends($theme.'.common.default')
@section('body')
<body class="page-the-global-conquest-tee template-search">
    @stop
    @section('content')
<section class="main-content">
    <div class="row">
      <div class="large-12 columns">
        <h1 class="page-title">Search</h1>
      </div>
    </div>

    <div class="row">
      <div class="columns">
        <div class="divider"></div>
      </div>
    </div>

    <div class="row summary narrow">
      <div class="large-4 columns">
        <p>Search results for <strong>"{{$name_search}}"</strong></p>
      </div>
      <div class="large-4 columns">
        <div class="searchbar-container">
          <form action="javascript:void(0)" method="get" role="search">
            <?php if(!is_null($name_search)){?>
                <input name="query" id="query" type="text" placeholder="Search store..." class="hint text" value="{{$name_search}}">
                <?php }?>
            <button aria-hidden="false" class="glyph search"></button>
          </form>

          <script type="text/javascript">
            $(".searchbar-container > form").submit(function(){
                window.location = "{{$url}}/search/1/"+toSlugger($("#query",this).val());
            });

             function toSlugger(value){
                value = $.trim(value);
                value = value.toLowerCase();
                value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\ \s]/g, '-');
                value= value.replace('/','');
                return value;
            }
        </script>

        </div>
      </div>
      <div class="large-4 columns">
        <p><strong>{{count($arr_searchs['arr_searchs']);}}</strong> results found</p>
      </div>
    </div>


    <div class="row">
      <div class="columns">
        <div class="divider results"></div>
      </div>
    </div>

  <!-- Begin results -->
  @if(!is_null($arr_searchs))
  <?php $arr_collection = array(); ?>
  @foreach($arr_searchs['arr_searchs'] as $key => $search)
      <?php
        $arr_collection = ProductCategory::where('id','=',$search['category_id'])
                                                    ->select('short_name')
                                                    ->get();
        foreach($arr_collection as $collection){
      ?>
      <div class="row results narrow">
        <div class="large-4 columns text-center">
            <div class="thumbnail">
              <a href="{{$url}}/collections/{{$collection['short_name']}}/products/{{$search['short_name']}}-{{$search['id']}}"><img style="width:164px;" src="{{$url}}/assets/upload/{{$search['main_image']}}" alt=""></a>
            </div>
        </div>
        <div class="large-8 columns" style="height:100px; overflow-y:hidden">
          <div class="search-result">
            <h3><a href="{{$url}}/collections/{{$collection['short_name']}}/products/{{$search['short_name']}}-{{$search['id']}}" title=""><strong class="highlight">{{$search['name']}}</strong></a></h3>
            {{$search['description']}}
          </div>
        </div>
      </div>

      <div class="row">
        <div class="columns">
          <div class="divider results"></div>
        </div>
      </div>
      <?php } ?>
    @endforeach
    @endif

<!-- End results -->
   <div class="row">
        @if($arr_searchs['total_pages'] > 1)
        <div class="right columns large-6">
            <ul class="pagination">
                <?php
                $page= $arr_searchs['page'];
                    if($page-3 > 0)
                        $min = $page-3;
                    else
                        $min=1;
                    if($page+3 <= $arr_searchs['total_pages'])
                        $max=$page+3;
                    else
                        $max=$arr_searchs['total_pages'];
                ?>
                @if($page-1 >= 1)
                <li class="arrow left"><a href="{{$url}}/search/{{$page-1}}/{{$name_search}}">Previous<span class="glyph arrow-left" aria-hidden="true"></span></a></li>
                @endif
                @for($i=$min; $i<$max;$i++)
                @if($i==$page)
                <li class="current"><a href="">{{$i}}</a></li>
                @endif
                @endfor
                @if(@page + 1 <=$arr_searchs['total_pages'])
                <li><a href="{{$url}}/search/{{$i}}/{{$name_search}}">{{$i}}</a></li>
                @endif
                @if($page + 1 <=$arr_searchs['total_pages'])

                <li class="arrow right"><a href="{{$url}}/search/{{$page+1}}/{{$name_search}}">Next <span class="glyph arrow-right" aria-hidden="true"></span></a></li>
                @endif
            </ul>
        </div>
        @endif
    </div>

</section>
@stop