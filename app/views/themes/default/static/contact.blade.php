@extends($theme.'.common.default')

@section('body')
	<body class="page-the-profound-aesthetic-academy customer-logged-in template-page">
@stop

@section('content')
<script type="text/javascript">
	$(function(){
		$("#submit").click(function(event){
			if($("#contact_name").val==''){
				alert('Name is required');
				return false;
			}
		});
	})
</script>

<section class="main-content">
	<div class="row">
		<div class="columns">
			<ul class="breadcrumbs colored-links">
				<li><a href="{{$url}}">Home</a></li>
				<li>{{$query->description}}</li>
			</ul>
    		<h1 class="page-title">{{$query->description}}</h1>
  		</div>
	</div>
	<div class="row">
		<div class="columns large-4">
			<div class="page-content rte-content colored-links">
				{{html_entity_decode($query->content)}}
			</div>
		</div>
		<div class="columns large-7 push-1  ">
			<form accept-charset="UTF-8" class="contact-form" method="post">
				@if($message)
				<div class="success feedback accent-text">
		        	<p>{{$message}}</p>
		      	</div>
				@else
                <p>
                	<label>Your Name:</label>
					<input type="text" id="contact_name" name="contact_name" placeholder="Your name" class="styled-input" value="">
				</p>
				<p>
					<label>Email:</label>
					<input required="required" type="email" id="contact_email" name="contact_email" placeholder="your@email.com" class="styled-input" value="">
				</p>
				<p>
					<label>Phone Number:</label>
					<input type="tel" id="contact_phone" name="contact_phone" placeholder="555-555-1234" class="styled-input" value="">
				</p>
				<p>
					<label>Message:</label>
					<textarea required="required" rows="10" cols="60" id="contact_message" name="contact_message" placeholder="Your Message" class="styled-input"></textarea>
				</p>
				<p>
					<input class="button styled-submit" type="submit" value="submit" id="submit">
				</p>
				@endif
			</form>
		</div>
	</div>
</section>
@stop