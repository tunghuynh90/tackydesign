@extends($theme.'.common.default')
@section('body')
	<body class="page-404-not-found template-404">
@stop
@section('content')
<section class="main-content">
    <div class="row">
		<div class="columns">
    		<ul class="breadcrumbs colored-links">
				<li><a href="{{$url}}">Home</a></li>
				<li>404</li>
           	</ul>
		</div>
    </div>

    <div class="row colored-links">
    	<div class="columns">
    		<h1 class="page-title">Page not found</h1>
    		<h2>We're sorry, but the page you're looking for could not be found. </h2>
    		<p>Go back to our <a href="{{$url}}">homepage</a> or view our range of <a href="{{$url}}/collections/all">products</a>.</p>
    	</div>
    </div>
</section>
@stop