<div class="account-content">
    <div id="customer-login">
      <div class="row">
        <div id="recover-password" style="display: block;" class="columns large-7">
          <h1 class="page-title">Reset Password</h1>
                <ul class="breadcrumbs colored-links">
                   <li><a href="/">Home</a></li>
                    <li>Account</li>
                </ul>
          <p class="note">We will send you an email to reset your password.</p>

          <form accept-charset="UTF-8" action="{{ (Confide::checkAction('UserController@do_forgot_password')) ?: URL::to('/user/forgot') }}" method="post">
             <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
              <label for="email" class="label">{{{ Lang::get('confide::confide.e_mail') }}}</label>
              <input type="email" value="{{{ Input::old('email') }}}" size="30" name="email" id="email" class="text">

              <input class="btn btn-default" type="submit" value="{{{ Lang::get('confide::confide.forgot.submit') }}}">
              <span class="note">or <a href="#">Cancel</a></span>
                  @if ( Session::get('error') )
                    <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
                @endif

                @if ( Session::get('notice') )
                    <div class="alert">{{{ Session::get('notice') }}}</div>
                @endif
          </form>
        </div>
      </div>
    </div>
</div>