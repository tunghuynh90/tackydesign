@extends($theme.'.common.default')
@section('body')
<body class="page-account template-customerslogin">
    @stop
    @section('content')
    <section class="main-content">
        <div class="account-content">
            <div id="customer-login">
                <div class="row">
                    <div id="login" class="columns large-7">
                        <h1 class="page-title">Login</h1>
                        <ul class="breadcrumbs colored-links">
                            <li><a href="#">Home</a></li>
                            <li>Account</li>
                        </ul>
                        <form accept-charset="UTF-8" action="{{{ Confide::checkAction('UserController@do_login') ?: URL::to('user/login') }}}" id="customer_login" method="post">
                            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
                            @if(isset($_GET['redirect_url']))
                            <input type="hidden" name="redirect_url" value="{{$_GET['redirect_url']}}" />
                            @endif
                            <label for="customer_email" class="label">{{{ Lang::get('confide::confide.username_e_mail') }}}</label>
                            <input type="email" value="{{{ Input::old('email') }}}" name="email" id="email" class="text">
                            <label for="customer_password" class="label">{{{ Lang::get('confide::confide.password') }}}
                            </label>
                            <input type="password" value="" name="password" id="password" class="text" size="16">
                            @if ( Session::get('error') )
                            <div class="alert alert-error">{{{ Session::get('error') }}}</div>
                            @endif
                            @if ( Session::get('notice') )
                            <div class="alert">{{{ Session::get('notice') }}}</div>
                            @endif
                            <p><a href="<?php echo $url;?>/user/forgot_password">Forgot your password?</a></p>
                            <div class="action_bottom">
                                <input class="button" type="submit" value="Sign In">
                                <span class="note">or <a href="<?php echo $url;?>/user/create">Register a New Account</a></span>
                            </div>
                        </form>
                    </div>
                    <div id="recover-password" style="display:none;" class="columns large-7">
                        <h1 class="page-title">Reset Password</h1>
                        <!--  <ul class="breadcrumbs colored-links">
                            <li><a href="#">Home</a></li>
                            <li>Account</li>
                            </ul> -->
                        <p class="note">We will send you an email to reset your password.</p>
                        <form accept-charset="UTF-8" action="#" method="post"><input name="form_type" type="hidden" value="recover_customer_password">
                            <input name="utf8" type="hidden" value="">
                            <label for="email" class="label">Email Address</label>
                            <input type="email" value="" size="30" name="email" id="recover-email" class="text">
                            <button tabindex="3" type="submit" class="but-yellow">{{{ Lang::get('confide::confide.login.submit') }}}</button>
                            <span class="note">or <a href="#" onclick="hideRecoverPasswordForm();return false;">Cancel</a></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            function showRecoverPasswordForm() {
              document.getElementById('recover-password').style.display = 'block';
              document.getElementById('login').style.display='none';
            }

            function hideRecoverPasswordForm() {
              document.getElementById('recover-password').style.display = 'none';
              document.getElementById('login').style.display = 'block';
            }

            if (window.location.hash == '#recover') { showRecoverPasswordForm() }
        </script>
    </section>
    @stop
