@extends($theme.'.common.default')

@section('body')
	<body class="page-create-account template-customersregister">
@stop

@section('content')
<section class="main-content">
    <div class="account-content">
        <div id="customer-register">
			<div class="row">
				<div id="register" class="columns large-12">
      				<h1 class="page-title">Create Account</h1>
      				<ul class="breadcrumbs colored-links">
   						<li><a href="#">Home</a></li>
					</ul>
	    			<form accept-charset="UTF-8" action="{{{ (Confide::checkAction('UserController@create')) ?: URL::to('user')  }}}" id="create_customer" method="POST">
                        <input type="hidden" name="_token" value="{{{ Session::getToken() }}}" id="token">
                        <input name="utf8" type="hidden" value="">

                        <div  class="clearfix large_form">
                        	<label for="first_name" class="label">First name</label>
                        	<input type="text" value="{{{ Input::old('firstname') }}}" name="first_name" id="first_name" class="text" size="30">
                        </div>

                        <div class="clearfix large_form">
                            <label for="last_name" class="label">Last name</label>
                            <input type="text" value="{{{ Input::old('lastname') }}}" name="last_name" id="last_name" class="text" size="30">
                        </div>

                        <div class="clearfix large_form">
                            <label for="email" class="label">{{{ Lang::get('confide::confide.e_mail') }}} <small>{{ Lang::get('confide::confide.signup.confirmation_required') }}</small></label>
                            <input type="email" value="{{{ Input::old('email') }}}" name="email" id="email" class="text" size="30">
                        </div>

                        <div class="clearfix large_form">
                        	<label for="password" class="label">{{{ Lang::get('confide::confide.password') }}}</label>
                        	<input type="password" value="" name="password" id="password" class="password text" size="30">
                        </div>

                         @if ( Session::get('error') )
                              <div class="alert alert-error alert-danger" style="color:red">
                                  @if ( is_array(Session::get('error')) )
                                      {{ head(Session::get('error')) }}
                                  @endif
                              </div>
                          @endif

                          @if ( Session::get('notice') )
                              <div class="alert">{{ Session::get('notice') }}</div>
                          @endif

                        <div class="action_bottom">
                            <button type="submit" class="button">{{{ Lang::get('confide::confide.signup.submit') }}}</button>
                        	<span class="note">
                            	or <a href="http://tackydesign.com/user/login">Login to an Existing Account</a>
                            </span>
                        </div>
	    			</form>
				</div><!-- #register -->
			</div><!-- .row -->
		</div><!-- #customer-register -->
	</div>
</section>

@stop