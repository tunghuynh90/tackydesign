@extends($theme.'.common.default')

@section('body')
    <body class="page-create-account template-customersregister">
@stop

@section('content')
<div id="customer-reset-password">
  <div class="row">
    <div class="columns large-4">
      <h1 class="page-title">Reset Password</h1>
       <ul class="breadcrumbs colored-links">
           <li><a href="/">Home</a></li>
            <li>Reset Account</li>
        </ul>

      <form accept-charset="UTF-8" action="{{{ (Confide::checkAction('UserController@do_reset_password'))?:URL::to('/user/reset') }}}" method="post">
            <input type="hidden" name="token" value="{{{ $token }}}">
            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">


          <label class="label" for="password">{{{ Lang::get('confide::confide.password') }}}</label>
          <input type="password" name="password" id="password" class="text" size="16">

          <label class="label" for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
          <input type="password" value="" name="password_confirmation" id="password_confirmation" class="text" size="16">

            @if ( Session::get('error') )
                <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
            @endif

            @if ( Session::get('notice') )
                <div class="alert">{{{ Session::get('notice') }}}</div>
            @endif

          <div class="action_bottom">
             <button type="submit" class="btn btn-primary">{{{ Lang::get('confide::confide.forgot.submit') }}}</button>
          </div>
    </form>
    </div>
  </div>
</div>
@stop