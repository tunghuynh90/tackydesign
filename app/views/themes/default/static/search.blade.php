@extends($theme.'.common.default')
@section('body')
	<body class="page-contact template-page">
@stop

@section('content')
<section class="main-content">
	<div class="row">
		<div class="columns">
			<ul class="breadcrumbs colored-links">
				<li><a href="http://profoundco.com/">Home</a></li>
				<li>Contact</li>
			</ul>
    		<h1 class="page-title">Contact</h1>
  		</div>
	</div>

	<div class="row">
		<div class="columns large-4">    
			<div class="page-content rte-content colored-links">
				<p><img src="{{$url}}/assets/themes/default/images/home-widget-image-text-2.png"></p>
                <p>Tel (732) 851-7223</p>
                <p>General Inquiries &nbsp;/ &nbsp;info@profoundco.com</p>
                <p>Orders / &nbsp;customerservice@profoundco.com</p>
                <p>Wholesale Inquiries &nbsp;/ &nbsp;wholesale@profoundco.com</p>
                <p>&nbsp;</p>
    		</div>
  		</div>
		<div class="columns large-7 push-1  ">
			<form accept-charset="UTF-8" action="http://profoundco.com/contact" class="contact-form" method="post">
            	<input name="form_type" type="hidden" value="contact">
            	<input name="utf8" type="hidden" value="">
				
                <p>
                	<label>Your Name:</label>
					<input type="text" id="contactFormName" name="contact[name]" placeholder="Your name" class="styled-input" value="">
				</p>
				<p>
					<label>Email:</label>
					<input required="required" type="email" id="contactFormEmail" name="contact[email]" placeholder="your@email.com" class="styled-input" value="">
				</p>
				<p>
					<label>Phone Number:</label>
					<input type="tel" id="contactFormTelephone" name="contact[phone]" placeholder="555-555-1234" class="styled-input" value="">
				</p> 
				<p>
					<label>Message:</label>
					<textarea required="required" rows="10" cols="60" id="contactFormMessage" name="contact[body]" placeholder="Your Message" class="styled-input"></textarea>
				</p>
				<p>
					<input class="button styled-submit" type="submit" id="contactFormSubmit" value="Send">
				</p>
			</form>
		</div>
	</div>
    
</section>
@stop