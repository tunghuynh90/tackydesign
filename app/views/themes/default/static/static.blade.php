@extends($theme.'.common.default')

@section('body')
	<body class="page-the-profound-aesthetic-academy customer-logged-in template-page">
@stop

@section('content')
<section class="main-content">
	<div class="row full-width">
		<div class="columns">
			<div class="page-content">
				<ul class="breadcrumbs colored-links">
					<li><a href="{{$url}}">Home</a></li>
					<li>{{$query->description}}</li>
				</ul>
				<h1 class="page-title">{{$query->description}}</h1>

				<div class="rte-content colored-links">
					{{html_entity_decode ($query->content)}}
	    		</div>
			</div>
		</div>
	</div>
</section>
@stop