@extends($theme.'.common.default')

@section('body')
    <body class="page-addresses customer-logged-in template-customersaddresses">
@stop
@section('content')
<section class="main-content">
    <div class="account-content">
        <div id="customer-addresses">
            <div class="row">
                <div class="columns large-12">
                    <h1 class="page-title">Account Addresses</h1>
                    <ul class="breadcrumbs colored-links">
                        <li><a href="{{$url}}">Home</a></li>
                        <li><a href="{{$url}}/user">Account</a></li>
                        <li>Addresses</li>
                    </ul>
                </div>
            </div>
            <div class="row large">
                <div class="columns large-6">
                    <div id="address_tables">
                        @if($user->address!='')
                        <?php $arr_addresses = unserialize($user->address); ?>
                        @foreach($arr_addresses as $key => $address)
                        <div class="address_table">
                            <div id="view_address_{{$key}}" class="customer_address">
                                @if($key == $user->address_default_key)
                                <h4 class="address_title">
                                    <span class="default_address note">{{$address['first_name'].', '.$address['address_2']}} (Default Address)</span>
                                </h4>
                                @endif
                                <p class="address_actions">
                                    <span class="action_link action_edit"><a href="javascript:void(0)" onclick="openForm({{$key}})">Edit</a></span>
                                    <span class="action_link action_delete"><a href="{{$url}}/user/delete-address/{{$key}}">Delete</a></span>
                                </p>
                                <div class="view_address">
                                    <p>{{$address['first_name'].' '.$address['last_name']}}</p>
                                    <p>{{$address['company']}}</p>
                                    <p>{{$address['address_1'].' '.$address['address_2']}}</p>
                                    <?php
                                        $country_id = $address['country'] ? (int)$address['country'] : 0;
                                        $province_id = $address['province'] ? (int)$address['province'] : 0;
                                        $country = $province = '';
                                        if(isset($countries[$country_id]['name']))
                                            $country = $countries[$country_id]['name'];
                                        if(isset($countries[$country_id]['cities'][$province_id]))
                                            $province = $countries[$country_id]['cities'][$province_id];
                                    ?>
                                    <p>{{$address['city'].' '.$province}}</p>
                                    <p>{{$country.' '.$address['zip']}}</p>
                                    <p>{{$address['phone']}}</p>
                                </div>
                            </div>
                            <div id="edit_address_{{$key}}" class="customer_address edit_address" style="display:none;">
                                <form accept-charset="UTF-8" id="address_form_{{$key}}" method="post">
                                    <input type="hidden" name="address[key]" value="{{$key}}" />
                                    <table class="customer_address_table">
                                        <tbody>
                                            <tr>
                                                <td class="label"><label for="address_first_name_{{$key}}">First Name</label></td>
                                                <td class="value"><input type="text" id="address_first_name_{{$key}}" class="address_form" name="address[first_name]" value="{{$address['first_name']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_last_name_{{$key}}">Last Name</label></td>
                                                <td class="value"><input type="text" id="address_last_name_{{$key}}" class="address_form" name="address[last_name]" value="{{$address['last_name']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_company_{{$key}}">Company</label></td>
                                                <td class="value"><input type="text" id="address_company_{{$key}}" class="address_form" name="address[company]" value="{{$address['company']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_address1_{{$key}}">Address1</label></td>
                                                <td class="value"><input type="text" id="address_address1_{{$key}}" class="address_form" name="address[address_1]" value="{{$address['address_1']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_address2_{{$key}}">Address2</label></td>
                                                <td class="value"><input type="text" id="address_address2_{{$key}}" class="address_form" name="address[address_2]" value="{{$address['address_2']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_city_{{$key}}">City</label></td>
                                                <td class="value"><input type="text" id="address_city_{{$key}}" class="address_form" name="address[city]" value="{{$address['city']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_country_{{$key}}">Country</label></td>
                                                <td class="value">
                                                    <select id="address_country_{{$key}}" onchange="changeCountry(this)" name="address[country]">
                                                        <option value="0">-- Please Select --</option>
                                                        @foreach($countries as $c_id => $country)
                                                        <option value="{{$c_id}}" <?php if(isset($address['country']) && $address['country']==$c_id) echo 'selected'; ?>>{{$country['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr class="address_province_container" <?php if($province == '') { ?> style="display: none;" <?php } ?>>
                                                <td class="label"><label for="address_province_{{$key}}">Province</label></td>
                                                <td class="value">
                                                    <select id="address_province_{{$key}}" class="address_province" name="address[province]">
                                                    @if($province_id && $country_id)
                                                    @foreach($countries[$country_id]['cities'] as $p_id => $province)
                                                    <option value="{{$p_id}}" <?php if($p_id==$country_id) echo 'selected'; ?>>{{$province}}</option>
                                                    @endforeach
                                                    @endif
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_zip_{{$key}}">Zip</label></td>
                                                <td class="value"><input type="text" id="address_zip_{{$key}}" class="address_form" name="address[zip]" value="{{$address['zip']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"><label for="address_phone_{{$key}}">Phone</label></td>
                                                <td class="value"><input type="text" id="address_phone_{{$key}}" class="address_form" name="address[phone]" value="{{$address['phone']}}" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td class="label"></td>
                                                <td class="value"><input type="checkbox" id="address_default_address_{{$key}}" name="address[default]" value="{{$user->address_default_key == $key ? 1 : 0}}" {{$user->address_default_key == $key ? 'checked' : ''}}> Set as Default Address?</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="action_bottom">
                                        <input class="button" type="submit" name="update_address" value="Update Address">
                                        <span class="note">or <a href="javascript:void(0)" onclick="openForm({{$key}})">Cancel</a></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <div id="address_pagination"></div>
                    </div>
                </div>
                <div class="columns large-6" id="address_container">
                    <a href="javascript:void(0)" onclick="addNewForm()" class="add-new-address">Add a new address</a>
                </div>
            </div>
        </div>
    </div>
    <div id="add_address" class="customer_address edit_address" style="display:none;">
        <form accept-charset="UTF-8"  id="address_form_new" class="address_form" method="post">
            <input name="form_type" type="hidden" value="customer_address"><input name="utf8" type="hidden" value="✓">
            <h4 id="add_address_title">Add a new address</h4>
            <table class="customer_address_table">
                <tbody>
                    <tr>
                        <td class="label"><label for="address_first_name_new">First Name</label></td>
                        <td class="value"><input type="text" id="address_first_name_new" class="address_form" name="address[first_name]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_last_name_new">Last Name</label></td>
                        <td class="value"><input type="text" id="address_last_name_new" class="address_form" name="address[last_name]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_company_new">Company</label></td>
                        <td class="value"><input type="text" for="address_company_new" class="address_form" name="address[company]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_address1_new">Address1</label></td>
                        <td class="value"><input type="text" id="address_address1_new" class="address_form" name="address[address_1]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_address2_new">Address2</label></td>
                        <td class="value"><input type="text" id="address_address2_new" class="address_form" name="address[address_2]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_city_new">City</label></td>
                        <td class="value"><input type="text" id="address_city_new" class="address_form" name="address[city]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_country_new">Country</label></td>
                        <td class="value">
                            <select id="address_country_new" onchange="changeCountry(this)" name="address[country]" data-default="">
                                <option value="0">-- Please Select --</option>
                                @foreach($countries as $country_id => $country)
                                <option value="{{$country_id}}" <?php if(isset($address['country']) && $address['country']==$country_id) echo 'selected'; ?>>{{$country['name']}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr id="address_province_container_new" class="address_province_container" style="display: none;">
                        <td class="label"><label for="address_province_new">Province</label></td>
                        <td class="value">
                            <select id="address_province_new" class="address_province" name="address[province]" data-default=""></select>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_zip_new">Zip</label></td>
                        <td class="value"><input type="text" id="address_zip_new" class="address_form" name="address[zip]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="address_phone_new">Phone</label></td>
                        <td class="value"><input type="text" id="address_phone_new" class="address_form" name="address[phone]" value="" size="40"></td>
                    </tr>
                    <tr>
                        <td class="label"></td>
                        <td class="value"><input type="checkbox" id="address_default_address_new" name="address[default]" value="1"> Set as Default Address?</td>
                    </tr>
                </tbody>
            </table>
            <div class="action_bottom">
                <input class="button" type="submit" name="update_address" value="Add Address">
                <span class="note">or <a href="#" onclick="$(this).parent().parent().parent().parent().remove()">Cancel</a></span>
            </div>
        </form>
    </div>
    <script type="text/javascript" charset="utf-8">
        function addNewForm(){
            if($("#add_address","#address_container").hasClass("new")){
                $("#add_address","#address_container").toggle();
                return false;
            }
            var html = $("#add_address")[0].outerHTML;
            $("#address_container").append(html);
            $("#add_address","#address_container").addClass("new").show();
        }
        @foreach($countries as $country_id => $country)
        @if(!empty($country['cities']))
        var country_{{$country_id}} = {{json_encode($country['cities'])}};
        @endif
        @endforeach
        function changeCountry(obj){
            var value = $(obj).val();
            var table = $(obj).parent().parent().parent().parent();
            $(".address_province_container",table).hide();
            if( window["country_"+value]!=undefined){
                var html = "";
                var country = window["country_"+value];
                for(var i in country){
                    html += '<option value="'+i+'">'+country[i]+'</option>';
                }
                $(".address_province",table).html(html);
                $(".address_province_container",table).show();
            }
        }
        function openForm(key){
            $("#view_address_"+key).toggle();
            $("#edit_address_"+key).toggle();
        }
    </script>
</section>
@stop