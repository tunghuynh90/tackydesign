@extends($theme.'.common.default')

@section('body')
    <body class="page-account customer-logged-in template-customersaccount">
@stop
@section('content')

<section class="main-content">
    <div class="account-content">
        <div id="customer-account">
            <div class="row">
                <div class="columns large-12">
                    <h1 class="page-title clearfix">Account Details</h1>
                    <ul class="breadcrumbs colored-links">
                        <li><a href="{{$url}}">Home</a></li>
                        <li>Account</li>
                    </ul>
                </div>
            </div>
            <div class="row large">
                <div id="customer_orders" class="columns large-8">
                    <p>You haven't placed any orders yet.</p>
                    <span class="note"><a href="{{$url}}/user/logout" id="customer_logout_link">Logout</a></span>
                </div>
                <div id="customer_sidebar" class="columns large-4">
                    <h2>{{$user->firstname.' '.$user->lastname }}</h2>
                    <p class="email note">{{$user->email}}</p>
                    <div class="address note">
                        @if($user->address_default_key != null && $user->address)
                        <?php $arr_addresses = unserialize($user->address); ?>
                        <?php $address =  $arr_addresses[$user->address_default_key]; ?>
                        {{$address['address_1'] ? "<p>{$address['address_1']}</p>": ''}}
                        {{$address['address_2'] ? "<p>{$address['address_2']}</p>" : ''}}
                        {{$address['city'] ? "<p>{$address['city']}".(isset($countries[$address['country']]) ? " ,{$countries[$address['country']]}" : '')."</p>" : ''}}
                        {{$address['zip'] ? "<p>{$address['zip']}</p>" : ''}}
                        {{$address['phone'] ? "<p>{$address['phone']}</p>" : ''}}
                        @endif
                        <a id="view_address" href="{{$url}}/user/addresses">View Addresses</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop