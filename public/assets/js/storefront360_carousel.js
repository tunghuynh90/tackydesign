sf360_carousel_data = {
    live: false,
    click_callback_url: 'https://storefront-shopify.engineroom360.com/storefront360/v1/carousel/click/',
    visit_callback_url: 'https://storefront-shopify.engineroom360.com/storefront360/v1/carousel/visit/',
    cdn_data_url: 'https://70c486d7bba2e70faf48-3f03fcc7013a02f84716621a57fcfd89.ssl.cf5.rackcdn.com/profound-3/storefront360_carousel_data',

    callback: function (data) {
        var app_content = sf360_carousel.jQuery.parseJSON(data);
        sf360_carousel_data['app_content'] = app_content;
        sf360_carousel.app_carousel360(app_content);
    }
};

;
var sf360_carousel_data, preview_app_data_json, jQuery;
var sf360_carousel = {
    cdn_core_url: 'https://5c290454d7dd091f914a-49d61627a5ff454232b0842595f7b399.ssl.cf1.rackcdn.com/',
    jquery_url: 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
    lib_url: 'https://5c290454d7dd091f914a-49d61627a5ff454232b0842595f7b399.ssl.cf1.rackcdn.com/storefront360_lib.js',
    img_bx_controls_url: 'https://5c290454d7dd091f914a-49d61627a5ff454232b0842595f7b399.ssl.cf1.rackcdn.com/controls.png',
    img_bx_loader_url: 'https://5c290454d7dd091f914a-49d61627a5ff454232b0842595f7b399.ssl.cf1.rackcdn.com/bx_loader.gif',
    img_flex_nav: 'slider-arrows-custom.png',
    img_flex_page: 'slideshow-pager.png',
    img_flex_nav_bg: '#d4c3a5',
    img_flex_nav_bg_hover: '#666',
    img_flex_nav_bg_op: 1.0,
    img_flex_nav_bg_hover_op: 1.0,
    img_flex_nav_height: 40,
    img_flex_nav_width: 40,
    img_flex_nav_prev_pos: '',
    img_flex_nav_next_pos: '',
    img_flex_nav_prev_pos_hover: '',
    img_flex_nav_next_pos_hover: '',
    show_pager: false,
    jQuery: '',
    cache: false,
    carousel_style: function (uid, carousel_data, images_data) {
        "use strict";
        var style;
        style = ".sf360-flex-container a:active, ";
        style += ".sf360-flexslider a:active,";
        style += ".sf360-flex-container a:focus, ";
        style += ".sf360-flexslider a:focus  {outline: none;}\n";
        style += ".sf360-slides, ul.sf360-slides, .sf360-flex-control-nav, ol.sf360-flex-control-nav, .sf360-flex-direction-nav, ul.sf360-flex-direction-nav {margin: 0; padding: 0; list-style: none;}\n";
        style += "ul.sf360-flex-direction-nav li {list-style: none;}\n";
        style += ".sf360-flexslider {margin:0; padding: 0;}\n";
        style += ".sf360-flexslider .sf360-slides > li {margin: 0; display: none; -webkit-backface-visibility: hidden;}\n";
        style += ".sf360-flexslider .sf360-slides img {width: 100%; display: block;}\n";
        style += ".sf360-flex-pauseplay span {text-transform: capitalize;}\n";
        style += ".sf360-slides:after {content: '\\0020\'; display: block; clear: both; visibility: hidden; line-height: 0; height: 0;}\n";
        style += "html[xmlns] .sf360-slides {display: block;}\n";
        style += "* html .sf360-slides {height: 1%;}\n";
        style += ".no-js .sf360-slides > li:first-child {display: block;}\n";
        style += ".sf360-flexslider { position: relative;}\n";
        style += ".sf360-flex-viewport { max-height: 2000px; -webkit-transition: all 1s ease; -moz-transition: all 1s ease; -o-transition: all 1s ease; transition: all 1s ease; }\n";
        style += ".loading .sf360-flex-viewport { max-height: 300px; }\n";
        style += ".sf360-flexslider .sf360-slides { zoom: 1; overflow: hidden}\n";
        style += ".carousel li { margin-right: 5px; }\n";
        style += ".sf360-flex-direction-nav {*height: 0;}\n";
        style += ".sf360-flex-direction-nav a  { background-color:green; display: block; width: " + sf360_carousel.img_flex_nav_width + "px; height: " + sf360_carousel.img_flex_nav_height + "px; margin: -20px 0 0; position: absolute; top: 50%; z-index: 10; overflow: hidden; cursor: pointer; color: rgba(0,0,0,0.8); text-shadow: 1px 1px 0 rgba(255,255,255,0.3); }\n";
        style += ".sf360-flex-direction-nav a.sf360-flex-prev  {";
        style += "  text-indent: 100%;";
        style += "  white-space: nowrap;";
        style += "  margin-left: -10px;";
        style += "  border-top-right-radius: 5px;";
        style += "  border-bottom-right-radius: 5px;";
        style += "  overflow: hidden;";
        style += "  background: " + sf360_carousel.img_flex_nav_bg + " url('" + sf360_carousel.cdn_core_url + sf360_carousel.img_flex_nav + "') " + sf360_carousel.img_flex_nav_prev_pos + ";";
        style += "  -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease; -o-transition: all 0.3s ease; transition: all 0.3s ease;";
        style += "}\n";
        style += ".sf360-flex-direction-nav a.sf360-flex-next  {";
        style += "text-indent: 100%;";
        style += "white-space: nowrap;";
        style += "margin-right: -10px;";
        style += "border-top-left-radius: 5px;";
        style += "border-bottom-left-radius: 5px;";
        style += "overflow: hidden;";
        style += "background: " + sf360_carousel.img_flex_nav_bg + " url('" + sf360_carousel.cdn_core_url + sf360_carousel.img_flex_nav + "') " + sf360_carousel.img_flex_nav_next_pos + ";";
        style += "-webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease; -o-transition: all 0.3s ease; transition: all 0.3s ease;";
        style += "}\n";
        style += ".sf360-flex-direction-nav a.sf360-flex-prev:hover  { ";
        style += "  background: " + sf360_carousel.img_flex_nav_bg_hover + " url('" + sf360_carousel.cdn_core_url + sf360_carousel.img_flex_nav + "') " + sf360_carousel.img_flex_nav_prev_pos_hover + ";";
        style += "  opacity: " + sf360_carousel.img_flex_nav_bg_hover_op + ";";
        style += "}\n";
        style += ".sf360-flex-direction-nav a.sf360-flex-next:hover  { ";
        style += "  background: " + sf360_carousel.img_flex_nav_bg_hover + " url('" + sf360_carousel.cdn_core_url + sf360_carousel.img_flex_nav + "') " + sf360_carousel.img_flex_nav_next_pos_hover + ";";
        style += "  opacity: " + sf360_carousel.img_flex_nav_bg_hover_op + ";";
        style += "}\n";
        style += ".sf360-flex-direction-nav .sf360-flex-prev { left: -70px; }\n";
        style += ".sf360-flex-direction-nav .sf360-flex-next { right: -70px; text-align: right; }\n";
        style += ".sf360-flexslider:hover .sf360-flex-prev { opacity: " + sf360_carousel.img_flex_nav_bg_op + "; left: 10px; }\n";
        style += ".sf360-flexslider:hover .sf360-flex-next { opacity: " + sf360_carousel.img_flex_nav_bg_op + "; right: 10px; }\n";
        style += ".sf360-flexslider:hover .sf360-flex-next:hover, .sf360-flexslider:hover .sf360-flex-prev:hover { opacity: " + sf360_carousel.img_flex_nav_bg_hover_op + "; }\n";
        style += ".sf360-flex-direction-nav .sf360-flex-disabled { opacity: 0!important; filter:alpha(opacity=0); cursor: default; }\n";
        style += ".sf360-flex-pauseplay a { display: block; width: 20px; height: 20px; position: absolute; bottom: 5px; left: 10px; opacity: 0.8; z-index: 10; overflow: hidden; cursor: pointer; color: #000; }\n";
        style += ".sf360-flex-pauseplay a:hover  { opacity: 1; }\n";
        style += ".sf360-flexslider{ overflow:hidden; }\n";
        if (!sf360_carousel.show_pager) {
            style += ".sf360-flex-control-paging { display: none !important; }\n";
        } else {
            style += ".sf360-flex-control-thumbs {margin: 0px; position: static; overflow: hidden;}\n";
            style += ".sf360-flex-control-thumbs li {width: 25%; float: left; margin: 0;}\n";
            style += ".sf360-flex-control-thumbs img {width: 100%; display: block; opacity: .7; cursor: pointer;}\n";
            style += ".sf360-flex-control-thumbs img:hover {opacity: 1;}\n";
            style += ".sf360-flex-control-thumbs .sf360-flex-active {opacity: 1; cursor: default;}\n";
            style += ".sf360-flex-control-nav {width: 100%; position: absolute; bottom: 1%; right: 1%; z-index: 10;}\n";
            style += ".sf360-flex-control-nav {text-align: " + sf360_carousel.pager_alignment + "}\n";
            if (sf360_carousel.pager_alignment == 'left') {
                style += ".sf360-flex-control-nav {left: 10px;}\n";
            }
            style += ".sf360-flex-control-nav li {margin: 0 2px; display: inline-block; zoom: 1; *display: inline;}\n";
            style += ".sf360-flex-control-paging li a {width: 11px; height: 11px; display: block; background: #666; background: rgba(0,0,0,0.5); cursor: pointer; text-indent: 100%; white-space: nowrap; overflow: hidden; -webkit-border-radius: 20px; -moz-border-radius: 20px; -o-border-radius: 20px; border-radius: 20px; -webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.3); -moz-box-shadow: inset 0 0 3px rgba(0,0,0,0.3); -o-box-shadow: inset 0 0 3px rgba(0,0,0,0.3); box-shadow: inset 0 0 3px rgba(0,0,0,0.3); }\n";
            style += ".sf360-flex-control-paging li a:hover { background: #333; }\n";
            style += ".sf360-flex-control-paging li a.sf360-flex-active { background: #000; cursor: default; }\n";
        }
        style += "@media only screen and (max-width: 767px) { ";
        style += "  .sf360-flex-control-paging { display: none; }\n";
        style += "  .sf360-flex-control-nav, .sf360-flex-direction-nav { display:none !important; }\n";
        if (!carousel_data.carousel_show_mobile) {
            style += ".sf360-flexslider { display: none; }\n";
        }
        if (!carousel_data.text_overlay_show_mobile) {
            style += ".sf360-flex-caption { display: none; "
            style += "}\n";
        }
        style += "}\n";
        style += "}\n";
        style += ".sf360-flexslider { background:none; border:none; box-shadow:none; margin:0px auto; }\n";
        images_data.forEach(function (image, idx) {
            style += ".sf360-flex-caption-" + idx + " { width: 100%; margin: 0; "
            if (image.text_overlay_bottom <= 0) {
                style += " height: 100%;"
            }
            style += "}\n";
            style += ".sf360-text-container-" + idx + " { width: 100%; padding: 5px; text-align: center";
            if (image.text_overlay_left || image.text_overlay_right) {
                if (image.text_overlay_bottom == 0) {
                    style += " height: 100%";
                }
            }
            style += "}\n";
            style += ".sf360-text-container-" + idx + "  h1{ margin: 5px 0; }\n";
            style += ".sf360-text-container-" + idx + "  p{ margin: 5px 0; }\n";
            style += "@media screen and (min-width: 768px) { ";
            style += ".sf360-text-container-" + idx + " { text-align: left}\n";
            style += "  .sf360-text-container-" + idx + "  h1{ margin: 5px; }\n";
            style += "  .sf360-text-container-" + idx + "  p{ margin: 5px; }\n";
            style += "  a.sf360-button-" + idx + " { margin: 5px; }\n";
            style += "  .sf360-flex-caption-" + idx + "  { width: 100%; max-width: 1600px; position: absolute; top: 0%; z-index: 100; }\n";
            style += "  .sf360-text-container-" + idx + " { ";
            style += "    position: relative; width: 100%; ";
            style += "    -webkit-animation: slideIn; -moz-animation: slideIn; -o-animation: slideIn; animation: slideIn; -webkit-animation-duration: 2s; -moz-animation-duration: 2s; -o-animation-duration: 2s; animation-duration: 2s; ";
            style += "  }\n";
            style += "}\n";
            style += ".sf360-social-icons-" + idx + "  { width: 110px; max-width: 1600px; position: absolute; z-index: 100; padding: 5px; opacity: 0.5;"
            if (image.social_icon_location == 'top_left') {
                style += " top: 0;"
                style += " left: 0;"
            };
            if (image.social_icon_location == 'top_right') {
                style += " top: 0;"
                style += " right: 0;"
            };
            if (image.social_icon_location == 'bottom_left') {
                style += " bottom: 0;"
                style += " left: 0;"
            };
            if (image.social_icon_location == 'bottom_right') {
                style += " bottom: 0;"
                style += " right: 0;"
            };
            style += "}\n";
            style += ".sf360-social-icons-" + idx + ":hover  { opacity: 1;"
            style += "}\n";
            style += ".sf360-social-icon-facebook { float: left; padding: 0 2px;";
            style += "}\n";
            if (image.text_overlay_bgclear) {
                var bg_rgba = sf360_carousel.hexToRgb(image.text_overlay_bgcolor);
                if (bg_rgba) {
                    style += ".sf360-text-container-" + idx + " { background-color: rgba(" + bg_rgba.r + "," + bg_rgba.g + "," + bg_rgba.b + "," + image.text_overlay_bgcolor_opacity + "); "
                    if (image.text_overlay_bottom <= 0) {
                        style += " height: 100%;"
                    }
                    style += "}\n";
                }
            }
            style += ".sf360-text-container-" + idx + " h1 { ";
            style += "  font-family: " + image.header_font + "; ";
            style += "  font-size: " + image.header_font_size_mobile + "px; ";
            style += "  font-weight: bold; ";
            style += "  line-height: 1.2; ";
            style += "  color:" + image.header_text_color + ";";
            style += " }\n";
            style += ".sf360-text-container-" + idx + " p { ";
            style += "  font-family: " + image.body_font + "; ";
            style += "  font-size: " + image.body_font_size_mobile + "px; ";
            style += "  line-height: 1.2; ";
            style += "  color:" + image.body_text_color + ";";
            style += " }\n";
            style += ".sf360-button-" + idx + " {  ";
            style += "  width: auto; ";
            style += "  background: " + image.button_bgcolor + "; ";
            style += "  display: inline-block; ";
            style += "  border:1px solid " + image.button_bgcolor + "; ";
            style += "  color:" + image.button_text_color + "; ";
            style += "  font-family: " + image.button_font + "; ";
            style += "  font-size:" + image.button_font_size_mobile + "px; ";
            style += "  opacity: 1; ";
            style += "  text-align: center; ";
            style += "  text-decoration:none; ";
            style += "  padding: 4px 10px 4px; ";
            style += "  cursor:pointer; ";
            style += "  border-radius: 2px; ";
            style += "}\n";
            style += "a.sf360-button-" + idx + ":hover { ";
            style += "  background: " + image.button_bgcolor_onhover + "; ";
            style += "  border:1px solid " + image.button_bgcolor_onhover + "; ";
            style += "  color:" + image.button_text_color + "; ";
            style += "}\n";
            style += "@media screen and (min-width: 860px) { ";
            if (image.text_overlay_left || image.text_overlay_right || image.text_overlay_top || image.text_overlay_bottom) {
                style += ".sf360-flex-caption-" + idx + " { left: " + image.text_overlay_left + "%; top: " + image.text_overlay_top + "%; margin: 0; "
            } else {
                var padding_top;
                if (image.text_overlay_vlocation == 1) {
                    padding_top = 5;
                } else if (image.text_overlay_vlocation == 2) {
                    padding_top = 33;
                } else {
                    padding_top = 55;
                }
                if (image.text_overlay_position == 2) {
                    style += ".sf360-flex-caption-" + idx + " { padding-left: 5%; top: " + padding_top + "%; margin: 0; }\n";
                } else if (image.text_overlay_position == 1) {
                    style += ".sf360-flex-caption-" + idx + " { padding-left: " + (100 - Math.max(10, parseInt(image.text_overlay_width))) + "%; top: " + padding_top + "%; margin: 0; }\n";
                } else {
                    style += ".sf360-flex-caption-" + idx + " { top: " + padding_top + "%; margin: auto; }\n";
                    style += ".sf360-text-container-" + idx + " { text-align: center; top: " + padding_top + "%; margin: auto; }\n";
                }
            }
            style += "}\n";
            style += " .sf360-text-container-" + idx + " h1 { ";
            style += " font-size: " + image.header_font_size + "px; ";
            if (image.header_text_italics) {
                style += " font-style: italic";
            }
            style += " }\n";
            style += " .sf360-text-container-" + idx + " p { ";
            style += "   font-size: " + image.body_font_size + "px; "
            if (image.body_text_italics) {
                style += " font-style: italic";
            }
            style += " }\n";
            style += " .sf360-button-" + idx + " {  ";
            style += "   font-size:" + image.button_font_size + "px; ";
            if (image.button_text_italics) {
                style += " font-style: italic";
            }
            style += "   padding: 2px 20px 2px; ";
            style += "   border-radius: 5px; ";
            style += "   }\n";
            style += " }\n";
        });
        if (uid == 'preview') {
            style += " .sf360-flex-direction-nav a.sf360-flex-prev, ";
            style += ".sf360-flex-direction-nav a.sf360-flex-prev:hover, ";
            style += ".sf360-flex-direction-nav a.sf360-flex-next, ";
            style += ".sf360-flex-direction-nav a.sf360-flex-next:hover { ";
            style += "  border: 0px; ";
            style += "  }\n";
        } else {
            if (!carousel_data.fit_to_container) {
                style += " .sf360-flexslider { ";
                style += " width: " + carousel_data.width + "px; ";
                style += " }\n";
            }
        }
        return style;
    },
    loadScript: function (url, callback) {
        "use strict";
        var script = document.createElement("script");
        script.type = "text/javascript";
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function () {
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    },
    hexToRgb: function (hex) {
        "use strict";
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    },
    getParameterByName: function (name, url) {
        "use strict";
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results == null ? "" : results[1] && decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    getEndingURL: function (url, count) {
        "use strict";
        var index, url_part, result;
        index = url.lastIndexOf("?");
        if (index != -1) {
            url = url.substring(0, index);
        }
        if (url.slice(-1) == '/') {
            index = url.lastIndexOf("/");
            url = url.substring(0, index);
        }
        url_part = url;
        for (var i = 0; i < count; i++) {
            index = url_part.lastIndexOf("/");
            url_part = url.substring(0, index);
        }
        result = "";
        if (index != -1) {
            result = url.substring(index);
        }
        return result + "/";
    },
    app_carousel360: function (app_content) {
        "use strict";
        var app_data, carousel_tag, carousel_index, ending_url, getData;
        var uid, schedule_index, cssText, head, style, carousel_width;
        var $ = sf360_carousel.jQuery;
        uid = $('#storefront360_carousel').data('uid');
        if (uid === 'preview') {
            app_data = app_content;
        } else {
            carousel_tag = sf360_carousel.getParameterByName('sf360', window.location);
            carousel_index = -1;
            if (carousel_tag) {
                carousel_index = app_content.carousel_tags.indexOf(carousel_tag);
            }
            if (carousel_index === -1) {
                ending_url = sf360_carousel.getEndingURL(window.location.href, 2);
                if (ending_url.lastIndexOf('/collections/') === -1 && ending_url.lastIndexOf('/pages/') === -1 && ending_url.lastIndexOf('/products/') === -1) {
                    ending_url = sf360_carousel.getEndingURL(window.location.href, 3);
                    if (ending_url.lastIndexOf('/collections/') === -1 && ending_url.lastIndexOf('/pages/') === -1 && ending_url.lastIndexOf('/products/') === -1) {
                        ending_url = "/";
                    }
                }
                schedule_index = app_content.schedule_tags.indexOf(ending_url);
                if (schedule_index === -1) {
                    ending_url = sf360_carousel.getEndingURL(window.location.href, 1);
                    schedule_index = app_content.schedule_tags.indexOf(ending_url);
                }
                if (schedule_index !== -1) {
                    carousel_index = app_content.carousel_tags.indexOf(app_content.schedules[schedule_index].carousel_tag);
                }
            }
            if (carousel_index != -1) {
                app_data = app_content.carousels[carousel_index];
            }
        }
        if (uid !== undefined && app_data && app_data.images.length > 0) {
            if (app_data.carousel.button_bgcolor !== "") {
                sf360_carousel.img_flex_nav_bg = app_data.carousel.button_bgcolor;
                sf360_carousel.img_flex_nav_bg_hover = app_data.carousel.button_bgcolor_onhover;
                sf360_carousel.pager_alignment = app_data.carousel.pager_alignment || 'none';
                sf360_carousel.show_pager = sf360_carousel.pager_alignment == 'none' ? false : true;
            }
            if (app_data.carousel.button_type == '1') {
                sf360_carousel.img_flex_nav_height = 60;
                sf360_carousel.img_flex_nav_width = 60;
                sf360_carousel.img_flex_nav = 'arrow_sprite_thin.png';
                sf360_carousel.img_flex_nav_prev_pos = '0 -60px';
                sf360_carousel.img_flex_nav_prev_pos_hover = '0 -60px';
                sf360_carousel.img_flex_nav_next_pos = '0 -120px';
                sf360_carousel.img_flex_nav_next_pos_hover = '0 -120px';
                sf360_carousel.img_flex_nav_bg_op = 0.8;
                sf360_carousel.img_flex_nav_bg_hover_op = 1;
                sf360_carousel.img_flex_nav_bg = '';
                sf360_carousel.img_flex_nav_bg_hover = '';
            } else if (app_data.carousel.button_type == '2') {
                sf360_carousel.img_flex_nav_height = 60;
                sf360_carousel.img_flex_nav_width = 60;
                sf360_carousel.img_flex_nav = 'arrow_sprite_thick.png';
                sf360_carousel.img_flex_nav_prev_pos = '0 -60px';
                sf360_carousel.img_flex_nav_prev_pos_hover = '0 -60px';
                sf360_carousel.img_flex_nav_next_pos = '0 -120px';
                sf360_carousel.img_flex_nav_next_pos_hover = '0 -120px';
                sf360_carousel.img_flex_nav_bg_op = 0.8;
                sf360_carousel.img_flex_nav_bg_hover_op = 1;
                sf360_carousel.img_flex_nav_bg = '';
                sf360_carousel.img_flex_nav_bg_hover = '';
            } else {
                sf360_carousel.img_flex_nav_height = 40;
                sf360_carousel.img_flex_nav = 'slider-arrows-custom.png';
                sf360_carousel.img_flex_nav_prev_pos = '0%  0%';
                sf360_carousel.img_flex_nav_prev_pos_hover = '0%  0%';
                sf360_carousel.img_flex_nav_next_pos = '0% 100%';
                sf360_carousel.img_flex_nav_next_pos_hover = '0% 100%';
            }
            cssText = sf360_carousel.carousel_style(uid, app_data.carousel, app_data.images);
            head = document.head || document.getElementsByTagName('head')[0];
            style = document.createElement('style');
            style.type = 'text/css';
            if (style.styleSheet) {
                style.styleSheet.cssText = cssText;
            } else {
                style.appendChild(document.createTextNode(cssText));
            }
            head.appendChild(style);
            $('#storefront360_carousel').replaceWith(function () {
                var tag, img_class;
                tag = '';
                img_class = 'sf360-img ';
                tag += '<div class="sf360-flexslider" >';
                tag += '<ul class="sf360-slides">';
                app_data.images.forEach(function (image, idx) {
                    tag += ' <li>';
                    if (!image.button_text && image.link) {
                        tag += ' <a href="' + image.link + '" >';
                    }
                    tag += ' <img class="' + img_class + '" src="' + app_data.cdn_image_url + image.name + '" ';
                    tag += ' data-image_id="' + image.image_id + '" ';
                    tag += ' data-carousel_id="' + app_data.carousel.id + '" ';
                    tag += ' data-link_url="' + image.link + '" ';
                    tag += ' data-text_overlay_autosize="' + image.text_overlay_autosize + '" ';
                    tag += ' data-text_overlay_width="' + image.text_overlay_width + '" ';
                    tag += ' data-header_font_size="' + image.header_font_size + '" ';
                    tag += ' data-body_font_size="' + image.body_font_size + '" ';
                    tag += ' data-button_font_size="' + image.button_font_size + '" ';
                    tag += ' />';
                    if (!image.button_text && image.link) {
                        tag += '</a>';
                    }
                    if (image.header_text || image.body_text || image.button_text) {
                        tag += '<div class="sf360-flex-caption">';
                        tag += '<div class="sf360-flex-caption-' + idx + '">';
                        tag += '<div class="sf360-text-container-' + idx + '">';
                        if (image.header_text) tag += '<h1>' + image.header_text + '</h1>';
                        if (image.body_text) tag += '<p>' + image.body_text + '</p>';
                        if (image.button_text) {
                            tag += '<a class="sf360-button-' + idx + '" href="' + image.link + '" style="opacity: 1;">' + image.button_text + '</a>';
                        }
                        tag += '</div>';
                        tag += '</div></div>';
                    }
                    if (image.social_icon_location != 'none') {
                        tag += '<div class="sf360-social-icons-' + idx + '">';
                        tag += '<div id="fb-root social"></div><div class="fb-share-button sf360-social-icon-facebook" data-href="' + (image.link || window.location) + '" data-type="button"></div>';
                        tag += '<div class="sf360-social-icon-pinterest">';
                        tag += '<a href="//www.pinterest.com/pin/create/button/?url=' + image.link + '&media=' + app_data.cdn_image_url + image.name + '&description=' + image.header_text + ': ' + image.body_text + '"';
                        tag += ' data-pin-do="buttonPin" data-pin-log="none"></a>';
                        tag += '</div>';
                        tag += '</div>';
                    }
                    tag += '</li>';
                });
                tag += '</ul>';
                tag += '</div>';
                return tag;
            });
            getData = {
                carousel_id: app_data.carousel.id
            };
            if (uid !== 'preview') {
                $.ajax({
                    type: "GET",
                    data: getData,
                    url: sf360_carousel_data.visit_callback_url,
                    dataType: "jsonp",
                    success: function (result) {
                        console.log('success', result);
                    },
                    error: function () {
                        console.log('error');
                    }
                });
                $(".sf360-img").click(function (e) {
                    var postData, original_url;
                    postData = {
                        image_id: $(this).data('image_id'),
                        carousel_id: $(this).data('carousel_id')
                    };
                    original_url = $(this).data('link_url');
                    $.ajax({
                        type: "POST",
                        data: postData,
                        url: sf360_carousel_data.click_callback_url,
                        dataType: "jsonp",
                        success: function (result) {
                            window.location = original_url;
                        },
                        error: function () {
                            window.location = original_url;
                        }
                    });
                    e.preventDefault();
                });
            }
            if (app_data.carousel.sliderType === 'flexslider') {
                if (app_data.carousel.fit_to_container) {
                    carousel_width = 0;
                } else {
                    carousel_width = app_data.carousel.width;
                }
                $('.sf360-flexslider').flexslider({
                    namespace: "sf360-flex-",
                    selector: ".sf360-slides > li",
                    animation: 'fade',
                    easing: "swing",
                    direction: "horizontal",
                    reverse: false,
                    animationLoop: true,
                    smoothHeight: false,
                    startAt: 0,
                    slideshow: true,
                    slideshowSpeed: app_data.carousel.speed,
                    animationSpeed: 500,
                    initDelay: 0,
                    randomize: false,
                    pauseOnAction: true,
                    pauseOnHover: false,
                    useCSS: true,
                    touch: true,
                    video: false,
                    controlNav: true,
                    directionNav: true,
                    prevText: "Previous",
                    nextText: "Next",
                    keyboard: true,
                    multipleKeyboard: false,
                    mousewheel: false,
                    pausePlay: false,
                    pauseText: 'Pause',
                    playText: 'Play',
                    controlsContainer: "",
                    manualControls: "",
                    sync: "",
                    asNavFor: "",
                    itemWidth: carousel_width,
                    itemMargin: 0,
                    minItems: 0,
                    maxItems: 0,
                    move: 0,
                    start: function () {
                        this.slideshowSpeed = 500;
                        this.slideshowSpeed = 1000;
                        $(".sf360-slides img").each(function (index, value) {
                            var img_width, overlay_width, header_font_size, body_font_size;
                            img_width = $(this).width();
                            if (screen.width > 600) {
                                overlay_width = parseInt($(this).data('text_overlay_width')) || 100;
                                overlay_width = overlay_width * img_width / 100;
                                $('.sf360-text-container-' + index).css('max-width', overlay_width);
                            }
                            header_font_size = parseInt($(this).data('header_font_size')) || 30;
                            header_font_size = header_font_size * img_width / 636;
                            $('.sf360-text-container-' + index + ' h1').css('font-size', header_font_size);
                            body_font_size = parseInt($(this).data('body_font_size')) || 15;
                            body_font_size = body_font_size * img_width / 636;
                            $('.sf360-text-container-' + index + ' p').css('font-size', body_font_size);
                            var button_font_size = parseInt($(this).data('button_font_size')) || 15;
                            button_font_size = button_font_size * img_width / 636;
                            $('.sf360-button-' + index).css('font-size', button_font_size);
                            $('.sf360-button-' + index).css('padding', button_font_size / 2);
                            (function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));
                            (function (d) {
                                var f = d.getElementsByTagName('SCRIPT')[0],
                                    p = d.createElement('SCRIPT');
                                p.type = 'text/javascript';
                                p.async = true;
                                p.src = '//assets.pinterest.com/js/pinit.js';
                                f.parentNode.insertBefore(p, f);
                            }(document));
                        });
                    },
                    before: function () {},
                    after: function () {},
                    end: function () {},
                    added: function () {},
                    removed: function () {}
                });
            }
        }
    },
    myAppJavaScript: function ($) {
        "use strict";
        sf360_carousel.jQuery = $;
        $(document).ready(function () {
            var uid = $('#storefront360_carousel').data('uid');
            if (uid == 'preview') {
                sf360_carousel.app_carousel360(preview_app_data_json);
                return undefined;
            }
            sf360_carousel.loadScript(sf360_carousel.lib_url, function () {
                if (sf360_carousel_data.live) {}
                if (sf360_carousel_data.cdn_data_url.indexOf('.json') !== -1) {
                    sf360_carousel_data.cdn_data_url = sf360_carousel_data.cdn_data_url.replace('.json', '');
                }
                sf360_carousel_data.tag = "";
                $.ajax({
                    url: sf360_carousel_data.cdn_data_url + sf360_carousel_data.tag + '.json?callback=sf360_carousel_data.callback',
                    type: 'get',
                    cache: sf360_carousel.cache,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'jsonp',
                    success: function (data) {
                        sf360_carousel.app_carousel360(data);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        if (sf360_carousel_data.tag) {
                            $.ajax({
                                url: sf360_carousel_data.cdn_data_url + '.json?callback=sf360_carousel_data.callback',
                                type: 'get',
                                cache: sf360_carousel.cache,
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'jsonp',
                                success: function (data) {
                                    sf360_carousel.app_carousel360(data);
                                },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {}
                            });
                        }
                    }
                });
            });
        });
    }
};
(function () {
    "use strict";
    if ((typeof jQuery === 'undefined') || (parseFloat(jQuery.fn.jquery) < 1.7)) {
        sf360_carousel.loadScript(sf360_carousel.jquery_url, function () {
            var jQuery191;
            jQuery191 = jQuery.noConflict(true);
            sf360_carousel.myAppJavaScript(jQuery191);
        });
    } else {
        sf360_carousel.myAppJavaScript(jQuery);
    }
})();